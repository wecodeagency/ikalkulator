<?php

$bodyClass = "archive-accounts_saving";
get_header();
include('template-parts/products/products-setup/accounts_saving.php');
?>

<div id="page" class="product-list accounts_saving" data-product-slug="accounts_saving">
    <div class="page-head">
        <div class="content">
            <?php the_breadcrumbs(); ?>
            <h1 class="title">Konta oszczędnościowe <span class="count">(<?php echo product_count_display(count($posts_array)); ?>)</span> </h1>

            <div class="description">
                <?php the_field('products_descriptions_accounts', 'options'); ?>
            </div>
            
        </div>
    </div>
    <?php

    include('template-parts/products/products-html-filters/accounts_saving.php');

    ?>
    <div class="container products header"><div class="mobile-label">Sortuj wg</div><ul><li>

                <div class="name data-row">
                    Nazwa produktu
                </div>
                <div class="amount data-row">
                    Kwota
                </div>
                <div class="interest data-row sortable">
                    <a href="#" data-sort-by="profit" data-sort-type="number">Oprocentowanie</a>
                </div>
                <div class="profit data-row sortable">
                    <a href="#" data-sort-by="profit" data-sort-type="number">Zysk miesięczny</a>
                </div>
                <div class="rank data-row sortable">
                    <a href="#" data-sort-by="rank" data-sort-type="number">Ocena</a>
                </div>
                <div class="cta data-row">
                    Złóż wniosek
                </div>
            </li></ul></div>
    <div class="container products list">
        <?php
        include('template-parts/products/products-html-list/accounts_saving.php');
        echo '</div>';

        ?>
        <div class="container description main-content">
            <div class="content">
                <?php the_field('products_descriptions_accounts_bottom', 'options'); ?>
            </div>
        </div>
    </div>


    <?php get_footer(); ?>
