<?php
require_once(__DIR__."/app/identify-external-links.php");
require_once(__DIR__."/app/api.php");
require_once(__DIR__."/app/config.php");
require_once(__DIR__."/app/post_types.php");
require_once(__DIR__."/app/func.php");
require_once(__DIR__."/app/comments.php");

function banki_redirect()
{
    global $post;
    if ($post)
    {
        $post_slug = $post->post_name;   

        if (!is_user_logged_in() && md5("banki") == $post_slug && !isset($_GET["force"]) )
        {
            wp_redirect( home_url("/banki/")); 
            die;
        }
    }
}
add_action( "template_redirect", "banki_redirect" );

/* update votes final score */
/*
$args = array(
    "post_type"     => "post",
    "posts_per_page"=> -1,
);

$posts = get_posts($args);

if ($posts)
{
    foreach($posts as $item)
    {
        $postId = $item->ID;

        $thumbsup = (int)get_post_meta($postId, 'thumbsup', true);
        $thumbsdown = (int)get_post_meta($postId, 'thumbsdown', true);

        if (!$thumbsup) $thumbsup = 0;
        if (!$thumbsdown) $thumbsdown = 0;

        $thumbsfinal = $thumbsup - $thumbsdown;  
        
        update_post_meta($postId, "thumbsfinal", (int)$thumbsfinal);        
    }
}  
*/  
