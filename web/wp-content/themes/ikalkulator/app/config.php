<?php

/* _____________________________________________________________________________ DEFINITIONS */

$productsPostTypes = array(
    "accounts_business",
    "accounts_currency",
    "accounts_personal",
    "accounts_saving",
    "credits",
    "investments",
    "loans",
    "quickloans",
);

/* _____________________________________________________________________________ CSS & JS */

add_action( "wp_enqueue_scripts", "theme_custom_scripts" );
function theme_custom_scripts()
{
	wp_enqueue_style(
		"vendor",
		get_stylesheet_directory_uri() . "/assets/css/vendor.css"
	);

	wp_enqueue_style(
		"style",
		get_stylesheet_directory_uri() . "/assets/css/style.css"
	);

	wp_enqueue_script(
		"vendor",
		get_stylesheet_directory_uri() . "/assets/js/vendor.js",
		array( "jquery" )
	);

	wp_enqueue_script(
		"app",
		get_stylesheet_directory_uri() . "/assets/js/app.js",
		array( "jquery", "vendor"  )
	);
}

/* _____________________________________________________________________________ Remove emoji */

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/* _____________________________________________________________________________ MENUS */

add_action( "init", "register_menus" );
function register_menus()
{
	register_nav_menus(
		array(
			"main-menu" => __( "Main Menu", "wcd" ),
            "main-menu-footer" => __( "Main Menu Footer", "wcd" ),
			"footer-copy" => __( "Footer Copyrights", "wcd" ),
			"social" => __( "Social", "wcd" ),
            "finance" => __( "Finansowanie", "wcd" ),
            "investment" => __( "Oszczędzanie", "wcd" ),
            "account" => __( "Konta", "wcd" ),
		)
	);
}

/* _____________________________________________________________________________ SIDEBARS */


add_action( "widgets_init", "register_custom_sidebars" );
function register_custom_sidebars()
{
	register_sidebar( array(
		"name"          => 'Sidebar - top',
		"id"            => 'sidebar-top',
		"before_widget" => '<div class="widget %2$s" id="%1$s">',
		"after_widget"  => '</div>',
		"before_title"  => '<h4 class="widget-title">',
		"after_title"   => '</h4>',
	) );
    
	register_sidebar( array(
		"name"          => 'Sidebar - right',
		"id"            => 'sidebar-right',
		"before_widget" => '<div class="widget %2$s" id="%1$s">',
		"after_widget"  => '</div>',
		"before_title"  => '<h4 class="widget-title">',
		"after_title"   => '</h4>',
	) );
    
	register_sidebar( array(
		"name"          => 'Sidebar - bottom',
		"id"            => 'sidebar-bottom',
		"before_widget" => '<div class="widget %2$s" id="%1$s">',
		"after_widget"  => '</div>',
		"before_title"  => '<h4 class="widget-title">',
		"after_title"   => '</h4>',
	) );

}

/* _____________________________________________________________________________ CUSTOM POST THUMBNAILS */

add_action( "init", "custom_tns" );
function custom_tns()
{
	add_theme_support( "post-thumbnails" );
    add_post_type_support( "post", "excerpt" );
    add_post_type_support( "page", "excerpt" );
    /*
	add_post_type_support( "inspirations", "thumbnail" );
	post_type_supports( "inspirations", "thumbnail" );
    */
}

add_action("after_setup_theme", "custom_thumb");
function custom_thumb()
{
	add_image_size( "avatar", 70, 70, array( "center", "center" ) );
	add_image_size( "company-logo", 400, 350, array( "center", "center" ) );
    add_image_size( "post-thumbnail", 350, 250, array( "center", "center") );
    add_image_size( "megabox-thumbnail", 333, 267, array( "center", "center") );
    add_image_size( "promobox-thumbnail", 267, 333, array( "center", "center") );
    add_image_size( "megabox-thumbnail-tall", 333, 534, array( "center", "center") );
    add_image_size( "megabox-thumbnail-wide", 666, 267, array( "center", "center") );
    add_image_size( "post-list-big", 657, 250, array( "center", "center") );
    add_image_size( "post-list-medium", 477, 230, array( "center", "center") );
    add_image_size( "post-list-small", 298, 180, array( "center", "center") );
    add_image_size( "featured_preview", 1024, 768, array( "center", "center") );
}


/* _____________________________________________________________________________ BODY CLASSES */
/*
add_filter( "body_class", "set_body_classes", 10, 2 );
function set_body_classes( $classes, $extra_classes )
{
    // List of the only WP generated classes allowed
    $whitelist = array( "home", "blog", "archive", "single", "category", "tag", "error404", "logged-in", "admin-bar" );

    // Filter the body classes
    $classes = array_intersect( $classes, $whitelist );


	switch ( basename(get_page_template()) )
	{
		case "page-blog-archive.php":
			$classes[] = "blog-archive";
			break;
		default:
			$classes[] = "page-default";
	}


    // Add the extra classes back untouched
    return array_merge( $classes, (array) $extra_classes );
}
*/
/* _____________________________________________________________________________ THEME OPTIONS */


add_action( "customize_register", "website_customizer" );
function website_customizer( $wp_customize )
{
    /* custom logo */
    $wp_customize->add_setting( "website_logo" );

    $wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			"website_logo",
			array(
				"label"    => __( "Logo strony", "wcd"),
				"description" => "",
				"section"  => "title_tagline",
			)
		)
	);
}

/* _____________________________________________________________________________ HIDE MENUS */

/*
add_action( "admin_menu", "admin_menu_page_removing" );
function admin_menu_page_removing()
{
    remove_menu_page("edit.php");				//Posts
    remove_menu_page("edit-comments.php");		//Comments
	remove_submenu_page("edit.php", "edit-tags.php?taxonomy=category");	//Post Categories
	remove_submenu_page("edit.php", "edit-tags.php?taxonomy=post_tag");	//Post Tags

	remove_meta_box( "categorydiv","post","normal" ); // Categories Metabox
	remove_meta_box( "tagsdiv-post_tag","post","normal" ); // Tags Metabox
}
*/

/* _____________________________________________________________________________ RENAME MENUS */

add_action( "admin_menu", "change_post_label" );
function change_post_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = "Okiem eksperta";
	$submenu["edit.php"][5][0] = "Okiem eksperta";
	$submenu["edit.php"][10][0] = "Dodaj News";
	$submenu["edit.php"][16][0] = "Tagi";
}

add_action( "init", "change_post_object" );
function change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types["post"]->labels;
    $labels->name = "Okiem eksperta";
    $labels->singular_name = "Okiem eksperta";
    $labels->add_new = "Dodaj News";
    $labels->add_new_item = "Dodaj News";
    $labels->edit_item = "Edytuj News";
    $labels->new_item = "News";
    $labels->view_item = "Zobacz News";
    $labels->not_found = "nie znalezionod";
    $labels->not_found_in_trash = "No News found in Trash";
    $labels->all_items = "All News";
    $labels->menu_name = "Okiem eksperta";
    $labels->name_admin_bar = "Okiem eksperta";
}



/* _____________________________________________________________________________ UNUSED WIDGETS */

add_filter( "widgets_init", "theme_unregister_widgets");
function theme_unregister_widgets( $widgets )
{
    unregister_widget("WP_Widget_Pages");
    unregister_widget("WP_Widget_Calendar");
    unregister_widget("WP_Widget_Archives");
    unregister_widget("WP_Widget_Links");
    unregister_widget("WP_Widget_Media_Audio");
//    unregister_widget("WP_Widget_Media_Image");
//    unregister_widget("WP_Widget_Media_Video");
    unregister_widget("WP_Widget_Meta");
    unregister_widget("WP_Widget_Search");
    //  unregister_widget("WP_Widget_Text");
    unregister_widget("WP_Widget_Categories");
    unregister_widget("WP_Widget_Recent_Posts");
    unregister_widget("WP_Widget_Recent_Comments");
    unregister_widget("WP_Widget_RSS");
    unregister_widget("WP_Widget_Tag_Cloud");
    unregister_widget("WP_Nav_Menu_Widget");
//    unregister_widget("WP_Widget_Custom_HTML");
}

add_filter("siteorigin_panels_widgets", "remove_siteorigin_widgets");
function remove_siteorigin_widgets( $widgets )
{

    unset($widgets["SiteOrigin_Widget_Button_Widget"]);
    unset($widgets["SiteOrigin_Widget_Features_Widget"]);
    unset($widgets["SiteOrigin_Widget_Image_Widget"]);
    unset($widgets["SiteOrigin_Widget_PostCarousel_Widget"]);
    unset($widgets["SiteOrigin_Widget_Slider_Widget"]);

    unset($widgets["SiteOrigin_Panels_Widgets_Layout"]);
    unset($widgets["SiteOrigin_Panels_Widgets_PostContent"]);
    unset($widgets["SiteOrigin_Panels_Widgets_PostLoop"]);

    return $widgets;
}

// Move Yoast to bottom
function yoasttobottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


/* _____________________________________________________________________________ REDEFINE PAGE TEMPLATE PATH */

/*
add_filter(
	'page_template',
	function ($template) {
		global $post;

		if ($post->post_parent) {

			// get top level parent page
			$parent = get_post(
				reset(array_reverse(get_post_ancestors($post->ID)))
			);


			// or ...
			// when you need closest parent post instead
			// $parent = get_post($post->post_parent);

			$child_template = locate_template(
				[
					'page-'.$parent->post_name. '-' . $parent->post_name . '.php',
					'page-'.$parent->post_name. '-' . $post->ID . '.php',
					'page-' .$parent->post_name. '.php',
				]
			);

			if ($child_template)
                return $child_template;
		}
		return $template;
	}
);
*/

/* _____________________________________________________________________________ ADDITIONAL SETTINGS PAGES */


add_action( "admin_menu", "register_custom_pages" );
function register_custom_pages()
{
	add_menu_page
    (
        "products_menu",
        "Produkty",
        "edit_others_posts",
        "products_menu",
        function() { echo "products page"; },
        "dashicons-arrow-right",
        6
    );
}

if( function_exists('acf_add_options_page') )
{

    $args = array(

        "page_title" => "Opisy",
        "menu_slug" => "custom-descriptions",
        "capability" => "edit_posts",
        "position" => false,
        "post_id" => "options",
        "autoload" => false,

    );
    acf_add_options_page($args);

}

/* _____________________________________________________________________________ CUSTOM COLUMNS */

function hide_columns( $columns )
{
    unset($columns['tags']);
    return $columns;
}

function custom_columns_order($columns, $move_column, $before_column)
{
    $n_columns = array();
    $move = $move_column; // what to move
    $before = $before_column; // move before this
    foreach ($columns as $key => $value) {
        if ($key == $before) {
            $n_columns[$move] = $move;
        }
        $n_columns[$key] = $value;
    }

    return $n_columns;
}

// get featured image
function get_featured_image($post_ID)
{
    $post_thumbnail_id = get_post_thumbnail_id($post_ID);
    if ($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'featured_preview');
        return $post_thumbnail_img[0];
    }
}

// add new column
function column_manage($columns)
{
    //custom columns head
    $columns["featured_image"] = "Obrazek wyróżniający";
    $columns["featured_status"] = "Wyróżniony";
    $columns["promotion_type"] = "Typ promocji";
    $columns["vote"] = "Ocena";

    $ordered_columns = custom_columns_order($columns, "featured_image","title"); //"featured_image" before"title"
    $ordered_columns = custom_columns_order($ordered_columns, "featured_status","author");
    $ordered_columns = custom_columns_order($ordered_columns, "promotion_type","author");
    $ordered_columns = custom_columns_order($ordered_columns, "vote","author");

    return $ordered_columns;
}

// show columns
function custom_columns_content($column_name)
{
    if ($column_name == "featured_image")
	{
        $post_featured_image = get_featured_image(get_the_ID());
        if ($post_featured_image)
        {
			echo '<div style="
						display: inline-block;
						width: 80px;
						height: 80px;
						background: url(\''. $post_featured_image . '\') center center no-repeat;
						background-size: cover;" ></div>';
        }
        else
        {
			echo '<div style="
						display: inline-block;
						width: 80px;
						height: 80px;
						background-color: rgb(235,235,235);" ></div>';
        }
    }
    elseif ($column_name == "featured_status")
	{
        $post_featured_status = get_field("blog_post_featured");
        if ($post_featured_status)
        {
            echo '<span class="dashicons dashicons-yes" style="font-size: 2em; color: rgb(100, 160, 100);"></span>';
        }
        else
        {
            echo "&mdash;";
        }
    }
    elseif ($column_name == "promotion_type")
	{
        $no_promotion = true;
        if (has_category(get_cat_ID("Promocje bankowe")))
        {
            $post_featured_status = get_field("promotion_type");
            
            if ($post_featured_status)
            {
                echo $post_featured_status["label"];
                $no_promotion = false;
            }
        }            
        
        if ($no_promotion)
        {
            echo "&mdash;";
        }
    }
    elseif ($column_name == "vote")
	{
        $thumbsup = get_post_meta(get_the_ID(), "thumbsup", true) ? get_post_meta(get_the_ID(), "thumbsup", true) : 0;
        $thumbsdown = get_post_meta(get_the_ID(), "thumbsdown", true) ? get_post_meta(get_the_ID(), "thumbsdown", true) : 0; 
        
        $final_results = $thumbsup - $thumbsdown;
        
        $thumbsup_display = $thumbsup != 0 ? '<span style="color: rgb(100, 160, 100);">+'. $thumbsup .'</span>' : 0;
        $thumbsdown_display = $thumbsdown != 0 ? '<span style="color: rgb(160, 100, 100);">-'. $thumbsdown .'</span>' : 0;
        
        if ($final_results > 0)
            $final_results_display = '<span style="font-weight:bold; color: rgb(100, 160, 100);">'. $final_results .'</span>';
        elseif ($final_results < 0)
            $final_results_display = '<span style="font-weight:bold; color: rgb(160, 100, 100);">'. $final_results .'</span>';
        else    
            $final_results_display = '<span style="font-weight:bold;">'. $final_results .'</span>';
        
        echo $final_results_display . " (" . $thumbsup_display . "/" . $thumbsdown_display . ")";
    }
}

// sortable columns

function columns_register_sortable($columns)
{
    $columns["featured_status"] = "featured_status";
    $columns["vote"] = "vote";
//    $columns["promotion_type"] = "promotion_type";

    //To make a column 'un-sortable' remove it from the array
    //unset($columns['date']);

    return $columns;
}

function custom_orderby( $query ) {
    if( !is_admin() )
        return;

    $orderby = $query->get( "orderby");

    if( $orderby == "featured_status" )
    {
        $query->set("meta_key","blog_post_featured");
        $query->set("orderby","meta_value");
    }
    
    elseif( $orderby == "vote" )
    {
        $query->set("meta_key","thumbsfinal");
        $query->set("orderby","meta_value");
    }
}

function custom_column_width()
{
    echo '<style type="text/css">';
    echo ".column-featured_image { text-align: center !important; width:100px !important; overflow:hidden }";
    echo ".column-featured_status { text-align: center; width:115px; overflow:hidden }";
    echo ".column-promotion_type { text-align: center; width:115px; overflow:hidden }";
    echo ".column-vote { text-align: left; width:115px; overflow:hidden }";
    echo "</style>";
}


// ONLY WORDPRESS DEFAULT POSTS
add_filter("manage_posts_columns" , "hide_columns" );
add_filter("manage_edit-post_columns", "column_manage");
add_action("manage_posts_custom_column", "custom_columns_content", 10, 2);
add_action("manage_edit-post_sortable_columns", "columns_register_sortable");
add_action("pre_get_posts", "custom_orderby" );
add_action("admin_head", "custom_column_width");



/* _____________________________________________________________________________ ADMIN CUSTOM STYLES */

add_action("admin_head", "megabox_layout");
function megabox_layout() {
  echo
   '<style>
        .acf-input .acf-radio-list li > label > span
        {
            display: inline-block;
            width: 32px;
            height: 32px;
            overflow: hidden;
            vertical-align: middle;
            text-indent: 64px;
            white-space: nowrap;
            background-position: center center;
            background-size: contain;
            background-repeat: no-repeat;
        }

        .acf-input .acf-radio-list li > label > span.layout_1t_2
        {
            background-image: url('. get_template_directory_uri() . '/assets/img/admin/megabox-grid-1t_2.svg);
        }
        .acf-input .acf-radio-list li > label > span.layout_1w_2
        {
            background-image: url('. get_template_directory_uri() . '/assets/img/admin/megabox-grid-1w_2.svg);
        }
        .acf-input .acf-radio-list li > label > span.layout_2_1t
        {
            background-image: url('. get_template_directory_uri() . '/assets/img/admin/megabox-grid-2_1t.svg);
        }
        .acf-input .acf-radio-list li > label > span.layout_2_1w
        {
            background-image: url('. get_template_directory_uri() . '/assets/img/admin/megabox-grid-2_1w.svg);
        }
        .acf-input .acf-radio-list li > label > span.layout_4
        {
            background-image: url('. get_template_directory_uri() . '/assets/img/admin/megabox-grid-4.svg);
        }
    </style>';
}



/* _____________________________________________________________________________ SEARCH */


//Exclude pages from WordPress Search
add_filter("pre_get_posts","custom_search_filter");
function custom_search_filter($query)
{
    if (is_admin()) // Only target front end
        return;

    if  (
            $query->is_main_query() // Only target the main query
        &&  $query->is_search()  // Only target the search page
        )
    {
        $query->set("post_type", ["post"]);
    }

    return $query;
}
/* _____________________________________________________________________________ TINYMCE */

function wpb_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

function my_mce_before_init_insert_formats( $init_array ) {
    // Define the style_formats array
    $style_formats = array(
        // Each array child is a format with it's own settings
        array(
            'title' => 'Highlighted',
            'block' => 'p',
            'classes' => 'highlighted',
            'wrapper' => false,

        ),
    );
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;

}
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );




/* _____________________________________________________________________________ SHORTCODES */

function ikalkulator_show_products_list($atts=[], $content = null, $tag = ''){

    global $post;

    // default values
    $defaultAtts = [
        "product_type" => "loans",
        "amount" => 4000,
        "period" => 12
    ];

    $atts = shortcode_atts( $defaultAtts, $atts, 'ikalkulator_show_products_list');

    foreach($atts as $name => $value)
    {
        if($name != 'product_type')
            $_GET[$name] = $value;
    }

    echo '<div class="product-list '.$atts['product_type'].'"><div class="products list">';

    include(get_template_directory()."/template-parts/products/products-setup/".$atts['product_type'].".php");
    include(get_template_directory()."/template-parts/products/products-html-list/".$atts['product_type'].".php");

    echo '</div></div>';
}
add_shortcode('products_list', 'ikalkulator_show_products_list');


function ikalkulator_button_shortcode($atts=[], $content = null, $tag = ''){

    global $post;

    // default values
    $defaultAtts = [
        "type" => "white",
        "text" => "Kliknij tutaj",
        "url" => "#",
        "target" => "_blank"
    ];

    $atts = shortcode_atts( $defaultAtts, $atts, 'ikalkulator_button_shortcode');

    $inverse = "";
    if($atts['type'] == 'green')
        $inverse = " btn-inverse";


    if($atts['target'])
        $target = "target=\"".$atts['target'] ."\"";

    return '<a href="' . $atts['url'] .'" class="btn' . $inverse .'" '.$target.' rel="nofollow">' . $atts['text'] .'</a>';

}
add_shortcode('ik_button', 'ikalkulator_button_shortcode');

function ikalkulator_highlight_shortcode( $atts, $content = null ) {
    return '<div class="highlighted">'.$content.'</div>';
}
add_shortcode("ik_highlight", "ikalkulator_highlight_shortcode");


function enqueue_plugin_scripts($plugin_array)
{
    $plugin_array["ik_button"] =  get_template_directory_uri() . "/assets/tinymce/button.js";
    $plugin_array["ik_highlight"] =  get_template_directory_uri() . "/assets/tinymce/highlight.js";
    return $plugin_array;
}

add_filter("mce_external_plugins", "enqueue_plugin_scripts");

function register_buttons_editor($buttons)
{
    array_push($buttons, "ik_button");
    array_push($buttons, "ik_highlight");
    return $buttons;
}
add_filter("mce_buttons", "register_buttons_editor");

function shortcode_button_script()
{
    if(wp_script_is("quicktags"))
    {
        ?>
        <script type="text/javascript">

            //this function is used to retrieve the selected text from the text editor
            function getSel()
            {
                var txtarea = document.getElementById("content");
                var start = txtarea.selectionStart;
                var finish = txtarea.selectionEnd;
                return txtarea.value.substring(start, finish);
            }

            QTags.addButton(
                "ikal_button",
                "ikal button",
                printButton
            );

            QTags.addButton(
                "ikal_higlighted",
                "ikal highlight",
                printHighlight
            );

            function printButton()
            {
                QTags.insertContent("[ik_button type=\"white\" text=\"Zobacz\" url=\"https://ikalkulator.pl\"]");
            }

            function printHighlight()
            {
                var selected_text = getSel();
                QTags.insertContent("[ik_highlight]" + selected_text + "[/ik_highlight]");
            }
        </script>
        <?php
    }
}

add_action("admin_print_footer_scripts", "shortcode_button_script");



/* _____________________________________________________________________________ CUSTOM POST ARCHIVES */

function wpseo_disable_rel_next_archive($link)
{
    global $productsPostTypes;
    if ( is_post_type_archive($productsPostTypes) )
    {
        return false;
    }

    return $link;
}
add_filter('wpseo_next_rel_link', 'wpseo_disable_rel_next_archive');


/* _____________________________________________________________________________ ADD FULL DOMAIN TO CANCEL COMMENT */

add_filter( 'cancel_comment_reply_link', 'ikalkulator_full_domain_comment' );

function ikalkulator_full_domain_comment($link)
{
    if (isset($_SERVER['HTTPS']) &&
        ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
        isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
        $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
        $protocol = 'https://';
    }
    else {
        $protocol = 'http://';
    }
    return str_replace('href="/', 'href="' . $protocol . $_SERVER['HTTP_HOST'] . '/', $link);
}

/* _____________________________________________________________________________ CHECK POST SLUG */

function ikalkulator_intercept_404(){
    if( is_404() )
    {
        $explode_uri = explode("/", str_replace("/", "", $_SERVER['REQUEST_URI']));
        $arrElNum = count($explode_uri) - 1;
        $the_slug = $explode_uri[$arrElNum];

        if($explode_uri[$arrElNum] == "" && $explode_uri[$arrElNum-1] != "")
            $the_slug = $explode_uri[$arrElNum-1];

        $args = array(
            'name'        => $the_slug,
            'post_type'   => 'post',
            'post_status' => 'publish',
            'numberposts' => 1
        );
        $my_posts = get_posts($args);
        if( $my_posts ) :
			header("HTTP/1.1 301 Moved Permanently");
             header("location: " .  get_permalink($my_posts[0]->ID));
        elseif(get_category_by_slug($the_slug)):
            $cat = get_category_by_slug($the_slug);
			header("HTTP/1.1 301 Moved Permanently");
            header("location: " .  get_category_link($cat->term_id));
        endif;
    }
}
add_action( 'template_redirect', 'ikalkulator_intercept_404' );

/* _____________________________________________________________________________ override WordPress's default template behavior */
add_filter( "template_include", "company_page_template", 99 );
function company_page_template( $template )
{
	if ( is_singular( "company" ) )
    {
		$new_template = locate_template( array( "single-company.php" ) );
		if ( !empty( $new_template ) ) {
			return $new_template;
		}
	}

	return $template;
}


/* _____________________________________________________________________________ add menu items to the main menu */

class Primary_Walker_Nav_Menu extends Walker_Nav_Menu
{
    function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) 
    {
        if ( ! $element )
        {
            return;
        }

        $id_field = $this->db_fields['id'];
        $id       = $element->$id_field;

        //display this element
        $this->has_children = ! empty( $children_elements[ $id ] );
        if ( isset( $args[0] ) && is_array( $args[0] ) ) {
            $args[0]['has_children'] = $this->has_children; // Backwards compatibility.
        }

        $cb_args = array_merge( array( &$output, $element, $depth ), $args );
        call_user_func_array( array( $this, 'start_el' ), $cb_args );

        // descend only when the depth is right and there are childrens for this element
        if ( ( $max_depth == 0 || $max_depth > $depth + 1 ) && isset( $children_elements[ $id ] ) )
        {

            foreach ( $children_elements[ $id ] as $child ) {

                if ( ! isset( $newlevel ) )
                {
                    $newlevel = true;
                    //start the child delimiter
                    $cb_args = array_merge( array( &$output, $depth ), $args );

                    /** Additional check for custom addition of id to sub-level */

                    if ( strtolower($element->title) == "blog")
                    {
                        $cb_args["sub_menu_class"] = "sub-menu-blog sub-menu";
                    }
                    /** End custom check */

                    call_user_func_array( array( $this, "start_lvl" ), $cb_args );
                }
                $this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
            }
            unset( $children_elements[ $id ] );
        }

        if ( isset( $newlevel ) && $newlevel )
        {
            //end the child delimiter
            $cb_args = array_merge( array( &$output, $depth ), $args );
            call_user_func_array( array( $this, "end_lvl" ), $cb_args );
        }

        //end this element
        $cb_args = array_merge( array( &$output, $element, $depth ), $args );
        call_user_func_array( array( $this, "end_el" ), $cb_args );
    }    
    
    
    function start_lvl( &$output, $depth = 0, $args = array(), $sub_blog_menu_div = null )
    {
        $indent = str_repeat("\t", $depth);
        if ( $sub_blog_menu_div )
        {
            $output .= "\n$indent<div class=\"$sub_blog_menu_div\"><div class=\"sub-menu-content\"><ul class=\"sub-menu-items\">\n";
        } else {
            $output .= "\n$indent<ul class=\"sub-menu\">\n";
        }
    }  

    function end_el(&$output, $item, $depth=0, $args=array())
    {
        $output .= ""; 
        
        if ( strtolower($item->title) == "blog")
        {
            $args = array(
                'posts_per_page' => 2,
                'post_type' => 'post'
            );

            $newestPosts = new WP_Query( $args );

            if($newestPosts->posts)
            {    
                $output .= "<div class=\"menu-posts-container\">";
                    $output .= "<span class=\"menu-posts-title\">Najnowsze</span>";
                        $output .= "<ul class=\"menu-posts\">";
            
                foreach($newestPosts->posts as $newestPost)
                {
                    $post_meta = get_post_custom_meta($newestPost, "post-thumbnail", "d.m.Y" );
                    $output .= '<li class="post-item">';
                        $output .= '<a href="'.$post_meta["url"].'">';
                            $output .= '<div class="item-content">';
                                $output .= '<div class="img"><img src="'. $post_meta["featured_image"] .'" alt="'. $post_meta["title"] .'"/></div>';
                                $output .= '<div class="content">';
                                    $output .= '<span class="title">'. $post_meta["title"] .'</span>';
                                    $output .= '<span class="date">'. $post_meta["date"] .'</span>';
                                $output .= '</div>';
                            $output .= '</div>';
                        $output .= '</a>';
                    $output .= '</li>';
                }
                
                        $output .= "</ul>"; // menu-posts
                    $output .= "</div>"; // menu-posts-container
                $output .= "</div>"; // sub-menu-content
            }
            
            $output .= "</div>"; // $sub_blog_menu_div
        }
        
        $output .= "</li>"; 
    }
    /*
    function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {


        parent::display_element( $element,$children_elements, $max_depth, $depth, $args,$output );
    }
    */

}