<?php


/* _____________________________________________________________________________ POST */

// zwraca tablice z informacjami o autorze
function get_post_author_info($post)
{
    $post = get_post($post);
    
    $author_id = get_post_field("post_author", $post->ID);
    
    $avatar = null;
    $default_avatar = get_template_directory_uri() . "/assets/img/avatar.png";
    
    if (function_exists("get_wp_user_avatar_src"))
    {    
        $avatar = get_wp_user_avatar_src( $author_id , "avatar");
    }
    else
    {
        $avatar = get_avatar_url( $author_id );
    }
    
    $author = array(
        "name"	=>	get_the_author_meta( "display_name" , $author_id ),
        "avatar" =>	$avatar ? $avatar : $default_avatar, //get_avatar_url( $author_id ),
    );

    return $author;
}

//zwraca wszystkie kategorie danego post typu
function get_post_type_categories($post_type = "post", $taxonomy = "category")
{
   $args = array(
               "type"       => $post_type,
               "taxonomy"   => $taxonomy,
               "hide_empty" => false,
           );

    $all_categories = get_categories($args);
    $categories = array();
    
    foreach ($all_categories as $category)
    {
        if (!treat_cat_as_tag($category))
        {
            $categories[] = $category;
        }
    }    
    return $categories;
}

//zwraca kategorie do ktorych jest podpiety post
function get_post_categories($post, $taxonomy = "category")
{
    $post = get_post($post);
    
    /* acf meta key doesnt work on terms
    $args = array(
        'orderby'    => 'name',
        'order'      => 'ASC',
        'meta_query' => array(
            'relation' => 'OR',
            array(
                'key'       => 'blog_category_as_tag',
                'compare'   => '!=',
                'value'     => '1'
            ),
            array(
                'key'       => 'blog_category_as_tag',
                'type'      => 'BOOLEAN',
                'value'     => false
            ),
            array(
                'key'       => 'blog_category_as_tag',
                'compare'   => 'NOT EXISTS'
            ),
        )
    );
    */
    $all_categories = get_the_terms($post->ID, $taxonomy);    
    $categories = array();
    
    foreach ($all_categories as $category)
    {
        if (!treat_cat_as_tag($category))
        {
            $categories[] = $category;
        }
    }
    
    return $categories;
}
//zwraca kategorie ktore maja byc traktowane jako tagi
function get_post_tags($post, $taxonomy = "category")
{
    $post = get_post($post);
        
    /* acf meta key doesnt work on terms    
    $args = array(
        'orderby'    => 'name',
        'order'      => 'ASC',
        'meta_query' => array(
            array(
                'key' => 'blog_category_as_tag',
                'value' => '1',
                'compare' => '='
            ),
        )
    );
    */
        
    $all_categories = get_the_terms($post->ID, $taxonomy);    
    $categories = array();
    
    foreach ($all_categories as $category)
    {
        if (treat_cat_as_tag($category))
        {
            $categories[] = $category;
        }
    }    

    return $categories;
}



function treat_cat_as_tag($category)
{
    $cat_as_tag = get_taxonomy_field("blog_category_as_tag", $category);
    
    return $cat_as_tag;
}

function get_post_root_category($post, $taxonomy = "category")
{
    $post = get_post($post);
    
    
    $wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy, $post->ID );
    $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
    
    if($wpseo_primary_term)
    {
        $category = get_term( $wpseo_primary_term );
    }
    else
    {
        $post_categories = get_the_terms($post->ID, $taxonomy );
        $category = $post_categories[0];
    }

    if (!$category)
        return;

    $category = is_object($category) ? $category : get_term($category);

    while ($category->parent > 0)
    {
        $category = get_category($category->parent);
    }

    return $category;
}


//zwraca url obrazka wyróżniającego, uwzgledniając typ miniatury
function get_featured_image_url($post, $type = "thumbnail")
{
    $post = get_post( $post ); 
    $path = "";

    if (has_post_thumbnail($post->ID) )
    {
        $image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $type );
        $path = $image[0];
    }

    return $path;
}

function get_post_custom_meta($post, $thumbnail="thumbnail", $date_fromat = "d F Y")
{
	$post = get_post( $post ); 
    
    $post_meta = array(
        "categories"        => get_post_categories($post),
        "featured_image"    => get_featured_image_url($post, $thumbnail),
        "title"             => get_the_title($post),
        "date"              => get_the_date($date_fromat, $post),
//        "excerpt"           => get_excerpt_with_words_limit(30, get_the_excerpt($post->ID)),
//        "excerpt"           => get_the_excerpt($post->ID),        
        "excerpt"           => get_excerpt_with_chars_limit(180, get_the_excerpt($post)),
        "url"               => get_permalink($post),
    );

    return $post_meta;
}

function get_excerpt_with_words_limit($limit = 55, $source = null)
{
    $excerpt = $source != null ? $source : get_the_excerpt();

	$excerpt = explode(" ", $excerpt, $limit);

	if (count($excerpt)>=$limit)
	{
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt)."&nbsp;&hellip;";
	}
	else
	{
	  $excerpt = implode(" ",$excerpt);
	}
	$excerpt = preg_replace("`[[^]]*]`","",$excerpt);
	return $excerpt;
}

function get_excerpt_with_chars_limit($limit = 180, $source = null)
{
    $excerpt = $source != null ? $source : get_the_excerpt();
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $limit);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace( "/\s+/", " ", $excerpt));
    $excerpt = $excerpt."&hellip;";
    return $excerpt;
}

//zwraca id postów oddzielone przecinkami, przydatne np przy exclude
function posts_comma_separated($posts)
{
    $posts_ids = array();

    foreach ($posts as $post)
    {
        $posts_ids[] = $post->ID;
    }
    return implode (",", $posts_ids);
}

// tworzy paginacje dla wskazanego wp_query
function build_posts_pagination($query)
{
	/*
	if( is_singular() )
		return;*/

	/* Nie twórz jezeli tylko jedna strona */
	if( $query->max_num_pages <= 1 )
	{
		return;
	}

	$paged = get_query_var( "paged" ) ? absint( get_query_var( "paged" ) ) : 1;
	$max   = intval( $query->max_num_pages );

	/*	dodaj aktualna strone do tablicy */
	if ( $paged >= 1 )
	{
		$links[] = $paged;
	}

	/*	Dodaj strony obok atkualnej do tablicy */
	if ( $paged >= 3 )
	{
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo "<div class=\"posts-pagination\"><ul>" . "\n";

	/*	link do poprzedniej strony */
	if ( get_previous_posts_link() )
	{
		printf( "<li class=\"prev\">%s</li>" . "\n", get_previous_posts_link("Poprzednia strona") );
	}
	else
	{
		echo "<li class=\"prev disabled\"><span>Poprzednia strona</span></li>" . "\n";
	}

	/*	link do pierwszej strony, plus wielokropek jeśli potrzebny */
	if ( ! in_array( 1, $links ) )
	{
		$class = 1 == $paged ? " class=\"page active\"" : " class=\"page\"";

		printf( "<li%s><a href=\"%s\"><span>%s</span></a></li>" . "\n", $class, esc_url( get_pagenum_link( 1 ) ), "1" );

		if ( ! in_array( 2, $links ) )
		{
			echo "<li class=\"hellip\"><span>&hellip;</span></li>";
		}
	}

	/*	link do obecnej strony, pluse, plus 2 strony w obu w obu kierunkach w razie potrzeby */
	sort( $links );
	foreach ( (array) $links as $link )
	{
		$class = $paged == $link ? " class=\"page active\"" : " class=\"page\"";
		printf( "<li%s><a href=\"%s\"><span>%s</span></a></li>" . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/*	link do ostatniej strony, plus wielokropek jeśli potrzebny */
	if ( ! in_array( $max, $links ) )
	{
		if ( ! in_array( $max - 1, $links ) )
		{
			echo "<li class=\"hellip\"><span>&hellip;</span></li>" . "\n";
		}

		$class = $paged == $max ? " class=\"page active\"" : " class=\"page\"";
		printf( "<li%s><a href=\"%s\"><span>%s</span></a></li>" . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/*	link do nastepnej strony */
	if ( get_next_posts_link("Next",$query->max_num_pages) )
	{
		printf( "<li class=\"next\">%s</li>" . "\n", get_next_posts_link("Następna strona", $query->max_num_pages) );
	}
	else
	{
		echo "<li class=\"next disabled\"><span>Następna strona</span></li>" . "\n";
	}

	echo "</ul></div>" . "\n";
}

//zwraca posty wykorzystując tabele w bazie stworozna przez plugin WordPress Popular Posts
function get_most_popular_posts($excludeArray = "")
{
    global $wpdb;
    $monthAgo = date("Y-m-d H:i:s", time()-30*24*3600);
    $query = "SELECT postid,pageviews from wp_popularpostsdata LEFT JOIN wp_posts ON wp_posts.id = wp_popularpostsdata.postid WHERE wp_posts.post_date > '".$monthAgo."' ORDER BY pageviews DESC"; /* LIMIT " . $number;*/
    $pageviews = $wpdb->get_results( $query );
    $exclude = !is_array($excludeArray) ? array($excludeArray) : $excludeArray;
    $popular_posts = array();

    foreach ($pageviews as $pageview)
    {
        $postID = $pageview -> postid;

        if ( get_post_type( $postID ) == "post" && !in_array($postID, $exclude))
        {
            $popular_posts[] = get_post($postID);
        }
    }
    return	$popular_posts;
}

/* Custom Search Queries */
/*
add_filter("pre_get_posts", "SearchFilter");  
function SearchFilter($query)
{

    if ($query->is_search && !is_admin() )   
    {  
        $products = isset($_GET["products"]) ? $_GET["products"] : false;
        
        if ($products)
        {
            $query->set("post_type", 
                array(
                    "accounts_business",
                    "accounts_currency",
                    "accounts_personal",
                    "accounts_saving",
                    "credits",
                    "investments",
                    "loans",
                    "post",
                    "quickloans",
                )
            );
            
            $query->set("orderby", 
                array(
                    "post_type",
//                    "post_type" => "DESC",
//                    "date" => "DESC",
                )
            );            
        }
    }
    return $query;
}
*/

function create_search_bar($classes = "", $in_products = false)
{
    $products_field = $in_products ? '<input type="hidden" name="products" value="1" />' : "";
    
	$result	=	'<div class="search-bar '. $classes .'">
                    <form role="search" method="get" id="searchform" class="searchform search-form" action="'. esc_url(home_url("/")) .'" >
                        <input name="s" type="text" class="search-input" placeholder="Szukaj&hellip;" value="'. get_search_query() . '" />'
                            . $products_field .
                        '<div class="submit-btn">
                            <input class="search search-submit" type="submit" value="" />
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="10.5" cy="10.5" r="7.5"></circle><line x1="21" y1="21" x2="15.8" y2="15.8"></line></svg>
                        </div>
                    </form>
                </div>';

	return $result;
}

function create_categories_labels_list($categories, $container_class = "", $wrap = true, $item_el = "a",  $data_name_attr = "")
{
    $cats_html = "";
    
    $item_el = !$item_el ? "a" : $item_el; 
    
    foreach($categories as $category)
    {
        $data_attr = $data_name_attr ? " {$data_name_attr}=\"{$category->slug}\"" : ""; 
        $item_link = $item_el == "a" ? ' href="'. get_term_link($category) .'"' : "";

        if ($category->taxonomy == "category")
        {
            if($category->term_id != 13)
            {
                $cats_html .= '<'.$item_el.' class="term category"' . $item_link . $data_attr .'>'. $category->name .'</'.$item_el.'>';
            }
        }
        else
        {
            $cats_html .= '<'.$item_el.' class="term category"' . $item_link . $data_attr .'>'. $category->name .'</'.$item_el.'>';
        }
    }

    if ($cats_html != "" && $wrap)
    {
        $cats_html = '<div class="terms-list categories-list '. $container_class .'">'. $cats_html .'</div>';
    }

    return $cats_html;
}

function create_tags_labels_list($tags, $container_class = "", $wrap = true, $max = 3)
{
    $tags_html = "";
    
    $k = 0;
    
    foreach($tags as $tag)
    {
        if ($k == $max)
            break;
        
        $tag_name = substr($tag->name, 0, 1) === "#" ? $tag->name : "#" . $tag->name;
        
        $tags_html .= '<a class="term tag" href="'. get_term_link($tag) .'">'. $tag_name .'</a>';
        
        $k++;
    }

    if ($tags_html != "" && $wrap)
    {
        $tags_html = '<div class="terms-list tags-list '. $container_class .'">'. $tags_html .'</div>';
    }

    return $tags_html;
}

/*
function create_tags_labels_list($tags, $container_class = "", $wrap = true, $max = 3)
{
    $tags_html = "";
    
    $k = 0;
    
    foreach($tags as $tag)
    {
        if ($k == $max)
            break;
        
        $tags_html .= '<a class="term tag" href="'. get_tag_link($tag->term_id) .'">#'. $tag->name .'</a>';
        
        $k++;
    }

    if ($tags_html != "" && $wrap)
    {
        $tags_html = '<div class="terms-list tags-list '. $container_class .'">'. $tags_html .'</div>';
    }

    return $tags_html;
}
*/

function create_terms_labels_list($categories, $tags, $container_class = "")
{
    $cats_html = create_categories_labels_list($categories,$container_class, false);
    $tags_html = create_tags_labels_list($tags,$container_class, false, 3);
    $html = "";
    
    
    if ($cats_html || $tags_html)
    {
        $html = '<div class="terms-list '. $container_class .'">'. $cats_html . $tags_html .'</div>';        
    }
    
    return $html;
}

function get_post_scarcity($post)
{
    $left = null;

    $post_meta = get_post_custom_meta($post);
    $cats = $post_meta["categories"];

    $isPromo = false;
    foreach($cats as $cat)
    {
        if($cat->name == "Promocje bankowe")
        {
            $isPromo = true;
            continue;
        }
    }

    if(!$isPromo) return null;

    $promotype = get_field("promotion_end_type", $post->ID);

    if($promotype == "cancellation")
    {
        if(get_field("promotion_end_text", $post->ID))
            $left = get_field("promotion_end_text", $post->ID);
        else
            $left = "Promocja ważna do odwołania.";
    }
    else
    {
        $scarcity = get_field("promotion_end_time", $post->ID);
        if ($scarcity)
        {
            $diff = (strtotime($scarcity) - time()) / (60 * 60 * 24);
            $diffDays = abs(ceil($diff));


            if ($diff >= 0)
            {
                $left = "Do zakończenia promocji pozostało " . ($diffDays-1) ." dni";

                if($diffDays < 3)
                {
                    $diffHours =  ceil ( $diff * 24 );
                    if($diffHours == 1)
                    {
                        $left = "Do zakończenia promocji pozostała mniej niż " . $diffHours ." godzina";
                    }
                    else if($diffHours%10 == 2 || $diffHours%10 == 3 || $diffHours%10 == 4)
                    {
                        $left = "Do zakończenia promocji pozostało mniej niż " . $diffHours ." godziny";
                    }
                    else
                    {
                        $left = "Do zakończenia promocji pozostało mniej niż " . $diffHours ." godzin";
                    }
                }
            }
            else
            {
                $left = "Promocja zakończona " . ($diffDays) ." dni temu";

                if($diffDays < 3)
                {
                    $diffHours =  ceil(abs( $diff * 24 ));
                    if($diffHours == 1)
                    {
                        $left = "Promocja zakończona godzinę temu";
                    }
                    else if($diffHours == 2 || $diffHours == 3 || $diffHours == 4)
                    {
                        $left = "Promocja zakończona " . $diffHours ." godziny temu";
                    }
                    else
                    {
                        $left = "Promocja zakończona " . $diffHours ." godzin temu";
                    }
                }
            }
        }
        else
        {
            $left = "no-date";
        }

    }

    return $left;
}

function create_product_list_item($post, $thumbnail = "post-list-big", $classes = "")
{
    $post_meta = get_post_custom_meta($post);
    $logo_attach = get_field("product_logo", $post->ID);
    $logo = wp_get_attachment_image_src( $logo_attach["id"], $thumbnail );    

    $image = $logo != "" ?
            '<div class="image">' .
                '<span style="background-image: url('. $logo[0] .');"></span>' .
            '</div>'
            : "";

    $result =  '<div class="post-item '. $classes  .'">
                    <a class="item-content" href="'. $post_meta["url"] .'">
                        <h3 class="title"><span>'. $post_meta["title"] .'</span></h3>'.
                        $image .                       
                        '<span class="read-more">Zobacz więcej</span>
                    </a>
                </div>';

    return $result;
}

function create_post_list_item($post, $thumbnail = "post-list-big", $classes = "", $heading="h2")
{
    $post_meta = get_post_custom_meta($post, $thumbnail);
//    $categories = create_categories_labels_list($post_meta["categories"], "dark");
    $cats = get_post_categories($post);
//    $tags = wp_get_post_tags($post->ID);
    $tags = get_post_tags($post);
    $terms =  create_terms_labels_list($cats, $tags, "dark");    
    $promo_time_left = get_post_scarcity($post);
    $outdated = "";
    
    ob_start();
        template_part("icon/calendar");
    $calendar_icon = ob_get_clean();       

    if ($promo_time_left != "no-date")
    {
        $promo_time_left = '<div class="scarcity">' . $calendar_icon . '<span>' . $promo_time_left . '</div>';

        if(strtotime(get_field("promotion_end_time", $post->ID)))
        {
            $diff = (strtotime(get_field("promotion_end_time", $post->ID)) - time()) / (60 * 60 * 24);
            if($diff < 0)
                $outdated = " outdated";
        }
    }
    else
    {
        $outdated = " outdated";
    }

    $image = $post_meta["featured_image"] != "" ?
            '<div class="image'. $outdated .'">
                    <img src="' . $post_meta["featured_image"] . '" alt="' . $post_meta["title"] . '"/>
            </div>'
            : "";

    $watchIco = file_get_contents(STYLESHEETPATH . '/template-parts/icon/watch.php');
    $author = get_post_author_info($post);

    /*
    ob_start();
        get_template_part( "template-parts/post/author" ) .
    $author = ob_get_clean();
    */
    
    $category_template = get_taxonomy_field("blog_category_template");

    if ((strpos($category_template, "one-column") !== false) && !is_single())
    {
        $result =  '<div class="post-item '. $classes  .'">
                        <div class="item-content">
                            <div class="item-head">
                                <div class="date">
                                    <div class="date-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                                    </div>
                                    <div class="date-text">'. $post_meta["date"] .'</div>
                                </div>
                               </div><a href="'. $post_meta["url"] .'">
                                <'.$heading.' class="title">'. $post_meta["title"] .'</'.$heading.'>'.
                                     $image .
                                      $promo_time_left .
                                 '<div class="excerpt">'. $post_meta["excerpt"] .'</div>
                            </a>';

        if($author)
        {
            $result .= '<div class="author">'.$author['name'].'</div>';
        }

        $wordCount = str_word_count(strip_tags(get_post_field("post_content", $post->ID)));
        if (isset($wordCount) && $wordCount != 0)
        {
            $result .= '<div class="word-count">';
            $result .= $watchIco;
            $result .= '<div>Czas czytania ok. ' . round($wordCount / 200) . ' min. (' . $wordCount . ' słów).</div></div>';
        }

        $result .=  $terms.'</div></div>';
    }
    else if (is_single())
    {
        $result =  '<div class="post-item '. $classes  .'">
                        <div class="item-content">
                            <a href="'. $post_meta["url"] .'">' . $image .'
                                <'.$heading.' class="title">'. $post_meta["title"] .'</'.$heading.'>
                                <div class="excerpt">'. $post_meta["excerpt"] .'</div>
                               <div class="read-more">Czytaj więcej</div>
                            </a>
                        </div>';

        $result .=  '</div>';
    }
    else
    {
        ob_start();
        template_part( "post/info/singleline" );
        $post_info = ob_get_clean();

        $result =  '<div class="post-item '. $classes  .'">
                        <a href="'. $post_meta["url"] .'">
                            <div class="item-content">' .
                               $image .
                                '<'.$heading.' class="title">'. $post_meta["title"] .'</'.$heading.'>'.
                               '<div class="excerpt">'. $post_meta["excerpt"] .'</div>
                            </div>
                        '.$post_info.'</a>';

//    echo create_categories_labels_list(get_post_categories(get_post()), "dark");
        $cats = get_post_categories(get_post());
//        $tags = wp_get_post_tags(get_post()->ID);
        $tags = get_post_tags(get_post());
        $result .= create_terms_labels_list($cats, $tags, "dark");
        $result .= '</div>';
    }

    return $result;
}

function product_count_display($number)
{
    if($number == 1)
    {
        return "1 oferta";
    }
    else if($number > 4 && $number < 22)
    {
        return $number." ofert";
    }
    else if($number%10 == 2 || $number%10 == 3 || $number%10 == 4)
    {
        return $number." oferty";
    }
    else
    {
        return $number." ofert";
    }
}

/* _____________________________________________________________________________ TAXONOMY */

//zwraca taxonomie w ktorej aktualnie jestesmy (np. 'category')
function get_current_taxonomy()
{
    return get_queried_object();
}

//zwraca fielda ACF, taxonomii w której jesteśmy (np. kategorii)
//lub konkretnej
//sam acf nie jest taki domyslny
function get_taxonomy_field($field, $tax = null)
{
    if (!$tax)
        $taxonomy = get_current_taxonomy();
    else
        $taxonomy = $tax;

    if ($taxonomy)
    {
        $from  = $taxonomy->taxonomy ."_". $taxonomy->term_id;
        return get_field($field, $from);
    }
    
    return null;
}

/* _____________________________________________________________________________ CATEGORY */

//zwraca posty z danych kategorii
function get_categories_posts($catsID, $number = -1)
{
    $queryParams = array(
        "post_type"			=> "post",
        "category"			=> implode (",", $catsID),
        "posts_per_page"	=> $number,
        'orderby'			=> 'date',
        'order'				=> 'DESC',
    );

    $posts = get_posts($queryParams);
    return $posts;
}

//zwraca podkategorie (wszystkie) wskazanej kategorii,
function get_subcategories($main_category, $include_self = false)
{
    $all_categories = get_categories(
            array(
                "child_of" => $main_category->cat_ID
            ));

    if ($include_self)
    {
        array_unshift($all_categories, $main_category);
    }
    
    $categories = array();    
    foreach ($all_categories as $category)
    {
        if (!treat_cat_as_tag($category))
        {
            $categories[] = $category;
        }
    }       

    return $categories;
}

//zwraca id kategorii które maja ustawione by traktowano je jak tagi
function get_all_tags_id($args = "category")
{
    $categories = get_terms($args);
    
    $categories_id = array();    
    foreach ($categories as $category)
    {
        if (treat_cat_as_tag($category))
        {
            $categories_id[] = $category->term_id;
        }
    } 
    
    return $categories_id;
}

//zwraca id categorii oddzielone przecinkami, przydatne
//np przy braniu postów tylko z danych kategorii
function categories_comma_separated($categories, $excluded_categories = null, $return_slugs = false)
{
    $categories_result = array();

    foreach ($categories as $category)
    {
        if(!is_array($excluded_categories) || !in_array($category->term_id, $excluded_categories))
        {
            if ($return_slugs)
            {
                $categories_result[] = $category->slug;                
            }
            else
            {
                $categories_result[] = $category->term_id;
            }
        }
    }
    return implode (",", $categories_result);
}


//zwraca adres url podanej kategorii
function get_category_url($category)
{
    $category_id = get_cat_ID($category->slug);
    $url = get_category_link( $category_id );

    return $url;
}

//zwraca kategorie ktora jest rootem dla wskazanej kategorii
function get_root_of_category($category)
{
    while ($category->parent > 0)
    {
        $category = get_category($category->parent);
    }

    return $category;
}

/* _____________________________________________________________________________ */

function array_unique_merge($array1, $array2)
{
    return array_unique(array_merge($array1,$array2), SORT_REGULAR);
}

function template_part($template_path)
{
    if (empty($template_path))
        return;
    
    if (substr($template_path, 0, 1) !== "/")      
        $template_path = "/" . $template_path;
    
    if (substr($template_path, -strlen(".php")) !== ".php")            
        $template_path .= ".php";
     
    include(get_template_directory() . "/template-parts" . $template_path);
}

/* Note: Strona /finansowanie zawsze ma nazwe Finansowanie w breadcrumbsach, niezależnie od tytułu tej stronie */
function the_breadcrumbs( $custom_taxonomy = "")
{       
    $home_title         = 'iKalkulator';
//    $separator          = '&gt;';
    $separator          = '&rsaquo;'; 
    $funding_slug       = "finansowanie";
    $funding_name       = "Finansowanie";
    $position           = 0;
    
    
    global $post, $wp_query;
    
    if ( !is_front_page() )
    {
        $bcJsonLd =
            [
                "@context" => "http://schema.org",
                 "@type" => "BreadcrumbList",
                "itemListElement" => []
            ];


        echo '<div class="breadcrumbs">';       
        // Build the breadcrums
        echo '<ul class="breadcrumbs-items">';
           
        // Home page
        echo '<li class="item-link item-home">
                  <a href="' . get_home_url() . '" title="' . $home_title . '"><span>' . $home_title . '</span></a>
               </li>';

        echo '<li class="separator separator-home"> ' . $separator . ' </li>';

        $bcJsonLdEls [] = ["url" => get_home_url(), "name" => $home_title];

        if ( is_archive() && !is_tax() && !is_category() && !is_tag() )
        {        
            //for custom slug 
            $archive_post_type = get_post_type();
            $post_type_object = get_post_type_object($archive_post_type);  
            $post_type_slug = $post_type_object->rewrite['slug'];
            
            $pages = explode("/", $post_type_slug);
            $slug = "";
            foreach ($pages as $page)
            { 
                $slug .= $page . "/";
                $page = get_page_by_path(untrailingslashit( $slug ));
                
                if ($page && $post_type_object->rewrite['slug'] != untrailingslashit( $slug ))   
                {
                    $page_title = untrailingslashit( $slug ) == $funding_slug ? $funding_name : get_the_title($page);
                    echo '<li itemprop="itemListElement" class="item-link">
                              <a href="' . get_permalink($page) . '" title="' . get_the_title($page) . '"><span>' . $page_title . '</span></a>
                              <meta itemprop="position" content="'.$position++ .'" />
                         </li>';
                    echo '<li class="separator"> ' . $separator . ' </li>';

                    $bcJsonLdEls [] = ["url" => get_permalink($page), "name" => $page_title];
                }
            }
            echo '<li class="item-current item-archive">
                      <span>' . post_type_archive_title("", false) . '</span>
                 </li>';

            $bcJsonLdEls [] = ["url" => "current", "name" => post_type_archive_title("", false)];
        }
        else if ( is_archive() && is_tax() && !is_category() && !is_tag() )
        {
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-link item-cat item-post-type-' . $post_type . '">
                          <a href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '"><span>' . $post_type_object->labels->name . '</span></a>
                     </li>';
                echo '<li class="separator"> ' . $separator . ' </li>';

                $bcJsonLdEls [] = ["url" => get_permalink($post_type_archive), "name" => $post_type_object->labels->name];
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive">
                        <span>' . $custom_tax_name . '</span>
                 </li>';
            $bcJsonLdEls [] = ["url" => "current", "name" => $custom_tax_name];
              
        } 
        elseif ( is_single() )
        {
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                
                //for custom slug                
                $post_type_slug = $post_type_object->rewrite['slug'];
                $pages = explode("/", $post_type_slug);
                $slug = "";

                foreach ($pages as $page)
                {
                    $slug .= $page . "/";
                    $page = get_page_by_path(untrailingslashit( $slug ));
                    if ($page && $post_type_slug != untrailingslashit( $slug ))     
                    {
                        $page_title = untrailingslashit( $slug ) == $funding_slug ? $funding_name : get_the_title($page);
                        echo '<li class="item-link">
                                  <a href="' . get_permalink($page) . '" title="' . $page_title . '"><span>' . $page_title . '</span></a>
                              </li>';
                        echo '<li class="separator"> ' . $separator . ' </li>';
                        $bcJsonLdEls [] = ["url" => get_permalink($page), "name" => $page_title];
                    }
                }
              
                $post_type_archive = get_post_type_archive_link($post_type);
                echo '<li class="item-link item-cat item-post-type-' . $post_type . '">
                          <a href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '"><span>' . $post_type_object->labels->name . '</span></a>
                      </li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
                $bcJsonLdEls [] = ["url" => $post_type_archive, "name" => $post_type_object->labels->name];
            }

            // Get post category info
            $category = get_the_category();
            $breadcrumbs = "";
            
            if(!empty($category))
            {
                $wpseo_primary_term = new WPSEO_Primary_Term( "category", get_the_id() );
                $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
                
                if($wpseo_primary_term)
                {
                    $term = get_term( $wpseo_primary_term );
                }
                else
                {
                    $term  = $category[0];
                }
                $term_name  = str_replace("#", "", $term->name);  
                $breadcrumbs = '<li class="item-link item-cat">
                                    <a href="' . esc_url( get_term_link( $term ) ) . '"><span>' . $term_name . '</span></a>
                                </li>';
                $bread_item = "";
                $delayedJsonLdAdd = ["url" => esc_url( get_term_link( $term ) ), "name" => $term_name];

                while ($term->parent > 0)
                {
                    $term = get_category($term->parent);
                    $term_name  = str_replace("#", "", $term->name);  
                    $bread_item .= '<li class="item-link item-cat">
                                        <a href="' . esc_url( get_term_link( $term ) ) . '"><span itemprop="name">' . $term_name . '</span></a>
                                    </li>';
                    $bread_item .= '<li class="separator"> ' . $separator . ' </li>';

                    $breadcrumbs = $bread_item . $breadcrumbs;
                    $bcJsonLdEls [] = ["url" => esc_url( get_term_link( $term ) ), "name" => $term_name];
                }
                $bcJsonLdEls [] = $delayedJsonLdAdd;
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
            
            if ($breadcrumbs)
            {
                echo  $breadcrumbs . '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '">
                          <span>' . get_the_title() . '</span>
                      </li>';
                $bcJsonLdEls [] = ["url" => "current", "name" => get_the_title()];
            }
            elseif(!empty($cat_id))
            {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '">
                          <a href="' . $cat_link . '" title="' . $cat_name . '"><span>' . $cat_name . '</span></a>
                      </li>';

                $bcJsonLdEls [] = ["url" => $cat_link, "name" => $cat_name];

                echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '">
                          <span>' . get_the_title() . '</span>
                      </li>';
                $bcJsonLdEls [] = ["url" => "current", "name" => get_the_title()];
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '">
                          <span>' . get_the_title() . '</span>
                      </li>';
                $bcJsonLdEls [] = ["url" => "current", "name" => get_the_title()];
                  
            }
        }
        elseif ( is_category() )
        {
            // Category page
            $term =         get_queried_object();
            $term_nicename  = $term->slug;
            $term_name      = str_replace("#", "", $term->name);  
            
            $breadcrumbs = '<li class="item-current item-cat">
                                <span>'. $term_name .'</span>
                            </li>';
            $bread_item = "";
            $delayedJsonLdAdd = ["url" => "current", "name" => $term_name];
            
            while ($term->parent > 0)
            {
                $term = get_category($term->parent);	
                $term_name      = str_replace("#", "", $term->name);  
                $bread_item .= '<li class="item-link item-cat">
                                    <a href="' . esc_url( get_term_link( $term ) ) . '"><span>' . $term_name . '</span></a>
                                </li>';
                $bread_item .= '<li class="separator"> ' . $separator . ' </li>';

                $bcJsonLdEls [] = ["url" =>  esc_url( get_term_link( $term ) ), "name" => $term_name];
                
                $breadcrumbs = $bread_item . $breadcrumbs;
            }
            echo $breadcrumbs;


            $bcJsonLdEls [] = $delayedJsonLdAdd;
//            echo '<li itemprop="itemListElement"
//itemscope itemtype="http://schema.org/ListItem" class="item-current item-cat"><span itemprop="name">' . single_cat_title('', false) . '</span></li>';               
        }
        elseif ( is_page() ) 
        {
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-link item-parent item-parent-' . $ancestor . '">
                                     <a href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '"><span>' . get_the_title($ancestor) . '</span></a>
                                </li>';
                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';

                    $bcJsonLdEls [] = ["url" =>  get_permalink($ancestor), "name" => get_the_title($ancestor)];
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li itemprop="itemListElement"
                          itemscope itemtype="http://schema.org/ListItem"
                          class="item-current item-' . $post->ID . '"><span itemprop="name" title="' . get_the_title() . '"> ' . get_the_title() . '</span>
                          <meta itemprop="position" content="'.$position++ .'" />
                      </li>';

                $bcJsonLdEls [] = ["url" =>  "current", "name" => get_the_title()];
                   
            } else {
                $page_title = $post->post_name == $funding_slug ? $funding_name : get_the_title();
                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '">
                          <span> ' . $page_title . '</span>
                      </li>';
                $bcJsonLdEls [] = ["url" =>  "current", "name" => get_the_title()];
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '">
                      <span>' . $get_term_name . '</span>
                      </li>';

            $bcJsonLdEls [] = ["url" =>  "current", "name" => $get_term_name];
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li itemprop="itemListElement"
                      itemscope itemtype="http://schema.org/ListItem"
                      class="item-year item-year-' . get_the_time('Y') . '">
                      <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '"><span itemprop="name">' . get_the_time('Y') . ' Archives</span></a>
                      <meta itemprop="position" content="'.$position++ .'" />
                  </li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month link
            echo '<li itemprop="itemListElement"
                      itemscope itemtype="http://schema.org/ListItem" 
                      class="item-month item-month-' . get_the_time('m') . '">
                      <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '"><span itemprop="name">' . get_the_time('M') . ' Archives</span></a>
                      <meta itemprop="position" content="'.$position++ .'" />
                  </li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
               
            // Day display
            echo '<li itemprop="itemListElement"
                      itemscope itemtype="http://schema.org/ListItem"
                      class="item-current item-' . get_the_time('j') . '"><span itemprop="name"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</span>
                      <meta itemprop="position" content="'.$position++ .'" />
                   </li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li itemprop="itemListElement"
                        itemscope itemtype="http://schema.org/ListItem"
                        class="item-year item-year-' . get_the_time('Y') . '">
                        <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '"><span itemprop="name">' . get_the_time('Y') . ' Archives</span></a>
                        <meta itemprop="position" content="'.$position++ .'" />
                  </li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month display
            echo '<li itemprop="itemListElement"
                      itemscope itemtype="http://schema.org/ListItem"
                      class="item-month item-month-' . get_the_time('m') . '">
                      <span itemprop="name">' . get_the_time('M') . ' Archives</span>
                  </li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li itemprop="itemListElement"
                        itemscope itemtype="http://schema.org/ListItem"
                        class="item-current item-current-' . get_the_time('Y') . '">
                        <span itemprop="name">' . get_the_time('Y') . ' Archives</span>
                        <meta itemprop="position" content="'.$position++ .'" />
                    </li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li itemprop="itemListElement"
                      itemscope itemtype="http://schema.org/ListItem" 
                      class="item-current item-current-' . $userdata->user_nicename . '"><span itemprop="name">' . 'Author: ' . $userdata->display_name . '</span>
                      <meta itemprop="position" content="'.$position++ .'" />
                  </li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li itemprop="itemListElement"
                      itemscope itemtype="http://schema.org/ListItem"
                      class="item-current item-current-' . get_query_var('paged') . '"><span itemprop="name">'.__('Page') . ' ' . get_query_var('paged') . '</span>
                      <meta itemprop="position" content="'.$position++ .'" />
                  </li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li itemprop="itemListElement"
                      itemscope itemtype="http://schema.org/ListItem"
                      class="item-current item-current-' . get_search_query() . '"><span itemprop="name">Search results for: ' . get_search_query() . '</span>
                      <meta itemprop="position" content="'.$position++ .'" />
                  </li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
       
        echo '</ul>';
        echo '</div>';

        global $wp;
        foreach($bcJsonLdEls as $k => $bcJsonLdEl)
        {
            $bcJsonLd["itemListElement"][] =
                [
                    "@type" => "ListItem",
                    "position" => $k+1,
                    "item" =>
                        [
                            "@id" => $bcJsonLdEl["url"] != "current" ? $bcJsonLdEl["url"] : home_url(add_query_arg($_GET,$wp->request)),
                            "name" => $bcJsonLdEl["name"]
                        ]
                ];
        }
        echo '<script type="application/ld+json">';
        echo json_encode($bcJsonLd);
        echo '</script>';
    }       
}



