<?php

/* _____________________________________________________________________________ PRODUCTS */

add_action("init", "custom_post_products");
function custom_post_products()
{
    register_post_type("quickloans",
        array(
            "labels" => array(
                "name" => __("Chwilówki", "wcd" ),
                "singular_name" => __("Chwilówka", "wcd" ),
                "add_new_item"       => __( "Dodaj chwilówkę", "wcd" ),
                "add_new"            => _x( "Dodaj chwilówkę", "wcd" ),
            ),
            "public"			=> true,
            "has_archive"		=> true,
            "supports"			=> array("title", "excerpt", "editor", "comments", "revisions", "author"),
            "map_meta_cap"		=> true,
            "show_ui" => true,
            "show_in_menu" => "products_menu",
            "rewrite" => array(
//            "slug" => "chwilowki"
            "with_front" => false,
            "slug" => "finansowanie/chwilowki"
            )
        )
    );

    register_post_type("loans",
        array(
            "labels" => array(
                "name" => __("Pożyczki ratalne", "wcd" ),
                "singular_name" => __("Pożyczka ratalna", "wcd" ),
                "add_new_item"       => __( "Dodaj pożyczkę", "wcd" ),
                "add_new"            => _x( "Dodaj pożyczkę", "wcd" ),
            ),
            "public"			=> true,
            "has_archive"		=> true,
            "supports"			=> array("title", "excerpt", "editor", "comments", "revisions", "author"),
            "map_meta_cap"		=> true,
            "show_ui" => true,
            "show_in_menu" => "products_menu",
            "rewrite" => array(
                "with_front" => false,
                "slug" => "finansowanie/pozyczki-na-raty"
            )
        )
    );

    register_post_type("credits",
        array(
            "labels" => array(
                "name" => __("Kredyty gotówkowe", "wcd"),
                "singular_name" => __("Kredyt gotówkowy", "wcd"),
                "add_new_item"       => __( "Dodaj kredyt", "wcd" ),
                "add_new"            => _x( "Dodaj kredyt", "wcd" ),
            ),
            "public"			=> true,
            "has_archive"		=> true,
            "supports"			=> array("title", "excerpt", "editor", "thumbnail", "comments", "author", "tags", "revisions", "author"),
            "map_meta_cap"		=> true,
            "show_ui" => true,
            "show_in_menu" => "products_menu",
            "rewrite" => array(
                "with_front" => false,
                "slug" => "finansowanie/kredyty-gotowkowe"
//                "slug" => "kredyty-gotowkowe"
            )
        )
    );

    register_post_type("accounts_personal",
        array(
            "labels" => array(
                "name" => __("Konta osobiste", "wcd"),
                "singular_name" => __("Konto osobiste", "wcd"),
                "add_new_item"       => __( "Dodaj konto osobiste", "wcd" ),
                "add_new"            => _x( "Dodaj konto osobiste", "wcd" ),
            ),
            "public"			=> true,
            "has_archive"		=> true,
            "supports"			=> array("title", "excerpt", "editor", "comments", "revisions", "author"),
            "map_meta_cap"		=> true,
            "show_ui" => true,
            "show_in_menu" => "products_menu",
            "rewrite" => array(
                "with_front" => false,
                "slug" => "konta-bankowe/konta-osobiste"
            )
        )
    );

    register_post_type("accounts_business",
        array(
            "labels" => array(
                "name" => __("Konta firmowe", "wcd"),
                "singular_name" => __("Konto firmowe", "wcd"),
                "add_new_item"       => __( "Dodaj konto firmowe", "wcd" ),
                "add_new"            => _x( "Dodaj konto firmowe", "wcd" ),
            ),
            "public"			=> true,
            "has_archive"		=> true,
            "supports"			=> array("title", "excerpt", "editor", "comments", "revisions", "author"),
            "map_meta_cap"		=> true,
            "show_ui" => true,
            "show_in_menu" => "products_menu",
            "rewrite" => array(
                "with_front" => false,
                "slug" => "konta-bankowe/konta-firmowe"
            )
        )
    );

    register_post_type("accounts_saving",
        array(
            "labels" => array(
                "name" => __("Konta oszczędnościowe", "wcd"),
                "singular_name" => __("Konto oszczędnościowe", "wcd"),
                "add_new_item"       => __( "Dodaj konto oszczędnościowe", "wcd" ),
                "add_new"            => _x( "Dodaj konto oszczędnościowe", "wcd" ),
            ),
            "public"			=> true,
            "has_archive"		=> true,
            "supports"			=> array("title", "excerpt", "editor", "comments", "revisions", "author"),
            "map_meta_cap"		=> true,
            "show_ui" => true,
            "show_in_menu" => "products_menu",
            "rewrite" => array(
                "with_front" => false,
                "slug" => "konta-bankowe/konta-oszczednosciowe"
            )
        )
    );

    register_post_type("accounts_currency",
        array(
            "labels" => array(
                "name" => __("Konta walutowe", "wcd"),
                "singular_name" => __("Konto walutowe", "wcd"),
                "add_new_item"       => __( "Dodaj konto walutowe", "wcd" ),
                "add_new"            => _x( "Dodaj konto walutowe", "wcd" ),
            ),
            "public"			=> true,
            "has_archive"		=> true,
            "supports"			=> array("title", "excerpt", "editor", "comments", "revisions", "author"),
            "map_meta_cap"		=> true,
            "show_ui" => true,
            "show_in_menu" => "products_menu",
            "rewrite" => array(
                "with_front" => false,
                "slug" => "konta-bankowe/konta-walutowe"
            )
        )
    );

    register_post_type("investments",
        array(
            "labels" => array(
                "name" => __("Lokaty", "wcd" ),
                "singular_name" => __("Lokata", "wcd" ),
                "add_new_item"       => __( "Dodaj lokatę", "wcd" ),
                "add_new"            => _x( "Dodaj lokatę", "wcd" ),
            ),
            "public"			=> true,
            "has_archive"		=> true,
            "supports"			=> array("title", "excerpt", "editor", "comments", "revisions", "author"),
            "map_meta_cap"		=> true,
            "show_ui" => true,
            "show_in_menu" => "products_menu",
            "rewrite" => array(
                "with_front" => false,
                "slug" => "lokaty"
            )
        )
    );
    
    register_post_type("company",
        array(
            "labels" => array(
                "name"               => __( "Firmy", "wcd" ),
                "singular_name"      => __( "Firma", "wcd" ),
                "menu_name"          => __( "Firmy", "wcd" ),
                "name_admin_bar"     => __( "Firma", "wcd" ),
                "add_new_item"       => __( "Dodaj firmę", "wcd" ),
                "new_item"           => __( "Nowa firma", "wcd" ),
                "edit_item"          => __( "Edytuj firmę", "wcd" ),
                "view_item"          => __( "Zobacz firmę", "wcd" ),
                "all_items"          => __( "Wszystkie firmy", "wcd" ),
                "search_items"       => __( "Wyszukaj firmy", "wcd" ),
            ),
            "public"			=> true,
            "has_archive"		=> true,
            "supports"          => array("title", "excerpt", "editor", "thumbnail", "revisions", "author"),
            "map_meta_cap"		=> true,
            "show_ui"           => true,
            "menu_icon"         => "dashicons-building",
//            "show_in_menu" => "products_menu",
            "rewrite" => array(
    //            "slug" => "chwilowki"
                "with_front" => false,
                "slug" => "firmy"
            )            
        )
    );

    register_taxonomy("company_type", "company", array(
        "public" => true,
        "show_ui" => true,
        "show_in_nav_menus" => true,
        "hierarchical" => true,
        "query_var" => true,
        "rewrite" => false,
    ));     

    add_filter('single_template', function($original){
        global $post;
        $post_type = $post->post_type;
        if($post_type != "post" && $post_type != "page" && $post_type != "attachment" && $post_type != "revision" && $post_type != "nav_menu_item" &&$post_type != "custom_css" &&$post_type != "customize_changeset")
        {
            $base_name = 'single-product.php';
            $template = locate_template($base_name);
            if ($template && ! empty($template)) return $template;
            return $original;
        }
        else if($post_type == "page")
        {
            $base_name = 'page.php';
            $template = locate_template($base_name);
            if ($template && ! empty($template)) return $template;
            return $original;
        }
        else
        {
            $base_name = 'single.php';
            $template = locate_template($base_name);
            if ($template && ! empty($template)) return $template;
            return $original;
        }
    });

}
