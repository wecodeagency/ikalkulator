<?php


/* _____________________________________________________________________________ PREPARE WEBSITE FROM COMMENTS */

function open_wrap_fields_action(){
	echo '<div class="details">';
}
add_action('comment_form_before_fields','open_wrap_fields_action',10);

function close_wrap_fields_action(){
	echo '</div>';
}
add_action('comment_form_after_fields','close_wrap_fields_action',10);


function ikal_prepare_post_comments($fields) {
	unset($fields['url']);
	$fields['email'] =
		'<p class="comment-form-email">
			<label for="email">' . __( "Email *", "text-domain" ) . '</label>
			<input id="email" name="email" type="email"  value="" size="30" required="required" />
		</p>';
	return $fields;
}

function ikal_prepare_product_comments($fields) {    
	unset($fields['url']);
    $comment_field = isset($fields['comment']) ? $fields['comment'] : false;

	$fields['email'] =
		'<p class="comment-form-email">
			<label for="email">' . __( "Email *", "text-domain" ) . '</label>
			<input id="email" name="email" type="email"  value="" size="30" required="required" />
		</p>';
	$fields['rating'] =
		'<div class="comment-form-rating">
			<label for="rating">' . __( "Twoja ocena", "text-domain" ) . '</label>
			<div class="star-rating selectable" data-rating="4">
				<div class="stars"></div>
			</div>
			<input id="rating" name="rating" type="hidden"  value="4" size="30" min="1" max="5" />
		</div>';
    $fields['comment'] = $comment_field;
    
	return $fields;
}

function ikal_prepare_comments()
{
	global $post;

	if(is_single())
	{
		if($post->post_type == "post")
			add_filter('comment_form_default_fields','ikal_prepare_post_comments');
		else
			add_filter('comment_form_default_fields','ikal_prepare_product_comments');
    }
}
add_filter('single_template','ikal_prepare_comments');


function ikal_handle_comment($comment_id)
{
	$comment = get_comment( $comment_id );
	$commentedPost = get_post($comment->comment_post_ID);
	if($commentedPost->post_type != "post")
	{
		if (  isset( $_POST['rating'] )  &&  in_array((int)$_POST['rating'], [1,2,3,4,5]) )
		{
			if($_GET['ikcmthash'])
			{
				$args = array(
					'meta_key' => 'hash',
					'meta_value' => $_GET['ikcmthash'],
					'status' => 'approve',
					'author_email' => $comment->comment_author_email
				);
				$comment_query = new WP_Comment_Query( $args );
			}
			if(!count($comment_query->comments))
			{
				wp_set_comment_status($comment_id, 'hold');
				$hash = md5(time().$_POST['rating'].rand(0,999));
				add_comment_meta( $comment_id, 'rating', $_POST['rating'] );
				add_comment_meta( $comment_id, 'hash', $hash );

				if(substr_count($_SERVER["HTTP_REFERER"], "?"))
					$ref = explode("?", $_SERVER["HTTP_REFERER"])[0];
				else
					$ref = $_SERVER["HTTP_REFERER"];

				wp_mail($comment->comment_author_email, "Potwierdzenie komentarza w serwisie ikalkulator.", $_SERVER["HTTP_REFERER"]."?ikcmthash=".$hash);
			}
		}
	}

}
add_filter('comment_post', 'ikal_handle_comment');

if(isset($_GET['ikcmthash']))
{
	$args = array(
		'meta_key' => 'hash',
		'meta_value' => $_GET['ikcmthash'],
//		'status' => 'hold'
	);
	$comment_query = new WP_Comment_Query( $args );

	foreach($comment_query->comments as $comment)
	{
			wp_set_comment_status($comment->comment_ID, 'approve');
			ikal_calculate_post_rank($comment->comment_post_ID);
			wp_redirect( get_permalink($comment->comment_post_ID)."#comment-".$comment->comment_ID);
	}
}

function ikal_calculate_post_rank($post_id)
{
	if ( !is_object( $post_id ) ){
		$post = get_post( $post_id );
	}

	if('post' != $post->post_type){
		$avg = 0;
		global $wpdb;
		$query = "SELECT AVG(meta_value) FROM $wpdb->commentmeta ";
		$query .= "INNER JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id = $wpdb->comments.comment_ID " ;
		$query .= "WHERE $wpdb->commentmeta.meta_key = 'rating' ";
		$query .= "AND $wpdb->comments.comment_approved = 1 ";
		$query .= "AND $wpdb->comments.comment_post_ID = '$post_id'";

		if( $avg = $wpdb->get_var($query) ){
			update_post_meta($post_id, 'avg_rating', $avg);
		}else{
			// report error
			$wpdb->print_error();
		}

		file_get_contents("https://www.ikalkulator.pl/kinsta-clear-cache-all/");
	}


}

add_action('edit_comment', 'ikal_calculate_post_rank');
add_action('comment_post', 'ikal_calculate_post_rank');
add_action('comment_unapproved_to_approved', 'ikal_calculate_post_rank');
add_action('comment_approved_to_unapproved', 'ikal_calculate_post_rank');
add_action('comment_spam_to_approved', 'ikal_calculate_post_rank');
add_action('comment_approved_to_spam', 'ikal_calculate_post_rank');
add_action('comment_approved_to_trash', 'ikal_calculate_post_rank');
add_action('comment_trash_to_approved', 'ikal_calculate_post_rank');


function ikal_comment_callback( $comment, $args, $depth ) {
	$tag = ( 'div' === $args['style'] ) ? 'div' : 'li';
	?>
	<<?php echo $tag; ?> id="comment-<?php comment_ID(); ?>" <?php comment_class( $comment->has_children ? 'parent' : '', $comment ); ?>>
	<article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
		<footer class="comment-meta">
			<div class="comment-author vcard">
				<?php /* printf( __( '%s <span class="says">says:</span>' ), sprintf( '<b class="fn">%s</b>', get_comment_author_link( $comment ) ) ); */ ?>
				<?php printf( sprintf( '<b class="fn">%s</b>', get_comment_author_link( $comment ) ) ); ?>
			</div><!-- .comment-author -->
			<div class="comment-metadata">
				<a class="comment-time" href="<?php echo esc_url( get_comment_link( $comment, $args ) ); ?>">
					<time datetime="<?php comment_time( 'c' ); ?>">
						<?php
						/* translators: 1: comment date, 2: comment time */
						printf( __( '%1$s at %2$s' ), get_comment_date( '', $comment ), get_comment_time() );
						?>
					</time>
				</a>                
				<?php edit_comment_link( __( 'Edit' ), '<span class="edit-link">', '</span>' ); ?>
				<?php if(get_comment_meta($comment->comment_ID, 'rating', true)): ?>
					<div class="star-rating" data-rating="<?php echo ( number_format(get_comment_meta($comment->comment_ID, 'rating', true), 1, ".", "")); ?>">
						<div class="stars"></div>
                        <?php /* <div class="mark"><?php if(get_comment_meta($comment->comment_ID, 'rating', true)) echo( number_format(floatval(get_comment_meta($comment->comment_ID, 'rating', true)), 1, ".", "")); ?> / 5.0</div> */ ?>
					</div>
				<?php endif; ?>
			</div><!-- .comment-metadata -->

			<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></p>
			<?php endif; ?>
		</footer><!-- .comment-meta -->

		<div class="comment-content">
			<?php comment_text(); ?>
		</div><!-- .comment-content -->

		<?php
		comment_reply_link( array_merge( $args, array(
			'add_below' => 'div-comment',
			'depth'     => $depth,
			'max_depth' => $args['max_depth'],
			'before'    => '<div class="reply">',
			'after'     => '</div>'
		) ) );
		?>
	</article><!-- .comment-body -->
	<?php
}
