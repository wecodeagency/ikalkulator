(function() {
	tinymce.create("tinymce.plugins.ik_highlight", {
		init : function(ed, url) {
			ed.addButton("ik_highlight", {
				title : "iKalkulator highlight",
				cmd : "ik_highlight",
				image : "/wp-content/themes/ikalkulator/assets/icons/grey/sun.svg"
			});
			ed.addCommand("ik_highlight", function() {
				var selected_text = ed.selection.getContent();
				var return_text = "[ik_highlight]" + selected_text + "[/ik_highlight]";
				ed.execCommand("mceInsertContent", 0, return_text);
			});
		},
		createControl : function(n, cm) {
			return null;
		},
		getInfo : function() {
			return {
				longname : "iKalkulator highlights",
				author : "we code",
				version : "1"
			};
		}
	});
	tinymce.PluginManager.add("ik_highlight", tinymce.plugins.ik_highlight);
})();