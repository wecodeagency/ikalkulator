ikalkulator = function ()
{
    function calculateChildrenHeight(container)
    {	
        var topOffset = bottomOffset = 0;

        container.children().each(function(i, child)
        {
            var element = jQuery(child),
            eTopOffset = element.offset().top,
            eBottomOffset = eTopOffset + element.outerHeight();

            if (eTopOffset < topOffset)
                topOffset = eTopOffset;

            if (eBottomOffset > bottomOffset)
                bottomOffset = eBottomOffset;
        });

        return  bottomOffset - topOffset - container.offset().top + parseInt(container.css("padding-bottom"));
    }    
    
    function handleCategoryWithSidebarMinHeight()
    {
        var sidebar = jQuery("body.category section.category-content .aside-panel");
        if (!sidebar.length) { return; }
        
        var categoryContent = jQuery("body.category section.category-content");        
        categoryContent.css("min-height", calculateChildrenHeight(categoryContent));
    }
    
    function handleListNumbering()
    {
        jQuery("ol[start]").each(function()
        {
            var val = parseFloat(jQuery(this).attr("start")) - 1;
            console.log(val);
            jQuery(this).css("counter-increment", "ol-counter " + val);
        });        
    }
    
    function handleProductsGrid()
    {
        var productGrid = jQuery(".product-grid");
        
        if (!productGrid.length)
            return;
        
        var showMore = jQuery(".products-show-more", productGrid);
        
        showMore.on("click", function()
        {
            var btn = jQuery(this);
            var moreProducts = jQuery(".posts-list-content.more-products", productGrid);
            
            if (moreProducts.hasClass("visible"))
            {
                jQuery(".show", btn).show(); 
                jQuery(".hide", btn).hide(); 
                moreProducts.removeClass("visible");
                moreProducts.slideUp();
                scrollTo(jQuery(".posts-list-content", productGrid), -30);
            }    
            else
            {
                jQuery(".show", btn).hide(); 
                jQuery(".hide", btn).show(); 
                moreProducts.addClass("visible");
                moreProducts.slideDown();
                scrollTo(jQuery(".posts-list-content", productGrid), -30);
            }   
        });
        
    }
    
    
	function handleShareLinks()
	{
		jQuery(".share .social-share").on("click", function(e)
        {
			e.preventDefault();
			window.open(jQuery(this).attr('href'), 'fbShareWindow', 'height=450, width=650, top=' + (jQuery(window).height() / 2 - 275) + ', left=' + (jQuery(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
			return false;
		});
	}
    
    function handleSpotlightSearch()
    {
        var searchBtn = jQuery(".site-header .search-btn a");
        var searchBar = jQuery(".search-bar.spotlight-search");
        
        if (!searchBtn.length || !searchBtn.length)
            return;
        
        function hideBar()
        {
            searchBar.removeClass("active");
            jQuery("input", searchBar).blur().attr("readonly", true);
        }
        
        jQuery("input", searchBar).attr("readonly", true);
        
        searchBtn.on("click", function(e)
        {
            e.stopPropagation();
            e.preventDefault();            
            searchBar.toggleClass("active");  
            
            if (searchBar.hasClass("active"))
            {
                jQuery("input", searchBar).attr("readonly", false); 
                
                window.setTimeout(function()
                {
                    jQuery("input[type=text]", searchBar).focus();
                } ,500);
                
        }
            else
                jQuery("input", searchBar).attr("readonly", true);
        });

        searchBar.on("click touch touchend", function(e)
        {
            e.stopPropagation();
        });        
        
        jQuery(document)
            .on("click touch touchend", function()
            {
                hideBar();
            })        
            .on("keydown", function(e)
            {
                if ( e.keyCode == 27 )
                {
                    hideBar();    
                }
            });
    }

    function checkElementsVisibilityOnScroll(elementsClass)
    {
        var elements = jQuery(elementsClass);

        if (!elements.length)
            return;

        elements
            .attr("data-onscreen", 0);

        var isScrolling  = false;

        jQuery(window).on("scroll", function()
        {
			if (isScrolling) { return; }

            var winHeight = jQuery(window).innerHeight();
            var scrollTop =  jQuery(window).scrollTop();
            var scrollBottom = scrollTop + winHeight;

			isScrolling = true;

			window.setTimeout(function()
			{
                elements.each(function()
                {
                    var element = jQuery(this);
                    var isVisible = element.attr("data-onscreen");
                    var elementHeight = element.outerHeight();
                    var elementTop = element.offset().top;
                    var elementBottom = elementTop + elementHeight;

                    if (isVisible == true)
                    {
                        if (elementBottom <= scrollTop || elementTop >= scrollBottom)
                        {
                            element
                                .trigger("ik:visiblity-change")
                                .trigger("ik:hidden")
                                .attr("data-onscreen", 0);
                        }
                    }
                    else
                    {
                        if (
                                (elementBottom > scrollTop && elementTop < scrollTop)
                                ||
                                (elementTop < scrollBottom && elementBottom > scrollBottom)
                                ||
                                (elementTop < scrollTop && elementBottom > scrollBottom)
                            )
                        {
                            element
                                .trigger("ik:visiblity-change")
                                .trigger("ik:visible")
                                .attr("data-onscreen", 1);
                        }
                    }

                    /*
                    if (elementBottom <= scrollBottom && elementTop >= scrollTop)
                        element.trigger("ik:reveal:full");
                    else*/
                });

				isScrolling = false;
			}, 5 );
        });

		jQuery(window).trigger("scroll");
    }
    function handleMenu()
    {
        var siteHeader = jQuery(".site-header");
        var menuItems = jQuery(".menu > .menu-item-has-children > a", siteHeader);
        
        
        menuItems.on("click", function(e)
        {
            if (window.innerWidth < 1024)
            {
                var isClicked = jQuery(this).parent().hasClass("clicked");
               
                
                if (!isClicked)
                {
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    e.stopPropagation();              
                    
                    jQuery(".menu > .menu-item-has-children").removeClass("clicked");
                    jQuery(this).parent().addClass("clicked");
                }
                else
                {
//                    jQuery(this).parent().removeClass("clicked");                    
                }
            }
        });
        
    }

	function handleMobileMenu()
	{
        var body = jQuery("body");
        var siteHeader = jQuery(".site-header");
		var btnMenu = jQuery(".btn-mobile-menu", siteHeader);
		var menuItems = jQuery(".menu-item a", siteHeader);

		btnMenu
			.on("click", function(e)
			{
				body.toggleClass("menu-opened");

				if (body.hasClass("menu-opened"))
				{
					jQuery(this).addClass("active");
					siteHeader.addClass("mobile-active");
				}
				else
				{
					jQuery(this).removeClass("active");
					siteHeader.removeClass("mobile-active");
				}
			});

		menuItems
			.on("click", function(e)
			{
                if(!jQuery(this).parent().hasClass("menu-item-has-children"))
                {
                    body.removeClass("menu-opened");
                    btnMenu.removeClass("active");
                    siteHeader.removeClass("mobile-active");

                }
			});
	};

    function handleAnalytics()
    {
        jQuery('.navigation-top li a').on('click', function(e)
        {
            var link = jQuery(this).attr("href");
            //e.preventDefault();
            gtag('event', 'TOP-menu', {
                'event_category': 'click',
                'event_label': jQuery(this).find("span").html(),
                //'event_callback': function(){document.location = link;}
            });
        });

        jQuery('.footer-nav li a').on('click', function(e)
        {
            //e.preventDefault();

            var link = jQuery(this).attr("href");

            gtag('event', 'footer-menu', {
                'event_category': 'click',
                'event_label': jQuery(this).find("span").html(),
                //'event_callback': function() {document.location = link;}
            });
        });

        jQuery('.products.list li .more .expander .expand').on('click', function(e)
        {
            var product_name = jQuery(this).closest("li").find(".name div").html();
            e.preventDefault();
            gtag('event', 'Zobacz więcej', {
                'event_category': 'click',
                'event_label': product_name + '  na ' + location.href
            });
        });

        jQuery('.products.list li .more .expander .fold').on('click', function(e)
        {
            var product_name = jQuery(this).closest("li").find(".name div").html();
            e.preventDefault();
            gtag('event', 'Zobacz mniej', {
                'event_category': 'click',
                'event_label': product_name + '  na ' + location.href
            });
        });

        jQuery('.products.list li .cta a').on('click', function(e)
        {
            //e.preventDefault();

            var link = jQuery(this).attr("href");
            var product_name = jQuery(this).closest("li").attr("id")
            var color = "biały";

            if(jQuery(this).hasClass("btn-inverse"))
                color = "zielony";

            gtag('event', 'compare-acquisition', {
                'event_category': 'click',
                'event_label': 'Złóż wniosek - ' + color + ' guzik - '  + product_name + ' na ' + location.href,
                //'event_callback': function(){document.location = link;}
            });
        });

        jQuery('.product .attachments ul li a').on('click', function(e)
        {
            //e.preventDefault();

            var link = jQuery(this).attr("href");

            gtag('event', 'PDF', {
                    'event_category' : 'pobranie',
                    'event_label' : location.href,
                    //'event_callback': function(){document.location = link;}
                }
            );
        });


        jQuery('.share .links a.social-share').on('click', function()
        {
            var media = jQuery(this).find("span").html();

            gtag('event', media,
                { 'event_category' : 'share', 'event_label' : location.href }
            );
        });

        jQuery('.post-content .external-link').on('click', function()
        {
            var link = jQuery(this).attr("href");
            var text = jQuery(this).text();
            gtag("event", "link_z_artykulu",
                {
                    "event_category" : "click",
                    "event_label" : link + " | " + text,
                    //"event_callback": function(){document.location = link;}
                }
            );
        });


    }

    function scrollTo(targetOrInt, margin, speed)
    {
        function isInt(n){ return Number(n) === n && n % 1 === 0; }
        function isjQuery(obj) { return (obj && (obj instanceof jQuery || obj.constructor.prototype.jquery)); }

        var target = targetOrInt;

        if (!isInt(target))
        {
            target = !isjQuery(target) ? jQuery(target).offset().top : target.offset().top;
        }

        margin = typeof margin !== "undefined" ? margin : 0;
        speed = typeof speed !== "undefined" ? speed : 750;

        if (navigator.userAgent.match(/(iPod|iPhone|iPad)/))
        {
            jQuery('body').animate(
            {
                scrollTop: target + margin
            }, speed);
        }
        else
        {
            jQuery('html, body').animate(
            {
                scrollTop: target + margin
            }, speed);
        }
    }

    function handleScrollToContent()
    {
        var scrollBtn = jQuery(".scroll-to-content");

        if (!scrollBtn.length)
            return;

        var content = jQuery(".main-content");

        if (!content.length)
            content = jQuery(".panel-layout");

        if (!content.length)
            return;

        scrollBtn.on("click", function()
        {
            scrollTo(content);
        })
    }

    function handleMageboxNavigation()
    {
        var megaboxes = jQuery(".megabox");

        if (!megaboxes.length)
            return;

        var autoPlay = false;
        var megaboxInterval;

        function toggleAutoPlay(megabox, turn_on)
        {
            turn_on = typeof turn_on !== "undefined" ? turn_on : true;

            if (turn_on)
            {
                clearInterval(megaboxInterval);
                megaboxInterval = setInterval(function()
                {
                    goToNextSlide(megabox, true);
                }, 3000);
            }
            else
            {
                clearInterval(megaboxInterval);
            }
        }

        function getActiveSlide(megabox)
        {
            return jQuery(".featured-posts-container .featured-post.active");
        }

        function getActiveSlideIndex(megabox)
        {
            return getActiveSlide(megabox).index();
        };

        function getSlideByIndex(megabox, index)
        {
            return jQuery(".featured-posts-container .featured-post", megabox).eq(index);
        };

        function updateActiveDotPosition(megabox)
        {
            jQuery(".dots .dot", megabox).removeClass("active")
                .eq(getActiveSlideIndex(megabox)).addClass("active");
        };

        function goToSlide(megabox, index)
        {
            if (getActiveSlideIndex(megabox) === index)
                return;

            var slides = jQuery(".featured-posts-container .featured-post", megabox);

            megabox.trigger( "ikalkulator:beforeMegaboxChange" );
            slides.removeClass("active");
            slides.eq(index).addClass("active");
            updateActiveDotPosition(megabox);
            megabox.trigger( "ikalkulator:afterSlideChange" );
        };

        function goToPrevSlide(megabox, loop)
        {
            loop = typeof loop !== "undefined" ? loop : true;
            var slides = jQuery(".featured-posts-container .featured-post", megabox);

            var activeIndex = getActiveSlideIndex(megabox);

            if (activeIndex >= 1 && getSlideByIndex(megabox, activeIndex - 1).length)
                goToSlide(megabox, activeIndex - 1);
            else
                if (loop)
                    goToSlide(megabox, slides.length - 1);
        };

        function goToNextSlide(megabox, loop)
        {
            loop = typeof loop !== "undefined" ? loop : true;
            var activeIndex = getActiveSlideIndex(megabox);

            if (getSlideByIndex(megabox, activeIndex + 1).length)
                goToSlide(megabox, activeIndex + 1);
            else
                if (loop)
                    goToSlide(megabox, 0);
        };

        megaboxes.each(function()
        {
            var megabox = jQuery(this);
            var dots = jQuery(".nav-panel .dot", megabox);
            var prev = jQuery(".nav-panel .prev", megabox);
            var next = jQuery(".nav-panel .next", megabox);

            prev.on("click", function()
            {
                goToPrevSlide(megabox, true);
            });

            next.on("click", function()
            {
                goToNextSlide(megabox, true);
            });

            dots.on("click", function(evnt)
            {
                var dotIndex =  jQuery(this).index();
                goToSlide(megabox, dotIndex);
            });

            if (autoPlay)
            {
                jQuery(".featured-posts-container, .nav-panel", megabox)
                    .on("mouseenter", function()
                    {
                        toggleAutoPlay(megabox, false);
                    })
                    .on("mouseleave", function()
                    {
                        toggleAutoPlay(megabox, true);
                    });

                toggleAutoPlay(megabox, true);
            }
            jQuery(".see-all .see-all-btn",megabox).on("click", function()
            {
                jQuery(".see-all",megabox).toggleClass("active");

            });

        });
    }

    function updatePostFloatingHeaderVisibility(value)
    {
        var floatingHeader = jQuery(".floating-header");

        if (value >= 0)
            floatingHeader.addClass("active");
        else
            floatingHeader.removeClass("active");
    }

    function updateProgressBar(progress, content, contentTop)
    {
        var winHeight = jQuery(window).innerHeight();

        var contentTopPadding = content.offset().top - contentTop;
        var contentHeight = contentTopPadding + content.outerHeight();
        var value = jQuery(window).scrollTop() - contentTop;
        progress.attr("max", contentHeight - winHeight);
        progress.attr("value", value);

        updatePostFloatingHeaderVisibility(value);

        return value;
    }

    function handleNewsletterScrollToResponse()
    {
        var resposeElement = jQuery(".mc4wp-response .mc4wp-alert");
        
        if (!resposeElement.length)
            return;
        
        if (resposeElement.html !== "")
        {
            scrollTo(resposeElement, -80);
        }
    }


    function handleReadingProgress()
    {
        var progress = jQuery(".reading-progress");
        var content = jQuery(".main-content .post-content");

        if (!progress.length || !content.length)
            return;

        var mainContent = content.closest(".main-content");

        var contentTop = mainContent.offset().top;
        updateProgressBar(progress, content, contentTop);

        jQuery(document).on("scroll", function()
        {
            contentTop = mainContent.offset().top;
            var value = jQuery(window).scrollTop() - contentTop;
            progress.attr("value",  value);
            updatePostFloatingHeaderVisibility(value);
        });

        jQuery(window).on("resize", function()
        {
            contentTop = mainContent.offset().top;
            updateProgressBar(progress, content, contentTop);
        });
    }
    
    function handleMobileSwapOrder()
    {
        var containers = jQuery(".mobile-swap");
        
        containers.each(function()
        {
            var current = jQuery(this);
            var elements = jQuery("> *", current);
            
            
            if (window.innerWidth < 768)
            {
                if (!current.hasClass("swapped"))
                {
                    elements.first().detach().insertAfter(elements.last());
                    current.addClass("swapped");
                }                
            }
            else
            {
                 if (current.hasClass("swapped"))
                {
                    elements.first().detach().insertAfter(elements.last());
                    current.removeClass("swapped");
                }                 
            }
        });
    }


    function disableImagesDragging()
    {
        jQuery(".post-list").on("mousedown", "img", function(event) { event.preventDefault(); });
    }
    
    function onResize()
    {	
        var isResizing  = false;

        jQuery(window).on("resize", function () 
        {
            if (isResizing) { return; }		
            isResizing = true;

            window.setTimeout(function()
            {
                //place functions here
                handleMobileSwapOrder();
                handleCategoryWithSidebarMinHeight();

                isResizing = false;
            }, 50 );
        });	
               
		jQuery(window).trigger("resize");
    }



    /* _________________________________________________________________________ public methods */

	var init = function()
	{
        handleMenu();
        handleMobileMenu();
        handleMageboxNavigation();
        handleScrollToContent();
        handleReadingProgress();
        handleSpotlightSearch();
        handleShareLinks();
        handleProductsGrid();
        handleAnalytics();
        handleListNumbering();
        handleCategoryWithSidebarMinHeight();
        
        checkElementsVisibilityOnScroll(".scroll-handle");
        disableImagesDragging();
        handleNewsletterScrollToResponse();

		onResize();
		jQuery(window).trigger("scroll");

	};

	init();
};

var ikalkulator;

jQuery(document).ready(function($)
{
	ikalkulator = new ikalkulator();
	product_filters = new productFilter();
	companies = new companies();
    searchbox = new searchbox();
    promobox = new promobox();
    comments = new comments();
});

comments = function ()
{
	var applySwitcher = function()
	{
		jQuery(".comments").each( function ()
		{
			var commentBox = jQuery(this);

			jQuery(".show-action button", commentBox).on("click", function()
			{
				jQuery(".comments-content", commentBox).stop().slideToggle();
				commentBox.toggleClass("visible");
			})
		})

	}

	var scrollToComments = function()
	{
		if(location.href.indexOf("replytocom=") > -1 || location.href.indexOf("#comment") > -1)
		{
			jQuery(".comments .comments-content").show();
			jQuery(".comments").addClass("visible");

			if(location.hash)
			{
				if(jQuery(location.hash).length)
				{
					jQuery("html,body").animate({ scrollTop: jQuery(location.hash).offset().top - 100})
				}
			}
		}
	}

	var handleDoRateRelease = function()
	{
		jQuery("#doRate").on("click", function() {
			jQuery(".comments .comments-content").show();
			jQuery(".comments").addClass("visible");
			jQuery("html,body").animate({ scrollTop: jQuery("#respond").offset().top - 100})
		});
	}

	var drawStars = function()
	{
		jQuery(".star-rating").each( function() {
			if(jQuery(this).data("rating"))
			{
				var rating = parseFloat(jQuery(this).data("rating"));
				var fulls = Math.floor(rating);
				for(var i = 0; i < fulls;i ++)
				{
					jQuery(this).find(".stars").append('<span class="full"></span>');
				}
				if(fulls < 5)
				{
					var fract = Math.round((rating - fulls) * 100);
					if(fract > 0) jQuery(this).find(".stars").append('<span class="part"><span style="width: ' + (fract+10) + '%"></span></span>');
					else jQuery(this).find(".stars").append('<span class="empty"></span>');
					fulls++;
					while(fulls < 5)
					{
						jQuery(this).find(".stars").append('<span class="empty"></span>');
						fulls++;
					}
				}
			}
		});
	}

	var handleRateInForm = function()
	{
		jQuery(".star-rating.selectable").each( function()
		{
			var rateContainer = jQuery(this);
			jQuery(".stars > span", rateContainer).each( function() {
				jQuery(this).on("mouseenter", function() {
					var selectedIndex = jQuery(this).index();
					jQuery(".stars > span",rateContainer).each( function() {
						if(jQuery(this).index() <= selectedIndex)
							jQuery(this).removeClass().addClass("full");
						else
							jQuery(this).removeClass().addClass("empty");
					});
				});

				jQuery(this).on("click", function() {
					var selectedIndex = jQuery(this).index();
					rateContainer.data('rating', selectedIndex);
					jQuery('input[name=rating]', rateContainer).val(selectedIndex);
				});

			});

			rateContainer.on("mouseleave", function() {
				var selectedIndex = jQuery(this).data('rating');
				jQuery(".stars > span",rateContainer).each( function() {
					if(jQuery(this).index() <= selectedIndex)
						jQuery(this).removeClass().addClass("full");
					else
						jQuery(this).removeClass().addClass("empty");
				});
			});


		})
	}

	var handleThumbs = function()
	{
		jQuery(".thumbs > button").on("click", function()
		{
			if(!jQuery(this).hasClass("locked-active") && !jQuery(this).hasClass("locked-inactive"))
			{
				jQuery(this).parent().find("button").fadeOut( function() {
					jQuery(this).fadeIn("fast");
				})
				jQuery(this).parent().find("button").addClass("locked-inactive")
				jQuery(this).addClass("active").removeClass("locked-inactive").addClass("locked-active")

				var dir = "down"
				if(jQuery(this).hasClass("up")) dir = "up";
				var postId = jQuery(this).closest(".thumbvote").data("postid");
				jQuery.post("/wp-json/ikal/v1/post/vote", {"post": postId, "dir": dir});
			}
		})
	}


	/* _________________________________________________________________________ public methods */

	var init = function()
	{
		handleDoRateRelease();
		scrollToComments();
		applySwitcher();
		drawStars();
		handleRateInForm();
		handleThumbs();
	};

	init();
};

var comments;

companies = function ()
{   
	function handleFixedFilter()
	{
        var pageHead =  jQuery(".page-head");
        var pageContent =  jQuery(".page-content");
        var filter =  jQuery(".filters");
        
        if (!filter.length)
            return;

		var switchAt = pageHead.offset().top + pageHead.outerHeight();

    
        if (switchAt < jQuery(window).scrollTop() && window.innerWidth >= 768)
        {
            if (!filter.hasClass("fixed"))
            {
                var diff = filter.outerHeight() -  (switchAt - filter.offset().top);
                pageContent
                    .css({
                        "padding-top" : diff + "px"
                    });
            }

            filter
                .addClass("fixed");
        }
        else
        {
            filter.removeClass("fixed");
            pageContent
                .css({
                    "padding-top" : ""
                });            
        }
    }    
    
    
    function filterGrid()
    {
        var filterItems = jQuery(".companies .filters .items-list .category");
        var gridItems = jQuery(".companies .tiles-grid .grid-item");
                
        if (!filterItems.length || !gridItems.length) { return; }
                
        var selectedCategories = [];
        
        filterItems.each(function()
        {
            currentItem = jQuery(this);            
            
            if (currentItem.hasClass("active"))
            {
                if (currentItem.attr("data-filter") == "all")
                {
                    selectedCategories = "all";
                    return false;
                }
                else
                {
                    selectedCategories.push(currentItem.attr("data-filter"));
                }
            }
        });
        
        if (selectedCategories == "all")
        {
            gridItems.addClass("active");
        }
        else
        {
            gridItems.each(function()
            {
                currentItem = jQuery(this);
                var categories = currentItem.attr("data-item-categories");

                var itemCategories = categories.split(",");
                
                var containAtLeastOneFilter = selectedCategories.some(function (v) { return itemCategories.indexOf(v) >= 0;} );

                if (containAtLeastOneFilter)
                    currentItem.addClass("active");
                else
                    currentItem.removeClass("active");

            });
        }
    }  
    
    
    
    function handleFilter(isConditionOR)
    {
        isConditionOR = typeof isConditionOR !== "undefined" ? isConditionOR : false;
        
        var filterItems = jQuery(".companies .filters .items-list .category");
        
        if (!filterItems.length) { return; }
        
        filterItems.on("click", function(e)
        {
            currentItem = jQuery(this);
            cirrentItemFilter = currentItem.attr("data-filter");        
                  
            if (cirrentItemFilter == "all")
            {
                if (currentItem.hasClass("active")) 
                { 
                    return;
                }
                else
                {
                    filterItems.removeClass("active");
                    currentItem.addClass("active");
                }
            }
            else
            {
                if (isConditionOR) //only last selected
                    filterItems.removeClass("active");
                else //all selected excepted "all"
                    filterItems.filter('[data-filter="all"]').removeClass("active"); 
                
                if (currentItem.hasClass("active"))
                {
                    if (filterItems.filter(".active").length > 1)
                    {
                        currentItem.removeClass("active");
                    }
                }
                else
                {
                    currentItem.addClass("active");                    
                }
            }
            
            filterGrid();
        });
        
        filterGrid();
    }    
    
/*
	function onScroll()
	{	
		var isScrolling  = false;

		jQuery(window).on("scroll", function () 
		{
			if (isScrolling) { return; }

			isScrolling = true;

			window.setTimeout(function()
			{
				//place functions here	
                handleFixedFilter();

				isScrolling = false;
			}, 10 );
		});
        
        jQuery(window).trigger("scroll");        
	}	
   
    
    function onResize()
    {	
        var isResizing  = false;

        jQuery(window).on("resize", function () 
        {
            if (isResizing) { return; }		
            isResizing = true;

            window.setTimeout(function()
            {
                //place functions here	
                handleFixedFilter();

                isResizing = false;
            }, 10 );
        });	
               
		jQuery(window).trigger("resize");
    }    
*/

	var init = function()
	{
        if (!jQuery(".page-main.companies").length) { return; }
        
		handleFilter(true);
        onScroll();
        onResize();

	};

	init();
};

var companies;


productFilter = function ()
{
	var sortAttribute = null;
	var sortType = null;
	var sortDirection = null;

	var applySliders = function()
	{
		jQuery(".slider[data-filter-name]").each( function()
		{
			var options = {};
			var sliderContainer = jQuery(this);

			if(jQuery(this).data("filter-type") == "between")
			{
				options.range = true;
				options.values = [];

				if(typeof(jQuery(this).data("value-min")) != "undefined" && jQuery(this).data("value-min") >= 0 && jQuery(this).data("value-max"))
				{
					options.values.push(jQuery(this).data("value-min"));
					options.values.push(jQuery(this).data("value-max"));
				};
			}
			else if(jQuery(this).data("filter-type") == "to")
			{
				options.range = "min";

				if(jQuery(this).data("value"))
				{
					options.value = jQuery(this).data("value");
				};
			};

			if(jQuery(this).data("min") != null)
			{
				options.min = jQuery(this).data("min");
			};

			if(jQuery(this).data("max"))
			{
				options.max = jQuery(this).data("max");
			};



			var pointsValues = [];

			if(jQuery(this).data("step"))
			{

				var stepVal = jQuery(this).data("step");
				var length = Math.ceil((options.max-options.min)/stepVal);
				var start = Math.floor(options.min/stepVal);
				for(var i = start; i <length+start; i++)
				{
					if(i == start && options.min < i*stepVal)
					{
						pointsValues.push(options.min);
					}
					pointsValues.push(i*stepVal)
					if(i == length+start-1 && options.max > i*stepVal)
					{
						pointsValues.push(options.max);
					}
				}


				if(pointsValues.length)
				{
					options.min = 0;
					options.max = pointsValues.length-1;

					if(options.value)
					{
						options.value = pointsValues.indexOf(options.value);
					}

					if(options.values)
					{
						options.values[0] = pointsValues.indexOf(options.values[0]);
						options.values[1] = pointsValues.indexOf(options.values[1]);
					}
				}

			}


			if(jQuery(this).data("progressive") == true)
			{
				var rangesConfig =
				{
					default:
					[
						{"min": 0, "max": 900, "step": 100},
						{"min": 1000, "max": 9000, "step": 1000},
						{"min": 10000, "max": 48000, "step": 2000},
						{"min": 50000, "max": 50000000, "step": 5000},
					],
					investments: [
						{"min": 0, "max": 5000, "step": 500},
						{"min": 6000, "max": 19000, "step": 1000},
						{"min": 20000, "max": 95000, "step": 5000},
						{"min": 100000, "max": 475000, "step": 25000},
						{"min": 500000, "max": 10000000, "step": 50000},
					],
					investmentsMonths:
					[
						{"min": 0, "max": 23, "step": 1},
						{"min": 24, "max": 600, "step": 12}
					]
				};

				var ranges = rangesConfig.default;

				if(jQuery(this).data("progressive-values") && rangesConfig[jQuery(this).data("progressive-values")])
					ranges = rangesConfig[jQuery(this).data("progressive-values")];

				for(i=0; i < ranges.length; i++)
				{
					var current = 0;
					if(options.min > ranges[i].max || options.max < ranges[i].min)
					{
						continue;
					}
					else
					{
						current = ranges[i].min;
						while(current < options.min)
						{
							current += ranges[i].step;
						}

						if(options.min > ranges[i].min && current-ranges[i].step < options.min)
						{
							pointsValues.push(options.min);
						}

						while(current <= options.max && current <= ranges[i].max)
						{
							pointsValues.push(current);
							current += ranges[i].step;
						}
					}
				}


				if(pointsValues.length)
				{
					options.min = 0;
					options.max = pointsValues.length-1;

					if(options.value)
					{
						options.value = pointsValues.indexOf(options.value);
					}

					if(options.values)
					{
						options.values[0] = pointsValues.indexOf(options.values[0]);
						options.values[1] = pointsValues.indexOf(options.values[1]);
					}
				}


			}

			options.slide = function( event, ui )
			{
				if(ui.value || pointsValues[ui.value])
				{
					var exVal = ui.value;

					if(pointsValues.length)
					{
						exVal = pointsValues[ui.value];
					}

					sliderContainer.find(".value").html( formatNumber(exVal) );
					sliderContainer.parents("form").first().find("input[name='" + sliderContainer.data("filter-name") + "']").val(exVal)
				}
				if(ui.values)
				{
					var exValMin = ui.values[0];
					var exValMax = ui.values[1];

					if(pointsValues.length)
					{
						exValMin = pointsValues[ui.values[0]];
						exValMax = pointsValues[ui.values[1]];
					}

					sliderContainer.find(".value").html( formatNumber(exValMin) + " - " + formatNumber(exValMax) );
					sliderContainer.parents("form").first().find("input[name='min-" + sliderContainer.data("filter-name") + "']").val(exValMin)
					sliderContainer.parents("form").first().find("input[name='max-" + sliderContainer.data("filter-name") + "']").val(exValMax)
				}
			}

			options.change = function( event, ui )
			{
				sliderContainer.parents("form").first().trigger("ikal:filter:changed");
			}

			if(pointsValues)
			{
				if(pointsValues[options.min])
					sliderContainer.find(".range-min-value").html(pointsValues[options.min]);
				else
					sliderContainer.find(".range-min-value").html(pointsValues[(options.min + 1)] );

				sliderContainer.find(".range-max-value").html(pointsValues[options.max]);

			}
			else
			{
				sliderContainer.find(".range-min-value").html(options.min);
				sliderContainer.find(".range-max-value").html(options.max);

			}
			sliderContainer.find(".slider-bar").slider(options);
		});

	}

	var applyInputs = function()
	{
		jQuery(".filters form").each( function()
		{
			var filterForm = jQuery(this);
			jQuery("input[type=checkbox], input[type=radio], input[type=text]", filterForm).on("change", function() {
				filterForm.trigger("ikal:filter:changed");
			})
		});
	}

	var prepareForms = function()
	{
		jQuery(".filters form").each( function()
		{
			var filterForm = jQuery(this);

			filterForm.on("ikal:filter:changed", function()
			{
				var list = filterForm.parents(".product-list").first().find(".products.list");
				list.addClass("loading");
				if(jQuery("#page").data("product-slug"))
				{
					var url = "/wp-content/themes/ikalkulator/product-api/products.php?ptype=" + jQuery("#page").data("product-slug") + "&" + filterForm.serialize();
					jQuery.get(url, function(response)
					{
						list.first().html(response);
						refreshSorting(list);
						list.removeClass("loading");
					})
				}
			});
		});
	}

	var applySorting = function()
	{
		jQuery(".products.list").each( function()
		{
			var list = jQuery(this);
			var header = list.parent().find(".products.header");
			jQuery("ul li div.sortable a", header)
                .append('<span class="arrow"></span>')
                .on("click", jQuery.proxy(sortList, this, list));
		})
	}

	var sortList = function(list, event)
	{
		event.preventDefault();

		var sortby = jQuery(event.target).data("sort-by");
		var sorttyp = jQuery(event.target).data("sort-type");
		var sortdir = jQuery(event.target).data("sort-dir");

		if(sortAttribute == sortby)
		{
			if(sortDirection != "desc")
				sortDirection = "desc";
			else
				sortDirection = "asc";
		}
		else
		{
			if(sortdir == "desc")
				sortDirection = "desc";
			else
				sortDirection = "asc";
		}

		jQuery("div.sortable").removeClass("desc asc");

        jQuery(event.target).closest("div.sortable")
            .addClass(sortDirection);

		sortType = sorttyp;
		sortAttribute = sortby;

		jQuery(".sorted-field").removeClass("sorted-field");
		jQuery("." + sortAttribute).addClass("sorted-field");

		refreshSorting(list);
	}

	var refreshSorting = function(list)
	{
		list.find(" > ul > li").sort(jQuery.proxy(sorter, this, sortAttribute, sortType, sortDirection)).appendTo(list.find(" > ul"));
	}

	var sorter = function(sortby, sorttype, sortdir, a, b)
	{
		if(sorttype == "number")
		{
			if(sortdir == "asc") return (parseFloat(jQuery(b).closest("li").data(sortby)) < parseFloat(jQuery(a).closest("li").data(sortby))) ? 1 : -1;
			else return (parseFloat(jQuery(a).closest("li").data(sortby)) < parseFloat(jQuery(b).closest("li").data(sortby))) ? 1 : -1;
		}
	}

	var handleDetails = function()
	{
		jQuery(document).on("click", ".product-list .products.list ul li .more .expander", function()
		{
			jQuery(this).toggleClass("unfolded")
			jQuery(this).parent().find(".expanded").slideToggle();
		})
	}

	var handleOptional = function()
	{
		jQuery(".filters form .show-all").each( function()
		{
				var moveDownHeader = function()
				{
					if(jQuery(".container.products.header").hasClass("fixed"))
					{
						jQuery(".container.products.header").css("top", jQuery(".product-list .filters.container").outerHeight());
					}
				}
				var showAllBtn = jQuery(this);
				jQuery(showAllBtn).on("click", function() {
				showAllBtn.toggleClass("active");
				setInterval( moveDownHeader, 20);
				showAllBtn.closest("form").find(".togglable").slideToggle( function() {
					clearInterval( moveDownHeader );
					jQuery(window).trigger("scroll touchmove");
				});
			})
		});
	}

	var handleTooltip = function()
	{
		if(jQuery(".tooltip").length)
		{
			jQuery(window).on("click", function() {
				jQuery(".tooltip").css("z-index", "").find(".content").stop().slideUp();
			});

			jQuery(".tooltip .show").on("click", function(e)
			{
				e.stopPropagation();
				jQuery(".products .tooltip").css("z-index", "").find(".content").stop().slideUp();
				jQuery(this).closest(".tooltip").css("z-index", 19).find(".content").stop().slideToggle();
				if(jQuery(this).closest(".tooltip").find(".content").outerWidth() + jQuery(this).closest(".tooltip").find(".content").offset().left > jQuery(window).width())
				{
					jQuery(this).closest(".tooltip").find(".content").css("left", -jQuery(this).closest(".tooltip").find(".content").outerWidth()/2 + 40);
				}
			})

		}
	}

	var applyFixedFilter = function()
	{
        var filter =  jQuery(".product-list .filters");
        var productHeader = jQuery(".product-list .products.header");
        var productList = jQuery(".product-list .products.list");
        
        if (!filter.length && !productHeader.length)
            return;

		var margin = filter.offset().top - productList.offset().top;

		if(jQuery(window).width() < 768)
		{
			margin = -productHeader.height();
		}

		var execOnScroll = function()
		{
			if (productList.offset().top + margin <= jQuery(window).scrollTop() && jQuery(window).width() >= 768)
			{
				productList
					.css({
						"margin-top" : (filter.outerHeight(true) + productHeader.outerHeight(true)) + "px"
					});

				filter
					.addClass("fixed");

				productHeader
					.css({
						"top" : filter.outerHeight() + "px"
					})
					.addClass("fixed");

			}
			else
			{
				filter.removeClass("fixed");

				productHeader
					.css({
						"top" : ""
					})
					.removeClass("fixed");

				productList
					.css({
						"margin-top" : ""
					});

			}
		}
        
        jQuery(window).on("scroll", function() {
	        clearTimeout(execOnScroll);
	        setTimeout(execOnScroll, 50)
        });

		jQuery(window).on("touchmove", execOnScroll);
    }

	/* _________________________________________________________________________ public methods */

	var init = function()
	{
		applyInputs();
		applySliders();
		applySorting();
		applyFixedFilter();
		prepareForms();
		handleDetails();
		handleOptional();
		handleTooltip();
	};

	init();
};

var productFilter;

formatNumber = function (num) {
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")
}
promobox = function ()
{
    var draggableList,
        postsList,
        promoboxElement,
        info,
        filters,
        filtersPos = null,
        filtersStartPos = null,
        listWidth,
        distansceFromRight;
        dragging = false;
        itemWidth = null, seeAllItemWidth = null;
    var pep;
    
    function getPostsListWidth(list)
    {
        var width = 0;

        jQuery(".post-item:not(.hide):not(.all-promotions)", list).each(function()
        {
//            width += jQuery(this).outerWidth(true);
            width += itemWidth;///jQuery(this).outerWidth(); //333
        });
        
        width += seeAllItemWidth;
        width += parseFloat(list.css("padding-left")) + parseFloat(list.css("padding-right"));        
        
        return width;
    }  
    
    function updateVars()
    {
        draggableList = jQuery(".draggable-list");
        postsList = jQuery(".posts-list-content", draggableList);

        if (!postsList.length)
            return;   

        promoboxElement = draggableList.closest(".promobox");
        info = jQuery(".info", promoboxElement);
        
        if (filtersPos === null)
        {
            filters = jQuery(".filters", draggableList);  
            filtersPos = filters.position();
        }
        
        if (filtersStartPos === null)
        {
            filtersStartPos = filters.position().left;
        }
        if (itemWidth === null)
        {
            itemWidth = jQuery(".post-item", postsList).not(".all-promotions").eq(0).outerWidth();
            seeAllItemWidth = jQuery(".post-item.all-promotions", postsList).eq(0).outerWidth();
        }
                
        listWidth = getPostsListWidth(postsList);
        postsList.outerWidth(listWidth);
        distansceFromRight = (jQuery(window).innerWidth() -  (draggableList.offset().left + draggableList.outerWidth()));
    }      
    
    function updateGallery()
    {  
        if  (typeof pep !== "undefined")
        {
            jQuery.pep.unbind( pep ); 
        }

        if(!draggableList || !draggableList.length)
            return;
    
        updateVars();

        if (listWidth >=  draggableList.offset().left + draggableList.outerWidth())
        {
            constrainLeft = -listWidth + draggableList.outerWidth() + distansceFromRight;
        }
        else
        if (listWidth >= draggableList.outerWidth())
        {
            constrainLeft = -listWidth + draggableList.outerWidth();
        }
        else
//        if (listWidth < draggableList.outerWidth())
        {
            constrainLeft = 0;
        }

        if(jQuery(window).width() < 1024)
        {
            var spd = false;
        }
        else
        {
            var spd = true;
        }
                
        pep = postsList.pep({
            axis: "x",
            drag: drag,
            easing: moving,
            start: startDrag,
            revert: false,
//          revertIf:   function(){ return false; },
            constrainTo: [0, 0, 0, constrainLeft],
            shouldPreventDefault: spd
          });
       
        
        info.css({
            "opacity" : "",
            "transform" : ""
        });    

        postsList.css({
            "left" : "0",
            "transform": "matrix(1, 0, 0, 1, 0, 0)"
        });           
          
/*      
        filters.css({
            "left" : filtersStartPos + "px"
        }); 
*/        
    }   
    
    function startDrag(e, obj)
    {
//            info = jQuery(".info", promoboxElement);
//            listWidth = getPostsListWidth(postsList);
//            postsList.width(listWidth);
    }
    
    function drag(e, obj)
    {
        dragging = true;
        moving(e, obj);
    }

    //drag or easing
    function moving(e, obj)
    {
        var drag = jQuery(obj.el);
        var infoWidth = info.outerWidth();
        var dragPos = drag.position().left;
        var infoDistance = Math.min(Math.abs(dragPos), infoWidth);

        if (jQuery(window).innerWidth() >= 1024)
        {
            info.css({
                "opacity" : 1- infoDistance/infoWidth,
                "transform" : "scale(" + (1 - infoDistance/infoWidth/2) + ")"
            });
        }
        else
        {
            info.css({
                "opacity" : "",
                "transform" : ""
            });
        }
        
        filters.css({
            "left" : Math.max((filtersPos.left - Math.abs(dragPos)), 0) + "px"
        });
    }
    

    function handleDraggablePostsList()
    {
        dragging = false;
        updateVars();
        
        if (!postsList.length)
            return;  

        updateGallery();
        
        jQuery(window).on("resize", function()
        {
            updateGallery();
        });

        //click when click, drag when drag
        jQuery("a", postsList)
            .on("mousedown pointerdown touchstart",function(e) {
                dragging = false;
            })
            .on("click touch",function(e)
            {
                if (dragging)
                {
                    e.preventDefault();
                }                
            })
            ;
    }

	var applyFilter = function()
	{
		jQuery(".promobox").each( function ()
		{
			var promobox = jQuery(this);
			jQuery(".filters .value", promobox).on("click touch touchend", function(e)
			{
                e.preventDefault();
                e.stopPropagation();
                jQuery(".filters ul", promobox).stop().fadeIn();
			});

			jQuery(".filters ul li", promobox).on("click touch touchend", function(e)
			{
                e.preventDefault();
                e.stopPropagation();
                jQuery(this).toggleClass("active");
                window.location.hash = "#filtruj-" + jQuery(this).html();
				applyFilterToContent(promobox);
			});

            //jQuery(document).on("click touch touchend", function(e)
            //{
            //    e.stopPropagation();
            //})

            var hideFilter = function()
            {
                jQuery(".filters ul", promobox).stop().fadeOut();

                if (typeof filters !== "undefined" && parseInt(postsList.css("left")) == 0)
                {
                    filters.css({
                        "left" : filtersStartPos + "px"
                    });
                }
            }

			jQuery(".filters ul", promobox).on("mouseleave", hideFilter);
            jQuery(document).on("click touchend", hideFilter)
        });
	}

	var applyFilterToContent = function(promobox)
	{
		var cats = [];
        var postItemsLength = jQuery(".post-item", promobox).length;

		jQuery(".filters ul li", promobox).each( function() {
			if(jQuery(this).hasClass("active"))
				cats.push(jQuery(this).attr("data-type"));
		});

		if(!cats.length)
        {
			jQuery(".post-item", promobox).removeClass( "hide" );
            updateGallery();
        }
		else
		jQuery(".post-item", promobox).each( function(index, element)
        {
            var currentItem = jQuery(this);
            if (!currentItem.hasClass("all-promotions"))
            {
                var postCats = currentItem.attr("data-type").toString().split(",");
                var disablePost = true;
                for(i in cats)
                {
                    if(postCats.indexOf(cats[i].toString()) > -1)
                        disablePost = false;
                }
                if(disablePost) currentItem.addClass( "hide" );
                else jQuery(this).removeClass( "hide" );
            }
            
            if (index == postItemsLength -1)
            {
                updateGallery();
            }            
		});
//        updateVars();
//            updateGallery();
	}

	/* _________________________________________________________________________ public methods */

	var init = function()
	{
        updateGallery();
		applyFilter();
        handleDraggablePostsList();
	};

	init();
};

var promobox;

searchbox = function ()
{
	var currentPath = "";
	var currentLevel = 1;
    
    // get closest tu num number from array
    function closest(num, arr)
    {
        var curr = arr[0];
        var index = 0;
        var diff = Math.abs(num - curr);
        for (var val = 0; val < arr.length; val++) {
            var newdiff = Math.abs(num - arr[val]);
            if (newdiff < diff) {
                diff = newdiff;
                curr = arr[val];
                index = val;
            }
        }
        return index;
    }    

	var applySliders = function()
	{
		jQuery(".searchbox .slider").each( function()
		{
			var options = {};
			var sliderContainer = jQuery(this);

			if(sliderContainer.data("filter-type") == "between")
			{
				options.range = true;
				options.values = [];
				if(sliderContainer.data("value-min") && sliderContainer.data("value-max"))
				{
					options.values.push(sliderContainer.data("value-min"));
					options.values.push(sliderContainer.data("value-max"));
				};
			}
			else if(sliderContainer.data("filter-type") == "to")
			{                
				options.range = "min";
				if(sliderContainer.data("value"))
				{
					options.value = sliderContainer.data("value");
				};
			};
            
			if(sliderContainer.data("min") != null)
			{
				options.min = sliderContainer.data("min");
			};

			if(sliderContainer.data("max"))
			{
				options.max = sliderContainer.data("max");
			};
			var pointsValues = [];

			if(sliderContainer.data("step"))
			{

				var stepVal = sliderContainer.data("step");
				var length = Math.ceil((options.max-options.min)/stepVal);
				var start = Math.floor(options.min/stepVal);
                
				for(var i = start; i <length+start; i++)
				{
					if(i == start && options.min < i*stepVal)
					{
						pointsValues.push(options.min);
					}
                    
					pointsValues.push(i*stepVal);
                    
					if(i == length+start-1 && options.max > i*stepVal)
					{
						pointsValues.push(options.max);
					}
				}


				if(pointsValues.length)
				{
					options.min = 0;
					options.max = pointsValues.length-1;

					if(options.value)
					{
						options.value = pointsValues.indexOf(options.value);
					}
				}
			}

			if(sliderContainer.data("progressive") == true)
			{
				var rangesConfig =
				{
					default:
						[
							{"min": 0, "max": 900, "step": 100},
							{"min": 1000, "max": 9000, "step": 1000},
							{"min": 10000, "max": 48000, "step": 2000},
							{"min": 50000, "max": 50000000, "step": 5000},
						],
					investments: [
						{"min": 0, "max": 5000, "step": 500},
						{"min": 6000, "max": 19000, "step": 1000},
						{"min": 20000, "max": 95000, "step": 5000},
						{"min": 100000, "max": 475000, "step": 25000},
						{"min": 500000, "max": 10000000, "step": 50000},
					],
					investmentsMonths:
						[
							{"min": 1, "max": 23, "step": 1},
							{"min": 24, "max": 600, "step": 12}
						]
				};

				var ranges = rangesConfig.default;

				if(sliderContainer.data("progressive-values") && rangesConfig[sliderContainer.data("progressive-values")])
					ranges = rangesConfig[sliderContainer.data("progressive-values")];

				for(i=0; i < ranges.length; i++)
				{
					var current = 0;
					if(options.min > ranges[i].max || options.max < ranges[i].min)
					{
						continue;
					}
					else
					{ 
						current = ranges[i].min;
						while(current < options.min)
						{
							current += ranges[i].step;
						}

						if(options.min > ranges[i].min && current-ranges[i].step < options.min)
						{
							pointsValues.push(options.min);
						}

						while(current <= options.max && current <= ranges[i].max)
						{
							pointsValues.push(current);
							current += ranges[i].step;
						}
					}
				}

				if(pointsValues.length)
				{
					options.min = 0;
					options.max = pointsValues.length-1;
//					options.max = pointsValues[pointsValues.length-1];

					if(options.value)
					{
						options.value = pointsValues.indexOf(options.value);
//						options.value = pointsValues[pointsValues.indexOf(options.value)];
					}
				}
			}

			options.slide = function( event, ui )
			{
				if(ui.value)
				{
					var exVal = ui.value;

					if(pointsValues.length)
					{
						exVal = pointsValues[ui.value];
					}

					sliderContainer.find(".value").html( formatNumber(exVal) );
					sliderContainer.parents("[data-level]").first().find("input").val(exVal);
                    sliderContainer.data("value", exVal);
                }
				if(ui.values)
				{
					sliderContainer.find(".value").html( formatNumber(ui.values[0]) + " - " + formatNumber(ui.values[1]) );
					sliderContainer.parents("[data-level]").first().find("input[name^='min-']").val(ui.values[0]).trigger("change");
					sliderContainer.parents("[data-level]").first().find("input[name^='max-']").val(ui.values[1]).trigger("change");
				}
			}

			options.change = function( event, ui )
			{
				sliderContainer.parents("[data-level]").first().trigger("ikal:filter:changed");
			}


			sliderContainer.closest("[data-level]").find("input").on("change", function()
			{
                var input = jQuery(this);

				if(parseFloat(input.attr("min")) && input.val() < parseFloat(input.attr("min")))
					input.val(parseFloat(input.attr("min")));

				if(parseFloat(input.attr("max")) && input.val() > parseFloat(input.attr("max")))
					input.val(parseFloat(input.attr("max")));
                
                var val = input.val();
                if(pointsValues.length)
                {
                    val = closest(input.val(), pointsValues);
                }
                sliderContainer.find(".slider-bar").slider("value", val);
				sliderContainer.find(".value").html( formatNumber(input.val()) );
				sliderContainer.closest("[data-level]").trigger("ikal:filter:changed");
			});
            
			sliderContainer.find(".slider-bar").slider(options);   
            sliderContainer.closest("[data-level]").find("input").trigger("change");
		});
        

	}

	var applyChoices = function()
	{
		jQuery(".searchbox .choice").each( function()
		{
			var choicer = jQuery(this);
			jQuery(".value", choicer).on("click", function(e)
			{
				jQuery(".topped").stop().fadeOut().removeClass("topped");
				jQuery("ul", choicer).stop().fadeIn().addClass("topped");
			})

			jQuery("li[data-value]", choicer).on("click", function(e)
			{
				e.stopPropagation();
				var optionValue = jQuery(this).data("value");
				var targetPath = jQuery(this).data("target-path");
				var targetLevel = jQuery(this).data("target-level");
				jQuery("ul", choicer).stop().fadeOut().removeClass("topped");
				jQuery(".value > span", choicer).html(jQuery(this).html());

				choicer.first().find("input").val(optionValue)
				setPath(targetPath, targetLevel);
			});
		});
	}

	var applyTooltips = function()
	{
		jQuery(".tooltip-toggler").each( function() {
			var toggler = jQuery(this);
			jQuery(".do-toggle", toggler).on("click", function(e)
			{
				jQuery(".topped").stop().fadeOut().removeClass("topped");
				jQuery(".tooltip", toggler).fadeIn().addClass("topped");

				toggler.parents("[data-level]").first().one("ikal:filter:changed", function()
				{
					jQuery(".tooltip", toggler).fadeOut().removeClass("topped");
				});
			});
		});
	}

	var handleSubmit = function()
	{
		jQuery(".submit-search").on("click", function()
		{
			var pathParts = getPathParts();
			var queryParams = {};
			for(var i = 0; i<currentLevel; i++)
			{
				jQuery("[data-level='" + parseInt(i+1) +"']").each( function()
				{
					var lvl = jQuery(this).data("level");
					if(lvl == i+1 && jQuery(this).data("path").indexOf(pathParts[i]) == 0)
					{
						var currEl = jQuery(this);
						jQuery("input", currEl).each( function()
						{
							if(jQuery(this).data("prop-name"))
								queryParams[jQuery(this).data("prop-name")] = jQuery(this).val();
						});
					}
				});
			}
			var url = "";
			if(pathParts[1] == "quickloans") url = "/finansowanie/chwilowki/";
			if(pathParts[1] == "loans") url = "/finansowanie/pozyczki-na-raty/";
			if(pathParts[1] == "credits") url = "/finansowanie/kredyty-gotowkowe/";
			if(pathParts[1] == "investments") url = "/lokaty/";

			if(pathParts[1] == "accounts")
			{
				if(jQuery("input[name='accounts.type']").val() == "personal") url = "/konta-bankowe/konta-osobiste/";
				if(jQuery("input[name='accounts.type']").val() == "business") url = "/konta-bankowe/konta-firmowe/";
				if(jQuery("input[name='accounts.type']").val() == "currency") url = "/konta-bankowe/konta-walutowe/";
				if(jQuery("input[name='accounts.type']").val() == "saving") url = "/konta-bankowe/konta-oszczednosciowe/";
			}
			url += "?";

//			console.log(queryParams);
			jQuery.each(queryParams,  function(k,v)
			{
				url += k + "=" + v + "&";
			})

			location.href = url;

		})
	}

	var handleScrollDown = function()
	{
		jQuery(".searchbox .cta a").on("click", function(e)
		{
			e.preventDefault();
			window.location.hash = jQuery(this).attr("href");
			jQuery("body,html").animate({ scrollTop: jQuery(".main-content.page-content").offset().top });
		})
	}

	var handleZindex = function()
	{
		jQuery(".searchbox [data-level]").on("click", function(e)
		{
			e.stopPropagation();

			jQuery(".searchbox [data-level]").css("z-index", "");
			jQuery(this).css("z-index", 20);
		})
	}


	var setPath = function(targetPath, targetLevel)
	{
		if(typeof(targetPath) != "undefined" && typeof(targetLevel) != "undefined")
		{
			currentPath = targetPath;
			currentLevel = targetLevel;

			var pathParts = getPathParts();

			for(var i = 0; i<currentLevel; i++)
			{
				jQuery("[data-level='" + parseInt(i+1) +"']").each( function()
				{
					var lvl = jQuery(this).data("level");
					if(lvl == i+1 && jQuery(this).data("path").indexOf(pathParts[i]) == 0)
					{
						var currEl = jQuery(this);
						setTimeout(function()
						{
							currEl.fadeIn("fast").css("display", "inline-block");
						}, 350)
					}
					else
						jQuery(this).fadeOut(300);

				});
			}

		}
	}

	var getPathParts = function()
	{
		var slicedPath = currentPath.split(".");
		var pathParts = [""];
		var tmpPath = "";

		for(var k in slicedPath)
		{
			if(k == 0) tmpPath = slicedPath[0];
			else tmpPath += "." + slicedPath [k];
			pathParts.push(tmpPath);
		}

		return pathParts;
	}

	var handleClickOutside = function()
	{
		jQuery(window).on("click", function() {
			jQuery(".topped").stop().fadeOut().removeClass("topped");
		});
	}

	/* _________________________________________________________________________ public methods */

	var init = function()
	{
		applyChoices();
		applyTooltips();
		applySliders();
		handleSubmit();
		handleScrollDown();
		handleZindex();
		handleClickOutside();
	};

	init();
};

var searchbox;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYi93cC1jb250ZW50L3RoZW1lcy9pa2Fsa3VsYXRvci9zcmMvanMvYXBwL2Jhc2UuanMiLCJ3ZWIvd3AtY29udGVudC90aGVtZXMvaWthbGt1bGF0b3Ivc3JjL2pzL2FwcC9jb21tZW50cy5qcyIsIndlYi93cC1jb250ZW50L3RoZW1lcy9pa2Fsa3VsYXRvci9zcmMvanMvYXBwL2NvbXBhbmllcy5qcyIsIndlYi93cC1jb250ZW50L3RoZW1lcy9pa2Fsa3VsYXRvci9zcmMvanMvYXBwL3Byb2R1Y3RfbGlzdC5qcyIsIndlYi93cC1jb250ZW50L3RoZW1lcy9pa2Fsa3VsYXRvci9zcmMvanMvYXBwL3Byb21vYm94LmpzIiwid2ViL3dwLWNvbnRlbnQvdGhlbWVzL2lrYWxrdWxhdG9yL3NyYy9qcy9hcHAvc2VhcmNoYm94LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNodUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNqSkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM1TUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDMWNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDelNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaWthbGt1bGF0b3IgPSBmdW5jdGlvbiAoKVxue1xuICAgIGZ1bmN0aW9uIGNhbGN1bGF0ZUNoaWxkcmVuSGVpZ2h0KGNvbnRhaW5lcilcbiAgICB7XHRcbiAgICAgICAgdmFyIHRvcE9mZnNldCA9IGJvdHRvbU9mZnNldCA9IDA7XG5cbiAgICAgICAgY29udGFpbmVyLmNoaWxkcmVuKCkuZWFjaChmdW5jdGlvbihpLCBjaGlsZClcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIGVsZW1lbnQgPSBqUXVlcnkoY2hpbGQpLFxuICAgICAgICAgICAgZVRvcE9mZnNldCA9IGVsZW1lbnQub2Zmc2V0KCkudG9wLFxuICAgICAgICAgICAgZUJvdHRvbU9mZnNldCA9IGVUb3BPZmZzZXQgKyBlbGVtZW50Lm91dGVySGVpZ2h0KCk7XG5cbiAgICAgICAgICAgIGlmIChlVG9wT2Zmc2V0IDwgdG9wT2Zmc2V0KVxuICAgICAgICAgICAgICAgIHRvcE9mZnNldCA9IGVUb3BPZmZzZXQ7XG5cbiAgICAgICAgICAgIGlmIChlQm90dG9tT2Zmc2V0ID4gYm90dG9tT2Zmc2V0KVxuICAgICAgICAgICAgICAgIGJvdHRvbU9mZnNldCA9IGVCb3R0b21PZmZzZXQ7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiAgYm90dG9tT2Zmc2V0IC0gdG9wT2Zmc2V0IC0gY29udGFpbmVyLm9mZnNldCgpLnRvcCArIHBhcnNlSW50KGNvbnRhaW5lci5jc3MoXCJwYWRkaW5nLWJvdHRvbVwiKSk7XG4gICAgfSAgICBcbiAgICBcbiAgICBmdW5jdGlvbiBoYW5kbGVDYXRlZ29yeVdpdGhTaWRlYmFyTWluSGVpZ2h0KClcbiAgICB7XG4gICAgICAgIHZhciBzaWRlYmFyID0galF1ZXJ5KFwiYm9keS5jYXRlZ29yeSBzZWN0aW9uLmNhdGVnb3J5LWNvbnRlbnQgLmFzaWRlLXBhbmVsXCIpO1xuICAgICAgICBpZiAoIXNpZGViYXIubGVuZ3RoKSB7IHJldHVybjsgfVxuICAgICAgICBcbiAgICAgICAgdmFyIGNhdGVnb3J5Q29udGVudCA9IGpRdWVyeShcImJvZHkuY2F0ZWdvcnkgc2VjdGlvbi5jYXRlZ29yeS1jb250ZW50XCIpOyAgICAgICAgXG4gICAgICAgIGNhdGVnb3J5Q29udGVudC5jc3MoXCJtaW4taGVpZ2h0XCIsIGNhbGN1bGF0ZUNoaWxkcmVuSGVpZ2h0KGNhdGVnb3J5Q29udGVudCkpO1xuICAgIH1cbiAgICBcbiAgICBmdW5jdGlvbiBoYW5kbGVMaXN0TnVtYmVyaW5nKClcbiAgICB7XG4gICAgICAgIGpRdWVyeShcIm9sW3N0YXJ0XVwiKS5lYWNoKGZ1bmN0aW9uKClcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIHZhbCA9IHBhcnNlRmxvYXQoalF1ZXJ5KHRoaXMpLmF0dHIoXCJzdGFydFwiKSkgLSAxO1xuICAgICAgICAgICAgY29uc29sZS5sb2codmFsKTtcbiAgICAgICAgICAgIGpRdWVyeSh0aGlzKS5jc3MoXCJjb3VudGVyLWluY3JlbWVudFwiLCBcIm9sLWNvdW50ZXIgXCIgKyB2YWwpO1xuICAgICAgICB9KTsgICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBmdW5jdGlvbiBoYW5kbGVQcm9kdWN0c0dyaWQoKVxuICAgIHtcbiAgICAgICAgdmFyIHByb2R1Y3RHcmlkID0galF1ZXJ5KFwiLnByb2R1Y3QtZ3JpZFwiKTtcbiAgICAgICAgXG4gICAgICAgIGlmICghcHJvZHVjdEdyaWQubGVuZ3RoKVxuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICBcbiAgICAgICAgdmFyIHNob3dNb3JlID0galF1ZXJ5KFwiLnByb2R1Y3RzLXNob3ctbW9yZVwiLCBwcm9kdWN0R3JpZCk7XG4gICAgICAgIFxuICAgICAgICBzaG93TW9yZS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKClcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIGJ0biA9IGpRdWVyeSh0aGlzKTtcbiAgICAgICAgICAgIHZhciBtb3JlUHJvZHVjdHMgPSBqUXVlcnkoXCIucG9zdHMtbGlzdC1jb250ZW50Lm1vcmUtcHJvZHVjdHNcIiwgcHJvZHVjdEdyaWQpO1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZiAobW9yZVByb2R1Y3RzLmhhc0NsYXNzKFwidmlzaWJsZVwiKSlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBqUXVlcnkoXCIuc2hvd1wiLCBidG4pLnNob3coKTsgXG4gICAgICAgICAgICAgICAgalF1ZXJ5KFwiLmhpZGVcIiwgYnRuKS5oaWRlKCk7IFxuICAgICAgICAgICAgICAgIG1vcmVQcm9kdWN0cy5yZW1vdmVDbGFzcyhcInZpc2libGVcIik7XG4gICAgICAgICAgICAgICAgbW9yZVByb2R1Y3RzLnNsaWRlVXAoKTtcbiAgICAgICAgICAgICAgICBzY3JvbGxUbyhqUXVlcnkoXCIucG9zdHMtbGlzdC1jb250ZW50XCIsIHByb2R1Y3RHcmlkKSwgLTMwKTtcbiAgICAgICAgICAgIH0gICAgXG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgalF1ZXJ5KFwiLnNob3dcIiwgYnRuKS5oaWRlKCk7IFxuICAgICAgICAgICAgICAgIGpRdWVyeShcIi5oaWRlXCIsIGJ0bikuc2hvdygpOyBcbiAgICAgICAgICAgICAgICBtb3JlUHJvZHVjdHMuYWRkQ2xhc3MoXCJ2aXNpYmxlXCIpO1xuICAgICAgICAgICAgICAgIG1vcmVQcm9kdWN0cy5zbGlkZURvd24oKTtcbiAgICAgICAgICAgICAgICBzY3JvbGxUbyhqUXVlcnkoXCIucG9zdHMtbGlzdC1jb250ZW50XCIsIHByb2R1Y3RHcmlkKSwgLTMwKTtcbiAgICAgICAgICAgIH0gICBcbiAgICAgICAgfSk7XG4gICAgICAgIFxuICAgIH1cbiAgICBcbiAgICBcblx0ZnVuY3Rpb24gaGFuZGxlU2hhcmVMaW5rcygpXG5cdHtcblx0XHRqUXVlcnkoXCIuc2hhcmUgLnNvY2lhbC1zaGFyZVwiKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKGUpXG4gICAgICAgIHtcblx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcblx0XHRcdHdpbmRvdy5vcGVuKGpRdWVyeSh0aGlzKS5hdHRyKCdocmVmJyksICdmYlNoYXJlV2luZG93JywgJ2hlaWdodD00NTAsIHdpZHRoPTY1MCwgdG9wPScgKyAoalF1ZXJ5KHdpbmRvdykuaGVpZ2h0KCkgLyAyIC0gMjc1KSArICcsIGxlZnQ9JyArIChqUXVlcnkod2luZG93KS53aWR0aCgpIC8gMiAtIDIyNSkgKyAnLCB0b29sYmFyPTAsIGxvY2F0aW9uPTAsIG1lbnViYXI9MCwgZGlyZWN0b3JpZXM9MCwgc2Nyb2xsYmFycz0wJyk7XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fSk7XG5cdH1cbiAgICBcbiAgICBmdW5jdGlvbiBoYW5kbGVTcG90bGlnaHRTZWFyY2goKVxuICAgIHtcbiAgICAgICAgdmFyIHNlYXJjaEJ0biA9IGpRdWVyeShcIi5zaXRlLWhlYWRlciAuc2VhcmNoLWJ0biBhXCIpO1xuICAgICAgICB2YXIgc2VhcmNoQmFyID0galF1ZXJ5KFwiLnNlYXJjaC1iYXIuc3BvdGxpZ2h0LXNlYXJjaFwiKTtcbiAgICAgICAgXG4gICAgICAgIGlmICghc2VhcmNoQnRuLmxlbmd0aCB8fCAhc2VhcmNoQnRuLmxlbmd0aClcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgXG4gICAgICAgIGZ1bmN0aW9uIGhpZGVCYXIoKVxuICAgICAgICB7XG4gICAgICAgICAgICBzZWFyY2hCYXIucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIik7XG4gICAgICAgICAgICBqUXVlcnkoXCJpbnB1dFwiLCBzZWFyY2hCYXIpLmJsdXIoKS5hdHRyKFwicmVhZG9ubHlcIiwgdHJ1ZSk7XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGpRdWVyeShcImlucHV0XCIsIHNlYXJjaEJhcikuYXR0cihcInJlYWRvbmx5XCIsIHRydWUpO1xuICAgICAgICBcbiAgICAgICAgc2VhcmNoQnRuLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oZSlcbiAgICAgICAge1xuICAgICAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTsgICAgICAgICAgICBcbiAgICAgICAgICAgIHNlYXJjaEJhci50b2dnbGVDbGFzcyhcImFjdGl2ZVwiKTsgIFxuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZiAoc2VhcmNoQmFyLmhhc0NsYXNzKFwiYWN0aXZlXCIpKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGpRdWVyeShcImlucHV0XCIsIHNlYXJjaEJhcikuYXR0cihcInJlYWRvbmx5XCIsIGZhbHNlKTsgXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgd2luZG93LnNldFRpbWVvdXQoZnVuY3Rpb24oKVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgalF1ZXJ5KFwiaW5wdXRbdHlwZT10ZXh0XVwiLCBzZWFyY2hCYXIpLmZvY3VzKCk7XG4gICAgICAgICAgICAgICAgfSAsNTAwKTtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgIGpRdWVyeShcImlucHV0XCIsIHNlYXJjaEJhcikuYXR0cihcInJlYWRvbmx5XCIsIHRydWUpO1xuICAgICAgICB9KTtcblxuICAgICAgICBzZWFyY2hCYXIub24oXCJjbGljayB0b3VjaCB0b3VjaGVuZFwiLCBmdW5jdGlvbihlKVxuICAgICAgICB7XG4gICAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICB9KTsgICAgICAgIFxuICAgICAgICBcbiAgICAgICAgalF1ZXJ5KGRvY3VtZW50KVxuICAgICAgICAgICAgLm9uKFwiY2xpY2sgdG91Y2ggdG91Y2hlbmRcIiwgZnVuY3Rpb24oKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGhpZGVCYXIoKTtcbiAgICAgICAgICAgIH0pICAgICAgICBcbiAgICAgICAgICAgIC5vbihcImtleWRvd25cIiwgZnVuY3Rpb24oZSlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpZiAoIGUua2V5Q29kZSA9PSAyNyApXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBoaWRlQmFyKCk7ICAgIFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNoZWNrRWxlbWVudHNWaXNpYmlsaXR5T25TY3JvbGwoZWxlbWVudHNDbGFzcylcbiAgICB7XG4gICAgICAgIHZhciBlbGVtZW50cyA9IGpRdWVyeShlbGVtZW50c0NsYXNzKTtcblxuICAgICAgICBpZiAoIWVsZW1lbnRzLmxlbmd0aClcbiAgICAgICAgICAgIHJldHVybjtcblxuICAgICAgICBlbGVtZW50c1xuICAgICAgICAgICAgLmF0dHIoXCJkYXRhLW9uc2NyZWVuXCIsIDApO1xuXG4gICAgICAgIHZhciBpc1Njcm9sbGluZyAgPSBmYWxzZTtcblxuICAgICAgICBqUXVlcnkod2luZG93KS5vbihcInNjcm9sbFwiLCBmdW5jdGlvbigpXG4gICAgICAgIHtcblx0XHRcdGlmIChpc1Njcm9sbGluZykgeyByZXR1cm47IH1cblxuICAgICAgICAgICAgdmFyIHdpbkhlaWdodCA9IGpRdWVyeSh3aW5kb3cpLmlubmVySGVpZ2h0KCk7XG4gICAgICAgICAgICB2YXIgc2Nyb2xsVG9wID0gIGpRdWVyeSh3aW5kb3cpLnNjcm9sbFRvcCgpO1xuICAgICAgICAgICAgdmFyIHNjcm9sbEJvdHRvbSA9IHNjcm9sbFRvcCArIHdpbkhlaWdodDtcblxuXHRcdFx0aXNTY3JvbGxpbmcgPSB0cnVlO1xuXG5cdFx0XHR3aW5kb3cuc2V0VGltZW91dChmdW5jdGlvbigpXG5cdFx0XHR7XG4gICAgICAgICAgICAgICAgZWxlbWVudHMuZWFjaChmdW5jdGlvbigpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICB2YXIgZWxlbWVudCA9IGpRdWVyeSh0aGlzKTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGlzVmlzaWJsZSA9IGVsZW1lbnQuYXR0cihcImRhdGEtb25zY3JlZW5cIik7XG4gICAgICAgICAgICAgICAgICAgIHZhciBlbGVtZW50SGVpZ2h0ID0gZWxlbWVudC5vdXRlckhlaWdodCgpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgZWxlbWVudFRvcCA9IGVsZW1lbnQub2Zmc2V0KCkudG9wO1xuICAgICAgICAgICAgICAgICAgICB2YXIgZWxlbWVudEJvdHRvbSA9IGVsZW1lbnRUb3AgKyBlbGVtZW50SGVpZ2h0O1xuXG4gICAgICAgICAgICAgICAgICAgIGlmIChpc1Zpc2libGUgPT0gdHJ1ZSlcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGVsZW1lbnRCb3R0b20gPD0gc2Nyb2xsVG9wIHx8IGVsZW1lbnRUb3AgPj0gc2Nyb2xsQm90dG9tKVxuICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnRyaWdnZXIoXCJpazp2aXNpYmxpdHktY2hhbmdlXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC50cmlnZ2VyKFwiaWs6aGlkZGVuXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5hdHRyKFwiZGF0YS1vbnNjcmVlblwiLCAwKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGVsZW1lbnRCb3R0b20gPiBzY3JvbGxUb3AgJiYgZWxlbWVudFRvcCA8IHNjcm9sbFRvcClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGVsZW1lbnRUb3AgPCBzY3JvbGxCb3R0b20gJiYgZWxlbWVudEJvdHRvbSA+IHNjcm9sbEJvdHRvbSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGVsZW1lbnRUb3AgPCBzY3JvbGxUb3AgJiYgZWxlbWVudEJvdHRvbSA+IHNjcm9sbEJvdHRvbSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAudHJpZ2dlcihcImlrOnZpc2libGl0eS1jaGFuZ2VcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnRyaWdnZXIoXCJpazp2aXNpYmxlXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5hdHRyKFwiZGF0YS1vbnNjcmVlblwiLCAxKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIC8qXG4gICAgICAgICAgICAgICAgICAgIGlmIChlbGVtZW50Qm90dG9tIDw9IHNjcm9sbEJvdHRvbSAmJiBlbGVtZW50VG9wID49IHNjcm9sbFRvcClcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnQudHJpZ2dlcihcImlrOnJldmVhbDpmdWxsXCIpO1xuICAgICAgICAgICAgICAgICAgICBlbHNlKi9cbiAgICAgICAgICAgICAgICB9KTtcblxuXHRcdFx0XHRpc1Njcm9sbGluZyA9IGZhbHNlO1xuXHRcdFx0fSwgNSApO1xuICAgICAgICB9KTtcblxuXHRcdGpRdWVyeSh3aW5kb3cpLnRyaWdnZXIoXCJzY3JvbGxcIik7XG4gICAgfVxuICAgIGZ1bmN0aW9uIGhhbmRsZU1lbnUoKVxuICAgIHtcbiAgICAgICAgdmFyIHNpdGVIZWFkZXIgPSBqUXVlcnkoXCIuc2l0ZS1oZWFkZXJcIik7XG4gICAgICAgIHZhciBtZW51SXRlbXMgPSBqUXVlcnkoXCIubWVudSA+IC5tZW51LWl0ZW0taGFzLWNoaWxkcmVuID4gYVwiLCBzaXRlSGVhZGVyKTtcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBtZW51SXRlbXMub24oXCJjbGlja1wiLCBmdW5jdGlvbihlKVxuICAgICAgICB7XG4gICAgICAgICAgICBpZiAod2luZG93LmlubmVyV2lkdGggPCAxMDI0KVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHZhciBpc0NsaWNrZWQgPSBqUXVlcnkodGhpcykucGFyZW50KCkuaGFzQ2xhc3MoXCJjbGlja2VkXCIpO1xuICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgaWYgKCFpc0NsaWNrZWQpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBlLnN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7ICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIGpRdWVyeShcIi5tZW51ID4gLm1lbnUtaXRlbS1oYXMtY2hpbGRyZW5cIikucmVtb3ZlQ2xhc3MoXCJjbGlja2VkXCIpO1xuICAgICAgICAgICAgICAgICAgICBqUXVlcnkodGhpcykucGFyZW50KCkuYWRkQ2xhc3MoXCJjbGlja2VkXCIpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAge1xuLy8gICAgICAgICAgICAgICAgICAgIGpRdWVyeSh0aGlzKS5wYXJlbnQoKS5yZW1vdmVDbGFzcyhcImNsaWNrZWRcIik7ICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICBcbiAgICB9XG5cblx0ZnVuY3Rpb24gaGFuZGxlTW9iaWxlTWVudSgpXG5cdHtcbiAgICAgICAgdmFyIGJvZHkgPSBqUXVlcnkoXCJib2R5XCIpO1xuICAgICAgICB2YXIgc2l0ZUhlYWRlciA9IGpRdWVyeShcIi5zaXRlLWhlYWRlclwiKTtcblx0XHR2YXIgYnRuTWVudSA9IGpRdWVyeShcIi5idG4tbW9iaWxlLW1lbnVcIiwgc2l0ZUhlYWRlcik7XG5cdFx0dmFyIG1lbnVJdGVtcyA9IGpRdWVyeShcIi5tZW51LWl0ZW0gYVwiLCBzaXRlSGVhZGVyKTtcblxuXHRcdGJ0bk1lbnVcblx0XHRcdC5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKGUpXG5cdFx0XHR7XG5cdFx0XHRcdGJvZHkudG9nZ2xlQ2xhc3MoXCJtZW51LW9wZW5lZFwiKTtcblxuXHRcdFx0XHRpZiAoYm9keS5oYXNDbGFzcyhcIm1lbnUtb3BlbmVkXCIpKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0alF1ZXJ5KHRoaXMpLmFkZENsYXNzKFwiYWN0aXZlXCIpO1xuXHRcdFx0XHRcdHNpdGVIZWFkZXIuYWRkQ2xhc3MoXCJtb2JpbGUtYWN0aXZlXCIpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0e1xuXHRcdFx0XHRcdGpRdWVyeSh0aGlzKS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcblx0XHRcdFx0XHRzaXRlSGVhZGVyLnJlbW92ZUNsYXNzKFwibW9iaWxlLWFjdGl2ZVwiKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cblx0XHRtZW51SXRlbXNcblx0XHRcdC5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKGUpXG5cdFx0XHR7XG4gICAgICAgICAgICAgICAgaWYoIWpRdWVyeSh0aGlzKS5wYXJlbnQoKS5oYXNDbGFzcyhcIm1lbnUtaXRlbS1oYXMtY2hpbGRyZW5cIikpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBib2R5LnJlbW92ZUNsYXNzKFwibWVudS1vcGVuZWRcIik7XG4gICAgICAgICAgICAgICAgICAgIGJ0bk1lbnUucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIik7XG4gICAgICAgICAgICAgICAgICAgIHNpdGVIZWFkZXIucmVtb3ZlQ2xhc3MoXCJtb2JpbGUtYWN0aXZlXCIpO1xuXG4gICAgICAgICAgICAgICAgfVxuXHRcdFx0fSk7XG5cdH07XG5cbiAgICBmdW5jdGlvbiBoYW5kbGVBbmFseXRpY3MoKVxuICAgIHtcbiAgICAgICAgalF1ZXJ5KCcubmF2aWdhdGlvbi10b3AgbGkgYScpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciBsaW5rID0galF1ZXJ5KHRoaXMpLmF0dHIoXCJocmVmXCIpO1xuICAgICAgICAgICAgLy9lLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICBndGFnKCdldmVudCcsICdUT1AtbWVudScsIHtcbiAgICAgICAgICAgICAgICAnZXZlbnRfY2F0ZWdvcnknOiAnY2xpY2snLFxuICAgICAgICAgICAgICAgICdldmVudF9sYWJlbCc6IGpRdWVyeSh0aGlzKS5maW5kKFwic3BhblwiKS5odG1sKCksXG4gICAgICAgICAgICAgICAgLy8nZXZlbnRfY2FsbGJhY2snOiBmdW5jdGlvbigpe2RvY3VtZW50LmxvY2F0aW9uID0gbGluazt9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgalF1ZXJ5KCcuZm9vdGVyLW5hdiBsaSBhJykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSlcbiAgICAgICAge1xuICAgICAgICAgICAgLy9lLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgICAgIHZhciBsaW5rID0galF1ZXJ5KHRoaXMpLmF0dHIoXCJocmVmXCIpO1xuXG4gICAgICAgICAgICBndGFnKCdldmVudCcsICdmb290ZXItbWVudScsIHtcbiAgICAgICAgICAgICAgICAnZXZlbnRfY2F0ZWdvcnknOiAnY2xpY2snLFxuICAgICAgICAgICAgICAgICdldmVudF9sYWJlbCc6IGpRdWVyeSh0aGlzKS5maW5kKFwic3BhblwiKS5odG1sKCksXG4gICAgICAgICAgICAgICAgLy8nZXZlbnRfY2FsbGJhY2snOiBmdW5jdGlvbigpIHtkb2N1bWVudC5sb2NhdGlvbiA9IGxpbms7fVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGpRdWVyeSgnLnByb2R1Y3RzLmxpc3QgbGkgLm1vcmUgLmV4cGFuZGVyIC5leHBhbmQnKS5vbignY2xpY2snLCBmdW5jdGlvbihlKVxuICAgICAgICB7XG4gICAgICAgICAgICB2YXIgcHJvZHVjdF9uYW1lID0galF1ZXJ5KHRoaXMpLmNsb3Nlc3QoXCJsaVwiKS5maW5kKFwiLm5hbWUgZGl2XCIpLmh0bWwoKTtcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIGd0YWcoJ2V2ZW50JywgJ1pvYmFjeiB3acSZY2VqJywge1xuICAgICAgICAgICAgICAgICdldmVudF9jYXRlZ29yeSc6ICdjbGljaycsXG4gICAgICAgICAgICAgICAgJ2V2ZW50X2xhYmVsJzogcHJvZHVjdF9uYW1lICsgJyAgbmEgJyArIGxvY2F0aW9uLmhyZWZcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcblxuICAgICAgICBqUXVlcnkoJy5wcm9kdWN0cy5saXN0IGxpIC5tb3JlIC5leHBhbmRlciAuZm9sZCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciBwcm9kdWN0X25hbWUgPSBqUXVlcnkodGhpcykuY2xvc2VzdChcImxpXCIpLmZpbmQoXCIubmFtZSBkaXZcIikuaHRtbCgpO1xuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgZ3RhZygnZXZlbnQnLCAnWm9iYWN6IG1uaWVqJywge1xuICAgICAgICAgICAgICAgICdldmVudF9jYXRlZ29yeSc6ICdjbGljaycsXG4gICAgICAgICAgICAgICAgJ2V2ZW50X2xhYmVsJzogcHJvZHVjdF9uYW1lICsgJyAgbmEgJyArIGxvY2F0aW9uLmhyZWZcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcblxuICAgICAgICBqUXVlcnkoJy5wcm9kdWN0cy5saXN0IGxpIC5jdGEgYScpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpXG4gICAgICAgIHtcbiAgICAgICAgICAgIC8vZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgICB2YXIgbGluayA9IGpRdWVyeSh0aGlzKS5hdHRyKFwiaHJlZlwiKTtcbiAgICAgICAgICAgIHZhciBwcm9kdWN0X25hbWUgPSBqUXVlcnkodGhpcykuY2xvc2VzdChcImxpXCIpLmF0dHIoXCJpZFwiKVxuICAgICAgICAgICAgdmFyIGNvbG9yID0gXCJiaWHFgnlcIjtcblxuICAgICAgICAgICAgaWYoalF1ZXJ5KHRoaXMpLmhhc0NsYXNzKFwiYnRuLWludmVyc2VcIikpXG4gICAgICAgICAgICAgICAgY29sb3IgPSBcInppZWxvbnlcIjtcblxuICAgICAgICAgICAgZ3RhZygnZXZlbnQnLCAnY29tcGFyZS1hY3F1aXNpdGlvbicsIHtcbiAgICAgICAgICAgICAgICAnZXZlbnRfY2F0ZWdvcnknOiAnY2xpY2snLFxuICAgICAgICAgICAgICAgICdldmVudF9sYWJlbCc6ICdaxYLDs8W8IHduaW9zZWsgLSAnICsgY29sb3IgKyAnIGd1emlrIC0gJyAgKyBwcm9kdWN0X25hbWUgKyAnIG5hICcgKyBsb2NhdGlvbi5ocmVmLFxuICAgICAgICAgICAgICAgIC8vJ2V2ZW50X2NhbGxiYWNrJzogZnVuY3Rpb24oKXtkb2N1bWVudC5sb2NhdGlvbiA9IGxpbms7fVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGpRdWVyeSgnLnByb2R1Y3QgLmF0dGFjaG1lbnRzIHVsIGxpIGEnKS5vbignY2xpY2snLCBmdW5jdGlvbihlKVxuICAgICAgICB7XG4gICAgICAgICAgICAvL2UucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgICAgdmFyIGxpbmsgPSBqUXVlcnkodGhpcykuYXR0cihcImhyZWZcIik7XG5cbiAgICAgICAgICAgIGd0YWcoJ2V2ZW50JywgJ1BERicsIHtcbiAgICAgICAgICAgICAgICAgICAgJ2V2ZW50X2NhdGVnb3J5JyA6ICdwb2JyYW5pZScsXG4gICAgICAgICAgICAgICAgICAgICdldmVudF9sYWJlbCcgOiBsb2NhdGlvbi5ocmVmLFxuICAgICAgICAgICAgICAgICAgICAvLydldmVudF9jYWxsYmFjayc6IGZ1bmN0aW9uKCl7ZG9jdW1lbnQubG9jYXRpb24gPSBsaW5rO31cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICApO1xuICAgICAgICB9KTtcblxuXG4gICAgICAgIGpRdWVyeSgnLnNoYXJlIC5saW5rcyBhLnNvY2lhbC1zaGFyZScpLm9uKCdjbGljaycsIGZ1bmN0aW9uKClcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIG1lZGlhID0galF1ZXJ5KHRoaXMpLmZpbmQoXCJzcGFuXCIpLmh0bWwoKTtcblxuICAgICAgICAgICAgZ3RhZygnZXZlbnQnLCBtZWRpYSxcbiAgICAgICAgICAgICAgICB7ICdldmVudF9jYXRlZ29yeScgOiAnc2hhcmUnLCAnZXZlbnRfbGFiZWwnIDogbG9jYXRpb24uaHJlZiB9XG4gICAgICAgICAgICApO1xuICAgICAgICB9KTtcblxuICAgICAgICBqUXVlcnkoJy5wb3N0LWNvbnRlbnQgLmV4dGVybmFsLWxpbmsnKS5vbignY2xpY2snLCBmdW5jdGlvbigpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciBsaW5rID0galF1ZXJ5KHRoaXMpLmF0dHIoXCJocmVmXCIpO1xuICAgICAgICAgICAgdmFyIHRleHQgPSBqUXVlcnkodGhpcykudGV4dCgpO1xuICAgICAgICAgICAgZ3RhZyhcImV2ZW50XCIsIFwibGlua196X2FydHlrdWx1XCIsXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImV2ZW50X2NhdGVnb3J5XCIgOiBcImNsaWNrXCIsXG4gICAgICAgICAgICAgICAgICAgIFwiZXZlbnRfbGFiZWxcIiA6IGxpbmsgKyBcIiB8IFwiICsgdGV4dCxcbiAgICAgICAgICAgICAgICAgICAgLy9cImV2ZW50X2NhbGxiYWNrXCI6IGZ1bmN0aW9uKCl7ZG9jdW1lbnQubG9jYXRpb24gPSBsaW5rO31cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICApO1xuICAgICAgICB9KTtcblxuXG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc2Nyb2xsVG8odGFyZ2V0T3JJbnQsIG1hcmdpbiwgc3BlZWQpXG4gICAge1xuICAgICAgICBmdW5jdGlvbiBpc0ludChuKXsgcmV0dXJuIE51bWJlcihuKSA9PT0gbiAmJiBuICUgMSA9PT0gMDsgfVxuICAgICAgICBmdW5jdGlvbiBpc2pRdWVyeShvYmopIHsgcmV0dXJuIChvYmogJiYgKG9iaiBpbnN0YW5jZW9mIGpRdWVyeSB8fCBvYmouY29uc3RydWN0b3IucHJvdG90eXBlLmpxdWVyeSkpOyB9XG5cbiAgICAgICAgdmFyIHRhcmdldCA9IHRhcmdldE9ySW50O1xuXG4gICAgICAgIGlmICghaXNJbnQodGFyZ2V0KSlcbiAgICAgICAge1xuICAgICAgICAgICAgdGFyZ2V0ID0gIWlzalF1ZXJ5KHRhcmdldCkgPyBqUXVlcnkodGFyZ2V0KS5vZmZzZXQoKS50b3AgOiB0YXJnZXQub2Zmc2V0KCkudG9wO1xuICAgICAgICB9XG5cbiAgICAgICAgbWFyZ2luID0gdHlwZW9mIG1hcmdpbiAhPT0gXCJ1bmRlZmluZWRcIiA/IG1hcmdpbiA6IDA7XG4gICAgICAgIHNwZWVkID0gdHlwZW9mIHNwZWVkICE9PSBcInVuZGVmaW5lZFwiID8gc3BlZWQgOiA3NTA7XG5cbiAgICAgICAgaWYgKG5hdmlnYXRvci51c2VyQWdlbnQubWF0Y2goLyhpUG9kfGlQaG9uZXxpUGFkKS8pKVxuICAgICAgICB7XG4gICAgICAgICAgICBqUXVlcnkoJ2JvZHknKS5hbmltYXRlKFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHNjcm9sbFRvcDogdGFyZ2V0ICsgbWFyZ2luXG4gICAgICAgICAgICB9LCBzcGVlZCk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZVxuICAgICAgICB7XG4gICAgICAgICAgICBqUXVlcnkoJ2h0bWwsIGJvZHknKS5hbmltYXRlKFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHNjcm9sbFRvcDogdGFyZ2V0ICsgbWFyZ2luXG4gICAgICAgICAgICB9LCBzcGVlZCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBoYW5kbGVTY3JvbGxUb0NvbnRlbnQoKVxuICAgIHtcbiAgICAgICAgdmFyIHNjcm9sbEJ0biA9IGpRdWVyeShcIi5zY3JvbGwtdG8tY29udGVudFwiKTtcblxuICAgICAgICBpZiAoIXNjcm9sbEJ0bi5sZW5ndGgpXG4gICAgICAgICAgICByZXR1cm47XG5cbiAgICAgICAgdmFyIGNvbnRlbnQgPSBqUXVlcnkoXCIubWFpbi1jb250ZW50XCIpO1xuXG4gICAgICAgIGlmICghY29udGVudC5sZW5ndGgpXG4gICAgICAgICAgICBjb250ZW50ID0galF1ZXJ5KFwiLnBhbmVsLWxheW91dFwiKTtcblxuICAgICAgICBpZiAoIWNvbnRlbnQubGVuZ3RoKVxuICAgICAgICAgICAgcmV0dXJuO1xuXG4gICAgICAgIHNjcm9sbEJ0bi5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKClcbiAgICAgICAge1xuICAgICAgICAgICAgc2Nyb2xsVG8oY29udGVudCk7XG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaGFuZGxlTWFnZWJveE5hdmlnYXRpb24oKVxuICAgIHtcbiAgICAgICAgdmFyIG1lZ2Fib3hlcyA9IGpRdWVyeShcIi5tZWdhYm94XCIpO1xuXG4gICAgICAgIGlmICghbWVnYWJveGVzLmxlbmd0aClcbiAgICAgICAgICAgIHJldHVybjtcblxuICAgICAgICB2YXIgYXV0b1BsYXkgPSBmYWxzZTtcbiAgICAgICAgdmFyIG1lZ2Fib3hJbnRlcnZhbDtcblxuICAgICAgICBmdW5jdGlvbiB0b2dnbGVBdXRvUGxheShtZWdhYm94LCB0dXJuX29uKVxuICAgICAgICB7XG4gICAgICAgICAgICB0dXJuX29uID0gdHlwZW9mIHR1cm5fb24gIT09IFwidW5kZWZpbmVkXCIgPyB0dXJuX29uIDogdHJ1ZTtcblxuICAgICAgICAgICAgaWYgKHR1cm5fb24pXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbChtZWdhYm94SW50ZXJ2YWwpO1xuICAgICAgICAgICAgICAgIG1lZ2Fib3hJbnRlcnZhbCA9IHNldEludGVydmFsKGZ1bmN0aW9uKClcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGdvVG9OZXh0U2xpZGUobWVnYWJveCwgdHJ1ZSk7XG4gICAgICAgICAgICAgICAgfSwgMzAwMCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbChtZWdhYm94SW50ZXJ2YWwpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gZ2V0QWN0aXZlU2xpZGUobWVnYWJveClcbiAgICAgICAge1xuICAgICAgICAgICAgcmV0dXJuIGpRdWVyeShcIi5mZWF0dXJlZC1wb3N0cy1jb250YWluZXIgLmZlYXR1cmVkLXBvc3QuYWN0aXZlXCIpO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gZ2V0QWN0aXZlU2xpZGVJbmRleChtZWdhYm94KVxuICAgICAgICB7XG4gICAgICAgICAgICByZXR1cm4gZ2V0QWN0aXZlU2xpZGUobWVnYWJveCkuaW5kZXgoKTtcbiAgICAgICAgfTtcblxuICAgICAgICBmdW5jdGlvbiBnZXRTbGlkZUJ5SW5kZXgobWVnYWJveCwgaW5kZXgpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHJldHVybiBqUXVlcnkoXCIuZmVhdHVyZWQtcG9zdHMtY29udGFpbmVyIC5mZWF0dXJlZC1wb3N0XCIsIG1lZ2Fib3gpLmVxKGluZGV4KTtcbiAgICAgICAgfTtcblxuICAgICAgICBmdW5jdGlvbiB1cGRhdGVBY3RpdmVEb3RQb3NpdGlvbihtZWdhYm94KVxuICAgICAgICB7XG4gICAgICAgICAgICBqUXVlcnkoXCIuZG90cyAuZG90XCIsIG1lZ2Fib3gpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpXG4gICAgICAgICAgICAgICAgLmVxKGdldEFjdGl2ZVNsaWRlSW5kZXgobWVnYWJveCkpLmFkZENsYXNzKFwiYWN0aXZlXCIpO1xuICAgICAgICB9O1xuXG4gICAgICAgIGZ1bmN0aW9uIGdvVG9TbGlkZShtZWdhYm94LCBpbmRleClcbiAgICAgICAge1xuICAgICAgICAgICAgaWYgKGdldEFjdGl2ZVNsaWRlSW5kZXgobWVnYWJveCkgPT09IGluZGV4KVxuICAgICAgICAgICAgICAgIHJldHVybjtcblxuICAgICAgICAgICAgdmFyIHNsaWRlcyA9IGpRdWVyeShcIi5mZWF0dXJlZC1wb3N0cy1jb250YWluZXIgLmZlYXR1cmVkLXBvc3RcIiwgbWVnYWJveCk7XG5cbiAgICAgICAgICAgIG1lZ2Fib3gudHJpZ2dlciggXCJpa2Fsa3VsYXRvcjpiZWZvcmVNZWdhYm94Q2hhbmdlXCIgKTtcbiAgICAgICAgICAgIHNsaWRlcy5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICAgICAgIHNsaWRlcy5lcShpbmRleCkuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XG4gICAgICAgICAgICB1cGRhdGVBY3RpdmVEb3RQb3NpdGlvbihtZWdhYm94KTtcbiAgICAgICAgICAgIG1lZ2Fib3gudHJpZ2dlciggXCJpa2Fsa3VsYXRvcjphZnRlclNsaWRlQ2hhbmdlXCIgKTtcbiAgICAgICAgfTtcblxuICAgICAgICBmdW5jdGlvbiBnb1RvUHJldlNsaWRlKG1lZ2Fib3gsIGxvb3ApXG4gICAgICAgIHtcbiAgICAgICAgICAgIGxvb3AgPSB0eXBlb2YgbG9vcCAhPT0gXCJ1bmRlZmluZWRcIiA/IGxvb3AgOiB0cnVlO1xuICAgICAgICAgICAgdmFyIHNsaWRlcyA9IGpRdWVyeShcIi5mZWF0dXJlZC1wb3N0cy1jb250YWluZXIgLmZlYXR1cmVkLXBvc3RcIiwgbWVnYWJveCk7XG5cbiAgICAgICAgICAgIHZhciBhY3RpdmVJbmRleCA9IGdldEFjdGl2ZVNsaWRlSW5kZXgobWVnYWJveCk7XG5cbiAgICAgICAgICAgIGlmIChhY3RpdmVJbmRleCA+PSAxICYmIGdldFNsaWRlQnlJbmRleChtZWdhYm94LCBhY3RpdmVJbmRleCAtIDEpLmxlbmd0aClcbiAgICAgICAgICAgICAgICBnb1RvU2xpZGUobWVnYWJveCwgYWN0aXZlSW5kZXggLSAxKTtcbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICBpZiAobG9vcClcbiAgICAgICAgICAgICAgICAgICAgZ29Ub1NsaWRlKG1lZ2Fib3gsIHNsaWRlcy5sZW5ndGggLSAxKTtcbiAgICAgICAgfTtcblxuICAgICAgICBmdW5jdGlvbiBnb1RvTmV4dFNsaWRlKG1lZ2Fib3gsIGxvb3ApXG4gICAgICAgIHtcbiAgICAgICAgICAgIGxvb3AgPSB0eXBlb2YgbG9vcCAhPT0gXCJ1bmRlZmluZWRcIiA/IGxvb3AgOiB0cnVlO1xuICAgICAgICAgICAgdmFyIGFjdGl2ZUluZGV4ID0gZ2V0QWN0aXZlU2xpZGVJbmRleChtZWdhYm94KTtcblxuICAgICAgICAgICAgaWYgKGdldFNsaWRlQnlJbmRleChtZWdhYm94LCBhY3RpdmVJbmRleCArIDEpLmxlbmd0aClcbiAgICAgICAgICAgICAgICBnb1RvU2xpZGUobWVnYWJveCwgYWN0aXZlSW5kZXggKyAxKTtcbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICBpZiAobG9vcClcbiAgICAgICAgICAgICAgICAgICAgZ29Ub1NsaWRlKG1lZ2Fib3gsIDApO1xuICAgICAgICB9O1xuXG4gICAgICAgIG1lZ2Fib3hlcy5lYWNoKGZ1bmN0aW9uKClcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIG1lZ2Fib3ggPSBqUXVlcnkodGhpcyk7XG4gICAgICAgICAgICB2YXIgZG90cyA9IGpRdWVyeShcIi5uYXYtcGFuZWwgLmRvdFwiLCBtZWdhYm94KTtcbiAgICAgICAgICAgIHZhciBwcmV2ID0galF1ZXJ5KFwiLm5hdi1wYW5lbCAucHJldlwiLCBtZWdhYm94KTtcbiAgICAgICAgICAgIHZhciBuZXh0ID0galF1ZXJ5KFwiLm5hdi1wYW5lbCAubmV4dFwiLCBtZWdhYm94KTtcblxuICAgICAgICAgICAgcHJldi5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKClcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBnb1RvUHJldlNsaWRlKG1lZ2Fib3gsIHRydWUpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIG5leHQub24oXCJjbGlja1wiLCBmdW5jdGlvbigpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgZ29Ub05leHRTbGlkZShtZWdhYm94LCB0cnVlKTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBkb3RzLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oZXZudClcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICB2YXIgZG90SW5kZXggPSAgalF1ZXJ5KHRoaXMpLmluZGV4KCk7XG4gICAgICAgICAgICAgICAgZ29Ub1NsaWRlKG1lZ2Fib3gsIGRvdEluZGV4KTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBpZiAoYXV0b1BsYXkpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgalF1ZXJ5KFwiLmZlYXR1cmVkLXBvc3RzLWNvbnRhaW5lciwgLm5hdi1wYW5lbFwiLCBtZWdhYm94KVxuICAgICAgICAgICAgICAgICAgICAub24oXCJtb3VzZWVudGVyXCIsIGZ1bmN0aW9uKClcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgdG9nZ2xlQXV0b1BsYXkobWVnYWJveCwgZmFsc2UpO1xuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAub24oXCJtb3VzZWxlYXZlXCIsIGZ1bmN0aW9uKClcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgdG9nZ2xlQXV0b1BsYXkobWVnYWJveCwgdHJ1ZSk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgdG9nZ2xlQXV0b1BsYXkobWVnYWJveCwgdHJ1ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBqUXVlcnkoXCIuc2VlLWFsbCAuc2VlLWFsbC1idG5cIixtZWdhYm94KS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKClcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBqUXVlcnkoXCIuc2VlLWFsbFwiLG1lZ2Fib3gpLnRvZ2dsZUNsYXNzKFwiYWN0aXZlXCIpO1xuXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiB1cGRhdGVQb3N0RmxvYXRpbmdIZWFkZXJWaXNpYmlsaXR5KHZhbHVlKVxuICAgIHtcbiAgICAgICAgdmFyIGZsb2F0aW5nSGVhZGVyID0galF1ZXJ5KFwiLmZsb2F0aW5nLWhlYWRlclwiKTtcblxuICAgICAgICBpZiAodmFsdWUgPj0gMClcbiAgICAgICAgICAgIGZsb2F0aW5nSGVhZGVyLmFkZENsYXNzKFwiYWN0aXZlXCIpO1xuICAgICAgICBlbHNlXG4gICAgICAgICAgICBmbG9hdGluZ0hlYWRlci5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiB1cGRhdGVQcm9ncmVzc0Jhcihwcm9ncmVzcywgY29udGVudCwgY29udGVudFRvcClcbiAgICB7XG4gICAgICAgIHZhciB3aW5IZWlnaHQgPSBqUXVlcnkod2luZG93KS5pbm5lckhlaWdodCgpO1xuXG4gICAgICAgIHZhciBjb250ZW50VG9wUGFkZGluZyA9IGNvbnRlbnQub2Zmc2V0KCkudG9wIC0gY29udGVudFRvcDtcbiAgICAgICAgdmFyIGNvbnRlbnRIZWlnaHQgPSBjb250ZW50VG9wUGFkZGluZyArIGNvbnRlbnQub3V0ZXJIZWlnaHQoKTtcbiAgICAgICAgdmFyIHZhbHVlID0galF1ZXJ5KHdpbmRvdykuc2Nyb2xsVG9wKCkgLSBjb250ZW50VG9wO1xuICAgICAgICBwcm9ncmVzcy5hdHRyKFwibWF4XCIsIGNvbnRlbnRIZWlnaHQgLSB3aW5IZWlnaHQpO1xuICAgICAgICBwcm9ncmVzcy5hdHRyKFwidmFsdWVcIiwgdmFsdWUpO1xuXG4gICAgICAgIHVwZGF0ZVBvc3RGbG9hdGluZ0hlYWRlclZpc2liaWxpdHkodmFsdWUpO1xuXG4gICAgICAgIHJldHVybiB2YWx1ZTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBoYW5kbGVOZXdzbGV0dGVyU2Nyb2xsVG9SZXNwb25zZSgpXG4gICAge1xuICAgICAgICB2YXIgcmVzcG9zZUVsZW1lbnQgPSBqUXVlcnkoXCIubWM0d3AtcmVzcG9uc2UgLm1jNHdwLWFsZXJ0XCIpO1xuICAgICAgICBcbiAgICAgICAgaWYgKCFyZXNwb3NlRWxlbWVudC5sZW5ndGgpXG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIFxuICAgICAgICBpZiAocmVzcG9zZUVsZW1lbnQuaHRtbCAhPT0gXCJcIilcbiAgICAgICAge1xuICAgICAgICAgICAgc2Nyb2xsVG8ocmVzcG9zZUVsZW1lbnQsIC04MCk7XG4gICAgICAgIH1cbiAgICB9XG5cblxuICAgIGZ1bmN0aW9uIGhhbmRsZVJlYWRpbmdQcm9ncmVzcygpXG4gICAge1xuICAgICAgICB2YXIgcHJvZ3Jlc3MgPSBqUXVlcnkoXCIucmVhZGluZy1wcm9ncmVzc1wiKTtcbiAgICAgICAgdmFyIGNvbnRlbnQgPSBqUXVlcnkoXCIubWFpbi1jb250ZW50IC5wb3N0LWNvbnRlbnRcIik7XG5cbiAgICAgICAgaWYgKCFwcm9ncmVzcy5sZW5ndGggfHwgIWNvbnRlbnQubGVuZ3RoKVxuICAgICAgICAgICAgcmV0dXJuO1xuXG4gICAgICAgIHZhciBtYWluQ29udGVudCA9IGNvbnRlbnQuY2xvc2VzdChcIi5tYWluLWNvbnRlbnRcIik7XG5cbiAgICAgICAgdmFyIGNvbnRlbnRUb3AgPSBtYWluQ29udGVudC5vZmZzZXQoKS50b3A7XG4gICAgICAgIHVwZGF0ZVByb2dyZXNzQmFyKHByb2dyZXNzLCBjb250ZW50LCBjb250ZW50VG9wKTtcblxuICAgICAgICBqUXVlcnkoZG9jdW1lbnQpLm9uKFwic2Nyb2xsXCIsIGZ1bmN0aW9uKClcbiAgICAgICAge1xuICAgICAgICAgICAgY29udGVudFRvcCA9IG1haW5Db250ZW50Lm9mZnNldCgpLnRvcDtcbiAgICAgICAgICAgIHZhciB2YWx1ZSA9IGpRdWVyeSh3aW5kb3cpLnNjcm9sbFRvcCgpIC0gY29udGVudFRvcDtcbiAgICAgICAgICAgIHByb2dyZXNzLmF0dHIoXCJ2YWx1ZVwiLCAgdmFsdWUpO1xuICAgICAgICAgICAgdXBkYXRlUG9zdEZsb2F0aW5nSGVhZGVyVmlzaWJpbGl0eSh2YWx1ZSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGpRdWVyeSh3aW5kb3cpLm9uKFwicmVzaXplXCIsIGZ1bmN0aW9uKClcbiAgICAgICAge1xuICAgICAgICAgICAgY29udGVudFRvcCA9IG1haW5Db250ZW50Lm9mZnNldCgpLnRvcDtcbiAgICAgICAgICAgIHVwZGF0ZVByb2dyZXNzQmFyKHByb2dyZXNzLCBjb250ZW50LCBjb250ZW50VG9wKTtcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIFxuICAgIGZ1bmN0aW9uIGhhbmRsZU1vYmlsZVN3YXBPcmRlcigpXG4gICAge1xuICAgICAgICB2YXIgY29udGFpbmVycyA9IGpRdWVyeShcIi5tb2JpbGUtc3dhcFwiKTtcbiAgICAgICAgXG4gICAgICAgIGNvbnRhaW5lcnMuZWFjaChmdW5jdGlvbigpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciBjdXJyZW50ID0galF1ZXJ5KHRoaXMpO1xuICAgICAgICAgICAgdmFyIGVsZW1lbnRzID0galF1ZXJ5KFwiPiAqXCIsIGN1cnJlbnQpO1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IDc2OClcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpZiAoIWN1cnJlbnQuaGFzQ2xhc3MoXCJzd2FwcGVkXCIpKVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudHMuZmlyc3QoKS5kZXRhY2goKS5pbnNlcnRBZnRlcihlbGVtZW50cy5sYXN0KCkpO1xuICAgICAgICAgICAgICAgICAgICBjdXJyZW50LmFkZENsYXNzKFwic3dhcHBlZFwiKTtcbiAgICAgICAgICAgICAgICB9ICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICBpZiAoY3VycmVudC5oYXNDbGFzcyhcInN3YXBwZWRcIikpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBlbGVtZW50cy5maXJzdCgpLmRldGFjaCgpLmluc2VydEFmdGVyKGVsZW1lbnRzLmxhc3QoKSk7XG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnQucmVtb3ZlQ2xhc3MoXCJzd2FwcGVkXCIpO1xuICAgICAgICAgICAgICAgIH0gICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cblxuICAgIGZ1bmN0aW9uIGRpc2FibGVJbWFnZXNEcmFnZ2luZygpXG4gICAge1xuICAgICAgICBqUXVlcnkoXCIucG9zdC1saXN0XCIpLm9uKFwibW91c2Vkb3duXCIsIFwiaW1nXCIsIGZ1bmN0aW9uKGV2ZW50KSB7IGV2ZW50LnByZXZlbnREZWZhdWx0KCk7IH0pO1xuICAgIH1cbiAgICBcbiAgICBmdW5jdGlvbiBvblJlc2l6ZSgpXG4gICAge1x0XG4gICAgICAgIHZhciBpc1Jlc2l6aW5nICA9IGZhbHNlO1xuXG4gICAgICAgIGpRdWVyeSh3aW5kb3cpLm9uKFwicmVzaXplXCIsIGZ1bmN0aW9uICgpIFxuICAgICAgICB7XG4gICAgICAgICAgICBpZiAoaXNSZXNpemluZykgeyByZXR1cm47IH1cdFx0XG4gICAgICAgICAgICBpc1Jlc2l6aW5nID0gdHJ1ZTtcblxuICAgICAgICAgICAgd2luZG93LnNldFRpbWVvdXQoZnVuY3Rpb24oKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIC8vcGxhY2UgZnVuY3Rpb25zIGhlcmVcbiAgICAgICAgICAgICAgICBoYW5kbGVNb2JpbGVTd2FwT3JkZXIoKTtcbiAgICAgICAgICAgICAgICBoYW5kbGVDYXRlZ29yeVdpdGhTaWRlYmFyTWluSGVpZ2h0KCk7XG5cbiAgICAgICAgICAgICAgICBpc1Jlc2l6aW5nID0gZmFsc2U7XG4gICAgICAgICAgICB9LCA1MCApO1xuICAgICAgICB9KTtcdFxuICAgICAgICAgICAgICAgXG5cdFx0alF1ZXJ5KHdpbmRvdykudHJpZ2dlcihcInJlc2l6ZVwiKTtcbiAgICB9XG5cblxuXG4gICAgLyogX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fXyBwdWJsaWMgbWV0aG9kcyAqL1xuXG5cdHZhciBpbml0ID0gZnVuY3Rpb24oKVxuXHR7XG4gICAgICAgIGhhbmRsZU1lbnUoKTtcbiAgICAgICAgaGFuZGxlTW9iaWxlTWVudSgpO1xuICAgICAgICBoYW5kbGVNYWdlYm94TmF2aWdhdGlvbigpO1xuICAgICAgICBoYW5kbGVTY3JvbGxUb0NvbnRlbnQoKTtcbiAgICAgICAgaGFuZGxlUmVhZGluZ1Byb2dyZXNzKCk7XG4gICAgICAgIGhhbmRsZVNwb3RsaWdodFNlYXJjaCgpO1xuICAgICAgICBoYW5kbGVTaGFyZUxpbmtzKCk7XG4gICAgICAgIGhhbmRsZVByb2R1Y3RzR3JpZCgpO1xuICAgICAgICBoYW5kbGVBbmFseXRpY3MoKTtcbiAgICAgICAgaGFuZGxlTGlzdE51bWJlcmluZygpO1xuICAgICAgICBoYW5kbGVDYXRlZ29yeVdpdGhTaWRlYmFyTWluSGVpZ2h0KCk7XG4gICAgICAgIFxuICAgICAgICBjaGVja0VsZW1lbnRzVmlzaWJpbGl0eU9uU2Nyb2xsKFwiLnNjcm9sbC1oYW5kbGVcIik7XG4gICAgICAgIGRpc2FibGVJbWFnZXNEcmFnZ2luZygpO1xuICAgICAgICBoYW5kbGVOZXdzbGV0dGVyU2Nyb2xsVG9SZXNwb25zZSgpO1xuXG5cdFx0b25SZXNpemUoKTtcblx0XHRqUXVlcnkod2luZG93KS50cmlnZ2VyKFwic2Nyb2xsXCIpO1xuXG5cdH07XG5cblx0aW5pdCgpO1xufTtcblxudmFyIGlrYWxrdWxhdG9yO1xuXG5qUXVlcnkoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCQpXG57XG5cdGlrYWxrdWxhdG9yID0gbmV3IGlrYWxrdWxhdG9yKCk7XG5cdHByb2R1Y3RfZmlsdGVycyA9IG5ldyBwcm9kdWN0RmlsdGVyKCk7XG5cdGNvbXBhbmllcyA9IG5ldyBjb21wYW5pZXMoKTtcbiAgICBzZWFyY2hib3ggPSBuZXcgc2VhcmNoYm94KCk7XG4gICAgcHJvbW9ib3ggPSBuZXcgcHJvbW9ib3goKTtcbiAgICBjb21tZW50cyA9IG5ldyBjb21tZW50cygpO1xufSk7XG4iLCJjb21tZW50cyA9IGZ1bmN0aW9uICgpXG57XG5cdHZhciBhcHBseVN3aXRjaGVyID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0alF1ZXJ5KFwiLmNvbW1lbnRzXCIpLmVhY2goIGZ1bmN0aW9uICgpXG5cdFx0e1xuXHRcdFx0dmFyIGNvbW1lbnRCb3ggPSBqUXVlcnkodGhpcyk7XG5cblx0XHRcdGpRdWVyeShcIi5zaG93LWFjdGlvbiBidXR0b25cIiwgY29tbWVudEJveCkub24oXCJjbGlja1wiLCBmdW5jdGlvbigpXG5cdFx0XHR7XG5cdFx0XHRcdGpRdWVyeShcIi5jb21tZW50cy1jb250ZW50XCIsIGNvbW1lbnRCb3gpLnN0b3AoKS5zbGlkZVRvZ2dsZSgpO1xuXHRcdFx0XHRjb21tZW50Qm94LnRvZ2dsZUNsYXNzKFwidmlzaWJsZVwiKTtcblx0XHRcdH0pXG5cdFx0fSlcblxuXHR9XG5cblx0dmFyIHNjcm9sbFRvQ29tbWVudHMgPSBmdW5jdGlvbigpXG5cdHtcblx0XHRpZihsb2NhdGlvbi5ocmVmLmluZGV4T2YoXCJyZXBseXRvY29tPVwiKSA+IC0xIHx8IGxvY2F0aW9uLmhyZWYuaW5kZXhPZihcIiNjb21tZW50XCIpID4gLTEpXG5cdFx0e1xuXHRcdFx0alF1ZXJ5KFwiLmNvbW1lbnRzIC5jb21tZW50cy1jb250ZW50XCIpLnNob3coKTtcblx0XHRcdGpRdWVyeShcIi5jb21tZW50c1wiKS5hZGRDbGFzcyhcInZpc2libGVcIik7XG5cblx0XHRcdGlmKGxvY2F0aW9uLmhhc2gpXG5cdFx0XHR7XG5cdFx0XHRcdGlmKGpRdWVyeShsb2NhdGlvbi5oYXNoKS5sZW5ndGgpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRqUXVlcnkoXCJodG1sLGJvZHlcIikuYW5pbWF0ZSh7IHNjcm9sbFRvcDogalF1ZXJ5KGxvY2F0aW9uLmhhc2gpLm9mZnNldCgpLnRvcCAtIDEwMH0pXG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9XG5cdH1cblxuXHR2YXIgaGFuZGxlRG9SYXRlUmVsZWFzZSA9IGZ1bmN0aW9uKClcblx0e1xuXHRcdGpRdWVyeShcIiNkb1JhdGVcIikub24oXCJjbGlja1wiLCBmdW5jdGlvbigpIHtcblx0XHRcdGpRdWVyeShcIi5jb21tZW50cyAuY29tbWVudHMtY29udGVudFwiKS5zaG93KCk7XG5cdFx0XHRqUXVlcnkoXCIuY29tbWVudHNcIikuYWRkQ2xhc3MoXCJ2aXNpYmxlXCIpO1xuXHRcdFx0alF1ZXJ5KFwiaHRtbCxib2R5XCIpLmFuaW1hdGUoeyBzY3JvbGxUb3A6IGpRdWVyeShcIiNyZXNwb25kXCIpLm9mZnNldCgpLnRvcCAtIDEwMH0pXG5cdFx0fSk7XG5cdH1cblxuXHR2YXIgZHJhd1N0YXJzID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0alF1ZXJ5KFwiLnN0YXItcmF0aW5nXCIpLmVhY2goIGZ1bmN0aW9uKCkge1xuXHRcdFx0aWYoalF1ZXJ5KHRoaXMpLmRhdGEoXCJyYXRpbmdcIikpXG5cdFx0XHR7XG5cdFx0XHRcdHZhciByYXRpbmcgPSBwYXJzZUZsb2F0KGpRdWVyeSh0aGlzKS5kYXRhKFwicmF0aW5nXCIpKTtcblx0XHRcdFx0dmFyIGZ1bGxzID0gTWF0aC5mbG9vcihyYXRpbmcpO1xuXHRcdFx0XHRmb3IodmFyIGkgPSAwOyBpIDwgZnVsbHM7aSArKylcblx0XHRcdFx0e1xuXHRcdFx0XHRcdGpRdWVyeSh0aGlzKS5maW5kKFwiLnN0YXJzXCIpLmFwcGVuZCgnPHNwYW4gY2xhc3M9XCJmdWxsXCI+PC9zcGFuPicpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGlmKGZ1bGxzIDwgNSlcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHZhciBmcmFjdCA9IE1hdGgucm91bmQoKHJhdGluZyAtIGZ1bGxzKSAqIDEwMCk7XG5cdFx0XHRcdFx0aWYoZnJhY3QgPiAwKSBqUXVlcnkodGhpcykuZmluZChcIi5zdGFyc1wiKS5hcHBlbmQoJzxzcGFuIGNsYXNzPVwicGFydFwiPjxzcGFuIHN0eWxlPVwid2lkdGg6ICcgKyAoZnJhY3QrMTApICsgJyVcIj48L3NwYW4+PC9zcGFuPicpO1xuXHRcdFx0XHRcdGVsc2UgalF1ZXJ5KHRoaXMpLmZpbmQoXCIuc3RhcnNcIikuYXBwZW5kKCc8c3BhbiBjbGFzcz1cImVtcHR5XCI+PC9zcGFuPicpO1xuXHRcdFx0XHRcdGZ1bGxzKys7XG5cdFx0XHRcdFx0d2hpbGUoZnVsbHMgPCA1KVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGpRdWVyeSh0aGlzKS5maW5kKFwiLnN0YXJzXCIpLmFwcGVuZCgnPHNwYW4gY2xhc3M9XCJlbXB0eVwiPjwvc3Bhbj4nKTtcblx0XHRcdFx0XHRcdGZ1bGxzKys7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fSk7XG5cdH1cblxuXHR2YXIgaGFuZGxlUmF0ZUluRm9ybSA9IGZ1bmN0aW9uKClcblx0e1xuXHRcdGpRdWVyeShcIi5zdGFyLXJhdGluZy5zZWxlY3RhYmxlXCIpLmVhY2goIGZ1bmN0aW9uKClcblx0XHR7XG5cdFx0XHR2YXIgcmF0ZUNvbnRhaW5lciA9IGpRdWVyeSh0aGlzKTtcblx0XHRcdGpRdWVyeShcIi5zdGFycyA+IHNwYW5cIiwgcmF0ZUNvbnRhaW5lcikuZWFjaCggZnVuY3Rpb24oKSB7XG5cdFx0XHRcdGpRdWVyeSh0aGlzKS5vbihcIm1vdXNlZW50ZXJcIiwgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0dmFyIHNlbGVjdGVkSW5kZXggPSBqUXVlcnkodGhpcykuaW5kZXgoKTtcblx0XHRcdFx0XHRqUXVlcnkoXCIuc3RhcnMgPiBzcGFuXCIscmF0ZUNvbnRhaW5lcikuZWFjaCggZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHRpZihqUXVlcnkodGhpcykuaW5kZXgoKSA8PSBzZWxlY3RlZEluZGV4KVxuXHRcdFx0XHRcdFx0XHRqUXVlcnkodGhpcykucmVtb3ZlQ2xhc3MoKS5hZGRDbGFzcyhcImZ1bGxcIik7XG5cdFx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0XHRcdGpRdWVyeSh0aGlzKS5yZW1vdmVDbGFzcygpLmFkZENsYXNzKFwiZW1wdHlcIik7XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdH0pO1xuXG5cdFx0XHRcdGpRdWVyeSh0aGlzKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdHZhciBzZWxlY3RlZEluZGV4ID0galF1ZXJ5KHRoaXMpLmluZGV4KCk7XG5cdFx0XHRcdFx0cmF0ZUNvbnRhaW5lci5kYXRhKCdyYXRpbmcnLCBzZWxlY3RlZEluZGV4KTtcblx0XHRcdFx0XHRqUXVlcnkoJ2lucHV0W25hbWU9cmF0aW5nXScsIHJhdGVDb250YWluZXIpLnZhbChzZWxlY3RlZEluZGV4KTtcblx0XHRcdFx0fSk7XG5cblx0XHRcdH0pO1xuXG5cdFx0XHRyYXRlQ29udGFpbmVyLm9uKFwibW91c2VsZWF2ZVwiLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0dmFyIHNlbGVjdGVkSW5kZXggPSBqUXVlcnkodGhpcykuZGF0YSgncmF0aW5nJyk7XG5cdFx0XHRcdGpRdWVyeShcIi5zdGFycyA+IHNwYW5cIixyYXRlQ29udGFpbmVyKS5lYWNoKCBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRpZihqUXVlcnkodGhpcykuaW5kZXgoKSA8PSBzZWxlY3RlZEluZGV4KVxuXHRcdFx0XHRcdFx0alF1ZXJ5KHRoaXMpLnJlbW92ZUNsYXNzKCkuYWRkQ2xhc3MoXCJmdWxsXCIpO1xuXHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRcdGpRdWVyeSh0aGlzKS5yZW1vdmVDbGFzcygpLmFkZENsYXNzKFwiZW1wdHlcIik7XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSk7XG5cblxuXHRcdH0pXG5cdH1cblxuXHR2YXIgaGFuZGxlVGh1bWJzID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0alF1ZXJ5KFwiLnRodW1icyA+IGJ1dHRvblwiKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKClcblx0XHR7XG5cdFx0XHRpZighalF1ZXJ5KHRoaXMpLmhhc0NsYXNzKFwibG9ja2VkLWFjdGl2ZVwiKSAmJiAhalF1ZXJ5KHRoaXMpLmhhc0NsYXNzKFwibG9ja2VkLWluYWN0aXZlXCIpKVxuXHRcdFx0e1xuXHRcdFx0XHRqUXVlcnkodGhpcykucGFyZW50KCkuZmluZChcImJ1dHRvblwiKS5mYWRlT3V0KCBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRqUXVlcnkodGhpcykuZmFkZUluKFwiZmFzdFwiKTtcblx0XHRcdFx0fSlcblx0XHRcdFx0alF1ZXJ5KHRoaXMpLnBhcmVudCgpLmZpbmQoXCJidXR0b25cIikuYWRkQ2xhc3MoXCJsb2NrZWQtaW5hY3RpdmVcIilcblx0XHRcdFx0alF1ZXJ5KHRoaXMpLmFkZENsYXNzKFwiYWN0aXZlXCIpLnJlbW92ZUNsYXNzKFwibG9ja2VkLWluYWN0aXZlXCIpLmFkZENsYXNzKFwibG9ja2VkLWFjdGl2ZVwiKVxuXG5cdFx0XHRcdHZhciBkaXIgPSBcImRvd25cIlxuXHRcdFx0XHRpZihqUXVlcnkodGhpcykuaGFzQ2xhc3MoXCJ1cFwiKSkgZGlyID0gXCJ1cFwiO1xuXHRcdFx0XHR2YXIgcG9zdElkID0galF1ZXJ5KHRoaXMpLmNsb3Nlc3QoXCIudGh1bWJ2b3RlXCIpLmRhdGEoXCJwb3N0aWRcIik7XG5cdFx0XHRcdGpRdWVyeS5wb3N0KFwiL3dwLWpzb24vaWthbC92MS9wb3N0L3ZvdGVcIiwge1wicG9zdFwiOiBwb3N0SWQsIFwiZGlyXCI6IGRpcn0pO1xuXHRcdFx0fVxuXHRcdH0pXG5cdH1cblxuXG5cdC8qIF9fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX18gcHVibGljIG1ldGhvZHMgKi9cblxuXHR2YXIgaW5pdCA9IGZ1bmN0aW9uKClcblx0e1xuXHRcdGhhbmRsZURvUmF0ZVJlbGVhc2UoKTtcblx0XHRzY3JvbGxUb0NvbW1lbnRzKCk7XG5cdFx0YXBwbHlTd2l0Y2hlcigpO1xuXHRcdGRyYXdTdGFycygpO1xuXHRcdGhhbmRsZVJhdGVJbkZvcm0oKTtcblx0XHRoYW5kbGVUaHVtYnMoKTtcblx0fTtcblxuXHRpbml0KCk7XG59O1xuXG52YXIgY29tbWVudHM7XG4iLCJjb21wYW5pZXMgPSBmdW5jdGlvbiAoKVxueyAgIFxuXHRmdW5jdGlvbiBoYW5kbGVGaXhlZEZpbHRlcigpXG5cdHtcbiAgICAgICAgdmFyIHBhZ2VIZWFkID0gIGpRdWVyeShcIi5wYWdlLWhlYWRcIik7XG4gICAgICAgIHZhciBwYWdlQ29udGVudCA9ICBqUXVlcnkoXCIucGFnZS1jb250ZW50XCIpO1xuICAgICAgICB2YXIgZmlsdGVyID0gIGpRdWVyeShcIi5maWx0ZXJzXCIpO1xuICAgICAgICBcbiAgICAgICAgaWYgKCFmaWx0ZXIubGVuZ3RoKVxuICAgICAgICAgICAgcmV0dXJuO1xuXG5cdFx0dmFyIHN3aXRjaEF0ID0gcGFnZUhlYWQub2Zmc2V0KCkudG9wICsgcGFnZUhlYWQub3V0ZXJIZWlnaHQoKTtcblxuICAgIFxuICAgICAgICBpZiAoc3dpdGNoQXQgPCBqUXVlcnkod2luZG93KS5zY3JvbGxUb3AoKSAmJiB3aW5kb3cuaW5uZXJXaWR0aCA+PSA3NjgpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGlmICghZmlsdGVyLmhhc0NsYXNzKFwiZml4ZWRcIikpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdmFyIGRpZmYgPSBmaWx0ZXIub3V0ZXJIZWlnaHQoKSAtICAoc3dpdGNoQXQgLSBmaWx0ZXIub2Zmc2V0KCkudG9wKTtcbiAgICAgICAgICAgICAgICBwYWdlQ29udGVudFxuICAgICAgICAgICAgICAgICAgICAuY3NzKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwicGFkZGluZy10b3BcIiA6IGRpZmYgKyBcInB4XCJcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZpbHRlclxuICAgICAgICAgICAgICAgIC5hZGRDbGFzcyhcImZpeGVkXCIpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2VcbiAgICAgICAge1xuICAgICAgICAgICAgZmlsdGVyLnJlbW92ZUNsYXNzKFwiZml4ZWRcIik7XG4gICAgICAgICAgICBwYWdlQ29udGVudFxuICAgICAgICAgICAgICAgIC5jc3Moe1xuICAgICAgICAgICAgICAgICAgICBcInBhZGRpbmctdG9wXCIgOiBcIlwiXG4gICAgICAgICAgICAgICAgfSk7ICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICB9ICAgIFxuICAgIFxuICAgIFxuICAgIGZ1bmN0aW9uIGZpbHRlckdyaWQoKVxuICAgIHtcbiAgICAgICAgdmFyIGZpbHRlckl0ZW1zID0galF1ZXJ5KFwiLmNvbXBhbmllcyAuZmlsdGVycyAuaXRlbXMtbGlzdCAuY2F0ZWdvcnlcIik7XG4gICAgICAgIHZhciBncmlkSXRlbXMgPSBqUXVlcnkoXCIuY29tcGFuaWVzIC50aWxlcy1ncmlkIC5ncmlkLWl0ZW1cIik7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgIGlmICghZmlsdGVySXRlbXMubGVuZ3RoIHx8ICFncmlkSXRlbXMubGVuZ3RoKSB7IHJldHVybjsgfVxuICAgICAgICAgICAgICAgIFxuICAgICAgICB2YXIgc2VsZWN0ZWRDYXRlZ29yaWVzID0gW107XG4gICAgICAgIFxuICAgICAgICBmaWx0ZXJJdGVtcy5lYWNoKGZ1bmN0aW9uKClcbiAgICAgICAge1xuICAgICAgICAgICAgY3VycmVudEl0ZW0gPSBqUXVlcnkodGhpcyk7ICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmIChjdXJyZW50SXRlbS5oYXNDbGFzcyhcImFjdGl2ZVwiKSlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpZiAoY3VycmVudEl0ZW0uYXR0cihcImRhdGEtZmlsdGVyXCIpID09IFwiYWxsXCIpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZENhdGVnb3JpZXMgPSBcImFsbFwiO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkQ2F0ZWdvcmllcy5wdXNoKGN1cnJlbnRJdGVtLmF0dHIoXCJkYXRhLWZpbHRlclwiKSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgXG4gICAgICAgIGlmIChzZWxlY3RlZENhdGVnb3JpZXMgPT0gXCJhbGxcIilcbiAgICAgICAge1xuICAgICAgICAgICAgZ3JpZEl0ZW1zLmFkZENsYXNzKFwiYWN0aXZlXCIpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2VcbiAgICAgICAge1xuICAgICAgICAgICAgZ3JpZEl0ZW1zLmVhY2goZnVuY3Rpb24oKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGN1cnJlbnRJdGVtID0galF1ZXJ5KHRoaXMpO1xuICAgICAgICAgICAgICAgIHZhciBjYXRlZ29yaWVzID0gY3VycmVudEl0ZW0uYXR0cihcImRhdGEtaXRlbS1jYXRlZ29yaWVzXCIpO1xuXG4gICAgICAgICAgICAgICAgdmFyIGl0ZW1DYXRlZ29yaWVzID0gY2F0ZWdvcmllcy5zcGxpdChcIixcIik7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdmFyIGNvbnRhaW5BdExlYXN0T25lRmlsdGVyID0gc2VsZWN0ZWRDYXRlZ29yaWVzLnNvbWUoZnVuY3Rpb24gKHYpIHsgcmV0dXJuIGl0ZW1DYXRlZ29yaWVzLmluZGV4T2YodikgPj0gMDt9ICk7XG5cbiAgICAgICAgICAgICAgICBpZiAoY29udGFpbkF0TGVhc3RPbmVGaWx0ZXIpXG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRJdGVtLmFkZENsYXNzKFwiYWN0aXZlXCIpO1xuICAgICAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudEl0ZW0ucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIik7XG5cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfSAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgZnVuY3Rpb24gaGFuZGxlRmlsdGVyKGlzQ29uZGl0aW9uT1IpXG4gICAge1xuICAgICAgICBpc0NvbmRpdGlvbk9SID0gdHlwZW9mIGlzQ29uZGl0aW9uT1IgIT09IFwidW5kZWZpbmVkXCIgPyBpc0NvbmRpdGlvbk9SIDogZmFsc2U7XG4gICAgICAgIFxuICAgICAgICB2YXIgZmlsdGVySXRlbXMgPSBqUXVlcnkoXCIuY29tcGFuaWVzIC5maWx0ZXJzIC5pdGVtcy1saXN0IC5jYXRlZ29yeVwiKTtcbiAgICAgICAgXG4gICAgICAgIGlmICghZmlsdGVySXRlbXMubGVuZ3RoKSB7IHJldHVybjsgfVxuICAgICAgICBcbiAgICAgICAgZmlsdGVySXRlbXMub24oXCJjbGlja1wiLCBmdW5jdGlvbihlKVxuICAgICAgICB7XG4gICAgICAgICAgICBjdXJyZW50SXRlbSA9IGpRdWVyeSh0aGlzKTtcbiAgICAgICAgICAgIGNpcnJlbnRJdGVtRmlsdGVyID0gY3VycmVudEl0ZW0uYXR0cihcImRhdGEtZmlsdGVyXCIpOyAgICAgICAgXG4gICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIGlmIChjaXJyZW50SXRlbUZpbHRlciA9PSBcImFsbFwiKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGlmIChjdXJyZW50SXRlbS5oYXNDbGFzcyhcImFjdGl2ZVwiKSkgXG4gICAgICAgICAgICAgICAgeyBcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmaWx0ZXJJdGVtcy5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudEl0ZW0uYWRkQ2xhc3MoXCJhY3RpdmVcIik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGlmIChpc0NvbmRpdGlvbk9SKSAvL29ubHkgbGFzdCBzZWxlY3RlZFxuICAgICAgICAgICAgICAgICAgICBmaWx0ZXJJdGVtcy5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICAgICAgICAgICBlbHNlIC8vYWxsIHNlbGVjdGVkIGV4Y2VwdGVkIFwiYWxsXCJcbiAgICAgICAgICAgICAgICAgICAgZmlsdGVySXRlbXMuZmlsdGVyKCdbZGF0YS1maWx0ZXI9XCJhbGxcIl0nKS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTsgXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgaWYgKGN1cnJlbnRJdGVtLmhhc0NsYXNzKFwiYWN0aXZlXCIpKVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGZpbHRlckl0ZW1zLmZpbHRlcihcIi5hY3RpdmVcIikubGVuZ3RoID4gMSlcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVudEl0ZW0ucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIik7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudEl0ZW0uYWRkQ2xhc3MoXCJhY3RpdmVcIik7ICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGZpbHRlckdyaWQoKTtcbiAgICAgICAgfSk7XG4gICAgICAgIFxuICAgICAgICBmaWx0ZXJHcmlkKCk7XG4gICAgfSAgICBcbiAgICBcbi8qXG5cdGZ1bmN0aW9uIG9uU2Nyb2xsKClcblx0e1x0XG5cdFx0dmFyIGlzU2Nyb2xsaW5nICA9IGZhbHNlO1xuXG5cdFx0alF1ZXJ5KHdpbmRvdykub24oXCJzY3JvbGxcIiwgZnVuY3Rpb24gKCkgXG5cdFx0e1xuXHRcdFx0aWYgKGlzU2Nyb2xsaW5nKSB7IHJldHVybjsgfVxuXG5cdFx0XHRpc1Njcm9sbGluZyA9IHRydWU7XG5cblx0XHRcdHdpbmRvdy5zZXRUaW1lb3V0KGZ1bmN0aW9uKClcblx0XHRcdHtcblx0XHRcdFx0Ly9wbGFjZSBmdW5jdGlvbnMgaGVyZVx0XG4gICAgICAgICAgICAgICAgaGFuZGxlRml4ZWRGaWx0ZXIoKTtcblxuXHRcdFx0XHRpc1Njcm9sbGluZyA9IGZhbHNlO1xuXHRcdFx0fSwgMTAgKTtcblx0XHR9KTtcbiAgICAgICAgXG4gICAgICAgIGpRdWVyeSh3aW5kb3cpLnRyaWdnZXIoXCJzY3JvbGxcIik7ICAgICAgICBcblx0fVx0XG4gICBcbiAgICBcbiAgICBmdW5jdGlvbiBvblJlc2l6ZSgpXG4gICAge1x0XG4gICAgICAgIHZhciBpc1Jlc2l6aW5nICA9IGZhbHNlO1xuXG4gICAgICAgIGpRdWVyeSh3aW5kb3cpLm9uKFwicmVzaXplXCIsIGZ1bmN0aW9uICgpIFxuICAgICAgICB7XG4gICAgICAgICAgICBpZiAoaXNSZXNpemluZykgeyByZXR1cm47IH1cdFx0XG4gICAgICAgICAgICBpc1Jlc2l6aW5nID0gdHJ1ZTtcblxuICAgICAgICAgICAgd2luZG93LnNldFRpbWVvdXQoZnVuY3Rpb24oKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIC8vcGxhY2UgZnVuY3Rpb25zIGhlcmVcdFxuICAgICAgICAgICAgICAgIGhhbmRsZUZpeGVkRmlsdGVyKCk7XG5cbiAgICAgICAgICAgICAgICBpc1Jlc2l6aW5nID0gZmFsc2U7XG4gICAgICAgICAgICB9LCAxMCApO1xuICAgICAgICB9KTtcdFxuICAgICAgICAgICAgICAgXG5cdFx0alF1ZXJ5KHdpbmRvdykudHJpZ2dlcihcInJlc2l6ZVwiKTtcbiAgICB9ICAgIFxuKi9cblxuXHR2YXIgaW5pdCA9IGZ1bmN0aW9uKClcblx0e1xuICAgICAgICBpZiAoIWpRdWVyeShcIi5wYWdlLW1haW4uY29tcGFuaWVzXCIpLmxlbmd0aCkgeyByZXR1cm47IH1cbiAgICAgICAgXG5cdFx0aGFuZGxlRmlsdGVyKHRydWUpO1xuICAgICAgICBvblNjcm9sbCgpO1xuICAgICAgICBvblJlc2l6ZSgpO1xuXG5cdH07XG5cblx0aW5pdCgpO1xufTtcblxudmFyIGNvbXBhbmllcztcblxuIiwicHJvZHVjdEZpbHRlciA9IGZ1bmN0aW9uICgpXG57XG5cdHZhciBzb3J0QXR0cmlidXRlID0gbnVsbDtcblx0dmFyIHNvcnRUeXBlID0gbnVsbDtcblx0dmFyIHNvcnREaXJlY3Rpb24gPSBudWxsO1xuXG5cdHZhciBhcHBseVNsaWRlcnMgPSBmdW5jdGlvbigpXG5cdHtcblx0XHRqUXVlcnkoXCIuc2xpZGVyW2RhdGEtZmlsdGVyLW5hbWVdXCIpLmVhY2goIGZ1bmN0aW9uKClcblx0XHR7XG5cdFx0XHR2YXIgb3B0aW9ucyA9IHt9O1xuXHRcdFx0dmFyIHNsaWRlckNvbnRhaW5lciA9IGpRdWVyeSh0aGlzKTtcblxuXHRcdFx0aWYoalF1ZXJ5KHRoaXMpLmRhdGEoXCJmaWx0ZXItdHlwZVwiKSA9PSBcImJldHdlZW5cIilcblx0XHRcdHtcblx0XHRcdFx0b3B0aW9ucy5yYW5nZSA9IHRydWU7XG5cdFx0XHRcdG9wdGlvbnMudmFsdWVzID0gW107XG5cblx0XHRcdFx0aWYodHlwZW9mKGpRdWVyeSh0aGlzKS5kYXRhKFwidmFsdWUtbWluXCIpKSAhPSBcInVuZGVmaW5lZFwiICYmIGpRdWVyeSh0aGlzKS5kYXRhKFwidmFsdWUtbWluXCIpID49IDAgJiYgalF1ZXJ5KHRoaXMpLmRhdGEoXCJ2YWx1ZS1tYXhcIikpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRvcHRpb25zLnZhbHVlcy5wdXNoKGpRdWVyeSh0aGlzKS5kYXRhKFwidmFsdWUtbWluXCIpKTtcblx0XHRcdFx0XHRvcHRpb25zLnZhbHVlcy5wdXNoKGpRdWVyeSh0aGlzKS5kYXRhKFwidmFsdWUtbWF4XCIpKTtcblx0XHRcdFx0fTtcblx0XHRcdH1cblx0XHRcdGVsc2UgaWYoalF1ZXJ5KHRoaXMpLmRhdGEoXCJmaWx0ZXItdHlwZVwiKSA9PSBcInRvXCIpXG5cdFx0XHR7XG5cdFx0XHRcdG9wdGlvbnMucmFuZ2UgPSBcIm1pblwiO1xuXG5cdFx0XHRcdGlmKGpRdWVyeSh0aGlzKS5kYXRhKFwidmFsdWVcIikpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRvcHRpb25zLnZhbHVlID0galF1ZXJ5KHRoaXMpLmRhdGEoXCJ2YWx1ZVwiKTtcblx0XHRcdFx0fTtcblx0XHRcdH07XG5cblx0XHRcdGlmKGpRdWVyeSh0aGlzKS5kYXRhKFwibWluXCIpICE9IG51bGwpXG5cdFx0XHR7XG5cdFx0XHRcdG9wdGlvbnMubWluID0galF1ZXJ5KHRoaXMpLmRhdGEoXCJtaW5cIik7XG5cdFx0XHR9O1xuXG5cdFx0XHRpZihqUXVlcnkodGhpcykuZGF0YShcIm1heFwiKSlcblx0XHRcdHtcblx0XHRcdFx0b3B0aW9ucy5tYXggPSBqUXVlcnkodGhpcykuZGF0YShcIm1heFwiKTtcblx0XHRcdH07XG5cblxuXG5cdFx0XHR2YXIgcG9pbnRzVmFsdWVzID0gW107XG5cblx0XHRcdGlmKGpRdWVyeSh0aGlzKS5kYXRhKFwic3RlcFwiKSlcblx0XHRcdHtcblxuXHRcdFx0XHR2YXIgc3RlcFZhbCA9IGpRdWVyeSh0aGlzKS5kYXRhKFwic3RlcFwiKTtcblx0XHRcdFx0dmFyIGxlbmd0aCA9IE1hdGguY2VpbCgob3B0aW9ucy5tYXgtb3B0aW9ucy5taW4pL3N0ZXBWYWwpO1xuXHRcdFx0XHR2YXIgc3RhcnQgPSBNYXRoLmZsb29yKG9wdGlvbnMubWluL3N0ZXBWYWwpO1xuXHRcdFx0XHRmb3IodmFyIGkgPSBzdGFydDsgaSA8bGVuZ3RoK3N0YXJ0OyBpKyspXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRpZihpID09IHN0YXJ0ICYmIG9wdGlvbnMubWluIDwgaSpzdGVwVmFsKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdHBvaW50c1ZhbHVlcy5wdXNoKG9wdGlvbnMubWluKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0cG9pbnRzVmFsdWVzLnB1c2goaSpzdGVwVmFsKVxuXHRcdFx0XHRcdGlmKGkgPT0gbGVuZ3RoK3N0YXJ0LTEgJiYgb3B0aW9ucy5tYXggPiBpKnN0ZXBWYWwpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0cG9pbnRzVmFsdWVzLnB1c2gob3B0aW9ucy5tYXgpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXG5cblx0XHRcdFx0aWYocG9pbnRzVmFsdWVzLmxlbmd0aClcblx0XHRcdFx0e1xuXHRcdFx0XHRcdG9wdGlvbnMubWluID0gMDtcblx0XHRcdFx0XHRvcHRpb25zLm1heCA9IHBvaW50c1ZhbHVlcy5sZW5ndGgtMTtcblxuXHRcdFx0XHRcdGlmKG9wdGlvbnMudmFsdWUpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0b3B0aW9ucy52YWx1ZSA9IHBvaW50c1ZhbHVlcy5pbmRleE9mKG9wdGlvbnMudmFsdWUpO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdGlmKG9wdGlvbnMudmFsdWVzKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdG9wdGlvbnMudmFsdWVzWzBdID0gcG9pbnRzVmFsdWVzLmluZGV4T2Yob3B0aW9ucy52YWx1ZXNbMF0pO1xuXHRcdFx0XHRcdFx0b3B0aW9ucy52YWx1ZXNbMV0gPSBwb2ludHNWYWx1ZXMuaW5kZXhPZihvcHRpb25zLnZhbHVlc1sxXSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cblx0XHRcdH1cblxuXG5cdFx0XHRpZihqUXVlcnkodGhpcykuZGF0YShcInByb2dyZXNzaXZlXCIpID09IHRydWUpXG5cdFx0XHR7XG5cdFx0XHRcdHZhciByYW5nZXNDb25maWcgPVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0XHRbXG5cdFx0XHRcdFx0XHR7XCJtaW5cIjogMCwgXCJtYXhcIjogOTAwLCBcInN0ZXBcIjogMTAwfSxcblx0XHRcdFx0XHRcdHtcIm1pblwiOiAxMDAwLCBcIm1heFwiOiA5MDAwLCBcInN0ZXBcIjogMTAwMH0sXG5cdFx0XHRcdFx0XHR7XCJtaW5cIjogMTAwMDAsIFwibWF4XCI6IDQ4MDAwLCBcInN0ZXBcIjogMjAwMH0sXG5cdFx0XHRcdFx0XHR7XCJtaW5cIjogNTAwMDAsIFwibWF4XCI6IDUwMDAwMDAwLCBcInN0ZXBcIjogNTAwMH0sXG5cdFx0XHRcdFx0XSxcblx0XHRcdFx0XHRpbnZlc3RtZW50czogW1xuXHRcdFx0XHRcdFx0e1wibWluXCI6IDAsIFwibWF4XCI6IDUwMDAsIFwic3RlcFwiOiA1MDB9LFxuXHRcdFx0XHRcdFx0e1wibWluXCI6IDYwMDAsIFwibWF4XCI6IDE5MDAwLCBcInN0ZXBcIjogMTAwMH0sXG5cdFx0XHRcdFx0XHR7XCJtaW5cIjogMjAwMDAsIFwibWF4XCI6IDk1MDAwLCBcInN0ZXBcIjogNTAwMH0sXG5cdFx0XHRcdFx0XHR7XCJtaW5cIjogMTAwMDAwLCBcIm1heFwiOiA0NzUwMDAsIFwic3RlcFwiOiAyNTAwMH0sXG5cdFx0XHRcdFx0XHR7XCJtaW5cIjogNTAwMDAwLCBcIm1heFwiOiAxMDAwMDAwMCwgXCJzdGVwXCI6IDUwMDAwfSxcblx0XHRcdFx0XHRdLFxuXHRcdFx0XHRcdGludmVzdG1lbnRzTW9udGhzOlxuXHRcdFx0XHRcdFtcblx0XHRcdFx0XHRcdHtcIm1pblwiOiAwLCBcIm1heFwiOiAyMywgXCJzdGVwXCI6IDF9LFxuXHRcdFx0XHRcdFx0e1wibWluXCI6IDI0LCBcIm1heFwiOiA2MDAsIFwic3RlcFwiOiAxMn1cblx0XHRcdFx0XHRdXG5cdFx0XHRcdH07XG5cblx0XHRcdFx0dmFyIHJhbmdlcyA9IHJhbmdlc0NvbmZpZy5kZWZhdWx0O1xuXG5cdFx0XHRcdGlmKGpRdWVyeSh0aGlzKS5kYXRhKFwicHJvZ3Jlc3NpdmUtdmFsdWVzXCIpICYmIHJhbmdlc0NvbmZpZ1tqUXVlcnkodGhpcykuZGF0YShcInByb2dyZXNzaXZlLXZhbHVlc1wiKV0pXG5cdFx0XHRcdFx0cmFuZ2VzID0gcmFuZ2VzQ29uZmlnW2pRdWVyeSh0aGlzKS5kYXRhKFwicHJvZ3Jlc3NpdmUtdmFsdWVzXCIpXTtcblxuXHRcdFx0XHRmb3IoaT0wOyBpIDwgcmFuZ2VzLmxlbmd0aDsgaSsrKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0dmFyIGN1cnJlbnQgPSAwO1xuXHRcdFx0XHRcdGlmKG9wdGlvbnMubWluID4gcmFuZ2VzW2ldLm1heCB8fCBvcHRpb25zLm1heCA8IHJhbmdlc1tpXS5taW4pXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0Y29udGludWU7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRjdXJyZW50ID0gcmFuZ2VzW2ldLm1pbjtcblx0XHRcdFx0XHRcdHdoaWxlKGN1cnJlbnQgPCBvcHRpb25zLm1pbilcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0Y3VycmVudCArPSByYW5nZXNbaV0uc3RlcDtcblx0XHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdFx0aWYob3B0aW9ucy5taW4gPiByYW5nZXNbaV0ubWluICYmIGN1cnJlbnQtcmFuZ2VzW2ldLnN0ZXAgPCBvcHRpb25zLm1pbilcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0cG9pbnRzVmFsdWVzLnB1c2gob3B0aW9ucy5taW4pO1xuXHRcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0XHR3aGlsZShjdXJyZW50IDw9IG9wdGlvbnMubWF4ICYmIGN1cnJlbnQgPD0gcmFuZ2VzW2ldLm1heClcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0cG9pbnRzVmFsdWVzLnB1c2goY3VycmVudCk7XG5cdFx0XHRcdFx0XHRcdGN1cnJlbnQgKz0gcmFuZ2VzW2ldLnN0ZXA7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cblxuXHRcdFx0XHRpZihwb2ludHNWYWx1ZXMubGVuZ3RoKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0b3B0aW9ucy5taW4gPSAwO1xuXHRcdFx0XHRcdG9wdGlvbnMubWF4ID0gcG9pbnRzVmFsdWVzLmxlbmd0aC0xO1xuXG5cdFx0XHRcdFx0aWYob3B0aW9ucy52YWx1ZSlcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRvcHRpb25zLnZhbHVlID0gcG9pbnRzVmFsdWVzLmluZGV4T2Yob3B0aW9ucy52YWx1ZSk7XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0aWYob3B0aW9ucy52YWx1ZXMpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0b3B0aW9ucy52YWx1ZXNbMF0gPSBwb2ludHNWYWx1ZXMuaW5kZXhPZihvcHRpb25zLnZhbHVlc1swXSk7XG5cdFx0XHRcdFx0XHRvcHRpb25zLnZhbHVlc1sxXSA9IHBvaW50c1ZhbHVlcy5pbmRleE9mKG9wdGlvbnMudmFsdWVzWzFdKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblxuXG5cdFx0XHR9XG5cblx0XHRcdG9wdGlvbnMuc2xpZGUgPSBmdW5jdGlvbiggZXZlbnQsIHVpIClcblx0XHRcdHtcblx0XHRcdFx0aWYodWkudmFsdWUgfHwgcG9pbnRzVmFsdWVzW3VpLnZhbHVlXSlcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHZhciBleFZhbCA9IHVpLnZhbHVlO1xuXG5cdFx0XHRcdFx0aWYocG9pbnRzVmFsdWVzLmxlbmd0aClcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRleFZhbCA9IHBvaW50c1ZhbHVlc1t1aS52YWx1ZV07XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0c2xpZGVyQ29udGFpbmVyLmZpbmQoXCIudmFsdWVcIikuaHRtbCggZm9ybWF0TnVtYmVyKGV4VmFsKSApO1xuXHRcdFx0XHRcdHNsaWRlckNvbnRhaW5lci5wYXJlbnRzKFwiZm9ybVwiKS5maXJzdCgpLmZpbmQoXCJpbnB1dFtuYW1lPSdcIiArIHNsaWRlckNvbnRhaW5lci5kYXRhKFwiZmlsdGVyLW5hbWVcIikgKyBcIiddXCIpLnZhbChleFZhbClcblx0XHRcdFx0fVxuXHRcdFx0XHRpZih1aS52YWx1ZXMpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHR2YXIgZXhWYWxNaW4gPSB1aS52YWx1ZXNbMF07XG5cdFx0XHRcdFx0dmFyIGV4VmFsTWF4ID0gdWkudmFsdWVzWzFdO1xuXG5cdFx0XHRcdFx0aWYocG9pbnRzVmFsdWVzLmxlbmd0aClcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRleFZhbE1pbiA9IHBvaW50c1ZhbHVlc1t1aS52YWx1ZXNbMF1dO1xuXHRcdFx0XHRcdFx0ZXhWYWxNYXggPSBwb2ludHNWYWx1ZXNbdWkudmFsdWVzWzFdXTtcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRzbGlkZXJDb250YWluZXIuZmluZChcIi52YWx1ZVwiKS5odG1sKCBmb3JtYXROdW1iZXIoZXhWYWxNaW4pICsgXCIgLSBcIiArIGZvcm1hdE51bWJlcihleFZhbE1heCkgKTtcblx0XHRcdFx0XHRzbGlkZXJDb250YWluZXIucGFyZW50cyhcImZvcm1cIikuZmlyc3QoKS5maW5kKFwiaW5wdXRbbmFtZT0nbWluLVwiICsgc2xpZGVyQ29udGFpbmVyLmRhdGEoXCJmaWx0ZXItbmFtZVwiKSArIFwiJ11cIikudmFsKGV4VmFsTWluKVxuXHRcdFx0XHRcdHNsaWRlckNvbnRhaW5lci5wYXJlbnRzKFwiZm9ybVwiKS5maXJzdCgpLmZpbmQoXCJpbnB1dFtuYW1lPSdtYXgtXCIgKyBzbGlkZXJDb250YWluZXIuZGF0YShcImZpbHRlci1uYW1lXCIpICsgXCInXVwiKS52YWwoZXhWYWxNYXgpXG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0b3B0aW9ucy5jaGFuZ2UgPSBmdW5jdGlvbiggZXZlbnQsIHVpIClcblx0XHRcdHtcblx0XHRcdFx0c2xpZGVyQ29udGFpbmVyLnBhcmVudHMoXCJmb3JtXCIpLmZpcnN0KCkudHJpZ2dlcihcImlrYWw6ZmlsdGVyOmNoYW5nZWRcIik7XG5cdFx0XHR9XG5cblx0XHRcdGlmKHBvaW50c1ZhbHVlcylcblx0XHRcdHtcblx0XHRcdFx0aWYocG9pbnRzVmFsdWVzW29wdGlvbnMubWluXSlcblx0XHRcdFx0XHRzbGlkZXJDb250YWluZXIuZmluZChcIi5yYW5nZS1taW4tdmFsdWVcIikuaHRtbChwb2ludHNWYWx1ZXNbb3B0aW9ucy5taW5dKTtcblx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdHNsaWRlckNvbnRhaW5lci5maW5kKFwiLnJhbmdlLW1pbi12YWx1ZVwiKS5odG1sKHBvaW50c1ZhbHVlc1sob3B0aW9ucy5taW4gKyAxKV0gKTtcblxuXHRcdFx0XHRzbGlkZXJDb250YWluZXIuZmluZChcIi5yYW5nZS1tYXgtdmFsdWVcIikuaHRtbChwb2ludHNWYWx1ZXNbb3B0aW9ucy5tYXhdKTtcblxuXHRcdFx0fVxuXHRcdFx0ZWxzZVxuXHRcdFx0e1xuXHRcdFx0XHRzbGlkZXJDb250YWluZXIuZmluZChcIi5yYW5nZS1taW4tdmFsdWVcIikuaHRtbChvcHRpb25zLm1pbik7XG5cdFx0XHRcdHNsaWRlckNvbnRhaW5lci5maW5kKFwiLnJhbmdlLW1heC12YWx1ZVwiKS5odG1sKG9wdGlvbnMubWF4KTtcblxuXHRcdFx0fVxuXHRcdFx0c2xpZGVyQ29udGFpbmVyLmZpbmQoXCIuc2xpZGVyLWJhclwiKS5zbGlkZXIob3B0aW9ucyk7XG5cdFx0fSk7XG5cblx0fVxuXG5cdHZhciBhcHBseUlucHV0cyA9IGZ1bmN0aW9uKClcblx0e1xuXHRcdGpRdWVyeShcIi5maWx0ZXJzIGZvcm1cIikuZWFjaCggZnVuY3Rpb24oKVxuXHRcdHtcblx0XHRcdHZhciBmaWx0ZXJGb3JtID0galF1ZXJ5KHRoaXMpO1xuXHRcdFx0alF1ZXJ5KFwiaW5wdXRbdHlwZT1jaGVja2JveF0sIGlucHV0W3R5cGU9cmFkaW9dLCBpbnB1dFt0eXBlPXRleHRdXCIsIGZpbHRlckZvcm0pLm9uKFwiY2hhbmdlXCIsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRmaWx0ZXJGb3JtLnRyaWdnZXIoXCJpa2FsOmZpbHRlcjpjaGFuZ2VkXCIpO1xuXHRcdFx0fSlcblx0XHR9KTtcblx0fVxuXG5cdHZhciBwcmVwYXJlRm9ybXMgPSBmdW5jdGlvbigpXG5cdHtcblx0XHRqUXVlcnkoXCIuZmlsdGVycyBmb3JtXCIpLmVhY2goIGZ1bmN0aW9uKClcblx0XHR7XG5cdFx0XHR2YXIgZmlsdGVyRm9ybSA9IGpRdWVyeSh0aGlzKTtcblxuXHRcdFx0ZmlsdGVyRm9ybS5vbihcImlrYWw6ZmlsdGVyOmNoYW5nZWRcIiwgZnVuY3Rpb24oKVxuXHRcdFx0e1xuXHRcdFx0XHR2YXIgbGlzdCA9IGZpbHRlckZvcm0ucGFyZW50cyhcIi5wcm9kdWN0LWxpc3RcIikuZmlyc3QoKS5maW5kKFwiLnByb2R1Y3RzLmxpc3RcIik7XG5cdFx0XHRcdGxpc3QuYWRkQ2xhc3MoXCJsb2FkaW5nXCIpO1xuXHRcdFx0XHRpZihqUXVlcnkoXCIjcGFnZVwiKS5kYXRhKFwicHJvZHVjdC1zbHVnXCIpKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0dmFyIHVybCA9IFwiL3dwLWNvbnRlbnQvdGhlbWVzL2lrYWxrdWxhdG9yL3Byb2R1Y3QtYXBpL3Byb2R1Y3RzLnBocD9wdHlwZT1cIiArIGpRdWVyeShcIiNwYWdlXCIpLmRhdGEoXCJwcm9kdWN0LXNsdWdcIikgKyBcIiZcIiArIGZpbHRlckZvcm0uc2VyaWFsaXplKCk7XG5cdFx0XHRcdFx0alF1ZXJ5LmdldCh1cmwsIGZ1bmN0aW9uKHJlc3BvbnNlKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGxpc3QuZmlyc3QoKS5odG1sKHJlc3BvbnNlKTtcblx0XHRcdFx0XHRcdHJlZnJlc2hTb3J0aW5nKGxpc3QpO1xuXHRcdFx0XHRcdFx0bGlzdC5yZW1vdmVDbGFzcyhcImxvYWRpbmdcIik7XG5cdFx0XHRcdFx0fSlcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fSk7XG5cdH1cblxuXHR2YXIgYXBwbHlTb3J0aW5nID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0alF1ZXJ5KFwiLnByb2R1Y3RzLmxpc3RcIikuZWFjaCggZnVuY3Rpb24oKVxuXHRcdHtcblx0XHRcdHZhciBsaXN0ID0galF1ZXJ5KHRoaXMpO1xuXHRcdFx0dmFyIGhlYWRlciA9IGxpc3QucGFyZW50KCkuZmluZChcIi5wcm9kdWN0cy5oZWFkZXJcIik7XG5cdFx0XHRqUXVlcnkoXCJ1bCBsaSBkaXYuc29ydGFibGUgYVwiLCBoZWFkZXIpXG4gICAgICAgICAgICAgICAgLmFwcGVuZCgnPHNwYW4gY2xhc3M9XCJhcnJvd1wiPjwvc3Bhbj4nKVxuICAgICAgICAgICAgICAgIC5vbihcImNsaWNrXCIsIGpRdWVyeS5wcm94eShzb3J0TGlzdCwgdGhpcywgbGlzdCkpO1xuXHRcdH0pXG5cdH1cblxuXHR2YXIgc29ydExpc3QgPSBmdW5jdGlvbihsaXN0LCBldmVudClcblx0e1xuXHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cblx0XHR2YXIgc29ydGJ5ID0galF1ZXJ5KGV2ZW50LnRhcmdldCkuZGF0YShcInNvcnQtYnlcIik7XG5cdFx0dmFyIHNvcnR0eXAgPSBqUXVlcnkoZXZlbnQudGFyZ2V0KS5kYXRhKFwic29ydC10eXBlXCIpO1xuXHRcdHZhciBzb3J0ZGlyID0galF1ZXJ5KGV2ZW50LnRhcmdldCkuZGF0YShcInNvcnQtZGlyXCIpO1xuXG5cdFx0aWYoc29ydEF0dHJpYnV0ZSA9PSBzb3J0YnkpXG5cdFx0e1xuXHRcdFx0aWYoc29ydERpcmVjdGlvbiAhPSBcImRlc2NcIilcblx0XHRcdFx0c29ydERpcmVjdGlvbiA9IFwiZGVzY1wiO1xuXHRcdFx0ZWxzZVxuXHRcdFx0XHRzb3J0RGlyZWN0aW9uID0gXCJhc2NcIjtcblx0XHR9XG5cdFx0ZWxzZVxuXHRcdHtcblx0XHRcdGlmKHNvcnRkaXIgPT0gXCJkZXNjXCIpXG5cdFx0XHRcdHNvcnREaXJlY3Rpb24gPSBcImRlc2NcIjtcblx0XHRcdGVsc2Vcblx0XHRcdFx0c29ydERpcmVjdGlvbiA9IFwiYXNjXCI7XG5cdFx0fVxuXG5cdFx0alF1ZXJ5KFwiZGl2LnNvcnRhYmxlXCIpLnJlbW92ZUNsYXNzKFwiZGVzYyBhc2NcIik7XG5cbiAgICAgICAgalF1ZXJ5KGV2ZW50LnRhcmdldCkuY2xvc2VzdChcImRpdi5zb3J0YWJsZVwiKVxuICAgICAgICAgICAgLmFkZENsYXNzKHNvcnREaXJlY3Rpb24pO1xuXG5cdFx0c29ydFR5cGUgPSBzb3J0dHlwO1xuXHRcdHNvcnRBdHRyaWJ1dGUgPSBzb3J0Ynk7XG5cblx0XHRqUXVlcnkoXCIuc29ydGVkLWZpZWxkXCIpLnJlbW92ZUNsYXNzKFwic29ydGVkLWZpZWxkXCIpO1xuXHRcdGpRdWVyeShcIi5cIiArIHNvcnRBdHRyaWJ1dGUpLmFkZENsYXNzKFwic29ydGVkLWZpZWxkXCIpO1xuXG5cdFx0cmVmcmVzaFNvcnRpbmcobGlzdCk7XG5cdH1cblxuXHR2YXIgcmVmcmVzaFNvcnRpbmcgPSBmdW5jdGlvbihsaXN0KVxuXHR7XG5cdFx0bGlzdC5maW5kKFwiID4gdWwgPiBsaVwiKS5zb3J0KGpRdWVyeS5wcm94eShzb3J0ZXIsIHRoaXMsIHNvcnRBdHRyaWJ1dGUsIHNvcnRUeXBlLCBzb3J0RGlyZWN0aW9uKSkuYXBwZW5kVG8obGlzdC5maW5kKFwiID4gdWxcIikpO1xuXHR9XG5cblx0dmFyIHNvcnRlciA9IGZ1bmN0aW9uKHNvcnRieSwgc29ydHR5cGUsIHNvcnRkaXIsIGEsIGIpXG5cdHtcblx0XHRpZihzb3J0dHlwZSA9PSBcIm51bWJlclwiKVxuXHRcdHtcblx0XHRcdGlmKHNvcnRkaXIgPT0gXCJhc2NcIikgcmV0dXJuIChwYXJzZUZsb2F0KGpRdWVyeShiKS5jbG9zZXN0KFwibGlcIikuZGF0YShzb3J0YnkpKSA8IHBhcnNlRmxvYXQoalF1ZXJ5KGEpLmNsb3Nlc3QoXCJsaVwiKS5kYXRhKHNvcnRieSkpKSA/IDEgOiAtMTtcblx0XHRcdGVsc2UgcmV0dXJuIChwYXJzZUZsb2F0KGpRdWVyeShhKS5jbG9zZXN0KFwibGlcIikuZGF0YShzb3J0YnkpKSA8IHBhcnNlRmxvYXQoalF1ZXJ5KGIpLmNsb3Nlc3QoXCJsaVwiKS5kYXRhKHNvcnRieSkpKSA/IDEgOiAtMTtcblx0XHR9XG5cdH1cblxuXHR2YXIgaGFuZGxlRGV0YWlscyA9IGZ1bmN0aW9uKClcblx0e1xuXHRcdGpRdWVyeShkb2N1bWVudCkub24oXCJjbGlja1wiLCBcIi5wcm9kdWN0LWxpc3QgLnByb2R1Y3RzLmxpc3QgdWwgbGkgLm1vcmUgLmV4cGFuZGVyXCIsIGZ1bmN0aW9uKClcblx0XHR7XG5cdFx0XHRqUXVlcnkodGhpcykudG9nZ2xlQ2xhc3MoXCJ1bmZvbGRlZFwiKVxuXHRcdFx0alF1ZXJ5KHRoaXMpLnBhcmVudCgpLmZpbmQoXCIuZXhwYW5kZWRcIikuc2xpZGVUb2dnbGUoKTtcblx0XHR9KVxuXHR9XG5cblx0dmFyIGhhbmRsZU9wdGlvbmFsID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0alF1ZXJ5KFwiLmZpbHRlcnMgZm9ybSAuc2hvdy1hbGxcIikuZWFjaCggZnVuY3Rpb24oKVxuXHRcdHtcblx0XHRcdFx0dmFyIG1vdmVEb3duSGVhZGVyID0gZnVuY3Rpb24oKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0aWYoalF1ZXJ5KFwiLmNvbnRhaW5lci5wcm9kdWN0cy5oZWFkZXJcIikuaGFzQ2xhc3MoXCJmaXhlZFwiKSlcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRqUXVlcnkoXCIuY29udGFpbmVyLnByb2R1Y3RzLmhlYWRlclwiKS5jc3MoXCJ0b3BcIiwgalF1ZXJ5KFwiLnByb2R1Y3QtbGlzdCAuZmlsdGVycy5jb250YWluZXJcIikub3V0ZXJIZWlnaHQoKSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHRcdHZhciBzaG93QWxsQnRuID0galF1ZXJ5KHRoaXMpO1xuXHRcdFx0XHRqUXVlcnkoc2hvd0FsbEJ0bikub24oXCJjbGlja1wiLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0c2hvd0FsbEJ0bi50b2dnbGVDbGFzcyhcImFjdGl2ZVwiKTtcblx0XHRcdFx0c2V0SW50ZXJ2YWwoIG1vdmVEb3duSGVhZGVyLCAyMCk7XG5cdFx0XHRcdHNob3dBbGxCdG4uY2xvc2VzdChcImZvcm1cIikuZmluZChcIi50b2dnbGFibGVcIikuc2xpZGVUb2dnbGUoIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdGNsZWFySW50ZXJ2YWwoIG1vdmVEb3duSGVhZGVyICk7XG5cdFx0XHRcdFx0alF1ZXJ5KHdpbmRvdykudHJpZ2dlcihcInNjcm9sbCB0b3VjaG1vdmVcIik7XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSlcblx0XHR9KTtcblx0fVxuXG5cdHZhciBoYW5kbGVUb29sdGlwID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0aWYoalF1ZXJ5KFwiLnRvb2x0aXBcIikubGVuZ3RoKVxuXHRcdHtcblx0XHRcdGpRdWVyeSh3aW5kb3cpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdGpRdWVyeShcIi50b29sdGlwXCIpLmNzcyhcInotaW5kZXhcIiwgXCJcIikuZmluZChcIi5jb250ZW50XCIpLnN0b3AoKS5zbGlkZVVwKCk7XG5cdFx0XHR9KTtcblxuXHRcdFx0alF1ZXJ5KFwiLnRvb2x0aXAgLnNob3dcIikub24oXCJjbGlja1wiLCBmdW5jdGlvbihlKVxuXHRcdFx0e1xuXHRcdFx0XHRlLnN0b3BQcm9wYWdhdGlvbigpO1xuXHRcdFx0XHRqUXVlcnkoXCIucHJvZHVjdHMgLnRvb2x0aXBcIikuY3NzKFwiei1pbmRleFwiLCBcIlwiKS5maW5kKFwiLmNvbnRlbnRcIikuc3RvcCgpLnNsaWRlVXAoKTtcblx0XHRcdFx0alF1ZXJ5KHRoaXMpLmNsb3Nlc3QoXCIudG9vbHRpcFwiKS5jc3MoXCJ6LWluZGV4XCIsIDE5KS5maW5kKFwiLmNvbnRlbnRcIikuc3RvcCgpLnNsaWRlVG9nZ2xlKCk7XG5cdFx0XHRcdGlmKGpRdWVyeSh0aGlzKS5jbG9zZXN0KFwiLnRvb2x0aXBcIikuZmluZChcIi5jb250ZW50XCIpLm91dGVyV2lkdGgoKSArIGpRdWVyeSh0aGlzKS5jbG9zZXN0KFwiLnRvb2x0aXBcIikuZmluZChcIi5jb250ZW50XCIpLm9mZnNldCgpLmxlZnQgPiBqUXVlcnkod2luZG93KS53aWR0aCgpKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0alF1ZXJ5KHRoaXMpLmNsb3Nlc3QoXCIudG9vbHRpcFwiKS5maW5kKFwiLmNvbnRlbnRcIikuY3NzKFwibGVmdFwiLCAtalF1ZXJ5KHRoaXMpLmNsb3Nlc3QoXCIudG9vbHRpcFwiKS5maW5kKFwiLmNvbnRlbnRcIikub3V0ZXJXaWR0aCgpLzIgKyA0MCk7XG5cdFx0XHRcdH1cblx0XHRcdH0pXG5cblx0XHR9XG5cdH1cblxuXHR2YXIgYXBwbHlGaXhlZEZpbHRlciA9IGZ1bmN0aW9uKClcblx0e1xuICAgICAgICB2YXIgZmlsdGVyID0gIGpRdWVyeShcIi5wcm9kdWN0LWxpc3QgLmZpbHRlcnNcIik7XG4gICAgICAgIHZhciBwcm9kdWN0SGVhZGVyID0galF1ZXJ5KFwiLnByb2R1Y3QtbGlzdCAucHJvZHVjdHMuaGVhZGVyXCIpO1xuICAgICAgICB2YXIgcHJvZHVjdExpc3QgPSBqUXVlcnkoXCIucHJvZHVjdC1saXN0IC5wcm9kdWN0cy5saXN0XCIpO1xuICAgICAgICBcbiAgICAgICAgaWYgKCFmaWx0ZXIubGVuZ3RoICYmICFwcm9kdWN0SGVhZGVyLmxlbmd0aClcbiAgICAgICAgICAgIHJldHVybjtcblxuXHRcdHZhciBtYXJnaW4gPSBmaWx0ZXIub2Zmc2V0KCkudG9wIC0gcHJvZHVjdExpc3Qub2Zmc2V0KCkudG9wO1xuXG5cdFx0aWYoalF1ZXJ5KHdpbmRvdykud2lkdGgoKSA8IDc2OClcblx0XHR7XG5cdFx0XHRtYXJnaW4gPSAtcHJvZHVjdEhlYWRlci5oZWlnaHQoKTtcblx0XHR9XG5cblx0XHR2YXIgZXhlY09uU2Nyb2xsID0gZnVuY3Rpb24oKVxuXHRcdHtcblx0XHRcdGlmIChwcm9kdWN0TGlzdC5vZmZzZXQoKS50b3AgKyBtYXJnaW4gPD0galF1ZXJ5KHdpbmRvdykuc2Nyb2xsVG9wKCkgJiYgalF1ZXJ5KHdpbmRvdykud2lkdGgoKSA+PSA3NjgpXG5cdFx0XHR7XG5cdFx0XHRcdHByb2R1Y3RMaXN0XG5cdFx0XHRcdFx0LmNzcyh7XG5cdFx0XHRcdFx0XHRcIm1hcmdpbi10b3BcIiA6IChmaWx0ZXIub3V0ZXJIZWlnaHQodHJ1ZSkgKyBwcm9kdWN0SGVhZGVyLm91dGVySGVpZ2h0KHRydWUpKSArIFwicHhcIlxuXHRcdFx0XHRcdH0pO1xuXG5cdFx0XHRcdGZpbHRlclxuXHRcdFx0XHRcdC5hZGRDbGFzcyhcImZpeGVkXCIpO1xuXG5cdFx0XHRcdHByb2R1Y3RIZWFkZXJcblx0XHRcdFx0XHQuY3NzKHtcblx0XHRcdFx0XHRcdFwidG9wXCIgOiBmaWx0ZXIub3V0ZXJIZWlnaHQoKSArIFwicHhcIlxuXHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0LmFkZENsYXNzKFwiZml4ZWRcIik7XG5cblx0XHRcdH1cblx0XHRcdGVsc2Vcblx0XHRcdHtcblx0XHRcdFx0ZmlsdGVyLnJlbW92ZUNsYXNzKFwiZml4ZWRcIik7XG5cblx0XHRcdFx0cHJvZHVjdEhlYWRlclxuXHRcdFx0XHRcdC5jc3Moe1xuXHRcdFx0XHRcdFx0XCJ0b3BcIiA6IFwiXCJcblx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdC5yZW1vdmVDbGFzcyhcImZpeGVkXCIpO1xuXG5cdFx0XHRcdHByb2R1Y3RMaXN0XG5cdFx0XHRcdFx0LmNzcyh7XG5cdFx0XHRcdFx0XHRcIm1hcmdpbi10b3BcIiA6IFwiXCJcblx0XHRcdFx0XHR9KTtcblxuXHRcdFx0fVxuXHRcdH1cbiAgICAgICAgXG4gICAgICAgIGpRdWVyeSh3aW5kb3cpLm9uKFwic2Nyb2xsXCIsIGZ1bmN0aW9uKCkge1xuXHQgICAgICAgIGNsZWFyVGltZW91dChleGVjT25TY3JvbGwpO1xuXHQgICAgICAgIHNldFRpbWVvdXQoZXhlY09uU2Nyb2xsLCA1MClcbiAgICAgICAgfSk7XG5cblx0XHRqUXVlcnkod2luZG93KS5vbihcInRvdWNobW92ZVwiLCBleGVjT25TY3JvbGwpO1xuICAgIH1cblxuXHQvKiBfX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fIHB1YmxpYyBtZXRob2RzICovXG5cblx0dmFyIGluaXQgPSBmdW5jdGlvbigpXG5cdHtcblx0XHRhcHBseUlucHV0cygpO1xuXHRcdGFwcGx5U2xpZGVycygpO1xuXHRcdGFwcGx5U29ydGluZygpO1xuXHRcdGFwcGx5Rml4ZWRGaWx0ZXIoKTtcblx0XHRwcmVwYXJlRm9ybXMoKTtcblx0XHRoYW5kbGVEZXRhaWxzKCk7XG5cdFx0aGFuZGxlT3B0aW9uYWwoKTtcblx0XHRoYW5kbGVUb29sdGlwKCk7XG5cdH07XG5cblx0aW5pdCgpO1xufTtcblxudmFyIHByb2R1Y3RGaWx0ZXI7XG5cbmZvcm1hdE51bWJlciA9IGZ1bmN0aW9uIChudW0pIHtcblx0cmV0dXJuIG51bS50b1N0cmluZygpLnJlcGxhY2UoLyhcXGQpKD89KFxcZHszfSkrKD8hXFxkKSkvZywgXCIkMSBcIilcbn0iLCJwcm9tb2JveCA9IGZ1bmN0aW9uICgpXG57XG4gICAgdmFyIGRyYWdnYWJsZUxpc3QsXG4gICAgICAgIHBvc3RzTGlzdCxcbiAgICAgICAgcHJvbW9ib3hFbGVtZW50LFxuICAgICAgICBpbmZvLFxuICAgICAgICBmaWx0ZXJzLFxuICAgICAgICBmaWx0ZXJzUG9zID0gbnVsbCxcbiAgICAgICAgZmlsdGVyc1N0YXJ0UG9zID0gbnVsbCxcbiAgICAgICAgbGlzdFdpZHRoLFxuICAgICAgICBkaXN0YW5zY2VGcm9tUmlnaHQ7XG4gICAgICAgIGRyYWdnaW5nID0gZmFsc2U7XG4gICAgICAgIGl0ZW1XaWR0aCA9IG51bGwsIHNlZUFsbEl0ZW1XaWR0aCA9IG51bGw7XG4gICAgdmFyIHBlcDtcbiAgICBcbiAgICBmdW5jdGlvbiBnZXRQb3N0c0xpc3RXaWR0aChsaXN0KVxuICAgIHtcbiAgICAgICAgdmFyIHdpZHRoID0gMDtcblxuICAgICAgICBqUXVlcnkoXCIucG9zdC1pdGVtOm5vdCguaGlkZSk6bm90KC5hbGwtcHJvbW90aW9ucylcIiwgbGlzdCkuZWFjaChmdW5jdGlvbigpXG4gICAgICAgIHtcbi8vICAgICAgICAgICAgd2lkdGggKz0galF1ZXJ5KHRoaXMpLm91dGVyV2lkdGgodHJ1ZSk7XG4gICAgICAgICAgICB3aWR0aCArPSBpdGVtV2lkdGg7Ly8valF1ZXJ5KHRoaXMpLm91dGVyV2lkdGgoKTsgLy8zMzNcbiAgICAgICAgfSk7XG4gICAgICAgIFxuICAgICAgICB3aWR0aCArPSBzZWVBbGxJdGVtV2lkdGg7XG4gICAgICAgIHdpZHRoICs9IHBhcnNlRmxvYXQobGlzdC5jc3MoXCJwYWRkaW5nLWxlZnRcIikpICsgcGFyc2VGbG9hdChsaXN0LmNzcyhcInBhZGRpbmctcmlnaHRcIikpOyAgICAgICAgXG4gICAgICAgIFxuICAgICAgICByZXR1cm4gd2lkdGg7XG4gICAgfSAgXG4gICAgXG4gICAgZnVuY3Rpb24gdXBkYXRlVmFycygpXG4gICAge1xuICAgICAgICBkcmFnZ2FibGVMaXN0ID0galF1ZXJ5KFwiLmRyYWdnYWJsZS1saXN0XCIpO1xuICAgICAgICBwb3N0c0xpc3QgPSBqUXVlcnkoXCIucG9zdHMtbGlzdC1jb250ZW50XCIsIGRyYWdnYWJsZUxpc3QpO1xuXG4gICAgICAgIGlmICghcG9zdHNMaXN0Lmxlbmd0aClcbiAgICAgICAgICAgIHJldHVybjsgICBcblxuICAgICAgICBwcm9tb2JveEVsZW1lbnQgPSBkcmFnZ2FibGVMaXN0LmNsb3Nlc3QoXCIucHJvbW9ib3hcIik7XG4gICAgICAgIGluZm8gPSBqUXVlcnkoXCIuaW5mb1wiLCBwcm9tb2JveEVsZW1lbnQpO1xuICAgICAgICBcbiAgICAgICAgaWYgKGZpbHRlcnNQb3MgPT09IG51bGwpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGZpbHRlcnMgPSBqUXVlcnkoXCIuZmlsdGVyc1wiLCBkcmFnZ2FibGVMaXN0KTsgIFxuICAgICAgICAgICAgZmlsdGVyc1BvcyA9IGZpbHRlcnMucG9zaXRpb24oKTtcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKGZpbHRlcnNTdGFydFBvcyA9PT0gbnVsbClcbiAgICAgICAge1xuICAgICAgICAgICAgZmlsdGVyc1N0YXJ0UG9zID0gZmlsdGVycy5wb3NpdGlvbigpLmxlZnQ7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGl0ZW1XaWR0aCA9PT0gbnVsbClcbiAgICAgICAge1xuICAgICAgICAgICAgaXRlbVdpZHRoID0galF1ZXJ5KFwiLnBvc3QtaXRlbVwiLCBwb3N0c0xpc3QpLm5vdChcIi5hbGwtcHJvbW90aW9uc1wiKS5lcSgwKS5vdXRlcldpZHRoKCk7XG4gICAgICAgICAgICBzZWVBbGxJdGVtV2lkdGggPSBqUXVlcnkoXCIucG9zdC1pdGVtLmFsbC1wcm9tb3Rpb25zXCIsIHBvc3RzTGlzdCkuZXEoMCkub3V0ZXJXaWR0aCgpO1xuICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgIGxpc3RXaWR0aCA9IGdldFBvc3RzTGlzdFdpZHRoKHBvc3RzTGlzdCk7XG4gICAgICAgIHBvc3RzTGlzdC5vdXRlcldpZHRoKGxpc3RXaWR0aCk7XG4gICAgICAgIGRpc3RhbnNjZUZyb21SaWdodCA9IChqUXVlcnkod2luZG93KS5pbm5lcldpZHRoKCkgLSAgKGRyYWdnYWJsZUxpc3Qub2Zmc2V0KCkubGVmdCArIGRyYWdnYWJsZUxpc3Qub3V0ZXJXaWR0aCgpKSk7XG4gICAgfSAgICAgIFxuICAgIFxuICAgIGZ1bmN0aW9uIHVwZGF0ZUdhbGxlcnkoKVxuICAgIHsgIFxuICAgICAgICBpZiAgKHR5cGVvZiBwZXAgIT09IFwidW5kZWZpbmVkXCIpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGpRdWVyeS5wZXAudW5iaW5kKCBwZXAgKTsgXG4gICAgICAgIH1cblxuICAgICAgICBpZighZHJhZ2dhYmxlTGlzdCB8fCAhZHJhZ2dhYmxlTGlzdC5sZW5ndGgpXG4gICAgICAgICAgICByZXR1cm47XG4gICAgXG4gICAgICAgIHVwZGF0ZVZhcnMoKTtcblxuICAgICAgICBpZiAobGlzdFdpZHRoID49ICBkcmFnZ2FibGVMaXN0Lm9mZnNldCgpLmxlZnQgKyBkcmFnZ2FibGVMaXN0Lm91dGVyV2lkdGgoKSlcbiAgICAgICAge1xuICAgICAgICAgICAgY29uc3RyYWluTGVmdCA9IC1saXN0V2lkdGggKyBkcmFnZ2FibGVMaXN0Lm91dGVyV2lkdGgoKSArIGRpc3RhbnNjZUZyb21SaWdodDtcbiAgICAgICAgfVxuICAgICAgICBlbHNlXG4gICAgICAgIGlmIChsaXN0V2lkdGggPj0gZHJhZ2dhYmxlTGlzdC5vdXRlcldpZHRoKCkpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGNvbnN0cmFpbkxlZnQgPSAtbGlzdFdpZHRoICsgZHJhZ2dhYmxlTGlzdC5vdXRlcldpZHRoKCk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZVxuLy8gICAgICAgIGlmIChsaXN0V2lkdGggPCBkcmFnZ2FibGVMaXN0Lm91dGVyV2lkdGgoKSlcbiAgICAgICAge1xuICAgICAgICAgICAgY29uc3RyYWluTGVmdCA9IDA7XG4gICAgICAgIH1cblxuICAgICAgICBpZihqUXVlcnkod2luZG93KS53aWR0aCgpIDwgMTAyNClcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIHNwZCA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIGVsc2VcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIHNwZCA9IHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgICAgICAgICBcbiAgICAgICAgcGVwID0gcG9zdHNMaXN0LnBlcCh7XG4gICAgICAgICAgICBheGlzOiBcInhcIixcbiAgICAgICAgICAgIGRyYWc6IGRyYWcsXG4gICAgICAgICAgICBlYXNpbmc6IG1vdmluZyxcbiAgICAgICAgICAgIHN0YXJ0OiBzdGFydERyYWcsXG4gICAgICAgICAgICByZXZlcnQ6IGZhbHNlLFxuLy8gICAgICAgICAgcmV2ZXJ0SWY6ICAgZnVuY3Rpb24oKXsgcmV0dXJuIGZhbHNlOyB9LFxuICAgICAgICAgICAgY29uc3RyYWluVG86IFswLCAwLCAwLCBjb25zdHJhaW5MZWZ0XSxcbiAgICAgICAgICAgIHNob3VsZFByZXZlbnREZWZhdWx0OiBzcGRcbiAgICAgICAgICB9KTtcbiAgICAgICBcbiAgICAgICAgXG4gICAgICAgIGluZm8uY3NzKHtcbiAgICAgICAgICAgIFwib3BhY2l0eVwiIDogXCJcIixcbiAgICAgICAgICAgIFwidHJhbnNmb3JtXCIgOiBcIlwiXG4gICAgICAgIH0pOyAgICBcblxuICAgICAgICBwb3N0c0xpc3QuY3NzKHtcbiAgICAgICAgICAgIFwibGVmdFwiIDogXCIwXCIsXG4gICAgICAgICAgICBcInRyYW5zZm9ybVwiOiBcIm1hdHJpeCgxLCAwLCAwLCAxLCAwLCAwKVwiXG4gICAgICAgIH0pOyAgICAgICAgICAgXG4gICAgICAgICAgXG4vKiAgICAgIFxuICAgICAgICBmaWx0ZXJzLmNzcyh7XG4gICAgICAgICAgICBcImxlZnRcIiA6IGZpbHRlcnNTdGFydFBvcyArIFwicHhcIlxuICAgICAgICB9KTsgXG4qLyAgICAgICAgXG4gICAgfSAgIFxuICAgIFxuICAgIGZ1bmN0aW9uIHN0YXJ0RHJhZyhlLCBvYmopXG4gICAge1xuLy8gICAgICAgICAgICBpbmZvID0galF1ZXJ5KFwiLmluZm9cIiwgcHJvbW9ib3hFbGVtZW50KTtcbi8vICAgICAgICAgICAgbGlzdFdpZHRoID0gZ2V0UG9zdHNMaXN0V2lkdGgocG9zdHNMaXN0KTtcbi8vICAgICAgICAgICAgcG9zdHNMaXN0LndpZHRoKGxpc3RXaWR0aCk7XG4gICAgfVxuICAgIFxuICAgIGZ1bmN0aW9uIGRyYWcoZSwgb2JqKVxuICAgIHtcbiAgICAgICAgZHJhZ2dpbmcgPSB0cnVlO1xuICAgICAgICBtb3ZpbmcoZSwgb2JqKTtcbiAgICB9XG5cbiAgICAvL2RyYWcgb3IgZWFzaW5nXG4gICAgZnVuY3Rpb24gbW92aW5nKGUsIG9iailcbiAgICB7XG4gICAgICAgIHZhciBkcmFnID0galF1ZXJ5KG9iai5lbCk7XG4gICAgICAgIHZhciBpbmZvV2lkdGggPSBpbmZvLm91dGVyV2lkdGgoKTtcbiAgICAgICAgdmFyIGRyYWdQb3MgPSBkcmFnLnBvc2l0aW9uKCkubGVmdDtcbiAgICAgICAgdmFyIGluZm9EaXN0YW5jZSA9IE1hdGgubWluKE1hdGguYWJzKGRyYWdQb3MpLCBpbmZvV2lkdGgpO1xuXG4gICAgICAgIGlmIChqUXVlcnkod2luZG93KS5pbm5lcldpZHRoKCkgPj0gMTAyNClcbiAgICAgICAge1xuICAgICAgICAgICAgaW5mby5jc3Moe1xuICAgICAgICAgICAgICAgIFwib3BhY2l0eVwiIDogMS0gaW5mb0Rpc3RhbmNlL2luZm9XaWR0aCxcbiAgICAgICAgICAgICAgICBcInRyYW5zZm9ybVwiIDogXCJzY2FsZShcIiArICgxIC0gaW5mb0Rpc3RhbmNlL2luZm9XaWR0aC8yKSArIFwiKVwiXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlXG4gICAgICAgIHtcbiAgICAgICAgICAgIGluZm8uY3NzKHtcbiAgICAgICAgICAgICAgICBcIm9wYWNpdHlcIiA6IFwiXCIsXG4gICAgICAgICAgICAgICAgXCJ0cmFuc2Zvcm1cIiA6IFwiXCJcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBmaWx0ZXJzLmNzcyh7XG4gICAgICAgICAgICBcImxlZnRcIiA6IE1hdGgubWF4KChmaWx0ZXJzUG9zLmxlZnQgLSBNYXRoLmFicyhkcmFnUG9zKSksIDApICsgXCJweFwiXG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBcblxuICAgIGZ1bmN0aW9uIGhhbmRsZURyYWdnYWJsZVBvc3RzTGlzdCgpXG4gICAge1xuICAgICAgICBkcmFnZ2luZyA9IGZhbHNlO1xuICAgICAgICB1cGRhdGVWYXJzKCk7XG4gICAgICAgIFxuICAgICAgICBpZiAoIXBvc3RzTGlzdC5sZW5ndGgpXG4gICAgICAgICAgICByZXR1cm47ICBcblxuICAgICAgICB1cGRhdGVHYWxsZXJ5KCk7XG4gICAgICAgIFxuICAgICAgICBqUXVlcnkod2luZG93KS5vbihcInJlc2l6ZVwiLCBmdW5jdGlvbigpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHVwZGF0ZUdhbGxlcnkoKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgLy9jbGljayB3aGVuIGNsaWNrLCBkcmFnIHdoZW4gZHJhZ1xuICAgICAgICBqUXVlcnkoXCJhXCIsIHBvc3RzTGlzdClcbiAgICAgICAgICAgIC5vbihcIm1vdXNlZG93biBwb2ludGVyZG93biB0b3VjaHN0YXJ0XCIsZnVuY3Rpb24oZSkge1xuICAgICAgICAgICAgICAgIGRyYWdnaW5nID0gZmFsc2U7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLm9uKFwiY2xpY2sgdG91Y2hcIixmdW5jdGlvbihlKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGlmIChkcmFnZ2luZylcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICB9ICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIDtcbiAgICB9XG5cblx0dmFyIGFwcGx5RmlsdGVyID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0alF1ZXJ5KFwiLnByb21vYm94XCIpLmVhY2goIGZ1bmN0aW9uICgpXG5cdFx0e1xuXHRcdFx0dmFyIHByb21vYm94ID0galF1ZXJ5KHRoaXMpO1xuXHRcdFx0alF1ZXJ5KFwiLmZpbHRlcnMgLnZhbHVlXCIsIHByb21vYm94KS5vbihcImNsaWNrIHRvdWNoIHRvdWNoZW5kXCIsIGZ1bmN0aW9uKGUpXG5cdFx0XHR7XG4gICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgICAgICAgalF1ZXJ5KFwiLmZpbHRlcnMgdWxcIiwgcHJvbW9ib3gpLnN0b3AoKS5mYWRlSW4oKTtcblx0XHRcdH0pO1xuXG5cdFx0XHRqUXVlcnkoXCIuZmlsdGVycyB1bCBsaVwiLCBwcm9tb2JveCkub24oXCJjbGljayB0b3VjaCB0b3VjaGVuZFwiLCBmdW5jdGlvbihlKVxuXHRcdFx0e1xuICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgICAgIGpRdWVyeSh0aGlzKS50b2dnbGVDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaGFzaCA9IFwiI2ZpbHRydWotXCIgKyBqUXVlcnkodGhpcykuaHRtbCgpO1xuXHRcdFx0XHRhcHBseUZpbHRlclRvQ29udGVudChwcm9tb2JveCk7XG5cdFx0XHR9KTtcblxuICAgICAgICAgICAgLy9qUXVlcnkoZG9jdW1lbnQpLm9uKFwiY2xpY2sgdG91Y2ggdG91Y2hlbmRcIiwgZnVuY3Rpb24oZSlcbiAgICAgICAgICAgIC8ve1xuICAgICAgICAgICAgLy8gICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICAgIC8vfSlcblxuICAgICAgICAgICAgdmFyIGhpZGVGaWx0ZXIgPSBmdW5jdGlvbigpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgalF1ZXJ5KFwiLmZpbHRlcnMgdWxcIiwgcHJvbW9ib3gpLnN0b3AoKS5mYWRlT3V0KCk7XG5cbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGZpbHRlcnMgIT09IFwidW5kZWZpbmVkXCIgJiYgcGFyc2VJbnQocG9zdHNMaXN0LmNzcyhcImxlZnRcIikpID09IDApXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmaWx0ZXJzLmNzcyh7XG4gICAgICAgICAgICAgICAgICAgICAgICBcImxlZnRcIiA6IGZpbHRlcnNTdGFydFBvcyArIFwicHhcIlxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cblx0XHRcdGpRdWVyeShcIi5maWx0ZXJzIHVsXCIsIHByb21vYm94KS5vbihcIm1vdXNlbGVhdmVcIiwgaGlkZUZpbHRlcik7XG4gICAgICAgICAgICBqUXVlcnkoZG9jdW1lbnQpLm9uKFwiY2xpY2sgdG91Y2hlbmRcIiwgaGlkZUZpbHRlcilcbiAgICAgICAgfSk7XG5cdH1cblxuXHR2YXIgYXBwbHlGaWx0ZXJUb0NvbnRlbnQgPSBmdW5jdGlvbihwcm9tb2JveClcblx0e1xuXHRcdHZhciBjYXRzID0gW107XG4gICAgICAgIHZhciBwb3N0SXRlbXNMZW5ndGggPSBqUXVlcnkoXCIucG9zdC1pdGVtXCIsIHByb21vYm94KS5sZW5ndGg7XG5cblx0XHRqUXVlcnkoXCIuZmlsdGVycyB1bCBsaVwiLCBwcm9tb2JveCkuZWFjaCggZnVuY3Rpb24oKSB7XG5cdFx0XHRpZihqUXVlcnkodGhpcykuaGFzQ2xhc3MoXCJhY3RpdmVcIikpXG5cdFx0XHRcdGNhdHMucHVzaChqUXVlcnkodGhpcykuYXR0cihcImRhdGEtdHlwZVwiKSk7XG5cdFx0fSk7XG5cblx0XHRpZighY2F0cy5sZW5ndGgpXG4gICAgICAgIHtcblx0XHRcdGpRdWVyeShcIi5wb3N0LWl0ZW1cIiwgcHJvbW9ib3gpLnJlbW92ZUNsYXNzKCBcImhpZGVcIiApO1xuICAgICAgICAgICAgdXBkYXRlR2FsbGVyeSgpO1xuICAgICAgICB9XG5cdFx0ZWxzZVxuXHRcdGpRdWVyeShcIi5wb3N0LWl0ZW1cIiwgcHJvbW9ib3gpLmVhY2goIGZ1bmN0aW9uKGluZGV4LCBlbGVtZW50KVxuICAgICAgICB7XG4gICAgICAgICAgICB2YXIgY3VycmVudEl0ZW0gPSBqUXVlcnkodGhpcyk7XG4gICAgICAgICAgICBpZiAoIWN1cnJlbnRJdGVtLmhhc0NsYXNzKFwiYWxsLXByb21vdGlvbnNcIikpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdmFyIHBvc3RDYXRzID0gY3VycmVudEl0ZW0uYXR0cihcImRhdGEtdHlwZVwiKS50b1N0cmluZygpLnNwbGl0KFwiLFwiKTtcbiAgICAgICAgICAgICAgICB2YXIgZGlzYWJsZVBvc3QgPSB0cnVlO1xuICAgICAgICAgICAgICAgIGZvcihpIGluIGNhdHMpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBpZihwb3N0Q2F0cy5pbmRleE9mKGNhdHNbaV0udG9TdHJpbmcoKSkgPiAtMSlcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVQb3N0ID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmKGRpc2FibGVQb3N0KSBjdXJyZW50SXRlbS5hZGRDbGFzcyggXCJoaWRlXCIgKTtcbiAgICAgICAgICAgICAgICBlbHNlIGpRdWVyeSh0aGlzKS5yZW1vdmVDbGFzcyggXCJoaWRlXCIgKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKGluZGV4ID09IHBvc3RJdGVtc0xlbmd0aCAtMSlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICB1cGRhdGVHYWxsZXJ5KCk7XG4gICAgICAgICAgICB9ICAgICAgICAgICAgXG5cdFx0fSk7XG4vLyAgICAgICAgdXBkYXRlVmFycygpO1xuLy8gICAgICAgICAgICB1cGRhdGVHYWxsZXJ5KCk7XG5cdH1cblxuXHQvKiBfX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fIHB1YmxpYyBtZXRob2RzICovXG5cblx0dmFyIGluaXQgPSBmdW5jdGlvbigpXG5cdHtcbiAgICAgICAgdXBkYXRlR2FsbGVyeSgpO1xuXHRcdGFwcGx5RmlsdGVyKCk7XG4gICAgICAgIGhhbmRsZURyYWdnYWJsZVBvc3RzTGlzdCgpO1xuXHR9O1xuXG5cdGluaXQoKTtcbn07XG5cbnZhciBwcm9tb2JveDtcbiIsInNlYXJjaGJveCA9IGZ1bmN0aW9uICgpXG57XG5cdHZhciBjdXJyZW50UGF0aCA9IFwiXCI7XG5cdHZhciBjdXJyZW50TGV2ZWwgPSAxO1xuICAgIFxuICAgIC8vIGdldCBjbG9zZXN0IHR1IG51bSBudW1iZXIgZnJvbSBhcnJheVxuICAgIGZ1bmN0aW9uIGNsb3Nlc3QobnVtLCBhcnIpXG4gICAge1xuICAgICAgICB2YXIgY3VyciA9IGFyclswXTtcbiAgICAgICAgdmFyIGluZGV4ID0gMDtcbiAgICAgICAgdmFyIGRpZmYgPSBNYXRoLmFicyhudW0gLSBjdXJyKTtcbiAgICAgICAgZm9yICh2YXIgdmFsID0gMDsgdmFsIDwgYXJyLmxlbmd0aDsgdmFsKyspIHtcbiAgICAgICAgICAgIHZhciBuZXdkaWZmID0gTWF0aC5hYnMobnVtIC0gYXJyW3ZhbF0pO1xuICAgICAgICAgICAgaWYgKG5ld2RpZmYgPCBkaWZmKSB7XG4gICAgICAgICAgICAgICAgZGlmZiA9IG5ld2RpZmY7XG4gICAgICAgICAgICAgICAgY3VyciA9IGFyclt2YWxdO1xuICAgICAgICAgICAgICAgIGluZGV4ID0gdmFsO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBpbmRleDtcbiAgICB9ICAgIFxuXG5cdHZhciBhcHBseVNsaWRlcnMgPSBmdW5jdGlvbigpXG5cdHtcblx0XHRqUXVlcnkoXCIuc2VhcmNoYm94IC5zbGlkZXJcIikuZWFjaCggZnVuY3Rpb24oKVxuXHRcdHtcblx0XHRcdHZhciBvcHRpb25zID0ge307XG5cdFx0XHR2YXIgc2xpZGVyQ29udGFpbmVyID0galF1ZXJ5KHRoaXMpO1xuXG5cdFx0XHRpZihzbGlkZXJDb250YWluZXIuZGF0YShcImZpbHRlci10eXBlXCIpID09IFwiYmV0d2VlblwiKVxuXHRcdFx0e1xuXHRcdFx0XHRvcHRpb25zLnJhbmdlID0gdHJ1ZTtcblx0XHRcdFx0b3B0aW9ucy52YWx1ZXMgPSBbXTtcblx0XHRcdFx0aWYoc2xpZGVyQ29udGFpbmVyLmRhdGEoXCJ2YWx1ZS1taW5cIikgJiYgc2xpZGVyQ29udGFpbmVyLmRhdGEoXCJ2YWx1ZS1tYXhcIikpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRvcHRpb25zLnZhbHVlcy5wdXNoKHNsaWRlckNvbnRhaW5lci5kYXRhKFwidmFsdWUtbWluXCIpKTtcblx0XHRcdFx0XHRvcHRpb25zLnZhbHVlcy5wdXNoKHNsaWRlckNvbnRhaW5lci5kYXRhKFwidmFsdWUtbWF4XCIpKTtcblx0XHRcdFx0fTtcblx0XHRcdH1cblx0XHRcdGVsc2UgaWYoc2xpZGVyQ29udGFpbmVyLmRhdGEoXCJmaWx0ZXItdHlwZVwiKSA9PSBcInRvXCIpXG5cdFx0XHR7ICAgICAgICAgICAgICAgIFxuXHRcdFx0XHRvcHRpb25zLnJhbmdlID0gXCJtaW5cIjtcblx0XHRcdFx0aWYoc2xpZGVyQ29udGFpbmVyLmRhdGEoXCJ2YWx1ZVwiKSlcblx0XHRcdFx0e1xuXHRcdFx0XHRcdG9wdGlvbnMudmFsdWUgPSBzbGlkZXJDb250YWluZXIuZGF0YShcInZhbHVlXCIpO1xuXHRcdFx0XHR9O1xuXHRcdFx0fTtcbiAgICAgICAgICAgIFxuXHRcdFx0aWYoc2xpZGVyQ29udGFpbmVyLmRhdGEoXCJtaW5cIikgIT0gbnVsbClcblx0XHRcdHtcblx0XHRcdFx0b3B0aW9ucy5taW4gPSBzbGlkZXJDb250YWluZXIuZGF0YShcIm1pblwiKTtcblx0XHRcdH07XG5cblx0XHRcdGlmKHNsaWRlckNvbnRhaW5lci5kYXRhKFwibWF4XCIpKVxuXHRcdFx0e1xuXHRcdFx0XHRvcHRpb25zLm1heCA9IHNsaWRlckNvbnRhaW5lci5kYXRhKFwibWF4XCIpO1xuXHRcdFx0fTtcblx0XHRcdHZhciBwb2ludHNWYWx1ZXMgPSBbXTtcblxuXHRcdFx0aWYoc2xpZGVyQ29udGFpbmVyLmRhdGEoXCJzdGVwXCIpKVxuXHRcdFx0e1xuXG5cdFx0XHRcdHZhciBzdGVwVmFsID0gc2xpZGVyQ29udGFpbmVyLmRhdGEoXCJzdGVwXCIpO1xuXHRcdFx0XHR2YXIgbGVuZ3RoID0gTWF0aC5jZWlsKChvcHRpb25zLm1heC1vcHRpb25zLm1pbikvc3RlcFZhbCk7XG5cdFx0XHRcdHZhciBzdGFydCA9IE1hdGguZmxvb3Iob3B0aW9ucy5taW4vc3RlcFZhbCk7XG4gICAgICAgICAgICAgICAgXG5cdFx0XHRcdGZvcih2YXIgaSA9IHN0YXJ0OyBpIDxsZW5ndGgrc3RhcnQ7IGkrKylcblx0XHRcdFx0e1xuXHRcdFx0XHRcdGlmKGkgPT0gc3RhcnQgJiYgb3B0aW9ucy5taW4gPCBpKnN0ZXBWYWwpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0cG9pbnRzVmFsdWVzLnB1c2gob3B0aW9ucy5taW4pO1xuXHRcdFx0XHRcdH1cbiAgICAgICAgICAgICAgICAgICAgXG5cdFx0XHRcdFx0cG9pbnRzVmFsdWVzLnB1c2goaSpzdGVwVmFsKTtcbiAgICAgICAgICAgICAgICAgICAgXG5cdFx0XHRcdFx0aWYoaSA9PSBsZW5ndGgrc3RhcnQtMSAmJiBvcHRpb25zLm1heCA+IGkqc3RlcFZhbClcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRwb2ludHNWYWx1ZXMucHVzaChvcHRpb25zLm1heCk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cblxuXHRcdFx0XHRpZihwb2ludHNWYWx1ZXMubGVuZ3RoKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0b3B0aW9ucy5taW4gPSAwO1xuXHRcdFx0XHRcdG9wdGlvbnMubWF4ID0gcG9pbnRzVmFsdWVzLmxlbmd0aC0xO1xuXG5cdFx0XHRcdFx0aWYob3B0aW9ucy52YWx1ZSlcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRvcHRpb25zLnZhbHVlID0gcG9pbnRzVmFsdWVzLmluZGV4T2Yob3B0aW9ucy52YWx1ZSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9XG5cblx0XHRcdGlmKHNsaWRlckNvbnRhaW5lci5kYXRhKFwicHJvZ3Jlc3NpdmVcIikgPT0gdHJ1ZSlcblx0XHRcdHtcblx0XHRcdFx0dmFyIHJhbmdlc0NvbmZpZyA9XG5cdFx0XHRcdHtcblx0XHRcdFx0XHRkZWZhdWx0OlxuXHRcdFx0XHRcdFx0W1xuXHRcdFx0XHRcdFx0XHR7XCJtaW5cIjogMCwgXCJtYXhcIjogOTAwLCBcInN0ZXBcIjogMTAwfSxcblx0XHRcdFx0XHRcdFx0e1wibWluXCI6IDEwMDAsIFwibWF4XCI6IDkwMDAsIFwic3RlcFwiOiAxMDAwfSxcblx0XHRcdFx0XHRcdFx0e1wibWluXCI6IDEwMDAwLCBcIm1heFwiOiA0ODAwMCwgXCJzdGVwXCI6IDIwMDB9LFxuXHRcdFx0XHRcdFx0XHR7XCJtaW5cIjogNTAwMDAsIFwibWF4XCI6IDUwMDAwMDAwLCBcInN0ZXBcIjogNTAwMH0sXG5cdFx0XHRcdFx0XHRdLFxuXHRcdFx0XHRcdGludmVzdG1lbnRzOiBbXG5cdFx0XHRcdFx0XHR7XCJtaW5cIjogMCwgXCJtYXhcIjogNTAwMCwgXCJzdGVwXCI6IDUwMH0sXG5cdFx0XHRcdFx0XHR7XCJtaW5cIjogNjAwMCwgXCJtYXhcIjogMTkwMDAsIFwic3RlcFwiOiAxMDAwfSxcblx0XHRcdFx0XHRcdHtcIm1pblwiOiAyMDAwMCwgXCJtYXhcIjogOTUwMDAsIFwic3RlcFwiOiA1MDAwfSxcblx0XHRcdFx0XHRcdHtcIm1pblwiOiAxMDAwMDAsIFwibWF4XCI6IDQ3NTAwMCwgXCJzdGVwXCI6IDI1MDAwfSxcblx0XHRcdFx0XHRcdHtcIm1pblwiOiA1MDAwMDAsIFwibWF4XCI6IDEwMDAwMDAwLCBcInN0ZXBcIjogNTAwMDB9LFxuXHRcdFx0XHRcdF0sXG5cdFx0XHRcdFx0aW52ZXN0bWVudHNNb250aHM6XG5cdFx0XHRcdFx0XHRbXG5cdFx0XHRcdFx0XHRcdHtcIm1pblwiOiAxLCBcIm1heFwiOiAyMywgXCJzdGVwXCI6IDF9LFxuXHRcdFx0XHRcdFx0XHR7XCJtaW5cIjogMjQsIFwibWF4XCI6IDYwMCwgXCJzdGVwXCI6IDEyfVxuXHRcdFx0XHRcdFx0XVxuXHRcdFx0XHR9O1xuXG5cdFx0XHRcdHZhciByYW5nZXMgPSByYW5nZXNDb25maWcuZGVmYXVsdDtcblxuXHRcdFx0XHRpZihzbGlkZXJDb250YWluZXIuZGF0YShcInByb2dyZXNzaXZlLXZhbHVlc1wiKSAmJiByYW5nZXNDb25maWdbc2xpZGVyQ29udGFpbmVyLmRhdGEoXCJwcm9ncmVzc2l2ZS12YWx1ZXNcIildKVxuXHRcdFx0XHRcdHJhbmdlcyA9IHJhbmdlc0NvbmZpZ1tzbGlkZXJDb250YWluZXIuZGF0YShcInByb2dyZXNzaXZlLXZhbHVlc1wiKV07XG5cblx0XHRcdFx0Zm9yKGk9MDsgaSA8IHJhbmdlcy5sZW5ndGg7IGkrKylcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHZhciBjdXJyZW50ID0gMDtcblx0XHRcdFx0XHRpZihvcHRpb25zLm1pbiA+IHJhbmdlc1tpXS5tYXggfHwgb3B0aW9ucy5tYXggPCByYW5nZXNbaV0ubWluKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGNvbnRpbnVlO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0eyBcblx0XHRcdFx0XHRcdGN1cnJlbnQgPSByYW5nZXNbaV0ubWluO1xuXHRcdFx0XHRcdFx0d2hpbGUoY3VycmVudCA8IG9wdGlvbnMubWluKVxuXHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHRjdXJyZW50ICs9IHJhbmdlc1tpXS5zdGVwO1xuXHRcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0XHRpZihvcHRpb25zLm1pbiA+IHJhbmdlc1tpXS5taW4gJiYgY3VycmVudC1yYW5nZXNbaV0uc3RlcCA8IG9wdGlvbnMubWluKVxuXHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHRwb2ludHNWYWx1ZXMucHVzaChvcHRpb25zLm1pbik7XG5cdFx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRcdHdoaWxlKGN1cnJlbnQgPD0gb3B0aW9ucy5tYXggJiYgY3VycmVudCA8PSByYW5nZXNbaV0ubWF4KVxuXHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHRwb2ludHNWYWx1ZXMucHVzaChjdXJyZW50KTtcblx0XHRcdFx0XHRcdFx0Y3VycmVudCArPSByYW5nZXNbaV0uc3RlcDtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRpZihwb2ludHNWYWx1ZXMubGVuZ3RoKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0b3B0aW9ucy5taW4gPSAwO1xuXHRcdFx0XHRcdG9wdGlvbnMubWF4ID0gcG9pbnRzVmFsdWVzLmxlbmd0aC0xO1xuLy9cdFx0XHRcdFx0b3B0aW9ucy5tYXggPSBwb2ludHNWYWx1ZXNbcG9pbnRzVmFsdWVzLmxlbmd0aC0xXTtcblxuXHRcdFx0XHRcdGlmKG9wdGlvbnMudmFsdWUpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0b3B0aW9ucy52YWx1ZSA9IHBvaW50c1ZhbHVlcy5pbmRleE9mKG9wdGlvbnMudmFsdWUpO1xuLy9cdFx0XHRcdFx0XHRvcHRpb25zLnZhbHVlID0gcG9pbnRzVmFsdWVzW3BvaW50c1ZhbHVlcy5pbmRleE9mKG9wdGlvbnMudmFsdWUpXTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0b3B0aW9ucy5zbGlkZSA9IGZ1bmN0aW9uKCBldmVudCwgdWkgKVxuXHRcdFx0e1xuXHRcdFx0XHRpZih1aS52YWx1ZSlcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHZhciBleFZhbCA9IHVpLnZhbHVlO1xuXG5cdFx0XHRcdFx0aWYocG9pbnRzVmFsdWVzLmxlbmd0aClcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRleFZhbCA9IHBvaW50c1ZhbHVlc1t1aS52YWx1ZV07XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0c2xpZGVyQ29udGFpbmVyLmZpbmQoXCIudmFsdWVcIikuaHRtbCggZm9ybWF0TnVtYmVyKGV4VmFsKSApO1xuXHRcdFx0XHRcdHNsaWRlckNvbnRhaW5lci5wYXJlbnRzKFwiW2RhdGEtbGV2ZWxdXCIpLmZpcnN0KCkuZmluZChcImlucHV0XCIpLnZhbChleFZhbCk7XG4gICAgICAgICAgICAgICAgICAgIHNsaWRlckNvbnRhaW5lci5kYXRhKFwidmFsdWVcIiwgZXhWYWwpO1xuICAgICAgICAgICAgICAgIH1cblx0XHRcdFx0aWYodWkudmFsdWVzKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0c2xpZGVyQ29udGFpbmVyLmZpbmQoXCIudmFsdWVcIikuaHRtbCggZm9ybWF0TnVtYmVyKHVpLnZhbHVlc1swXSkgKyBcIiAtIFwiICsgZm9ybWF0TnVtYmVyKHVpLnZhbHVlc1sxXSkgKTtcblx0XHRcdFx0XHRzbGlkZXJDb250YWluZXIucGFyZW50cyhcIltkYXRhLWxldmVsXVwiKS5maXJzdCgpLmZpbmQoXCJpbnB1dFtuYW1lXj0nbWluLSddXCIpLnZhbCh1aS52YWx1ZXNbMF0pLnRyaWdnZXIoXCJjaGFuZ2VcIik7XG5cdFx0XHRcdFx0c2xpZGVyQ29udGFpbmVyLnBhcmVudHMoXCJbZGF0YS1sZXZlbF1cIikuZmlyc3QoKS5maW5kKFwiaW5wdXRbbmFtZV49J21heC0nXVwiKS52YWwodWkudmFsdWVzWzFdKS50cmlnZ2VyKFwiY2hhbmdlXCIpO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cblx0XHRcdG9wdGlvbnMuY2hhbmdlID0gZnVuY3Rpb24oIGV2ZW50LCB1aSApXG5cdFx0XHR7XG5cdFx0XHRcdHNsaWRlckNvbnRhaW5lci5wYXJlbnRzKFwiW2RhdGEtbGV2ZWxdXCIpLmZpcnN0KCkudHJpZ2dlcihcImlrYWw6ZmlsdGVyOmNoYW5nZWRcIik7XG5cdFx0XHR9XG5cblxuXHRcdFx0c2xpZGVyQ29udGFpbmVyLmNsb3Nlc3QoXCJbZGF0YS1sZXZlbF1cIikuZmluZChcImlucHV0XCIpLm9uKFwiY2hhbmdlXCIsIGZ1bmN0aW9uKClcblx0XHRcdHtcbiAgICAgICAgICAgICAgICB2YXIgaW5wdXQgPSBqUXVlcnkodGhpcyk7XG5cblx0XHRcdFx0aWYocGFyc2VGbG9hdChpbnB1dC5hdHRyKFwibWluXCIpKSAmJiBpbnB1dC52YWwoKSA8IHBhcnNlRmxvYXQoaW5wdXQuYXR0cihcIm1pblwiKSkpXG5cdFx0XHRcdFx0aW5wdXQudmFsKHBhcnNlRmxvYXQoaW5wdXQuYXR0cihcIm1pblwiKSkpO1xuXG5cdFx0XHRcdGlmKHBhcnNlRmxvYXQoaW5wdXQuYXR0cihcIm1heFwiKSkgJiYgaW5wdXQudmFsKCkgPiBwYXJzZUZsb2F0KGlucHV0LmF0dHIoXCJtYXhcIikpKVxuXHRcdFx0XHRcdGlucHV0LnZhbChwYXJzZUZsb2F0KGlucHV0LmF0dHIoXCJtYXhcIikpKTtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB2YXIgdmFsID0gaW5wdXQudmFsKCk7XG4gICAgICAgICAgICAgICAgaWYocG9pbnRzVmFsdWVzLmxlbmd0aClcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIHZhbCA9IGNsb3Nlc3QoaW5wdXQudmFsKCksIHBvaW50c1ZhbHVlcyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHNsaWRlckNvbnRhaW5lci5maW5kKFwiLnNsaWRlci1iYXJcIikuc2xpZGVyKFwidmFsdWVcIiwgdmFsKTtcblx0XHRcdFx0c2xpZGVyQ29udGFpbmVyLmZpbmQoXCIudmFsdWVcIikuaHRtbCggZm9ybWF0TnVtYmVyKGlucHV0LnZhbCgpKSApO1xuXHRcdFx0XHRzbGlkZXJDb250YWluZXIuY2xvc2VzdChcIltkYXRhLWxldmVsXVwiKS50cmlnZ2VyKFwiaWthbDpmaWx0ZXI6Y2hhbmdlZFwiKTtcblx0XHRcdH0pO1xuICAgICAgICAgICAgXG5cdFx0XHRzbGlkZXJDb250YWluZXIuZmluZChcIi5zbGlkZXItYmFyXCIpLnNsaWRlcihvcHRpb25zKTsgICBcbiAgICAgICAgICAgIHNsaWRlckNvbnRhaW5lci5jbG9zZXN0KFwiW2RhdGEtbGV2ZWxdXCIpLmZpbmQoXCJpbnB1dFwiKS50cmlnZ2VyKFwiY2hhbmdlXCIpO1xuXHRcdH0pO1xuICAgICAgICBcblxuXHR9XG5cblx0dmFyIGFwcGx5Q2hvaWNlcyA9IGZ1bmN0aW9uKClcblx0e1xuXHRcdGpRdWVyeShcIi5zZWFyY2hib3ggLmNob2ljZVwiKS5lYWNoKCBmdW5jdGlvbigpXG5cdFx0e1xuXHRcdFx0dmFyIGNob2ljZXIgPSBqUXVlcnkodGhpcyk7XG5cdFx0XHRqUXVlcnkoXCIudmFsdWVcIiwgY2hvaWNlcikub24oXCJjbGlja1wiLCBmdW5jdGlvbihlKVxuXHRcdFx0e1xuXHRcdFx0XHRqUXVlcnkoXCIudG9wcGVkXCIpLnN0b3AoKS5mYWRlT3V0KCkucmVtb3ZlQ2xhc3MoXCJ0b3BwZWRcIik7XG5cdFx0XHRcdGpRdWVyeShcInVsXCIsIGNob2ljZXIpLnN0b3AoKS5mYWRlSW4oKS5hZGRDbGFzcyhcInRvcHBlZFwiKTtcblx0XHRcdH0pXG5cblx0XHRcdGpRdWVyeShcImxpW2RhdGEtdmFsdWVdXCIsIGNob2ljZXIpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oZSlcblx0XHRcdHtcblx0XHRcdFx0ZS5zdG9wUHJvcGFnYXRpb24oKTtcblx0XHRcdFx0dmFyIG9wdGlvblZhbHVlID0galF1ZXJ5KHRoaXMpLmRhdGEoXCJ2YWx1ZVwiKTtcblx0XHRcdFx0dmFyIHRhcmdldFBhdGggPSBqUXVlcnkodGhpcykuZGF0YShcInRhcmdldC1wYXRoXCIpO1xuXHRcdFx0XHR2YXIgdGFyZ2V0TGV2ZWwgPSBqUXVlcnkodGhpcykuZGF0YShcInRhcmdldC1sZXZlbFwiKTtcblx0XHRcdFx0alF1ZXJ5KFwidWxcIiwgY2hvaWNlcikuc3RvcCgpLmZhZGVPdXQoKS5yZW1vdmVDbGFzcyhcInRvcHBlZFwiKTtcblx0XHRcdFx0alF1ZXJ5KFwiLnZhbHVlID4gc3BhblwiLCBjaG9pY2VyKS5odG1sKGpRdWVyeSh0aGlzKS5odG1sKCkpO1xuXG5cdFx0XHRcdGNob2ljZXIuZmlyc3QoKS5maW5kKFwiaW5wdXRcIikudmFsKG9wdGlvblZhbHVlKVxuXHRcdFx0XHRzZXRQYXRoKHRhcmdldFBhdGgsIHRhcmdldExldmVsKTtcblx0XHRcdH0pO1xuXHRcdH0pO1xuXHR9XG5cblx0dmFyIGFwcGx5VG9vbHRpcHMgPSBmdW5jdGlvbigpXG5cdHtcblx0XHRqUXVlcnkoXCIudG9vbHRpcC10b2dnbGVyXCIpLmVhY2goIGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyIHRvZ2dsZXIgPSBqUXVlcnkodGhpcyk7XG5cdFx0XHRqUXVlcnkoXCIuZG8tdG9nZ2xlXCIsIHRvZ2dsZXIpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oZSlcblx0XHRcdHtcblx0XHRcdFx0alF1ZXJ5KFwiLnRvcHBlZFwiKS5zdG9wKCkuZmFkZU91dCgpLnJlbW92ZUNsYXNzKFwidG9wcGVkXCIpO1xuXHRcdFx0XHRqUXVlcnkoXCIudG9vbHRpcFwiLCB0b2dnbGVyKS5mYWRlSW4oKS5hZGRDbGFzcyhcInRvcHBlZFwiKTtcblxuXHRcdFx0XHR0b2dnbGVyLnBhcmVudHMoXCJbZGF0YS1sZXZlbF1cIikuZmlyc3QoKS5vbmUoXCJpa2FsOmZpbHRlcjpjaGFuZ2VkXCIsIGZ1bmN0aW9uKClcblx0XHRcdFx0e1xuXHRcdFx0XHRcdGpRdWVyeShcIi50b29sdGlwXCIsIHRvZ2dsZXIpLmZhZGVPdXQoKS5yZW1vdmVDbGFzcyhcInRvcHBlZFwiKTtcblx0XHRcdFx0fSk7XG5cdFx0XHR9KTtcblx0XHR9KTtcblx0fVxuXG5cdHZhciBoYW5kbGVTdWJtaXQgPSBmdW5jdGlvbigpXG5cdHtcblx0XHRqUXVlcnkoXCIuc3VibWl0LXNlYXJjaFwiKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKClcblx0XHR7XG5cdFx0XHR2YXIgcGF0aFBhcnRzID0gZ2V0UGF0aFBhcnRzKCk7XG5cdFx0XHR2YXIgcXVlcnlQYXJhbXMgPSB7fTtcblx0XHRcdGZvcih2YXIgaSA9IDA7IGk8Y3VycmVudExldmVsOyBpKyspXG5cdFx0XHR7XG5cdFx0XHRcdGpRdWVyeShcIltkYXRhLWxldmVsPSdcIiArIHBhcnNlSW50KGkrMSkgK1wiJ11cIikuZWFjaCggZnVuY3Rpb24oKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0dmFyIGx2bCA9IGpRdWVyeSh0aGlzKS5kYXRhKFwibGV2ZWxcIik7XG5cdFx0XHRcdFx0aWYobHZsID09IGkrMSAmJiBqUXVlcnkodGhpcykuZGF0YShcInBhdGhcIikuaW5kZXhPZihwYXRoUGFydHNbaV0pID09IDApXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0dmFyIGN1cnJFbCA9IGpRdWVyeSh0aGlzKTtcblx0XHRcdFx0XHRcdGpRdWVyeShcImlucHV0XCIsIGN1cnJFbCkuZWFjaCggZnVuY3Rpb24oKVxuXHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHRpZihqUXVlcnkodGhpcykuZGF0YShcInByb3AtbmFtZVwiKSlcblx0XHRcdFx0XHRcdFx0XHRxdWVyeVBhcmFtc1tqUXVlcnkodGhpcykuZGF0YShcInByb3AtbmFtZVwiKV0gPSBqUXVlcnkodGhpcykudmFsKCk7XG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXHRcdFx0fVxuXHRcdFx0dmFyIHVybCA9IFwiXCI7XG5cdFx0XHRpZihwYXRoUGFydHNbMV0gPT0gXCJxdWlja2xvYW5zXCIpIHVybCA9IFwiL2ZpbmFuc293YW5pZS9jaHdpbG93a2kvXCI7XG5cdFx0XHRpZihwYXRoUGFydHNbMV0gPT0gXCJsb2Fuc1wiKSB1cmwgPSBcIi9maW5hbnNvd2FuaWUvcG96eWN6a2ktbmEtcmF0eS9cIjtcblx0XHRcdGlmKHBhdGhQYXJ0c1sxXSA9PSBcImNyZWRpdHNcIikgdXJsID0gXCIvZmluYW5zb3dhbmllL2tyZWR5dHktZ290b3drb3dlL1wiO1xuXHRcdFx0aWYocGF0aFBhcnRzWzFdID09IFwiaW52ZXN0bWVudHNcIikgdXJsID0gXCIvbG9rYXR5L1wiO1xuXG5cdFx0XHRpZihwYXRoUGFydHNbMV0gPT0gXCJhY2NvdW50c1wiKVxuXHRcdFx0e1xuXHRcdFx0XHRpZihqUXVlcnkoXCJpbnB1dFtuYW1lPSdhY2NvdW50cy50eXBlJ11cIikudmFsKCkgPT0gXCJwZXJzb25hbFwiKSB1cmwgPSBcIi9rb250YS1iYW5rb3dlL2tvbnRhLW9zb2Jpc3RlL1wiO1xuXHRcdFx0XHRpZihqUXVlcnkoXCJpbnB1dFtuYW1lPSdhY2NvdW50cy50eXBlJ11cIikudmFsKCkgPT0gXCJidXNpbmVzc1wiKSB1cmwgPSBcIi9rb250YS1iYW5rb3dlL2tvbnRhLWZpcm1vd2UvXCI7XG5cdFx0XHRcdGlmKGpRdWVyeShcImlucHV0W25hbWU9J2FjY291bnRzLnR5cGUnXVwiKS52YWwoKSA9PSBcImN1cnJlbmN5XCIpIHVybCA9IFwiL2tvbnRhLWJhbmtvd2Uva29udGEtd2FsdXRvd2UvXCI7XG5cdFx0XHRcdGlmKGpRdWVyeShcImlucHV0W25hbWU9J2FjY291bnRzLnR5cGUnXVwiKS52YWwoKSA9PSBcInNhdmluZ1wiKSB1cmwgPSBcIi9rb250YS1iYW5rb3dlL2tvbnRhLW9zemN6ZWRub3NjaW93ZS9cIjtcblx0XHRcdH1cblx0XHRcdHVybCArPSBcIj9cIjtcblxuLy9cdFx0XHRjb25zb2xlLmxvZyhxdWVyeVBhcmFtcyk7XG5cdFx0XHRqUXVlcnkuZWFjaChxdWVyeVBhcmFtcywgIGZ1bmN0aW9uKGssdilcblx0XHRcdHtcblx0XHRcdFx0dXJsICs9IGsgKyBcIj1cIiArIHYgKyBcIiZcIjtcblx0XHRcdH0pXG5cblx0XHRcdGxvY2F0aW9uLmhyZWYgPSB1cmw7XG5cblx0XHR9KVxuXHR9XG5cblx0dmFyIGhhbmRsZVNjcm9sbERvd24gPSBmdW5jdGlvbigpXG5cdHtcblx0XHRqUXVlcnkoXCIuc2VhcmNoYm94IC5jdGEgYVwiKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKGUpXG5cdFx0e1xuXHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0d2luZG93LmxvY2F0aW9uLmhhc2ggPSBqUXVlcnkodGhpcykuYXR0cihcImhyZWZcIik7XG5cdFx0XHRqUXVlcnkoXCJib2R5LGh0bWxcIikuYW5pbWF0ZSh7IHNjcm9sbFRvcDogalF1ZXJ5KFwiLm1haW4tY29udGVudC5wYWdlLWNvbnRlbnRcIikub2Zmc2V0KCkudG9wIH0pO1xuXHRcdH0pXG5cdH1cblxuXHR2YXIgaGFuZGxlWmluZGV4ID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0alF1ZXJ5KFwiLnNlYXJjaGJveCBbZGF0YS1sZXZlbF1cIikub24oXCJjbGlja1wiLCBmdW5jdGlvbihlKVxuXHRcdHtcblx0XHRcdGUuc3RvcFByb3BhZ2F0aW9uKCk7XG5cblx0XHRcdGpRdWVyeShcIi5zZWFyY2hib3ggW2RhdGEtbGV2ZWxdXCIpLmNzcyhcInotaW5kZXhcIiwgXCJcIik7XG5cdFx0XHRqUXVlcnkodGhpcykuY3NzKFwiei1pbmRleFwiLCAyMCk7XG5cdFx0fSlcblx0fVxuXG5cblx0dmFyIHNldFBhdGggPSBmdW5jdGlvbih0YXJnZXRQYXRoLCB0YXJnZXRMZXZlbClcblx0e1xuXHRcdGlmKHR5cGVvZih0YXJnZXRQYXRoKSAhPSBcInVuZGVmaW5lZFwiICYmIHR5cGVvZih0YXJnZXRMZXZlbCkgIT0gXCJ1bmRlZmluZWRcIilcblx0XHR7XG5cdFx0XHRjdXJyZW50UGF0aCA9IHRhcmdldFBhdGg7XG5cdFx0XHRjdXJyZW50TGV2ZWwgPSB0YXJnZXRMZXZlbDtcblxuXHRcdFx0dmFyIHBhdGhQYXJ0cyA9IGdldFBhdGhQYXJ0cygpO1xuXG5cdFx0XHRmb3IodmFyIGkgPSAwOyBpPGN1cnJlbnRMZXZlbDsgaSsrKVxuXHRcdFx0e1xuXHRcdFx0XHRqUXVlcnkoXCJbZGF0YS1sZXZlbD0nXCIgKyBwYXJzZUludChpKzEpICtcIiddXCIpLmVhY2goIGZ1bmN0aW9uKClcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHZhciBsdmwgPSBqUXVlcnkodGhpcykuZGF0YShcImxldmVsXCIpO1xuXHRcdFx0XHRcdGlmKGx2bCA9PSBpKzEgJiYgalF1ZXJ5KHRoaXMpLmRhdGEoXCJwYXRoXCIpLmluZGV4T2YocGF0aFBhcnRzW2ldKSA9PSAwKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdHZhciBjdXJyRWwgPSBqUXVlcnkodGhpcyk7XG5cdFx0XHRcdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKClcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0Y3VyckVsLmZhZGVJbihcImZhc3RcIikuY3NzKFwiZGlzcGxheVwiLCBcImlubGluZS1ibG9ja1wiKTtcblx0XHRcdFx0XHRcdH0sIDM1MClcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdFx0alF1ZXJ5KHRoaXMpLmZhZGVPdXQoMzAwKTtcblxuXHRcdFx0XHR9KTtcblx0XHRcdH1cblxuXHRcdH1cblx0fVxuXG5cdHZhciBnZXRQYXRoUGFydHMgPSBmdW5jdGlvbigpXG5cdHtcblx0XHR2YXIgc2xpY2VkUGF0aCA9IGN1cnJlbnRQYXRoLnNwbGl0KFwiLlwiKTtcblx0XHR2YXIgcGF0aFBhcnRzID0gW1wiXCJdO1xuXHRcdHZhciB0bXBQYXRoID0gXCJcIjtcblxuXHRcdGZvcih2YXIgayBpbiBzbGljZWRQYXRoKVxuXHRcdHtcblx0XHRcdGlmKGsgPT0gMCkgdG1wUGF0aCA9IHNsaWNlZFBhdGhbMF07XG5cdFx0XHRlbHNlIHRtcFBhdGggKz0gXCIuXCIgKyBzbGljZWRQYXRoIFtrXTtcblx0XHRcdHBhdGhQYXJ0cy5wdXNoKHRtcFBhdGgpO1xuXHRcdH1cblxuXHRcdHJldHVybiBwYXRoUGFydHM7XG5cdH1cblxuXHR2YXIgaGFuZGxlQ2xpY2tPdXRzaWRlID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0alF1ZXJ5KHdpbmRvdykub24oXCJjbGlja1wiLCBmdW5jdGlvbigpIHtcblx0XHRcdGpRdWVyeShcIi50b3BwZWRcIikuc3RvcCgpLmZhZGVPdXQoKS5yZW1vdmVDbGFzcyhcInRvcHBlZFwiKTtcblx0XHR9KTtcblx0fVxuXG5cdC8qIF9fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX18gcHVibGljIG1ldGhvZHMgKi9cblxuXHR2YXIgaW5pdCA9IGZ1bmN0aW9uKClcblx0e1xuXHRcdGFwcGx5Q2hvaWNlcygpO1xuXHRcdGFwcGx5VG9vbHRpcHMoKTtcblx0XHRhcHBseVNsaWRlcnMoKTtcblx0XHRoYW5kbGVTdWJtaXQoKTtcblx0XHRoYW5kbGVTY3JvbGxEb3duKCk7XG5cdFx0aGFuZGxlWmluZGV4KCk7XG5cdFx0aGFuZGxlQ2xpY2tPdXRzaWRlKCk7XG5cdH07XG5cblx0aW5pdCgpO1xufTtcblxudmFyIHNlYXJjaGJveDtcbiJdfQ==
