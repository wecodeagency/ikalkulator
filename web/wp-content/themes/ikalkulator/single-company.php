<?php get_header(); ?>

<main class="page-main product" role="main" itemid="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" itemscope itemtype="http://schema.org/Article">
    <?php include("template-parts/head/head.php" ); ?>
    <div class="main-content">
        <article class="post-content">
            <div class="content" itemprop="articleBody">
            <?php             
                the_content();
            ?>
            </div>
			<div class="sidebar">

                <?php if( have_rows('address', $post) ):
                while( have_rows('address') ) : the_row();?>
                <?php if( get_sub_field('address_name', $post) ||
                get_sub_field('address_address', $post) ||
                get_sub_field('address_email', $post) ||
                get_sub_field('address_phone', $post) ||
                get_sub_field('address_hours_week', $post) ||
                get_sub_field('address_hours_sat', $post) ||
                get_sub_field('address_hours_sun', $post) ||
                get_field('product_banks', $post) ||
                get_field('product_checking', $post) ):


                ?>

                <div class="address">
                    <h3>&nbsp;&nbsp;&nbsp;Dane teleadresowe firmy</h3>
                    <ul>

                        <?php if( get_sub_field('address_name', $post) ):
                            ?>
                            <li><strong>Nazwa firmy:</strong> <?php echo( get_sub_field('address_name', $post) ); ?></li>
                        <?php endif;?>

                        <?php if( get_sub_field('address_address', $post) ):
                            ?>
                            <li><strong>Adres firmy:</strong> <?php echo( get_sub_field('address_address', $post) ); ?></li>
                        <?php endif;?>

                        <?php if( get_sub_field('address_phone', $post) ):
                            ?>
                            <li><strong>Telefon:</strong> <?php echo( get_sub_field('address_phone', $post) ); ?></li>
                        <?php endif;?>

                        <?php if( get_sub_field('address_email', $post) ):
                            ?>
                            <li>
                                <strong>E-mail:</strong>
                                <a href="mailto:<?php echo( get_sub_field('address_email', $post) ); ?>">
                                    <?php echo( get_sub_field('address_email', $post) ); ?>
                                </a>
                            </li>
                        <?php endif;?>
                    </ul>

                </div>

                <?php
                    endif;
                endwhile;
                endif;
                ?>
			</div>            
        </article>

        <?php get_sidebar("bottom"); ?>
   </div>
</main>
<?php get_footer(); ?>
