<?php get_header();


$wordCount = str_word_count(strip_tags(get_the_content()));
$current_category = get_post_categories(get_post());
$categories = categories_comma_separated($current_category);

set_query_var("word_count", $wordCount);

$args = array(
    "post_type"			=> "post",
    "category"			=> $categories,
    "exclude"           => get_the_ID(),
    "posts_per_page"	=> -1,
    "orderby"			=> "rand",
//    "orderby"			=> "date",
//    "order"				=> "DESC",
);

$posts = get_posts($args);

$filtered_posts = array();

    foreach ($posts as $filtered_post)
    {
        if(get_field("promotion_end_time", $filtered_post->ID))
        {

            $diff = (strtotime(get_field("promotion_end_time", $filtered_post->ID)) - time()) / (60 * 60 * 24);
            if($diff >= 0)
            {
                $filtered_posts[] = $filtered_post;
            }
        }
        else
        {
                $filtered_posts[] = $filtered_post;                
        }
        
        if (count($filtered_posts) == 2)
            break;
    }

    if ( $filtered_posts )
    {
            $additional_posts = '<div class="posts-list-content">';
            foreach ($filtered_posts as $p)
            {
                $additional_posts .=  create_post_list_item($p, "post-list-medium", "", "h3");
            }
            $additional_posts .= '</div>';
    }
?>
<?php get_template_part( "template-parts/post/floating-header" ); ?>
<main class="page-main" role="main" itemid="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" itemscope itemtype="http://schema.org/Article">
    <?php include("template-parts/head/head.php" ); ?>
    <div class="main-content">
        <article class="post-content">
            <?php get_sidebar("top"); ?>
            <div class="content" itemprop="articleBody">
            <?php
                if(get_featured_image($post->ID))
                {
                    echo '<div class="image" ><img src="'.get_featured_image($post->ID).'" itemprop="image" alt="' . get_the_title($post->ID) . '" /></div>';
                }                
                the_content();
        ?>
            </div>

            <?php
                $cats = get_post_categories(get_post());
//                $tags = wp_get_post_tags(get_post()->ID);
                $tags = get_post_tags(get_post());
                echo create_terms_labels_list($cats, $tags, "dark");

                include("template-parts/post/share.php" );
            ?>
            <section class="thumbvote" data-postid="<?php echo get_the_ID(); ?>">
                <div class="label">Czy ten artykuł był pomocny?</div>
                <div class="thumbs"><button class="up"></button><button class="down"></button></div>
                <?php
                if(is_user_logged_in())
                {
                    echo '(';
                    echo get_post_meta($post->ID, 'thumbsup', true) ? "+".get_post_meta($post->ID, 'thumbsup', true) : "0";
                    echo '/';
                    echo get_post_meta($post->ID, 'thumbsdown', true) ? "-".get_post_meta($post->ID, 'thumbsdown', true) : "0";
                    echo ')';

                }
                ?>
            </section>            
            <section class="posts post-list col-2">
                <?php echo $additional_posts; ?>
            </section>
            <meta itemprop="mainEntityOfPage" content="<?php echo get_permalink(); ?>" />

        </article>
        <section class="comments">
            <?php $comments_number = count(get_approved_comments($post->ID)); ?>
            <div class="show-action">
                <button class="btn"><span class="invisible">Komentarze (<?php echo $comments_number; ?>)</span><span class="visible">Schowaj komentarze</span></button>
            </div>
            <div class="comments-content">
                <?php
                if ( comments_open() || $comments_number ) :
                    comments_template();
                endif;
                ?>
            </div>
        </section>
        <?php get_sidebar("bottom"); ?>
   </div>
</main>
<?php get_footer(); ?>
