<?php get_header(); ?>
<main class="page-main home" role="main">

		<?php
			include( "template-parts/searchbox/search.php" );
		?>
	<?php
	include( "template-parts/category/promobox.php" );
	?>
		<?php
			set_query_var("excluded_posts", []);
			set_query_var("excluded_categories", [31]); //array_merge(get_all_tags_id(), ));
			set_query_var( "cat", 13 );
			set_query_var("grid_layout", "layout_4");
			get_template_part( "template-parts/category/megabox" );
		?>

    <div class="main-content page-content">
		<?php the_content(); ?>
	</div>
</main>
<?php get_footer(); ?>