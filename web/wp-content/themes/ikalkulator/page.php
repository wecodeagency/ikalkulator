<?php get_header(); ?>
<main class="page-main" role="main">
	<?php get_template_part( "template-parts/head/head" ); ?>
    <div class="main-content page-content">
		<?php the_content(); ?>
	</div>
</main>
<?php get_footer(); ?>