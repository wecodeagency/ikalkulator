<?php
get_header();

$schemaData = [];
$schemaData["@context"] = "http://schema.org";
include("template-parts/products/schema-definition/".$post->post_type.".php" );
$comments_number = count(get_approved_comments($post->ID));
?>
	<main class="page-main product" role="main">
		<?php

			$postContent = get_the_content();
//			$wordCount = str_word_count(strip_tags($post->post_content));
			include("template-parts/head/head.php" );
		?>

		<div class="main-content post-content">
			<div class="content"> <?php

			include("template-parts/products/products-single-metrics/".$post->post_type.".php" );

			while ( have_posts() ) : the_post();
				the_content();
			endwhile;
			?>
			</div>
			<div class="sidebar">
				<?php if(!get_field('product_inactive')): ?>
						<div class=""><a class="btn" href="<?php echo get_field('product_lender_tracking_url'); ?>" target="_blank">Złóż wniosek</a></div>
					<?php endif; ?> 
				<div class="rating">
					<h3>Nasza ocena</h3>
					<div class="star-rating" data-rating="<?php echo( get_field('product_ikalkulator_rating')); ?>">
						<div class="stars"></div> <div class="mark"><span><?php if(get_field('product_ikalkulator_rating')) echo( number_format(get_field('product_ikalkulator_rating'), 1, ".", "")); ?></span> / 5.0</div>
						<?php
                            
							/*
							 * Strucutred Data - rank
							 */

							if($comments_number)
							{
								if (get_post_meta($post->ID, 'avg_rating', true))
								{
									$schemaData["aggregateRating"] = [
										"@type" =>	"AggregateRating",
										"ratingValue" =>  number_format(floatval(get_post_meta($post->ID, 'avg_rating', true)), 1, ".", ""),
										"ratingCount" => $comments_number
									];
								}
							}
						?>
					</div>


					<h3>Oceny klientów</h3>

					<?php if(get_post_meta($post->ID, 'avg_rating', true)): ;?>

					<div class="star-rating" data-rating="<?php echo ( number_format(get_post_meta($post->ID, 'avg_rating', true), 1, ".", "")); ?>">
						<div class="stars"></div><div class="mark" ><?php if(get_post_meta($post->ID,'avg_rating')) echo( number_format(floatval(get_post_meta($post->ID, 'avg_rating', true)), 1, ".", "")); ?> / 5.0</div>
					</div>
						<?php else: ?>
						brak ocen<br/>
					<?php endif; ?>
					<button class="btn" id="doRate">Oceń</button>
				</div>
				<div class="advantages">
					<h3><span>+</span> Zalety produktu</h3>
					<?php if( have_rows('product_advantages', $post) ):
						echo '<ul>';
						while( have_rows('product_advantages') ) : the_row();

							echo '<li>' . get_sub_field('product_advantages_content') . '</li>';

						endwhile;
						echo '</ul>';
					endif;?>
				</div>
				<div class="disadvantages">
					<h3><span>-</span> Wady produktu</h3>
					<?php if( have_rows('product_disadvantages', $post) ):
						echo '<ul>';
						while( have_rows('product_disadvantages') ) : the_row();

							echo '<li>' . get_sub_field('product_disadvantages_content') . '</li>';

						endwhile;
						echo '</ul>';
					endif;?>
				</div>
				<?php if($post->post_type == "quickloans" || $post->post_type == "loans"): ?>
				<div class="requirements">

					<h3>&nbsp;&nbsp;&nbsp;Wymagania</h3>
					<ul>
						<li class="age"> od <?php echo get_field('product_min_age'); ?> do <?php echo get_field('product_max_age'); ?> lat</li>
						<li class="id"> ważny dowód osobisty</li>
						<li class="phone"> własny numer tel. komórkowego</li>
						<li class="baccount"> indywidualne konto bankowe</li>
					</ul>
				</div>
				<?php endif; ?>

				<?php if( have_rows('product_attachments', $post) ): ?>
					<div class="attachments">
						<h3>&nbsp;&nbsp;&nbsp;Załączniki</h3>
						<?php	echo '<ul>';
						while( have_rows('product_attachments') ) : the_row();

							echo '<li><a href="'.get_sub_field('product_attachments_file') .'" target="_blank" rel="nofollow">' . get_sub_field('product_attachments_description') . '</a></li>';

						endwhile;
						echo '</ul>'; ?>
					</div>
				<?php endif;?>


<?php if( have_rows('address', $post) ):
	while( have_rows('address') ) : the_row();?>
				<?php if( get_sub_field('address_name', $post) ||
							get_sub_field('address_address', $post) ||
							get_sub_field('address_email', $post) ||
							get_sub_field('address_phone', $post) ||
							get_sub_field('address_hours_week', $post) ||
							get_sub_field('address_hours_sat', $post) ||
							get_sub_field('address_hours_sun', $post) ||
							get_field('product_banks', $post) ||
							get_field('product_checking', $post) ):

					$schemaData["provider"] = ["@type" => "Organization"];

			?>

					<div class="address">
						<h3>&nbsp;&nbsp;&nbsp;Dane teleadresowe firmy</h3>
						<ul>

							<?php if( get_sub_field('address_name', $post) ):
								$schemaData["provider"]["name"] = 	get_sub_field('address_name', $post);
							?>
								<li><strong>Nazwa firmy:</strong> <?php echo( get_sub_field('address_name', $post) ); ?></li>
							<?php endif;?>

							<?php if( get_sub_field('address_address', $post) ):
								$schemaData["provider"]["address"] = 	get_sub_field('address_address', $post);
							?>
								<li><strong>Adres firmy:</strong> <?php echo( get_sub_field('address_address', $post) ); ?></li>
							<?php endif;?>

							<?php if( get_sub_field('address_phone', $post) ):
								$schemaData["provider"]["telephone"] = 	get_sub_field('address_phone', $post);
							?>
								<li><strong>Telefon:</strong> <?php echo( get_sub_field('address_phone', $post) ); ?></li>
							<?php endif;?>

							<?php if( get_sub_field('address_email', $post) ):
								$schemaData["provider"]["email"] = 	get_sub_field('address_email', $post);
							?>
								<li>
									<strong>E-mail:</strong>
									<a href="mailto:<?php echo( get_sub_field('address_email', $post) ); ?>">
										<?php echo( get_sub_field('address_email', $post) ); ?>
									</a>
								</li>
							<?php endif;?>

							<?php if( get_sub_field('address_hours_week', $post) || get_sub_field('address_hours_sat', $post) || get_sub_field('address_hours_sun', $post) ):
								$schemaData["hoursAvailable"] = ["@type" => "OpeningHoursSpecification"];
							?>
								<li><strong>Godziny otwarcia:</strong>
									<?php
									$hours = [];
									$schemaData["hoursAvailable"] = [];

									if( get_sub_field('address_hours_week', $post) )
									{
										$hours []= "pn.-pt.&nbsp;" .  get_sub_field('address_hours_week', $post);
										$splitHours = explode("-", get_sub_field('address_hours_week', $post));
										$startHours = trim($splitHours[0]);
										$endHours = trim($splitHours[1]);

										$days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];

										foreach($days as $day)
										{
											$schemaData["hoursAvailable"][] = [
												"@type" => "OpeningHoursSpecification",
												"closes" => $startHours,
												"opens" => $endHours,
												"dayOfWeek" => "http://schema.org/".$day
											];
										}

									}

									if( get_sub_field('address_hours_sat', $post) )
									{
										$hours []= "sob.&nbsp;" .  get_sub_field('address_hours_sat', $post);
										$splitHours = explode("-", get_sub_field('address_hours_sat', $post));
										$startHours = trim($splitHours[0]);
										$endHours = trim($splitHours[1]);
										$schemaData["hoursAvailable"][] = [
											"@type" => "OpeningHoursSpecification",
											"closes" => $startHours,
											"opens" => $endHours,
											"dayOfWeek" => "http://schema.org/Saturday"
										];

									}

									if( get_sub_field('address_hours_sun', $post) )
									{
										$hours []= "nd.&nbsp;" .  get_sub_field('address_hours_sun', $post);
										$splitHours = explode("-", get_sub_field('address_hours_sun', $post));
										$startHours = trim($splitHours[0]);
										$endHours = trim($splitHours[1]);
										$schemaData["hoursAvailable"][] = [
											"@type" => "OpeningHoursSpecification",
											"closes" => $startHours,
											"opens" => $endHours,
											"dayOfWeek" => "http://schema.org/Sunday"
										];

									}

									echo implode(", ", $hours);
									?>
								</li>
							<?php endif;?>


						<?php if( get_field('product_banks', $post) ): ?>
							<li><strong>Konta bankowe:</strong><br/>
								<?php foreach(get_field('product_banks', $post) as $bank)
								{
									echo '<img src="/wp-content/themes/ikalkulator/assets/img/icons/banki/'.$bank.'.png" alt="Bank '.$bank.'" />';
								}
								?>
							</li>
						<?php endif;?>

							<?php if( get_field('product_checking', $post) ): ?>
								<li><strong>Instytucje:</strong><br/>
									<?php foreach(get_field('product_checking', $post) as $bank)
									{
										echo '<img src="/wp-content/themes/ikalkulator/assets/img/icons/bik/'.$bank.'.png"  alt="'.$bank.'"/>';
									}
									?>
								</li>
							<?php endif;?>
						</ul>

					</div>

				<?php endif;
			endwhile;
		endif;?>

				<div class="related-posts">

					<?php
					if(get_field('product_related_taxonomy'))
					{
						$args = array(
							'posts_per_page' => 3,
							'post_type' => 'post',
							'cat' => get_field('product_related_taxonomy'),
						);

						$relatedPosts = new WP_Query( $args );

						if($relatedPosts->posts) echo '<h3>Powiązane artykuły</h3><ul>';
						foreach($relatedPosts->posts as $relatedPost)
						{
							echo '<li><a href="'.get_permalink($relatedPost->ID).'">';
							echo '<div class="img"><img src="'.get_the_post_thumbnail_url( $relatedPost->ID , 'post-thumbnail' ).'" alt="'.$relatedPost->post_title.'"/></div>';
							echo '<div>'.$relatedPost->post_title.'</div>';
							echo '</a></li>';
						}
						if($relatedPosts) echo '</ul>';
					}

					?>
				</div>
			</div>

            <?php include("template-parts/post/share.php" ); ?>

			<?php if(!get_field('product_inactive')): ?>
				<div class="cta"><a class="btn" href="<?php echo get_field('product_lender_tracking_url'); ?>" target="_blank">Złóż wniosek</a></div>
			<?php endif; ?>

			<section class="comments">
				<div class="show-action">
					<button class="btn"><span class="invisible">Komentarze (<?php echo $comments_number; ?>)</span><span class="visible">Schowaj komentarze</span></button>
				</div>
				<div class="comments-content">
					<?php
					if ( comments_open() || $comments_number ) :
						comments_template();
					endif;
					?>
				</div>

			</section>
		</div>

	</main>

<script type="application/ld+json">
	<?php

		$schemaData["areaServed"] = "Polska";
		$schemaData["availableChannel"] = "strona www";
			echo json_encode($schemaData);
	?>
</script>
<?php get_footer(); ?>
