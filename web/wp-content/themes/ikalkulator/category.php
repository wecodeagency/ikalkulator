<?php
get_header();
$excluded_posts = array();
set_query_var("excluded_posts", $excluded_posts);

if (treat_cat_as_tag(get_current_taxonomy()))
{?>
    <main class="page-main" role="main">
        <?php
        get_template_part( "template-parts/head/head" );
        ?>
        <div class="main-content category-content xxx">
            <?php get_sidebar("top"); ?>
            <section class="category-content">
            <?php
                set_query_var("two_column_posts_list", true);
                get_template_part( "template-parts/category/posts-list-paginated");
            ?>
            </section>
            <?php get_sidebar("bottom"); ?>
        </div>
    </main> 
<?php
} else
{
    $category_template = get_taxonomy_field("blog_category_template");
    $category_with_megabox = get_taxonomy_field("blog_category_megabox_visible");
    $category_content = get_taxonomy_field("blog_category_content"); 

    $meta_args = [
        'post_type' => ['quickloans', 'loans', 'credits', 'investments', 'accounts_personal', 'accounts_business', 'accounts_currency', 'accounts_saving'],
        'meta_key'     => 'display_in_megabox',
        'meta_value'   => '"'.get_queried_object()->term_id.'"',
        'meta_compare' => 'LIKE'

    ];
    $find_related_products = new WP_Query( $meta_args );




    //$category_with_megabox = count($find_related_products->posts) ? true : false;
    $megabox_class = $category_with_megabox  ? " with-megabox" : "";
    set_query_var("with_metabox", $category_with_megabox);
    set_query_var("featured_posts", $find_related_products->posts);
    ?>
    <main class="page-main<?php echo $megabox_class; ?>" role="main">
        <?php
        get_template_part( "template-parts/head/head" );
        if ($category_with_megabox && $category_template != "megabox-content-news")
        {
            get_template_part( "template-parts/category/megabox" );
        }
        ?>
        <div class="main-content category-content">
            <?php get_sidebar("top"); ?>
            <?php 
            if ($category_content && $category_template != "one-column-and-sidebar" )
            { 
                get_template_part( "template-parts/category/text-content" );
            }
            ?>
            <section class="category-content">
            <?php
            switch ($category_template)
            {            
                case "one-column":
                    get_template_part( "template-parts/category/posts-list-paginated" );
                    break; 

                case "one-column-and-sidebar":
                    get_template_part( "template-parts/post/aside" ); 
                    get_template_part( "template-parts/category/text-content" );
                    get_template_part( "template-parts/category/posts-list-paginated" );
                    break;

                case "two-columns":
                    set_query_var("two_column_posts_list", true);
                    get_template_part( "template-parts/category/posts-list-paginated");
                    break;            

                default:
                    set_query_var("two_column_posts_list", true);
                    get_template_part( "template-parts/category/posts-list-paginated");
                    break;
            }
            ?>
            </section>
            <?php get_sidebar("bottom"); ?>
        </div>
    </main>
<?php
} ?>
<?php get_footer();
