<?php if ( is_active_sidebar( "sidebar-top" ) ) : ?>
	<div class="sidebar sidebar-top widget-area" role="complementary">
		<?php dynamic_sidebar( "sidebar-top" ); ?>
	</div>
<?php endif; ?>