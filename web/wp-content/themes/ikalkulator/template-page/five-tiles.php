<?php
/* Template Name: Pięć kafli */ 

global $no_scroll_to_content;
$no_scroll_to_content = true;

$post_in_cat = get_field("post_in_category");    
$post_in_tag = get_field("post_in_tag");  

set_query_var("posts_categories_id", $post_in_cat);
set_query_var("posts_tags_id", $post_in_tag);
?>
<?php get_header(); ?>
<main class="page-main five-tiles" role="main">
	<?php get_template_part( "template-parts/head/head" ); ?>
    <div class="main-content page-content">
        <?php get_template_part( "template-parts/specific/tiles" ); ?>
        <?php get_template_part( "template-parts/post/blocks-list" ); ?>
		<?php the_content(); ?>
	</div>
</main>
<?php get_footer(); ?>
