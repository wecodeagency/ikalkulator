productFilter = function ()
{
	var sortAttribute = null;
	var sortType = null;
	var sortDirection = null;

	var applySliders = function()
	{
		jQuery(".slider[data-filter-name]").each( function()
		{
			var options = {};
			var sliderContainer = jQuery(this);

			if(jQuery(this).data("filter-type") == "between")
			{
				options.range = true;
				options.values = [];

				if(typeof(jQuery(this).data("value-min")) != "undefined" && jQuery(this).data("value-min") >= 0 && jQuery(this).data("value-max"))
				{
					options.values.push(jQuery(this).data("value-min"));
					options.values.push(jQuery(this).data("value-max"));
				};
			}
			else if(jQuery(this).data("filter-type") == "to")
			{
				options.range = "min";

				if(jQuery(this).data("value"))
				{
					options.value = jQuery(this).data("value");
				};
			};

			if(jQuery(this).data("min") != null)
			{
				options.min = jQuery(this).data("min");
			};

			if(jQuery(this).data("max"))
			{
				options.max = jQuery(this).data("max");
			};



			var pointsValues = [];

			if(jQuery(this).data("step"))
			{

				var stepVal = jQuery(this).data("step");
				var length = Math.ceil((options.max-options.min)/stepVal);
				var start = Math.floor(options.min/stepVal);
				for(var i = start; i <length+start; i++)
				{
					if(i == start && options.min < i*stepVal)
					{
						pointsValues.push(options.min);
					}
					pointsValues.push(i*stepVal)
					if(i == length+start-1 && options.max > i*stepVal)
					{
						pointsValues.push(options.max);
					}
				}


				if(pointsValues.length)
				{
					options.min = 0;
					options.max = pointsValues.length-1;

					if(options.value)
					{
						options.value = pointsValues.indexOf(options.value);
					}

					if(options.values)
					{
						options.values[0] = pointsValues.indexOf(options.values[0]);
						options.values[1] = pointsValues.indexOf(options.values[1]);
					}
				}

			}


			if(jQuery(this).data("progressive") == true)
			{
				var rangesConfig =
				{
					default:
					[
						{"min": 0, "max": 900, "step": 100},
						{"min": 1000, "max": 9000, "step": 1000},
						{"min": 10000, "max": 48000, "step": 2000},
						{"min": 50000, "max": 50000000, "step": 5000},
					],
					investments: [
						{"min": 0, "max": 5000, "step": 500},
						{"min": 6000, "max": 19000, "step": 1000},
						{"min": 20000, "max": 95000, "step": 5000},
						{"min": 100000, "max": 475000, "step": 25000},
						{"min": 500000, "max": 10000000, "step": 50000},
					],
					investmentsMonths:
					[
						{"min": 0, "max": 23, "step": 1},
						{"min": 24, "max": 600, "step": 12}
					]
				};

				var ranges = rangesConfig.default;

				if(jQuery(this).data("progressive-values") && rangesConfig[jQuery(this).data("progressive-values")])
					ranges = rangesConfig[jQuery(this).data("progressive-values")];

				for(i=0; i < ranges.length; i++)
				{
					var current = 0;
					if(options.min > ranges[i].max || options.max < ranges[i].min)
					{
						continue;
					}
					else
					{
						current = ranges[i].min;
						while(current < options.min)
						{
							current += ranges[i].step;
						}

						if(options.min > ranges[i].min && current-ranges[i].step < options.min)
						{
							pointsValues.push(options.min);
						}

						while(current <= options.max && current <= ranges[i].max)
						{
							pointsValues.push(current);
							current += ranges[i].step;
						}
					}
				}


				if(pointsValues.length)
				{
					options.min = 0;
					options.max = pointsValues.length-1;

					if(options.value)
					{
						options.value = pointsValues.indexOf(options.value);
					}

					if(options.values)
					{
						options.values[0] = pointsValues.indexOf(options.values[0]);
						options.values[1] = pointsValues.indexOf(options.values[1]);
					}
				}


			}

			options.slide = function( event, ui )
			{
				if(ui.value || pointsValues[ui.value])
				{
					var exVal = ui.value;

					if(pointsValues.length)
					{
						exVal = pointsValues[ui.value];
					}

					sliderContainer.find(".value").html( formatNumber(exVal) );
					sliderContainer.parents("form").first().find("input[name='" + sliderContainer.data("filter-name") + "']").val(exVal)
				}
				if(ui.values)
				{
					var exValMin = ui.values[0];
					var exValMax = ui.values[1];

					if(pointsValues.length)
					{
						exValMin = pointsValues[ui.values[0]];
						exValMax = pointsValues[ui.values[1]];
					}

					sliderContainer.find(".value").html( formatNumber(exValMin) + " - " + formatNumber(exValMax) );
					sliderContainer.parents("form").first().find("input[name='min-" + sliderContainer.data("filter-name") + "']").val(exValMin)
					sliderContainer.parents("form").first().find("input[name='max-" + sliderContainer.data("filter-name") + "']").val(exValMax)
				}
			}

			options.change = function( event, ui )
			{
				sliderContainer.parents("form").first().trigger("ikal:filter:changed");
			}

			if(pointsValues)
			{
				if(pointsValues[options.min])
					sliderContainer.find(".range-min-value").html(pointsValues[options.min]);
				else
					sliderContainer.find(".range-min-value").html(pointsValues[(options.min + 1)] );

				sliderContainer.find(".range-max-value").html(pointsValues[options.max]);

			}
			else
			{
				sliderContainer.find(".range-min-value").html(options.min);
				sliderContainer.find(".range-max-value").html(options.max);

			}
			sliderContainer.find(".slider-bar").slider(options);
		});

	}

	var applyInputs = function()
	{
		jQuery(".filters form").each( function()
		{
			var filterForm = jQuery(this);
			jQuery("input[type=checkbox], input[type=radio], input[type=text]", filterForm).on("change", function() {
				filterForm.trigger("ikal:filter:changed");
			})
		});
	}

	var prepareForms = function()
	{
		jQuery(".filters form").each( function()
		{
			var filterForm = jQuery(this);

			filterForm.on("ikal:filter:changed", function()
			{
				var list = filterForm.parents(".product-list").first().find(".products.list");
				list.addClass("loading");
				if(jQuery("#page").data("product-slug"))
				{
					var url = "/wp-content/themes/ikalkulator/product-api/products.php?ptype=" + jQuery("#page").data("product-slug") + "&" + filterForm.serialize();
					jQuery.get(url, function(response)
					{
						list.first().html(response);
						refreshSorting(list);
						list.removeClass("loading");
					})
				}
			});
		});
	}

	var applySorting = function()
	{
		jQuery(".products.list").each( function()
		{
			var list = jQuery(this);
			var header = list.parent().find(".products.header");
			jQuery("ul li div.sortable a", header)
                .append('<span class="arrow"></span>')
                .on("click", jQuery.proxy(sortList, this, list));
		})
	}

	var sortList = function(list, event)
	{
		event.preventDefault();

		var sortby = jQuery(event.target).data("sort-by");
		var sorttyp = jQuery(event.target).data("sort-type");
		var sortdir = jQuery(event.target).data("sort-dir");

		if(sortAttribute == sortby)
		{
			if(sortDirection != "desc")
				sortDirection = "desc";
			else
				sortDirection = "asc";
		}
		else
		{
			if(sortdir == "desc")
				sortDirection = "desc";
			else
				sortDirection = "asc";
		}

		jQuery("div.sortable").removeClass("desc asc");

        jQuery(event.target).closest("div.sortable")
            .addClass(sortDirection);

		sortType = sorttyp;
		sortAttribute = sortby;

		jQuery(".sorted-field").removeClass("sorted-field");
		jQuery("." + sortAttribute).addClass("sorted-field");

		refreshSorting(list);
	}

	var refreshSorting = function(list)
	{
		list.find(" > ul > li").sort(jQuery.proxy(sorter, this, sortAttribute, sortType, sortDirection)).appendTo(list.find(" > ul"));
	}

	var sorter = function(sortby, sorttype, sortdir, a, b)
	{
		if(sorttype == "number")
		{
			if(sortdir == "asc") return (parseFloat(jQuery(b).closest("li").data(sortby)) < parseFloat(jQuery(a).closest("li").data(sortby))) ? 1 : -1;
			else return (parseFloat(jQuery(a).closest("li").data(sortby)) < parseFloat(jQuery(b).closest("li").data(sortby))) ? 1 : -1;
		}
	}

	var handleDetails = function()
	{
		jQuery(document).on("click", ".product-list .products.list ul li .more .expander", function()
		{
			jQuery(this).toggleClass("unfolded")
			jQuery(this).parent().find(".expanded").slideToggle();
		})
	}

	var handleOptional = function()
	{
		jQuery(".filters form .show-all").each( function()
		{
				var moveDownHeader = function()
				{
					if(jQuery(".container.products.header").hasClass("fixed"))
					{
						jQuery(".container.products.header").css("top", jQuery(".product-list .filters.container").outerHeight());
					}
				}
				var showAllBtn = jQuery(this);
				jQuery(showAllBtn).on("click", function() {
				showAllBtn.toggleClass("active");
				setInterval( moveDownHeader, 20);
				showAllBtn.closest("form").find(".togglable").slideToggle( function() {
					clearInterval( moveDownHeader );
					jQuery(window).trigger("scroll touchmove");
				});
			})
		});
	}

	var handleTooltip = function()
	{
		if(jQuery(".tooltip").length)
		{
			jQuery(window).on("click", function() {
				jQuery(".tooltip").css("z-index", "").find(".content").stop().slideUp();
			});

			jQuery(".tooltip .show").on("click", function(e)
			{
				e.stopPropagation();
				jQuery(".products .tooltip").css("z-index", "").find(".content").stop().slideUp();
				jQuery(this).closest(".tooltip").css("z-index", 19).find(".content").stop().slideToggle();
				if(jQuery(this).closest(".tooltip").find(".content").outerWidth() + jQuery(this).closest(".tooltip").find(".content").offset().left > jQuery(window).width())
				{
					jQuery(this).closest(".tooltip").find(".content").css("left", -jQuery(this).closest(".tooltip").find(".content").outerWidth()/2 + 40);
				}
			})

		}
	}

	var applyFixedFilter = function()
	{
        var filter =  jQuery(".product-list .filters");
        var productHeader = jQuery(".product-list .products.header");
        var productList = jQuery(".product-list .products.list");
        
        if (!filter.length && !productHeader.length)
            return;

		var margin = filter.offset().top - productList.offset().top;

		if(jQuery(window).width() < 768)
		{
			margin = -productHeader.height();
		}

		var execOnScroll = function()
		{
			if (productList.offset().top + margin <= jQuery(window).scrollTop() && jQuery(window).width() >= 768)
			{
				productList
					.css({
						"margin-top" : (filter.outerHeight(true) + productHeader.outerHeight(true)) + "px"
					});

				filter
					.addClass("fixed");

				productHeader
					.css({
						"top" : filter.outerHeight() + "px"
					})
					.addClass("fixed");

			}
			else
			{
				filter.removeClass("fixed");

				productHeader
					.css({
						"top" : ""
					})
					.removeClass("fixed");

				productList
					.css({
						"margin-top" : ""
					});

			}
		}
        
        jQuery(window).on("scroll", function() {
	        clearTimeout(execOnScroll);
	        setTimeout(execOnScroll, 50)
        });

		jQuery(window).on("touchmove", execOnScroll);
    }

	/* _________________________________________________________________________ public methods */

	var init = function()
	{
		applyInputs();
		applySliders();
		applySorting();
		applyFixedFilter();
		prepareForms();
		handleDetails();
		handleOptional();
		handleTooltip();
	};

	init();
};

var productFilter;

formatNumber = function (num) {
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")
}