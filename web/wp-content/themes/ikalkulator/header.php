<?php
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<title><?php wp_title(); ?></title>
	<meta charset="<?php bloginfo( "charset" ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php wp_head(); ?>
	<?php
	/*
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-9039982-61"></script>
	*/
	?>
	<?php if(is_front_page()): ?>
		<script type="application/ld+json">
		  {
		    "@context": "http://schema.org",
		    "@type": "Blog",
		    "url": "https://www.ikalkulator.pl/blog"
		  }
		</script>
	<?php endif ?>
	<?php
	/*	
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag()

		{dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-9039982-61', {'page_location': document.location.href});
	</script>
	*/
	?>
</head>
<body <?php body_class(); ?>>
<div class="site">
	<header class="site-header">
		<nav class="navigation-top" role="navigation">
			<div class="site-logo">
				<?php if (get_theme_mod("website_logo")) : ?>
					<a href="<?php echo esc_url(home_url("/")); ?>" class="logo"
					   title="<?php echo esc_attr(get_bloginfo("name", "display")); ?>" rel="home">
						<img src="<?php echo get_theme_mod("website_logo"); ?>"
						     alt="<?php echo esc_attr(get_bloginfo("name", "display")); ?>" />
					</a>
				<?php else : ?>
					<a href="<?php echo esc_url(home_url("/")); ?>"></a>
				<?php endif; ?>
			</div>
			<div class="btn-mobile-menu"><span></span></div>
			<?php if ( has_nav_menu( "main-menu" ) ) : ?>
				<div class="main-nav">
					<?php
					wp_nav_menu( array(
						"theme_location"	=> "main-menu",
						"menu"				=> "",
						"container_class"	=> "",
						"container"			=> "",
						"link_before"       => "<span>",
						"link_after"		=> "</span>",
						"items_wrap"		=> '<ul class="%2$s">%3$s</ul>',
                        "walker"         => new Primary_Walker_Nav_Menu()
					));
					?>
				</div>
			<?php endif; ?>
		</nav>
	</header>
	<div class="site-content">