<?php

echo '<ul>';
foreach ( $posts_array as $post ) : setup_postdata( $post );

	if($amount)
	{
		if((get_field('product_min_amount') && $amount < get_field('product_min_amount')) || (get_field('product_max_amount') && $amount > get_field('product_max_amount')))
		{
			continue;
		}
		if((get_field('product_min_period') && $period < get_field('product_min_period')) || (get_field('product_max_period') && $period > get_field('product_max_period')))
		{
			continue;
		}
		if($age && (get_field('product_min_age') && $age < get_field('product_min_age')) || (get_field('product_max_age') && $age > get_field('product_max_age')))
		{
			continue;
		}
		if($firstpromo && !get_field('product_first_promo'))
		{
			continue;
		}
		if($nobik && !get_field('product_no_bik') && $nobik==true)
		{
			continue;
		}

		$cost = 0;
		if(get_field('product_loan_costs') == "variable")
		{
			if( have_rows('product_costs', $post) ):
				while( have_rows('product_costs') ) : the_row();
					if($period >= get_sub_field('product_costs_days_from') && $period <= get_sub_field('product_costs_days_to'))
					{
						$cost += get_sub_field('product_costs_percentage')/100*$amount + get_field('product_global_interest')/100*$amount / 365*$period;
					}

				endwhile;
			endif;
		}
		else if(get_field('product_loan_costs') == "unknown")
		{
			$cost = null;
		}
		else if(get_field('product_loan_costs') == "constant")
		{
			if(get_field('product_fixed_commision'))
			{
				$cost += floatval(get_field('product_fixed_commision'));
			}
		}
		else
		{
			if( have_rows('product_costs', $post) ):
				while( have_rows('product_costs') ) : the_row();
					if($period >= get_sub_field('product_costs_days_from') && $period <= get_sub_field('product_costs_days_to'))
					{
						$cost += get_sub_field('product_costs_percentage')/100*$amount + get_field('product_global_interest')/100*$amount / 365*$period;
					}

				endwhile;
			endif;
		}
	}
	?>
	<li id="chwilowka-<?php echo $post->post_name; ?>-<?php echo $post->ID; ?>" data-cost="<?php if($cost) echo number_format($cost, 2, ".", ""); ?>" data-rank="<?php echo( get_field('product_ikalkulator_rating')); ?>" <?php if( get_field('product_highlighted')) { ?>data-highlight="true" class="highlighted"<?php }?>>
		<div class="labels">
			<?php if( have_rows('product_labels', $post) ):
				echo '<ul>';
				while( have_rows('product_labels') ) : the_row();

					$style = "";
					if(get_sub_field('product_labels_color')) $style .= "background: " . get_sub_field('product_labels_color') ."; ";
					if(get_sub_field('product_labels_textcolor')) $style .= "color: " . get_sub_field('product_labels_textcolor') ."; ";

					if($style)
						$style = ' style="' . $style . '"';
					echo '<li'.($style!="" ? $style : '').'>' . get_sub_field('product_labels_label') . '</li>';

				endwhile;
				echo '</ul>';
			endif;?>
		</div>
		<div class="name data-row">

			<?php if(!get_field("product_hide_analysis")) : ?>
				<a href="<?= get_the_permalink(); ?>">
			<?php endif; ?>

				<?php if( get_field('product_logo')['url']) { ?>
					<img src="<?php echo get_field('product_logo')['url']; ?>" alt="<?php echo( get_field('product_name')); ?>" />
				<?php } ?>
                    <h2 class="name-text"><?php echo( get_field('product_name')); ?></h2>
                    
			<?php if(!get_field("product_hide_analysis")) : ?>
				</a>
			<?php endif; ?>

		</div>
		<div class="amount data-row">
			<?php echo number_format($amount, 0, "", "&nbsp;"); ?> zł<br/>
			<small>od&nbsp;<?php echo number_format(get_field('product_min_amount'), 0, "", "&nbsp;"); ?> do&nbsp;<?php echo number_format(get_field('product_max_amount'), 0, "", "&nbsp;"); ?>&nbsp;zł</small>
		</div>
		<div class="maxperiod data-row">
			<?php echo $period; ?>&nbsp;dni<br/>
			<small>od&nbsp;<?php echo( get_field('product_min_period')); ?> do&nbsp;<?php echo( get_field('product_max_period')); ?>&nbsp;dni</small>
		</div>
		<div class="cost data-row">
			<?php
				$prefix = "";
				$suffix = "";
				if(get_field('product_first_promo') && get_field('product_first_promo_maxamount') >= $amount && get_field('product_first_promo_maxperiod') >= $period)
				{
					$prefix = get_field('product_promo_cost')." zł <del>";
					$suffix = "</del>";
				}
				if($cost) echo $prefix.number_format($cost, 2, ".", " ")." zł ".$suffix;
			    else echo $prefix.$suffix;

				if( get_field('product_cost_rules'))
				{
					echo '<div class="tooltip"><div class="content">'.get_field('product_cost_rules').'<span class="close"></span></div><span class="show"></span></div>';
				}
			?>
		</div>
		<div class="rank data-row">
				<?php if(get_field('product_ikalkulator_rating'))
				{
                    echo '<div><div class="star-rating" data-rating="'.number_format(floatval(get_field('product_ikalkulator_rating')), 1, ".", "").'"><div class="stars"></div> </div>';
                        echo '<small class="rating-value">'. number_format(floatval(get_field('product_ikalkulator_rating')), 1, ".", "") . '</small>';
                    echo '</div>';
				} ?>

				<?php
				//			if(get_comments_number())
				//			{
				//				if (get_post_meta($post->ID, 'avg_rating', true))
				//				{
				//					echo '/ '. number_format(floatval(get_post_meta($post->ID, 'avg_rating', true)), 1, ".", "").'';
				//					echo '<div class="opinions">'. get_comments_number() . 'opinii</div>';
				//				}
				//			}
				?>

		</div>
		<div class="cta data-row">
			<a href="<?php echo( get_field('product_lender_tracking_url')); ?>" class="btn btn-inverse" target="_blank" rel="nofollow">Wnioskuj</a>
		</div>

		<?php include('product-description.php'); ?>
	</li>
<?php endforeach;

echo '</ul>';
wp_reset_postdata();

?>
