<?php

echo '<ul>';
foreach ( $posts_array as $post ) : setup_postdata( $post );

	if($amount)
	{
		if((get_field('product_min_amount') && $amount < get_field('product_min_amount')) || (get_field('product_max_amount') && $amount > get_field('product_max_amount')))
		{
			continue;
		}
		if((get_field('product_min_period') && $period < get_field('product_min_period')) || (get_field('product_max_period') && $period > get_field('product_max_period')))
		{
			continue;
		}
		if($age && (get_field('product_min_age') && $age < get_field('product_min_age')) || (get_field('product_max_age') && $age > get_field('product_max_age')))
		{
			continue;
		}
		if($nobik && !get_field('product_no_bik') && $nobik==true)
		{
			continue;
		}


		$installment = 0;
		if(get_field('product_loan_costs') == "variable")
		{
			if( have_rows('product_costs', $post) ):
				while( have_rows('product_costs') ) : the_row();
					if($period >= get_sub_field('product_costs_months_from') && $period <= get_sub_field('product_costs_months_to'))
					{
						$percentage = get_sub_field('product_costs_percentage')/100;
						$installment = $amount * (1+$percentage) / $period;
					}
				endwhile;
			endif;
		}
		else if(get_field('product_loan_costs') == "unknown")
		{
			$installment = null;
		}
		else if(get_field('product_loan_costs') == "constant")
		{
			if(get_field('product_fixed_commision'))
			{
				$installment = $amount/$period + get_field('product_fixed_commision');
			}
		}
		else
		{
			if( have_rows('product_costs', $post) ):
				while( have_rows('product_costs') ) : the_row();
					if($period >= get_sub_field('product_costs_months_from') && $period <= get_sub_field('product_costs_months_to'))
					{
						$percentage = get_sub_field('product_costs_percentage')/100;
						$installment = $amount * (1+$percentage) / $period;
					}
				endwhile;
			endif;
		}
	}
	?>
	<li id="pozyczka-<?php echo $post->post_name; ?>-<?php echo $post->ID; ?>" data-cost="<?php echo $installment ? number_format($installment, 2, ".", "") : ""; ?>" data-rank="<?php echo( get_field('product_ikalkulator_rating')); ?>" <?php if( get_field('product_highlighted')) { ?>data-highlight="true" class="highlighted"<?php }?>>
		<div class="labels">
			<?php if( have_rows('product_labels', $post) ):
				echo '<ul>';
				while( have_rows('product_labels') ) : the_row();

					$style = "";
					if(get_sub_field('product_labels_color')) $style .= "background: " . get_sub_field('product_labels_color') ."; ";
					if(get_sub_field('product_labels_textcolor')) $style .= "color: " . get_sub_field('product_labels_textcolor') ."; ";

					if($style)
						$style = ' style="' . $style . '"';
					echo '<li'.($style!="" ? $style : '').'>' . get_sub_field('product_labels_label') . '</li>';

				endwhile;
				echo '</ul>';
			endif;?>
		</div>
		<div class="name data-row">
			<?php if(!get_field("product_hide_analysis")) : ?>
				<a href="<?= get_the_permalink(); ?>">
			<?php endif; ?>
				<?php if( get_field('product_logo')['url']) { ?>
					<img src="<?php echo get_field('product_logo')['url']; ?>" alt="<?php echo( get_field('product_name')); ?>" />
				<?php } ?>
                    <h2 class="name-text"><?php echo( get_field('product_name')); ?></h2>
                    
			<?php if(!get_field("product_hide_analysis")) : ?>
				</a>
			<?php endif; ?>
		</div>
		<div class="amount data-row">
			<?php echo number_format($amount, 0, "", "&nbsp;"); ?> zł<br/>
			<small>od&nbsp;<?php echo number_format(get_field('product_min_amount'), 0, "", "&nbsp;"); ?> do&nbsp;<?php echo number_format(get_field('product_max_amount'), 0, "", "&nbsp;"); ?>&nbsp;zł</small>
		</div>
		<div class="maxperiod data-row">
			<?php echo $period; ?>&nbsp;mies.<br/>
			<small>od&nbsp;<?php echo( get_field('product_min_period')); ?> do&nbsp;<?php echo( get_field('product_max_period')); ?>&nbsp;mies.</small>
		</div>
		<div class="cost data-row">
			<?php echo $installment ? number_format($installment, 2, ".", " ")." zł" : "b.d."; ?>

			<?php
			if( get_field('product_cost_rules'))
			{
				echo '<div class="tooltip"><div class="content">'.get_field('product_cost_rules').'<span class="close"></span></div><span class="show"></span></div>';
			}
			?>
		</div>
		<div class="rank data-row">
			<?php if(get_field('product_ikalkulator_rating'))
			{
				echo '<div><div class="star-rating" data-rating="'.number_format(floatval(get_field('product_ikalkulator_rating')), 1, ".", "").'"><div class="stars"></div> </div>';
                    echo '<small class="rating-value">'. number_format(floatval(get_field('product_ikalkulator_rating')), 1, ".", "") . '</small>';
                echo '</div>';
			} ?>

			<?php
			//			if(get_comments_number())
			//			{
			//				if (get_post_meta($post->ID, 'avg_rating', true))
			//				{
			//					echo '/ '. number_format(floatval(get_post_meta($post->ID, 'avg_rating', true)), 1, ".", "").'';
			//					echo '<div class="opinions">'. get_comments_number() . 'opinii</div>';
			//				}
			//			}
			?>

		</div>
		<div class="cta data-row">
			<a href="<?php echo( get_field('product_lender_tracking_url')); ?>" class="btn btn-inverse" target="_blank" rel="nofollow">Wnioskuj</a>
		</div>

		<?php include('product-description.php'); ?>
	</li>
<?php endforeach;

echo '</ul>';
wp_reset_postdata();

?>
