<?php

echo '<ul>';
foreach ( $posts_array as $post ) : setup_postdata( $post );
	if($amount)
	{
		if((get_field('product_min_amount') && $amount < get_field('product_min_amount')) || (get_field('product_max_amount') && $amount > get_field('product_max_amount')))
		{
			continue;
		}
		if((get_field('product_min_period') && $maxperiod < get_field('product_min_period')/30) || (get_field('product_max_period') && $minperiod > get_field('product_max_period')/30))
		{
			continue;
		}

		if($nonewaccount && !get_field('product_only_with_personal_account') && $nonewaccount==true)
		{
			continue;
		}

		if($nonewclient && !get_field('product_only_for_new_customers') && $nonewclient==true)
		{
			continue;
		}

		if($nonewresources && !get_field('product_only_for_new_resources') && $nonewresources==true)
		{
			continue;
		}

		$variants = [];
		if( have_rows('product_interests', $post) ):
			while( have_rows('product_interests') ) : the_row();

				$percentage = "";
				$profit = "";
				if($minperiod*30 <= get_sub_field('product_investment_from') && $maxperiod*30 >= get_sub_field('product_investment_to') && $amount <= get_sub_field('product_investment_amount_to') && $amount >= get_sub_field('product_investment_amount_from'))
				{
					$percentage = get_sub_field('product_investment_interest');
					$realPercentage = $percentage*0.0081;
					$profit = $amount * $realPercentage * get_sub_field('product_investment_to')/365;
					$variants[get_sub_field('product_investment_to')] = [$profit, $percentage];
				}
			endwhile;
		endif;

	}

	foreach($variants as $periodIterator => $variant):
		?>

		<li id="lokata-<?php echo $post->post_name; ?>-<?php echo $post->ID; ?>" data-percentage="<?php echo number_format($variant[1], 2, ".", ""); ?>" data-profit="<?php echo number_format($variant[0], 2, ".", ""); ?>" data-rank="<?php echo( get_field('product_ikalkulator_rating')); ?>" <?php if( get_field('product_highlighted')) { ?>data-highlight="true" class="highlighted"<?php }?>>
			<div class="labels">
				<?php if( have_rows('product_labels', $post) ):
					echo '<ul>';
					while( have_rows('product_labels') ) : the_row();

						$style = "";
						if(get_sub_field('product_labels_color')) $style .= "background: " . get_sub_field('product_labels_color') ."; ";
						if(get_sub_field('product_labels_textcolor')) $style .= "color: " . get_sub_field('product_labels_textcolor') ."; ";

						if($style)
							$style = ' style="' . $style . '"';
						echo '<li'.($style!="" ? $style : '').'>' . get_sub_field('product_labels_label') . '</li>';

					endwhile;
					echo '</ul>';
				endif;?>
			</div>
			<div class="name data-row">
				<?php if(!get_field("product_hide_analysis")) : ?>
					<a href="<?= get_the_permalink(); ?>">
				<?php endif; ?>
					<?php if( get_field('product_logo')['url']) { ?>
						<img src="<?php echo get_field('product_logo')['url']; ?>" alt="<?php echo( get_field('product_account_name')); ?>" />
					<?php } ?>
                        <h2 class="name-text"><?php echo( get_field('product_account_name')); ?></h2>
                        
				<?php if(!get_field("product_hide_analysis")) : ?>
					</a>
				<?php endif; ?>
			</div>
			<div class="amount data-row">
				<?php echo number_format($amount, 0, "", "&nbsp;"); ?> zł<br/>
				<small>od&nbsp;<?php echo number_format(get_field('product_min_amount'), 0, "", "&nbsp;"); ?> do&nbsp;<?php echo number_format(get_field('product_max_amount'), 0, "", "&nbsp;"); ?>&nbsp;zł</small>
			</div>
			<div class="maxperiod data-row">
				<?php
				if($periodIterator %30 == 0)
				{
					echo ($periodIterator/30)."&nbsp;mies.";
				}
				else
					echo $periodIterator."&nbsp;dni";
				?><br/>
				<?php
				if(get_field('product_min_period')%30 == 0 && get_field('product_max_period')%30 == 0 && (get_field('product_min_period') != get_field('product_max_period'))): ?>
					<small>od&nbsp;<?php echo( get_field('product_min_period')/30); ?> do&nbsp;<?php echo( get_field('product_max_period')/30); ?>&nbsp;mies.</small>
				<?php elseif(get_field('product_min_period') != get_field('product_max_period')): ?>
					<small>od&nbsp;<?php echo( get_field('product_min_period')); ?> do&nbsp;<?php echo( get_field('product_max_period')); ?>&nbsp;dni</small>
				<?php endif; ?>

			</div>

			<div class="percentage data-row">
				<?php
				echo $variant[1];
				?> %
			</div>

			<div class="profit data-row">
				<?php echo number_format($variant[0], 2, ".", "&nbsp;"); ?> zł
			</div>

			<div class="rank data-row">
				<?php if(get_field('product_ikalkulator_rating'))
				{
                    echo '<div><div class="star-rating" data-rating="'.number_format(floatval(get_field('product_ikalkulator_rating')), 1, ".", "").'"><div class="stars"></div> </div>';
                        echo '<small class="rating-value">'. number_format(floatval(get_field('product_ikalkulator_rating')), 1, ".", "") . '</small>';
                    echo '</div>';
				} ?>

				<?php
				//			if(get_comments_number())
				//			{
				//				if (get_post_meta($post->ID, 'avg_rating', true))
				//				{
				//					echo '/ '. number_format(floatval(get_post_meta($post->ID, 'avg_rating', true)), 1, ".", "").'';
				//					echo '<div class="opinions">'. get_comments_number() . 'opinii</div>';
				//				}
				//			}
				?>

			</div>
			<div class="cta data-row">
				<a href="<?php echo( get_field('product_lender_tracking_url')); ?>" class="btn btn-inverse" target="_blank" rel="nofollow">Wnioskuj</a>
			</div>

			<?php include('product-description.php'); ?>
		</li>
	<?php endforeach;
endforeach;
echo '</ul>';
wp_reset_postdata();

?>