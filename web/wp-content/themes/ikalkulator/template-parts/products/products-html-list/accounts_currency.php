<?php

echo '<ul>';
foreach ( $posts_array as $post ) : setup_postdata( $post );

	if($nonewaccount && !get_field('product_only_with_personal_account') && $nonewaccount==true)
	{
		continue;
	}
	if($freeatms && (get_field('product_atm_fee_lower') == null || get_field('product_atm_fee_lower') == "" || get_field('product_atm_fee_lower') != 0) && get_field('product_atm_fee') != 0)
	{
		continue;
	}
	if($freeaccount && (get_field('product_account_fee_lower') == null || get_field('product_account_fee_lower') == "" || get_field('product_account_fee_lower') != 0) && get_field('product_account_fee') != 0)
	{
		continue;
	}
	if($freecard && (get_field('product_card_fee_lower') == null || get_field('product_card_fee_lower') == "" || get_field('product_card_fee_lower')) != 0 && get_field('product_card_fee') != 0)
	{
		continue;
	}
	if( $currency && have_rows('product_currencies', $post) ):
		$currenciesMatch = false;
		while( have_rows('product_currencies') ) : the_row();
			if(get_sub_field("product_currency_symbol") ==  $currency)
			{
				$currenciesMatch = true;
			}
		endwhile;
		if(!$currenciesMatch):
			continue ;
		endif;

	elseif($currency):
		continue;
	endif;

	?>
	<li id="konto_walutowe-<?php echo $post->post_name; ?>-<?php echo $post->ID; ?>" data-cost="<?php echo number_format($cost, 2, ".", ""); ?>" data-rank="<?php echo( get_field('product_ikalkulator_rating')); ?>" <?php if( get_field('product_highlighted')) { ?>data-highlight="true" class="highlighted"<?php }?>>
		<div class="labels">
			<?php if( have_rows('product_labels', $post) ):
				echo '<ul>';
				while( have_rows('product_labels') ) : the_row();

					$style = "";
					if(get_sub_field('product_labels_color')) $style .= "background: " . get_sub_field('product_labels_color') ."; ";
					if(get_sub_field('product_labels_textcolor')) $style .= "color: " . get_sub_field('product_labels_textcolor') ."; ";

					if($style)
						$style = ' style="' . $style . '"';
					echo '<li'.($style!="" ? $style : '').'>' . get_sub_field('product_labels_label') . '</li>';

				endwhile;
				echo '</ul>';
			endif;?>
		</div>
		<div class="name data-row">
			<?php if(!get_field("product_hide_analysis")) : ?>
				<a href="<?= get_the_permalink(); ?>">
			<?php endif; ?>

				<?php if( get_field('product_logo')['url']) { ?>
					<img src="<?php echo get_field('product_logo')['url']; ?>" alt="<?php echo( get_field('product_account_name')); ?>" />
				<?php } ?>
                    <h2 class="name-text"><?php echo( get_field('product_account_name')); ?></h2>
                    
			<?php if(!get_field("product_hide_analysis")) : ?>
				</a>
			<?php endif; ?>
		</div>
		<div class="payment data-row">
			<?php

			$lower = false;

			if (get_field('product_account_fee_lower') != null)
			{
				echo  get_field('product_account_fee_lower');
				$lower = true;
			}

			if (get_field('product_account_fee')  != null)
			{
				if($lower) echo "&nbsp;/ ";
				echo get_field('product_account_fee');
			}

			if( get_field('product_free_account_rules'))
			{
				echo '<div class="tooltip"><div class="content">'.get_field('product_free_account_rules').'<span class="close"></span></div><span class="show"></span></div>';
			}
			?>
		</div>
		<div class="cardpayment data-row">
			<?php
			$lower = false;
			if (get_field('product_card_fee_lower') != null) {
				echo  get_field('product_card_fee_lower');
				$lower = true;
			}

			if (get_field('product_card_fee') != null)
			{
				if($lower) echo "&nbsp;/ ";
				echo get_field('product_card_fee');
			}

			if( get_field('product_free_card_rules'))
			{
				echo '<div class="tooltip"><div class="content">'.get_field('product_free_card_rules').'<span class="close"></span></div><span class="show"></span></div>';
			}
			?>
		</div>
		<div class="atmpayment data-row">
			<?php
			$lower = false;
			if (get_field('product_atm_fee_lower') != null) {
				echo  get_field('product_atm_fee_lower');
				$lower = true;
			}

			if (get_field('product_atm_fee') != null)
			{
				if($lower) echo "&nbsp;/ ";
				echo get_field('product_atm_fee');
			}

			if( get_field('product_free_atm_rules'))
			{
				echo '<div class="tooltip"><div class="content">'.get_field('product_free_atm_rules').'<span class="close"></span></div><span class="show"></span></div>';
			}
			?>
		</div>
		<div class="rank data-row">
			<?php if(get_field('product_ikalkulator_rating'))
			{
				echo '<div><div class="star-rating" data-rating="'.number_format(floatval(get_field('product_ikalkulator_rating')), 1, ".", "").'"><div class="stars"></div> </div>';
                    echo '<small class="rating-value">'. number_format(floatval(get_field('product_ikalkulator_rating')), 1, ".", "") . '</small>';
                echo '</div>';
			} ?>

			<?php
			//			if(get_comments_number())
			//			{
			//				if (get_post_meta($post->ID, 'avg_rating', true))
			//				{
			//					echo '/ '. number_format(floatval(get_post_meta($post->ID, 'avg_rating', true)), 1, ".", "").'';
			//					echo '<div class="opinions">'. get_comments_number() . 'opinii</div>';
			//				}
			//			}
			?>

		</div>
		<div class="cta data-row">
			<a href="<?php echo( get_field('product_lender_tracking_url')); ?>" class="btn btn-inverse" target="_blank" rel="nofollow">Wnioskuj</a>
		</div>

		<?php include('product-description.php'); ?>
	</li>
<?php endforeach;

echo '</ul>';
wp_reset_postdata();

?>