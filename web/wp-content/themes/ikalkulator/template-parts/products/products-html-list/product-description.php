<div class="more">
	<div class="expander"><div class="expand"> Zobacz więcej</div><div class="fold">Zobacz mniej</div><span class="arrow"></span> </div>
	<div class="expanded">
		<div class="description">
			<h3><?php the_title(); ?> - opis produktu</h3>
			<?php the_field('product_description'); ?>
			<?php /* if(!get_field("product_hide_analysis")) : ?>
			 <a href="<?= get_the_permalink(); ?>">Zobacz więcej</a>
			<?php endif; */ ?>
		</div>
		<div class="sidebar">
				<?php if( get_field('product_expert_opinion')): ?>
					<div class="expert">
						<h3>Okiem eksperta</h3>
						<?php the_field('product_expert_opinion'); ?>
					</div>
				<?php endif;?>

			<div class="advantages">
				<?php if( have_rows('product_advantages', $post) ):
					echo '<h3><span>+</span> Zalety</h3><ul>';
					while( have_rows('product_advantages') ) : the_row();

						echo '<li>' . get_sub_field('product_advantages_content') . '</li>';

					endwhile;
					echo '</ul>';
				endif;?>
			</div>
			<div class="disadvantages">
				<?php if( have_rows('product_disadvantages', $post) ):
					echo '<h3><span>-</span> Wady</h3><ul>';
					while( have_rows('product_disadvantages') ) : the_row();

						echo '<li>' . get_sub_field('product_disadvantages_content') . '</li>';

					endwhile;
					echo '</ul>';
				endif;?>
			</div>

		</div>

		<div class="cta data-row">
			<a href="<?php echo( get_field('product_lender_tracking_url')); ?>" class="btn" target="_blank">Wnioskuj</a>
		</div>

	</div>
</div>