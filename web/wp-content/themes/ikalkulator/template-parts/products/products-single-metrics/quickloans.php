<?php
	$schemaData["loanType"] = "Pożyczka krótkoterminowa, tzw. chwilówka";
?>
	<div class="product-info">
		<div class="labels">
			<div class="kind"></div>
			<div class="period">Okres trwania</div>
			<div class="amount">Kwota pożyczki</div>
			<div class="firstfree">Pierwsza pożyczka</div>
			<div class="nobik">Bez BIK</div>
		</div>
		<div class="data">
			<div class="kind"><?php if( get_field('product_logo')['url']) { ?>
					<img src="<?php echo get_field('product_logo')['url']; ?>" itemprop="image" alt="Logo <?php echo( get_field('product_name')); ?>" />
				<?php
						$schemaData["logo"] = get_field('product_logo')['url'];
					}
				?></div>
			<?php
			$schemaData["loanTerm"] = [
				"@type" => "QuantitativeValue",
				"minValue" => get_field('product_min_period'),
				"maxValue" => get_field('product_max_period'),
			    "unitCode" => "DAY"
			];
			?>
			<div class="period"><?php echo( get_field('product_min_period')); ?> - <?php echo( get_field('product_max_period')); ?> dni</div>

			<?php
			$schemaData["amount"][] = [
				"@type" => "MonetaryAmount",
				"name" => "max. kwota pożyczki",
				"value" => get_field('product_max_amount'),
				"currency" => "PLN"
			];
			?>
			<div class="amount"><?php echo( number_format(get_field('product_min_amount'), 0, "", "&nbsp;")); ?> - <?php echo( number_format(get_field('product_max_amount'), 0, "", "&nbsp;")); ?> zł</div>

			<div class="firstpromo">
				<?php
					$schemaData["annualPercentageRate"] = [
						"@type" => "QuantitativeValue",
					    "name" => "Oprocentowanie zmienne",

					];
					$amountSchema = null;
					$amountSchemaName = "";

					if(get_field('product_first_promo') && get_field('product_promo_cost') == 0)
					{
						echo "Darmowa<br/><small>";
						if( get_field('product_first_promo_maxamount'))
						{
							echo "do ".get_field('product_first_promo_maxamount')."&nbsp;zł";
							$amountSchemaVal = get_field('product_first_promo_maxamount');
							$amountSchemaName = "Pierwsza pożyczka";
							$schemaData["amount"][] = [
								"@type" => "MonetaryAmount",
								"name" => "max. kwota promocyjnej/pierwszej pożyczki",
								"value" => get_field('product_first_promo_maxamount'),
								"currency" => "PLN"
							];
						}
						if( get_field('product_first_promo_maxperiod'))
						{
							echo "<br/>do ".get_field('product_first_promo_maxperiod')."&nbsp;dni";
						}
						echo "</small>";
						$schemaData["annualPercentageRate"]["minValue"] = 0;
					}
					else if(get_field('product_first_promo') && get_field('product_promo_cost') > 0)
					{
						echo "Tylko ".get_field('product_promo_cost')."&nbsp;zł<br/><small>";
						if( get_field('product_first_promo_maxamount'))
						{
							echo "do ".get_field('product_first_promo_maxamount')."&nbsp;zł";
							$amountSchemaVal = get_field('product_first_promo_maxamount');
							$amountSchemaName = "Pierwsza pożyczka";
							$schemaData["amount"][] = [
								"@type" => "MonetaryAmount",
								"name" => "promocyjnej/pierwszej pożyczki",
								"value" => get_field('product_first_promo_maxamount'),
								"currency" => "PLN"
							];
						}
						if( get_field('product_first_promo_maxperiod'))
						{
							echo "<br/>do ".get_field('product_first_promo_maxperiod')."&nbsp;dni";
						}
						echo "</small>";
						$schemaData["annualPercentageRate"]["minValue"] = 0;
					}
					else
					{
						echo "<small>";
						if( get_field('product_first_promo_maxamount'))
						{
							echo "do ".get_field('product_first_promo_maxamount')."&nbsp;zł";
							$schemaData["amount"][] = [
								"@type" => "MonetaryAmount",
								"name" => "promocyjnej/pierwszej pożyczki",
								"value" => get_field('product_first_promo_maxamount'),
								"currency" => "PLN"
							];
						}
						if( get_field('product_first_promo_maxperiod'))
						{
							echo "<br/>do ".get_field('product_first_promo_maxperiod')."&nbsp;dni";
						}
						echo "</small>";
					}

				if(get_field("product_max_rrso"))
				{
					$schemaData["annualPercentageRate"]["maxValue"] = get_field("product_max_rrso");
				}

				?></div>
			<div class="nobik"><?php echo get_field('product_no_bik') ? "TAK" : "NIE"; ?></div>
		</div>
	</div>


<?php
	if (get_field('product_cost_rules'))
	{
		$schemaData["feesAndCommissionsSpecification"] = get_field('product_cost_rules');
	}
?>