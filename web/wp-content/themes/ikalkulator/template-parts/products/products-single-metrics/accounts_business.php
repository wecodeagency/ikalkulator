
<div class="product-info">
	<div class="labels n4">
		<div class="kind"></div>
		<div class="account">Konto</div>
		<div class="card">Karta</div>
		<div class="atm">Bankomaty</div>
	</div>
	<div class="data n4">
		<div class="kind"><?php if( get_field('product_logo')['url']) { ?>
				<img src="<?php echo get_field('product_logo')['url']; ?>" itemprop="image" alt="Logo <?php echo( get_field('product_account_name')); ?>" />
			<?php
				$schemaData["logo"] = get_field('product_logo')['url'];
			}
			?>

			<div>
				<?php echo( get_field('product_account_name')); ?>
			</div></div>
		<div class="account">
		<?php
			$lower = false;

			$schemaData["offers"] = ["@type" => "Offer"];
			$schemaData["offers"]["eligibleTransactionVolume"] = ["@type" => "PriceSpecification"];

			if (get_field('product_account_fee_lower') != null)
			{
				echo  "<span>" . get_field('product_account_fee_lower')."</span>&nbsp;zł";
				$lower = true;
				$schemaData["offers"]["eligibleTransactionVolume"]["minPrice"] = get_field('product_account_fee_lower');
			}

			if (get_field('product_account_fee')  != null)
			{
				if($lower) echo "&nbsp;/ ";
				else echo  "<span>";

				echo get_field('product_account_fee')."&nbsp;zł";

				if(!$lower)
				{
					echo "</span>";
					$schemaData["offers"]["eligibleTransactionVolume"]["minPrice"] = get_field('product_account_fee');
				}
			}

			if( get_field('product_free_account_rules'))
			{
				echo '<div class="tooltip"><div class="content">'.get_field('product_free_account_rules').'<span class="close"></span></div><span class="show"></span></div>';
			}
			?>
		</div>
		<div class="card"><?php
			$lower = false;
			if (get_field('product_card_fee_lower') != null) {
				echo  get_field('product_card_fee_lower')."&nbsp;zł";
				$lower = true;
			}

			if (get_field('product_card_fee') != null)
			{
				if($lower) echo "&nbsp;/ ";
				echo get_field('product_card_fee')."&nbsp;zł";
			}

			if( get_field('product_free_card_rules'))
			{
				echo '<div class="tooltip"><div class="content">'.get_field('product_free_card_rules').'<span class="close"></span></div><span class="show"></span></div>';
			}
			?></div>
		<div class="atm"><?php
			$lower = false;
			if (get_field('product_atm_fee_lower') != null) {
				echo  get_field('product_atm_fee_lower')."&nbsp;zł";
				$lower = true;
			}

			if (get_field('product_atm_fee') != null)
			{
				if($lower) echo "&nbsp;/ ";
				echo get_field('product_atm_fee')."&nbsp;zł";
			}

			if( get_field('product_free_atm_rules'))
			{
				echo '<div class="tooltip"><div class="content">'.get_field('product_free_atm_rules').'<span class="close"></span></div><span class="show"></span></div>';
			}
			?></div>
	</div>
</div>