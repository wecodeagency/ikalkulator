<?php
$minpercentage = 999;
$maxpercentage = 0;
if(have_rows('product_interests'))
{
	while( have_rows('product_interests') ) : the_row();
		if($minpercentage > get_sub_field('product_investment_interest')) $minpercentage = floatval(get_sub_field('product_investment_interest'));
		if($maxpercentage < get_sub_field('product_investment_interest')) $maxpercentage = floatval(get_sub_field('product_investment_interest'));
	endwhile;
}

?>
<div class="product-info">
	<div class="labels n4">
		<div class="kind"></div>
		<div class="period">Okres</div>
		<div class="amount">Kwota lokaty</div>
		<div class="interest">Oprocentowanie</div>
	</div>
	<div class="data n4">
		<div class="kind">	<?php if( get_field('product_logo')['url']) { ?>
				<img src="<?php echo get_field('product_logo')['url']; ?>" itemprop="image" alt="Logo <?php echo( get_field('product_name')); ?>" />
				<?php
					$schemaData["logo"] = get_field('product_logo')['url'];
					}
				?>

			<div>
				<?php echo( get_field('product_account_name')); ?>
			</div></div>
		<div class="period">
			<?php
			if(get_field('product_min_period')%30 == 0 && get_field('product_max_period')%30 == 0 && (get_field('product_min_period') != get_field('product_max_period'))): ?>
				<small>od&nbsp;<?php echo( get_field('product_min_period')/30); ?> do&nbsp;<?php echo( get_field('product_max_period')/30); ?>&nbsp;mies.</small>
			<?php elseif(get_field('product_min_period') != get_field('product_max_period')): ?>
				<small>od&nbsp;<?php echo( get_field('product_min_period')); ?> do&nbsp;<?php echo( get_field('product_max_period')); ?>&nbsp;dni</small>
			<?php elseif(get_field('product_min_period')%30 == 0 && get_field('product_max_period')%30 == 0 && (get_field('product_min_period') == get_field('product_max_period'))): ?>
				<small><?php echo( get_field('product_max_period')/30); ?>&nbsp;mies.</small>
			<?php elseif(get_field('product_min_period') == get_field('product_max_period')): ?>
				<small><?php echo( get_field('product_max_period')); ?>&nbsp;dni</small>
			<?php endif; ?>

		</div>


		<?php
		$schemaData["amount"][] = [
			"@type" => "MonetaryAmount",
			"minValue" => get_field('product_min_amount'),
			"maxValue" => get_field('product_max_amount'),
			"currency" => "PLN"
		];
		?>
		<div class="amount"><?php echo( number_format(get_field('product_min_amount'), 0, "", "&nbsp;")); ?> - <?php echo( number_format(get_field('product_max_amount'), 0, "", "&nbsp;")); ?> zł</div>
		<div class="interest">
			<?php
				if($minpercentage != $maxpercentage)
				{
					$schemaData["interestRate"] = [
						"@type" => "QuantitativeValue",
						"minValue" => $minpercentage,
						"maxValue" => $maxpercentage
					];
					echo $minpercentage . "% - ".$maxpercentage;
				}
				else
				{
					$schemaData["interestRate"] = $minpercentage;
					echo $minpercentage;
				}
			?>
		</div>
	</div>
</div>