

<div class="filters container">
	<form>
		<h2>Dopasuj kryteria <div class="show-all">Pokaż wszystkie</div> </h2>

		<div class="sliders visible">
			<div class="slider"  data-progressive="true" data-filter-name="amount" data-filter-type="to" data-min="<?php echo $minAmount;?>" data-max="<?php echo $maxAmount;?>" data-value="<?php echo $amount;?>">
				<h3>Wysokość depozytu <div class="value"><?php echo number_format($amount, 0, ".", " "); ?></div></h3>

				<div class="slider-bar"></div>
				<div class="range-min"><span class="range-min-value"><?php echo number_format($minAmount, 0, ".", " "); ?></span> zł</div>
				<div class="range-max"><span class="range-max-value"><?php echo number_format($maxAmount, 0, ".", " "); ?></span> zł</div>
				<input type="hidden" name="amount" />
			</div>
			<div class="slider">
				<div class="option">
					<div class="checkbox">
						<input type="checkbox" name="nonewaccount" id="nonewaccount" value="true" <?php if($nonewaccount) echo 'checked'; ?>/>
						<label for="nonewaccount">
							Bez zakładania konta osobistego
						</label>
					</div>
				</div>
				<div class="option">
					<div class="checkbox">
						<input type="checkbox" name="nonewclient" id="nonewclient" value="true" <?php if($nonewclient) echo 'checked'; ?>/>
						<label for="nonewclient">
							Bez promocji dla nowych klientów
						</label>
					</div>
				</div>
				<div class="option">
					<div class="checkbox">
						<input type="checkbox" name="nonewresources" id="nonewresources" value="true" <?php if($nonewresources) echo 'checked'; ?>/>
						<label for="nonewresources">
							Bez promocji dla nowych środków
						</label>
					</div>
				</div>
			</div>
		</div>

		<div class="options togglable">

		</div>
	</form>
</div>
