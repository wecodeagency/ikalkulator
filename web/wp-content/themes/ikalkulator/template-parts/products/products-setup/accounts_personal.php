<?php

$freeatms = $_GET["freeatms"] ? $_GET["freeatms"] : null;
$freeaccount = $_GET["freeaccount"] ? $_GET["freeaccount"] : null;
$freecard = $_GET["freecard"] ? $_GET["freecard"] : null;
$promoted = $_GET["promoted"] ? $_GET["promoted"] : null;
$premium = $_GET["premium"] ? $_GET["premium"] : null;

$args = array(
	'post_type'        => 'accounts_personal',
	'post_status'      => 'publish',
	'posts_per_page'   => -1,
	'suppress_filters' => true,
	'meta_query'	=> array(
		'relation'		=> 'OR',
		array(
			'key'	  	=> 'product_inactive',
			'compare' => 'NOT EXISTS'
		),
		array(
			'key'	  	=> 'product_inactive',
			'value'	  	=> '1',
			'compare' 	=> '!=',
		),
	),
);
$posts_array = get_posts( $args );
