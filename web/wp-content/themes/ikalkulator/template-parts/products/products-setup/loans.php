<?php

$amount = isset($_GET["amount"]) && $_GET["amount"] != "" ? (int)$_GET["amount"] : 2000;
$period = isset($_GET["period"]) && $_GET["period"] != "" ? (int)$_GET["period"] : 12;
$age = isset($_GET["age"]) ? (int)$_GET["age"] : 25;
$nobik = isset($_GET["nobik"]) ? $_GET["nobik"] : null;

$minAmount = 999999999;
$maxAmount = 0;
$minMonths = 999999999;
$maxMonths = 0;
$minAge = 18;
$maxAge = 120;


$args = array(
	'post_type'        => 'loans',
	'post_status'      => 'publish',
	'posts_per_page'   => -1,
	'suppress_filters' => true,
	'meta_query'	=> array(
		'relation'		=> 'OR',
		array(
			'key'	  	=> 'product_inactive',
			'compare' => 'NOT EXISTS'
		),
		array(
			'key'	  	=> 'product_inactive',
			'value'	  	=> '1',
			'compare' 	=> '!=',
		),
	),
);
$posts_array = get_posts( $args );

foreach ( $posts_array as $post ) : setup_postdata( $post );

	if(get_field('product_max_amount') > $maxAmount) $maxAmount = get_field('product_max_amount');
	if(get_field('product_min_amount') < $minAmount) $minAmount = get_field('product_min_amount');
	if(get_field('product_min_period') < $minMonths) $minMonths = get_field('product_min_period');
	if(get_field('product_max_period') > $maxMonths) $maxMonths = get_field('product_max_period');
	if(get_field('product_min_age') < $minAge) $minAge = get_field('product_min_age');
	if(get_field('product_max_age') > $maxAge) $maxAge = get_field('product_max_age');

endforeach;
