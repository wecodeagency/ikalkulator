<?php

$amount = $_GET["amount"] ? $_GET["amount"] : 10000;
$nonewaccount = $_GET["nonewaccount"] ? $_GET["nonewaccount"] : null;
$nonewresources = $_GET["nonewresources"] ? $_GET["nonewresources"] : null;
$nonewclient = $_GET["nonewclient"] ? $_GET["nonewclient"] : null;

$minAmount = 999999999;
$maxAmount = 0;

$args = array(
	'post_type'        => 'accounts_saving',
	'post_status'      => 'publish',
	'posts_per_page'   => -1,
	'suppress_filters' => true,
	'meta_query'	=> array(
		'relation'		=> 'OR',
		array(
			'key'	  	=> 'product_inactive',
			'compare' => 'NOT EXISTS'
		),
		array(
			'key'	  	=> 'product_inactive',
			'value'	  	=> '1',
			'compare' 	=> '!=',
		),
	),
);
$posts_array = get_posts( $args );


foreach ( $posts_array as $post ) : setup_postdata( $post );

	if(get_field('product_max_amount') > $maxAmount) $maxAmount = get_field('product_max_amount');
	if(get_field('product_min_amount') < $minAmount) $minAmount = get_field('product_min_amount');

endforeach;