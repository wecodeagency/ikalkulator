<?php
    $current_page = get_post($post);
?>


<?php if( have_rows("bank_accounts_tiles") ): ?>    
<div class="tiles-list">
    <?php while ( have_rows("bank_accounts_tiles") ) : the_row(); ?>
        <?php                
            $is_page = false;
            $page_url = get_sub_field("bank_accounts_tile_page");
            $page_id = url_to_postid($page_url);

            if ($page_id != 0)
            {
                $page = get_posts(url_to_postid($page_url)); 
                $is_page = true;
            }

            $image = get_sub_field("bank_accounts_tile_img");
            $title = get_sub_field("bank_accounts_tile_title");
            $desc = get_sub_field("bank_accounts_tile_description");

            $img_size = "thumbnail";

            if (!$image && $is_page)
            {
                $image = get_featured_image_url($page, $img_size);
            }

            if (!$title && $is_page)
            {
                $title = get_the_title($page_id);
            }
        ?>    
        <div class="tile-item">
            <a href="<?php echo $page_url ?>" class="tile-panel">  
                <div class="tile-content">
                    <div class="image-container">
                        <div class="image"<?php echo $image ? ' style="background-image:url('. $image["url"] .')"' : ""; ?>></div>
                    </div>
                    <h2 class="title"><?php echo $title; ?></h2>
                </div>                    
                <div class="desciption"><?php echo $desc; ?></div>
            </a>
        </div>
    <?php endwhile; ?>  
</div>
<?php endif; ?>