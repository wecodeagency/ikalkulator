<?php
    global $no_scroll_to_content;
    $head_title = "";
    $head_description = "";
    $page_type = "";
    $root_category = null;

    $root_title = "";

    $category_with_megabox = false;
    
    $paged = (get_query_var("paged")) ? get_query_var("paged") : 1;
    $paged_info = $paged > 1 ? " (strona {$paged})" : "";


    if (is_category())
    {
        $current_category = get_current_taxonomy();
        
        if (!treat_cat_as_tag($current_category))
        {
            $alt_title = get_taxonomy_field("blog_category_title");
            $head_title =  $alt_title ? $alt_title : single_cat_title("", false);
            $head_description = category_description( $current_category->term_id );
            $category_with_megabox = get_query_var("with_metabox");

            $root_category = get_root_of_category($current_category);

            if ($current_category->term_id == $root_category->term_id)
            {
                $root_category = null;
            }

            $page_type = " category-head";
        }
        else
        {
            $head_title =  str_replace("#", "", single_cat_title("", false));  
            $head_description = category_description( $current_category->term_id);
            $page_type = " tag-head";            
        }
    }
  /*  elseif (is_tag())
    {
        $current_tag = get_current_taxonomy();
        $head_title =  single_tag_title("", false);
        $head_description = tag_description( $current_tag->term_id );
        $page_type = " tag-head";
    }*/
    else
    {
        $head_title = get_the_title();
        $head_description = get_the_excerpt();

        if (is_single())
        {
            $page_type = " post-head";
            $root_category = get_post_root_category(get_post());
        }
    }
    if ($root_category)
    {
        $alt_title = get_taxonomy_field("blog_category_title", $root_category);
        $root_title =  $alt_title ? $alt_title : get_cat_name($root_category->term_id);
    }
?>
<div class="page-head<?php echo $page_type;?>">
	<div class="content">
        <?php
        if (!$root_title)
        {
            the_breadcrumbs();
        } ?>
        
        <?php
        if ($root_title)
        { ?>
            <div class="root-category">
                <div class="info-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12" y2="8"></line></svg>
                </div>
                <div class="name">
                    <?php echo $root_title; ?>
                </div>
            </div>
        <?php
        } ?>
                           
        <?php
        if ($root_title)
        {
            the_breadcrumbs();
        } ?>        
        
		<?php
		if ($head_title != "")
		{ ?>
			<h1 class="title" itemprop="headline"><?php echo $head_title . $paged_info;?></h1>
		<?php
		} ?>


		<?php
		if ($head_description != "")
		{ ?>
            <div class="description" itemprop="description">
                <?php echo $head_description; ?>
            </div>
		<?php
		} ?>
        
        <?php if (is_single() && get_post_type() == "post")
        {
            template_part("post/info/multiline");           
        }?>
        
        
        <?php // the_breadcrumbs(); ?>  
            
        <?php
        if (!$category_with_megabox && !$no_scroll_to_content)
        {
            get_template_part( "template-parts/navigation/scroll-to-content" );
        } ?>

	</div>
</div>