<?php
    global $post;
    $post_author = get_post_author_info($post);
    $post_meta = get_post_custom_meta($post, "thumbnail", "d.m.Y");
    $wordCount = str_word_count(strip_tags(get_post_field("post_content", $post->ID)));
    
    ob_start();
    template_part("icon/calendar");
    $calendar_icon = ob_get_clean();    
?>
<div class="post-info multiline">
    <?php
    ?>
    <div class="avatar">
        <img src="<?php echo $post_author["avatar"]; ?>" alt="<?php echo $post_author["name"] ?>">
    </div>  
    
    <div class="info">
        <div itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
            <meta itemprop="name" content="iKalkulator.pl"/>
            <div itemprop="logo" itemscope itemtype="http://schema.org/ImageObject">
                <meta itemprop="url" content="https://www.ikalkulator.pl/wp-content/themes/ikalkulator/assets/img/logo.png" />
            </div>
        </div>
        <h4 class="author-name" itemprop="author" itemscope itemtype="http://schema.org/Person"><span itemprop="name"><?php echo $post_author["name"] ?></span></h4>
        <div class="details">
            <div class="date">
                <?php echo $calendar_icon; ?>
                <div><?php echo $post_meta["date"]; ?></div>
                    <time datetime="<?php echo date("Y-m-d", strtotime($post_meta["date"])); ?>" itemprop="datePublished" />
                    <time datetime="<?php echo date("Y-m-d", strtotime($post_meta["date"])); ?>" itemprop="dateModified" />
            </div>
            
            <?php
            $promo_time_left = get_post_scarcity($post);
            if ($promo_time_left && $promo_time_left != "no-date") $promo_time_left = '<h4 class="scarcity">' . $calendar_icon . '<span>' . $promo_time_left . '</span></h4>';

            if ($promo_time_left && $promo_time_left != "no-date" && !is_category())
            {
                echo $promo_time_left;
            }
            else if (isset($wordCount) && $wordCount != 0 && !is_category())
            {
                ?>
                <div class="word-count">
                    <?php template_part("icon/watch"); ?>
                    <div>Czas czytania ok. <?php echo round($wordCount / 200); ?> min. (<?php echo $wordCount; ?> słów).</div>
                </div>
                <?php
            }
            ?>

        </div>
    </div>
</div>