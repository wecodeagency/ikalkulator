<?php
    global $post;
    $post_author = get_post_author_info($post);
    $post_meta = get_post_custom_meta($post, "thumbnail", "d.m.Y");
    $wordCount = str_word_count(strip_tags(get_post_field("post_content", $post->ID)));
   
    $promo_time_left = get_post_scarcity($post);  
    
    ob_start();
    template_part("icon/calendar");
    $calendar_icon = ob_get_clean();    
    
    $more_info = "";
    
    if ($promo_time_left && $promo_time_left != "no-date")
    {
        $promo_time_left = '<div class="scarcity">' . $calendar_icon . '<span>' . $promo_time_left . '</div>'; 
        
        $more_info = $promo_time_left;
    }
    elseif (isset($wordCount) && $wordCount != 0)
    { 
        ob_start();
        ?>
            <div class="word-count">
                <?php template_part("icon/watch"); ?>
                <div>Czas czytania ok. <?php echo round($wordCount / 200); ?> min. (<?php echo $wordCount; ?> słów).</div>
            </div>
        <?php
        $more_info = ob_get_clean();  
    }    
?>
<div class="post-info singleline">
    <div class="info">   
        <div class="author">
            <?php /*
            <div class="avatar">
                <img src="<?php echo $post_author["avatar"]; ?>" title="<?php echo $post_author["name"] ?>">
            </div>  
            */ ?>
            <div class="author-name"><?php echo $post_author["name"] ?></div>        
        </div>
        <div class="date">
            <?php echo $calendar_icon; ?>
            <div><?php echo $post_meta["date"]; ?></div>
        </div>            

        <?php
        echo $more_info;?>
    </div>

</div>