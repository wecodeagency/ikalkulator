<div class="share">
    <div class="share-text">Podziel się: </div>
    <div class="links">        
        <a class="social-share twitter" target="_blank" href="https://twitter.com/home?status=<?php echo urlencode(get_permalink()); ?>"><span>Twiter</span></a>
        <a class="social-share facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>"><span>Facebook</span></a>
        <a class="social-share googleplus" target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode(get_permalink()); ?>"><span>Google+</span></a>
    </div>    
</div>
