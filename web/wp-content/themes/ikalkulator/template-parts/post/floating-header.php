<?php
    $root_category = null;
    $root_title = "";

    if (is_single())
    {
        $root_category = get_post_root_category(get_post());

        if ($root_category)
        {
            $alt_title = get_taxonomy_field("blog_category_title", $root_category);
            $root_title =  $alt_title ? $alt_title : get_cat_name($root_category->term_id);
        }
    }

?>

<div class="floating-header">
    <progress class="reading-progress" value="0"></progress>
    <div class="floating-header-main">
        <div class="floating-header-content">
            <?php
            if ($root_title != "")
            { ?>
                <div class="root-category">
                    <div class="info-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12" y2="8"></line></svg>
                    </div>
                    <div class="name">
                        <?php echo $root_title; ?>
                    </div>
                </div>
            <?php
            } ?>
        </div>
    </div>
</div>