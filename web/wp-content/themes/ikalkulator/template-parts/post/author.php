<?php
$post_author = get_post_author_info($post);
$post_meta = get_post_custom_meta($post, "thumbnail", "d.m.Y");

$avatar_exists = $post_author["avatar"];

//if ($post_author["name"] != "admin")
{
?>
    <section class="author-block<?php echo $avatar_exists ? "" : " no-avatar"; ?>">
        <div class="block-content">
            <?php if ($avatar_exists)
            { ?>
            <div class="avatar">
                <img src="<?php echo $post_author["avatar"]; ?>" alt="<?php echo $post_author["name"] ?>">
            </div>
            <?php
            } ?>
            <div class="info">
                <h4 class="name"><span><?php echo $post_author["name"] ?></span></h4>
                <div class="date">
                    <?php template_part("icon/calendar"); ?>
                    <div><?php echo $post_meta["date"]; ?></div>
                </div>
            </div>
        </div>
    </section>
<?php
} ?>