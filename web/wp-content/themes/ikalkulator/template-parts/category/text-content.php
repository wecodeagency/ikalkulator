<?php
    
$category_content = get_taxonomy_field("blog_category_content");

if ($category_content)
{?>
    <div class="content category-text-content">
        <?php echo $category_content; ?>
    </div>
<?php
}?>