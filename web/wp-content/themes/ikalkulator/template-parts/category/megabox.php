<?php

$excluded_posts = get_query_var("excluded_posts");
$excluded_categories = get_query_var("excluded_categories");
$current_category = get_category( get_query_var( "cat" ) );
$categories = categories_comma_separated(get_subcategories($current_category, true), $excluded_categories);
$grid_layout = get_query_var("grid_layout") ? get_query_var("grid_layout") : get_taxonomy_field("blog_category_megabox_layout");
$gird_posts_number = $grid_layout == "layout_4" ? 4 : 3;

/* get featured posts */
if(get_query_var("featured_posts"))
    $featured_posts = get_query_var("featured_posts");
else
{
    $featured_args = array(
        "post_type"			=> "post",
        "category"			=> $categories,
        "exclude"           => posts_comma_separated($excluded_posts),
        "category__not_in"  => $excluded_categories,
        "posts_per_page"	=> -1,
        "orderby"			=> "date",
        "order"				=> "DESC",
        "meta_key"          => "blog_post_featured",
        "meta_value"        => true
    );
    $featured_posts = get_posts($featured_args);
}


/* if not featured post, get newest */
if (!$featured_posts)
{
    $featured_args = array(
        "post_type"			=> "post",
        "category"			=> $categories,
        "exclude"           => posts_comma_separated($excluded_posts),
        "category__not_in"  => $excluded_categories,
        "posts_per_page"	=> 1,
        "orderby"			=> "date",
        "order"				=> "DESC",
    );
    $featured_posts = get_posts($featured_args);
}

$excluded_posts = array_unique_merge($excluded_posts, $featured_posts);

/* get grid posts */
$grid_args = array(
    "post_type"			=> "post",
    "category"			=> $categories,
    "exclude"           => posts_comma_separated($excluded_posts),
    "category__not_in"  => $excluded_categories,
    "posts_per_page"	=> $gird_posts_number,
    "orderby"			=> "date",
    "order"				=> "DESC",
);

$grid_posts = get_posts($grid_args);
$excluded_posts = array_unique_merge($excluded_posts, $grid_posts);
set_query_var("excluded_posts", $excluded_posts);

function megabox_create_featured_post($post)
{
    $post_meta = get_post_custom_meta($post);
    
//    $cats = create_categories_labels_list($post_meta["categories"]);
    $cats = get_post_categories($post);
//    $tags = wp_get_post_tags($post->ID);
    $tags = get_post_tags($post);
    $terms =  create_terms_labels_list($cats, $tags, "dark");  

    $result =  '<div class="item-content">'
                    . $terms .
                    '<h2 class="title"><a href="'. $post_meta["url"] .'">'. $post_meta["title"] .'</a></h2>
                    <div class="excerpt">'. $post_meta["excerpt"] .'</div>
                    <a class="read-more" href="'. $post_meta["url"] .'">Czytaj więcej</a>
                </div>';

    return $result;
}

function megabox_create_grid_post($post, $thumbnail = "thumbnail")
{
    $post_meta = get_post_custom_meta($post, $thumbnail);

    $result =  '<div class="item-content">
                    <img src="' . $post_meta["featured_image"] . '" alt="' . $post_meta["title"] . '"/>
                    <a class="link" href="'. $post_meta["url"] .'">
                        <span>
                            <h3 class="title">'. $post_meta["title"] .'</h3>
                        </span>
                    </a>
                </div>';

    return $result;
}

?>
<div class="megabox">
	<div class="content">
		<?php
        $featured_count = count($featured_posts);
        ?>
        <div class="posts-container">
            <?php
            if ($featured_count > 1)
            {
            ?>
            <div class="nav-panel">
                <div class="dots">
                <?php
                    for ($i = 1; $i <= $featured_count ; $i++) { ?>
                        <div class="dot<?php echo $i == 1 ? " active" : ""; ?>"><span></span></div>
                <?php
                } ?>
                </div>
               <div class="nav">
                   <div class="prev">
                       <span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left">
                                <line x1="20" y1="12" x2="4" y2="12"/>
                                <polyline points="10 18 4 12 10 6"/>
                            </svg>
                       </span>
                   </div>
                   <div class="next">
                       <span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right">
                                <line x1="4" y1="12" x2="20" y2="12"/>
                                <polyline points="14 6 20 12 14 18"/>
                            </svg>
                       </span>
                   </div>
               </div>
            </div>
            <?php
            } ?>

            <div class="posts">
                <div class="featured-posts-container">
                    <?php
                    $index = 0;
                    foreach ($featured_posts as $post)
                    { ?>
                        <div class="featured-post item item-<?php echo $index + 1; ?><?php echo $index == 0 ? " active" : ""; ?>">
                                <?php echo megabox_create_featured_post($post); ?>
                        </div>
                        <?php
                        $index++;
                    } ?>
                </div>
                <div class="grid-posts-container <?php echo $grid_layout; ?>">
                    <?php
                    $index = 0;

                    foreach ($grid_posts as $post)
                    { ?>
                        <div class="grid-post item item-<?php echo $index + 1; ?>">
                            <?php
                            $thumbnail = "megabox-thumbnail";

                            if  (
                                    (($grid_layout == "layout_1t_2") && ($index == 0))
                                    ||
                                    (($grid_layout == "layout_2_1t") && ($index == 2))
                                )
                            {
                                $thumbnail = "megabox-thumbnail-tall";
                            }
                            elseif  (
                                    (($grid_layout == "layout_1w_2") && ($index == 0))
                                    ||
                                    (($grid_layout == "layout_2_1w") && ($index == 2))
                                )
                            {
                                $thumbnail = "megabox-thumbnail-wide";
                            }

                            echo megabox_create_grid_post($post, $thumbnail);
                            ?>
                        </div>
                        <?php
                        $index++;
                    } ?>
                </div>
            </div>

            <?php
            if (is_front_page())
            {
                $posts_root_cat = get_post_root_category($grid_posts[0]);
                $posts_root_cat_url = get_term_link($posts_root_cat);
                ?>
            <div class="see-all">
                <div class="see-all-btn"><span></span></div>
                <div class="see-all-link">
                    <a href="<?php echo $posts_root_cat_url; ?>">Zobacz wszystkie</a>
                </div>
            </div>
            <?php
            } ?>
        </div>
	</div>
</div>
<?php
wp_reset_postdata();
?>