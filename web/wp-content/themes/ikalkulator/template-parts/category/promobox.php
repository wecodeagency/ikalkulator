<div class="promobox">

    <div class="info">
        <h2>Sprawdź promocje bankowe</h2>

        <p>
            Znajdziesz tutaj aktualne promocje w bankach, dotyczące pożyczek, lokat i zwrotów.
        </p>
    </div>


<?php

//$category = [11,7,8,6,10];
//blog_post_promo
$promotion_category = get_cat_ID("Promocje bankowe");

$args = array(
    "post_type"     => "post",
    "cat"           => $promotion_category,
    "posts_per_page"=> -1,
);

$posts = get_posts($args);

function create_post($post)
{
    $post_meta = get_post_custom_meta($post, "megabox-thumbnail");
    $cats = wp_get_post_categories($post->ID);
    $promotion_type = get_field("promotion_type", $post->ID);

    $promo_time_left = get_post_scarcity($post);
    
    $type = $promotion_type["value"] ? $promotion_type["value"] : "others";

    $result =  '<div class="post-item" data-type="'. $type .'">
                    <div class="item-content">
                    <a href="'. $post_meta["url"] .'">
                        <div class="image">
                            <img src="' . $post_meta["featured_image"] . '" alt="' . $post_meta["title"] . '"/>
                        </div>
                        <h3 class="title">'. $post_meta["title"] .'</h3>';
    if($promo_time_left && $promo_time_left != "no-date") $result .= '<div class="time">'.$promo_time_left.'</div>';

    $result .= '</a>
                    </div>
                </div>';

    return $result;
}
?>

<div class="posts post-list draggable-list">
    
    <?php
    if ($posts)
    { ?>
    <div class="filters">
        <div class="value">Filtruj</div>
        <ul>
            <?php
            
            $filters = array();
            
            foreach($posts as $post)
            {
                $field = get_field("promotion_type", $post->ID);
                
                if ($field)
                {                
                    if (!in_array($field, $filters))
                    {
                        $filters[] = $field;
                    }  
                }
            }
       
//          $field = get_field_object("promotion_type", $posts[0]->ID);
//          $choices = $field["choices"];
            
            foreach($filters as $filter):
                echo '<li data-type="'. $filter["value"] .'">'. $filter["label"] .'</li>';
            endforeach; 
            ?>
        </ul>
    </div>
    <?php
    } ?>
    <div class="drag">Przeciągnij aby zobaczyć więcej</div>
    <?php
    if ($posts)
    {
        $now = new DateTime(current_time("Y-m-d H:i:s"));        
    ?>
        <div class="posts-list-content">
            <?php
            foreach ($posts as $post)
            {
                $promotion_end_type = get_field("promotion_end_type", $post->ID);
                if($promotion_end_type == "cancellation")

                {
                    echo create_post($post);
                }
                else
                {
                    $promotion_end_time = get_field("promotion_end_time", $post->ID);
                    $end_time = new DateTime($promotion_end_time);

                    if ($now <= $end_time || !isset($promotion_end_time) || $promotion_end_time == "")
                    {
                        echo create_post($post);
                    }
                }

            }
            ?><div class="post-item all-promotions">
                <div class="item-content">
                    <a href="<?php echo get_category_link($promotion_category); ?>"><span>Zobacz wszystkie</span></a>    
                </div>
            </div>
        </div>
    <?php
    } ?>
</div>
    <div class="links">
        <ul>
            <li class="credits"><a href="<?php echo get_category_link(get_cat_ID("Pożyczanie")); ?>">Pożyczanie</a></li>
            <li class="loans"><a href="<?php echo get_category_link(get_cat_ID("Zarabianie")); ?>">Zarabianie</a></li>
            <li class="investments"><a href="<?php echo get_category_link(get_cat_ID("Inwestowanie")); ?>">Inwestowanie</a></li>
            <li class="insurances"><a href="<?php echo get_category_link(get_cat_ID("Ubezpieczenia")); ?>">Ubezpieczenia</a></li>
            <li class="savings"><a href="<?php echo get_category_link(get_cat_ID("Oszczędzanie")); ?>">Oszczędzanie</a></li>
        </ul>
    </div>
</div>
