<?php
$two_columns = get_query_var("two_column_posts_list");
$excluded_posts = get_query_var("excluded_posts");
$excluded_posts_ids = array();
$paged = (get_query_var("paged")) ? get_query_var("paged") : 1;

$excluded_posts = $excluded_posts ? $excluded_posts : array();
foreach ($excluded_posts as $post)
{
    $excluded_posts_ids[] = $post->ID;
}

if (is_category())
{
    $current_category = get_category( get_query_var( "cat" ) );
    
    if (!treat_cat_as_tag($current_category))
    {
        $categories = categories_comma_separated(get_subcategories($current_category, true));

        $args = array(
            "post_type"			=> "post",
            "paged"             => $paged,
            "cat"               => $categories,
            "post__not_in"      => $excluded_posts_ids,
        );

        // hack dla promocji
        if($categories == "31")
        {
            $postIds = [];

            $args = array(
                "post_type"			=> "post",
                "cat"               => 31,
                'orderby'           => 'publish_date',
                'nopaging' 			=> true,
                'meta_query'        => array(
                    'relation' => 'OR',
                    array(
                        'key' => 'promotion_end_type',
                        'value' => 'cancellation'
                    ),
                    array(
                        'relation' => 'AND',
                        array(
                            'key' => 'promotion_end_type',
                            'value' => 'date'
                        ),
                        array(
                            'key' => 'promotion_end_time',
                            'value' => date("Y-m-d H:i:s"),
                            'compare' => '>'
                        )
                    ),
                )
            );

            $posts1 = get_posts($args);

            $customPosts = new WP_Query($args);

            foreach ($posts1 as $post)
            {
                $postIds[] = $post->ID;
            }

            $args = array(
                "post_type" => "post",
                "cat" => 31,
                "nopaging" => true,
                "post__not_in" => array_merge($excluded_posts_ids, $postIds),
            );

            $posts2 = get_posts($args);

            foreach ($posts2 as $post)
            {
                $postIds[] = $post->ID;
            }

            $args = array(
                "paged"             => $paged,
                "post__in" => array_merge($excluded_posts_ids, $postIds),
                'orderby' => 'post__in'
            );
        }
    }
    else
    {
        $args = array(
            "post_type"			=> "post",
            "paged"             => $paged,
            "cat"               => $current_category->term_id,
            "post__not_in"      => $excluded_posts_ids,
        );        
    }
}
/*
elseif (is_tag()) 
{
    $current_tag = get_query_var( "tag" );

    $args = array(
        "post_type"			=> "post",
        "paged"             => $paged,
        "tag"               => $current_tag,
        "post__not_in"      => $excluded_posts_ids,
    );
}
*/
else
{
    $post_in_cat = get_field("post_in_category");    
    $post_in_tag = get_field("post_in_tag");    

    $post_in_cat = $post_in_cat ? implode(",",$post_in_cat) : "";
    $post_in_tag = $post_in_tag ? implode(",",$post_in_tag) : "";
        
    $two_columns = true; 
    $args = array(
        "post_type"			=> "post",
        "paged"             => $paged,
        "post__not_in"      => $excluded_posts_ids,
        "tax_query" => array(
          "relation" => "OR",
          array(
            "taxonomy" => "post_tag",
            "field" => "term_id",
            "terms" => $post_in_tag ,
          ),
          array(
            "taxonomy" => "category",
            "field" => "term_id",
            "terms" => $post_in_cat,
          ),
        ),        
    );
}

$posts = new WP_Query($args);


?>

<div class="posts post-list<?php echo $two_columns == true ? " col-2" : ""; ?>">
    <?php
    if ( $posts->have_posts() )
    { ?>
        <div class="posts-list-content">
            <?php
            while ( $posts->have_posts() ) : $posts->the_post();
                if ($two_columns)
                    echo create_post_list_item($post, "post-list-medium");
                else
                    echo create_post_list_item($post);

            endwhile; ?>
        </div>
        <?php
        build_posts_pagination($posts);
        wp_reset_postdata();
    } ?>
</div>
