<?php
	$image_id = isset($instance["image"]) ? $instance["image"] : "";
	

if($image_id != "")
{
	$description_type = isset($instance["description_type"]) ? $instance["description_type"] : "no-description";

	$img_atts = wp_get_attachment_image_src( $image_id, $default );
	$image_src = $img_atts[0];
    $image_description = "";
    switch ($description_type)
    {
        case "image-description":
            $image_description = get_post($image_id)->post_content;
            break;
        case "custom-description":
            $image_description = isset($instance["image_description"]) ? $instance["image_description"] : "";
            break;
    }
?>

<div class="wecode-widget image-widget">
	<div class="image">
		<img src="<?php echo $image_src; ?>" alt="" />
	</div>

	<?php if ($image_description != "") { ?>
		<p class="description"><?php echo $image_description; ?></p>
	<?php } ?>
</div>
<?php
}