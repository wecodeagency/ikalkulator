<?php
function homeurl()
{
    return sprintf(
        "%s://%s",
        isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off" ? "https" : "http",
        $_SERVER["HTTP_HOST"]
    );
}
$page_slug = md5("banki");
$page = file_get_contents(homeurl() . "/{$page_slug}/?force=1");
echo str_replace($page_slug, "banki", $page);
exit();