var bower		= require("gulp-bower");
var concat		= require("gulp-concat");
var fs			= require("fs");
var glob		= require("glob")
var gulp		= require("gulp");
var gutil		= require("gulp-util");
var less		= require("gulp-less");
var merge		= require("merge-stream");
var minifyCSS	= require("gulp-minify-css");	
var notify		= require("gulp-notify");
var path		= require("path");
var rename		= require("gulp-rename");
var sourcemaps	= require("gulp-sourcemaps");
var uglifyjs	= require("gulp-uglifyjs");

// common usage:
// 
// gulp bower|dev|prod|watch|watch_dep


// search for directories

function getDirectories(dir)
{
	if (!fs.existsSync(dir))
		return [];
	
	return fs.readdirSync(dir)
		.filter
		(
			function(file)
			{
				return fs.statSync(path.join(dir, file)).isDirectory();
			}
		);
}


// css tasks

gulp.task
(
	"less_compile", 
	gulp.series(function()
	{
		return gulp.src
		([
			"web/wp-content/themes/ikalkulator/src/less/*.less"
		])
			.pipe(sourcemaps.init())
			.pipe(less())
			.pipe(sourcemaps.write())
  			.pipe(gulp.dest("web/wp-content/themes/ikalkulator/assets/css/"))
			;
	})
);


gulp.task
(
	"css_concat_vendor",
	gulp.series(function()
	{
		return gulp.src
		(
			[
				"web/wp-content/themes/ikalkulator/src/vendor/jquery-ui/jquery-ui.min.css",
//				"web/wp-content/themes/ikalkulator/src/vendor/owl-carousel/owl.carousel.min.css",
 			]
		)
			.pipe(concat("vendor.css", { sourcesContent: true }))
			.pipe(gulp.dest("web/wp-content/themes/ikalkulator/assets/css/"))			
			;
	})
);

gulp.task
(
	"css_compress",
	gulp.series([
		"less_compile",
		"css_concat_vendor"
	],
	function()
	{
		return gulp.src
		([
			"web/wp-content/themes/ikalkulator/assets/css/*.css"
		])
			.pipe(minifyCSS())
			.pipe(gulp.dest("web/wp-content/themes/ikalkulator/assets/css/"))
			;
	})
);


// js tasks

gulp.task
(
	"js_concat",
	gulp.series(function()
	{
		var tasks = [];
		
		var dirs = getDirectories("web/wp-content/themes/ikalkulator/src/js/");
		dirs.map
		(
			function(dir)
			{		
				tasks.push
				(
					gulp.src(path.join("web/wp-content/themes/ikalkulator/src/js/", dir, "/**/*.js"))
						.pipe(sourcemaps.init())
						.pipe(concat(dir + ".js"))
						.pipe(sourcemaps.write())
						.pipe(gulp.dest("web/wp-content/themes/ikalkulator/assets/js/"))
				);
			}
		)
		
		return merge(tasks);
	})
);


gulp.task
(
	"js_concat_vendor",
	function()
	{
		return gulp.src
		(
			[
				"web/wp-content/themes/ikalkulator/src/vendor/jquery-ui/jquery-ui.min.js",
				"web/wp-content/themes/ikalkulator/src/vendor/jquery-ui/ui-touch.js",
				"web/wp-content/themes/ikalkulator/src/vendor/jquery.pep/src/jquery.pep.js",
			]
		)
			.pipe(concat("vendor.js", { sourcesContent: true }))
			.pipe(gulp.dest("web/wp-content/themes/ikalkulator/assets/js/"))
			;
	}
);

gulp.task
(
	"js_compress",
	gulp.series([
		"js_concat",
		"js_concat_vendor"
	],
	function()
	{
		var tasks = [];
		
		var files = glob.sync(path.join("web/wp-content/themes/ikalkulator/assets/js/*.js"));
		files.map
		(
			function(file)
			{
				tasks.push
				(
					gulp.src(file)
						.pipe(uglifyjs())
						.pipe(gulp.dest(path.join("web/wp-content/themes/ikalkulator/assets/js/")))
				);
			}
		);
	
		return merge(tasks);
	})
);


// generation tasks

gulp.task
(
	"dev",
	gulp.series([
		"less_compile",
		"css_concat_vendor",
		"js_concat",
		"js_concat_vendor"
	])
);

gulp.task
(
	"prod",
	gulp.series([
		"css_compress",
		"js_compress"
	])
);


// watch tasks

gulp.task
(
    "watch",
	gulp.series("dev",
	
	    function()
	    {
	        gulp.watch("web/wp-content/themes/ikalkulator/src/less/**/*.less",	gulp.series("less_compile"));
	        gulp.watch("web/wp-content/themes/ikalkulator/src/js/**/*.js",		gulp.series("js_concat"));
	    }
	)
);



// dependecies import task

gulp.task
(
	"bower",
	gulp.series(function()
	{
		return bower("web/wp-content/themes/ikalkulator/src/vendor/");
	})
);



